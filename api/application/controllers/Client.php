<?php

class Client extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS");  
        // header('Access-Control-Allow-Headers: origin, x-requested-with, accept');
        // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
      
        // header('Access-Control-Allow-Credentials: true');

        date_default_timezone_set('Asia/Kolkata');
        $this->load->model('client_model');
         $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->library('zip');

    }
    
    
    public function signup()
    {
        $this->load->helper('phpass');
        $req = json_decode(file_get_contents('php://input'), true);

        $res='';
        $token=$this->token();
        $ip = $_SERVER['REMOTE_ADDR'];

        $date=date('Y-m-d H:i');
        $app_type=$req['app_type'];


        if($req['device_id']=='')
        {
            $res=['status'=>2, 'message'=>'Please send us device_id','data'=>''];
            echo json_encode($res);
            die;
        }


        if($req['app_type']=='')
        {
            $res=['status'=>2, 'message'=>'Please send us app_type','data'=>''];
            echo json_encode($res);
            die;
        }


        if($req['signup_type']=='')
        {
            $res=['status'=>2, 'message'=>'Please send us signup_type','data'=>''];
            echo json_encode($res);
            die;
        }

        if($req['signup_type']=='manual')
        {

            if($req['register_type']=='')
            {
                $res=['status'=>2, 'message'=>'Please send us register_type','data'=>''];
                echo json_encode($res);
                die;
            }

//            $customer='';
            $leads='';
            $password = '';
            if ($req['password'] != '') {
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                $password = $hasher->HashPassword($req['password']);



            if($req['register_type']=='mobile')
            {

                if($req['mobile']=='')
                {
                    $res=['status'=>2, 'message'=>'Please enter the mobile number','data'=>''];
                    echo json_encode($res);
                    die;
                }
                else
                {

                    $mobile_number_check=$this->client_model->check_mobile($req['mobile']);
                    if($mobile_number_check==1)
                    {

                        $leads = array(
                            'email' => $req['email'],
                            'mobile' => $req['mobile'],
                            'datecreated' => $date,
                            'password' => $password,
                            'app_type'=>$app_type,
                        );

                    }
                    else
                    {
//                        $res=['status'=>2, 'message'=>'already exit the mobile number','data'=>''];
//                        echo json_encode($res);
//                        die;



                        $password=$mobile_number_check[0]['password'];
                        if($password !=""){
                        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                        if (!$hasher->CheckPassword($req['password'],$password)) {

                            $res=['status'=>0, 'message'=>'Invalid username password','data'=>''];
                            echo json_encode($res);
                            exit;
                        }
                        else {

                            $name=$mobile_number_check[0]['firstname'].' '.$mobile_number_check[0]['lastname'];
                            $log = array(

                                'description' => "A customer login in app. The customer username: .$name",
                                'date' => $date,
                                'staffid' => $mobile_number_check[0]['userid']
                            );
                            $this->client_model->add_log($log);


                            $userid = $mobile_number_check[0]['userid'];

                            $session = array(
                                'device_id' => $req['device_id'],
                                'ip_address' => $ip,
                                'token' => $token,
                                'created_date' =>$date,
                                'username' => $userid,
                                'app_type'=>$app_type,
                                'role_type'=>'customer'
                            );


                            $login = $this->client_model->add_session($session);

                            //  print_r($login);


                            if ($login == false) {
                                $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                                echo json_encode($res);
                                exit;
                            } else {

                                $last_login_details = array(

                                    'last_ip' => $ip,
                                    'last_login' => $date
                                );

                                $this->client_model->last_login_update($userid, $last_login_details);
                                $res = ['status' => 1, 'message' => 'login success fully', 'data' => ['username' => $mobile_number_check[0]['firstname'] . ' ' . $mobile_number_check[0]['lastname'], 'token' => $token,'type'=>'customer']];
                                echo json_encode($res);
                                exit;

                            }
                        }
                        }
                    else{
                        $this->load->helper('phpass');
                        $hasher              = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                        $password    = $hasher->HashPassword($req['password']);

                        $response = $this->client_model->update_password($password,$req['mobile']);
                       
if($response > 0){
$res = ['status' => 1, 'message' => 'signup success fully', 'data' => ['username' => $mobile_number_check[0]['firstname'] . ' ' . $mobile_number_check[0]['lastname'], 'token' => $token,'type'=>'customer']];

    echo json_encode($res);
    exit;
}
                    }






                        }
                }
            }
            else if($req['register_type']=='email')
            {

                if($req['email']=='')
                {
                    $res=['status'=>2, 'message'=>'Please enter the email','data'=>''];
                    echo json_encode($res);
                    die;
                }
                else{

                    $email_check=$this->client_model->check_email($req['email']);

                    if($email_check==1)
                    {

//
//                        $customer=array('email_id'=>$req['email'],
//
//                            'datecreated' => date('Y-m-d H:i'),
//                            'created_by' => 'app');


                        $leads = array(
                            'email' => $req['email'],
                            'mobile' => $req['mobile'],
                            'datecreated' => $date,
                            'password' => $password,
                            'app_type'=>$app_type,
                        );



                    }
                    else
                    {
//                        $res=['status'=>2, 'message'=>'already exit the email','data'=>''];
////                        echo json_encode($res);
////                        die;





                        $password=$email_check[0]['password'];
                        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                        if (!$hasher->CheckPassword($req['password'],$password)) {

                            $res=['status'=>0, 'message'=>'Invalid username password','data'=>''];
                            echo json_encode($res);
                            exit;
                        }
                        else {

                            $name=$email_check[0]['firstname'].' '.$email_check[0]['lastname'];
                            $log = array(

                                'description' => "A customer login in app. The customer username: .$name",
                                'date' => $date,
                                'staffid' => $email_check[0]['userid']
                            );
                            $this->client_model->add_log($log);


                            $userid = $email_check[0]['userid'];

                            $session = array(
                                'device_id' => $req['device_id'],
                                'ip_address' => $ip,
                                'token' => $token,
                                'created_date' =>$date,
                                'username' => $userid,
                                'app_type'=>$app_type,
                                'role_type'=>'customer'
                            );


                            $login = $this->client_model->add_session($session);


                            if ($login == false) {
                                $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                                echo json_encode($res);
                                exit;
                            } else {

                                $last_login_details = array(

                                    'last_ip' => $ip,
                                    'last_login' => $date
                                );

                                $this->client_model->last_login_update($userid, $last_login_details);
                                $res = ['status' => 1, 'message' => 'login sucessfully', 'data' => ['username' => $email_check[0]['firstname'] . ' ' . $email_check[0]['lastname'], 'token' => $token,'type'=>'customer']];
                                echo json_encode($res);
                                exit;
                            }
                        }





                    }
                }
            }

                $response = $this->client_model->insert_app_leads($leads,$req['register_type']);

                if ($response == '0')
                {
                    $res = ['status' => 0, 'message' => 'database_error create leads', 'data' => ''];
                }
                else if($response == '1')
                {

                    $username='';
                    if($req['register_type']=='email')
                    {
                        $username=$req['email'];

                    }
                    else
                    {
                        $username=$req['mobile'];
                    }

                    $log = array(

                            'description' => "New lead register in app. The username:$username",
                            'date' => date('Y-m-d H:i'),
                            'staffid' => $username,
                        );

                        $this->client_model->add_log($log);


                        $session = array(
                            'device_id' => $req['device_id'],
                            'ip_address' => $ip,
                            'token' => $token,
                            'created_date' => date('Y-m-d H:i'),
                            'username' => $username,
                            'role_type'=>'app_leads'
                        );

                        $login = $this->client_model->add_session($session);

                        if ($login == false) {
                            $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                        } else {

                            $res = ['status' => 1, 'message' => 'signup success fully', 'data' => ['type'=>'app_leads', 'token' => $token]];
                        }

//                    }
                }
                else {

                        $password=$response[0]['password'];
                        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                        if (!$hasher->CheckPassword($req['password'],$password)) {

                            $res=['status'=>0, 'message'=>'Invalid username password','data'=>''];
                            echo json_encode($res);
                            exit;
                        }
                        else {

                            $username='';
                            if($req['register_type']=='email')
                            {
                                $username=$req['email'];

                            }
                            else
                            {
                                $username=$req['mobile'];
                            }



                            $log = array(

                                'description' => "A user app lead login in app. The username: .$username",
                                'date' => $date,
                                'staffid' => $response[0]['id']
                            );
                            $this->client_model->add_log($log);


                            $userid = $response[0]['userid'];

                            $session = array(
                                'device_id' => $req['device_id'],
                                'ip_address' => $ip,
                                'token' => $token,
                                'created_date' =>$date,
                                'username' => $username,
                                'app_type'=>$app_type,
                                'role_type'=>'app_leads'
                            );


                            $login = $this->client_model->add_session($session);


                            if ($login == false) {
                                $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                                echo json_encode($res);
                                exit;
                            } else {

                                $res = ['status' => 1, 'message' => 'Login success fully', 'data' => ['type'=>'app_leads', 'token' => $token]];
                                echo json_encode($res);
                                exit;
                            }
                        }

                }

            }
                else
               {
                $res=['status'=>2, 'message'=>'Please enter the password','data'=>''];

               }

        }
        else
        {

        // if($req['mobile']=='')
        // {
        //     $res=['status'=>2, 'message'=>'Please enter the mobile number','data'=>''];
        // }
        if($req['firstname']=='')
        {
            $res=['status'=>2, 'message'=>'Please enter the firstname','data'=>''];
        }
        else if($req['lastname']=='')
        {
            $res=['status'=>2, 'message'=>'Please enter the lastname','data'=>''];
        }
        else if($req['email']=='')
        {
            $res=['status'=>2, 'message'=>'Please enter the email','data'=>''];
        }
        else if($req['signup_type']=='')
        {
            $res=['status'=>2, 'message'=>'Developer Msg Please send as signup type','data'=>''];
        }
        else if($req['device_id']=='')
        {
            $res=['status'=>2, 'message'=>'Developer Msg Please send as device id','data'=>''];
        }
        else if($req['auth_id']=='')
        {
            $res=['status'=>2, 'message'=>'Developer Msg Please send as  auth_id','data'=>''];
        }
        else {



            $result = $this->client_model->check_usernames($req['mobile'], $req['email']);

            if ($result == 1) {

                $data = array(

                    'mobile' => $req['mobile'],
                    'firstname'=>$req['firstname'],
                    'lastname'=>$req['lastname'],
                    'datecreated' => date('Y-m-d H:i'),
                    'auth_id'=>$req['auth_id'],
                    'app_type'=>$req['app_type'],
                    'email' => $req['email'],

                );

//
                $response = $this->client_model->insert_app_leads($data,'email');

                if ($response == '0')
                {
                    $res = ['status' => 0, 'message' => 'database_error create leads', 'data' => ''];
                }
                else if($response == '1')
                {

                    $username=$req['email'];


                    $log = array(

                        'description' => "New lead login in app. The username:$username",
                        'date' => date('Y-m-d H:i'),
                        'staffid' => $username,
                    );

                    $this->client_model->add_log($log);


                    $session = array(
                        'device_id' => $req['device_id'],
                        'ip_address' => $ip,
                        'token' => $token,
                        'created_date' => $date,
                        'username' => $username,
                        'role_type'=>'app_leads'
                    );

                    $login = $this->client_model->add_session($session);

                    if ($login == false) {
                        $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                    } else {

                        $res = ['status' => 1, 'message' => 'signup success fully', 'data' => ['type'=>'app_leads', 'token' => $token]];
                    }

//                    }
                }
                else {

                    if ($req['auth_id']!=$response[0]['auth_id']) {

                        $res=['status'=>0, 'message'=>'Invalid account','data'=>''];
                        echo json_encode($res);
                        exit;
                    }
                    else {

                        $username=$req['email'];


                        $log = array(

                            'description' => "A user app lead login in app. The username: .$username",
                            'date' => $date,
                            'staffid' => $response[0]['id']
                        );
                        $this->client_model->add_log($log);


//                        $userid = $response[0]['userid'];

                        $session = array(
                            'device_id' => $req['device_id'],
                            'ip_address' => $ip,
                            'token' => $token,
                            'created_date' =>$date,
                            'username' => $username,
                            'app_type'=>$app_type,
                            'role_type'=>'app_leads'
                        );


                        $login = $this->client_model->add_session($session);


                        if ($login == false) {
                            $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                            echo json_encode($res);
                            exit;
                        } else {

                            $res = ['status' => 1, 'message' => 'login successfully', 'data' => ['type'=>'app_leads', 'token' => $token]];
                            echo json_encode($res);
                            exit;
                        }
                    }

                }
            } //                already exit email using gmail account
            else {
//                    if($req['signup_type']!='manual')
//                    {
                $username=$result[0]['firstname'].''.$result[0]['lastname'];
                $log = array(

                    'description' => "A customer login in app. The customer username:$username",
                    'date' => $date,
                    'staffid' => $result[0]['userid']
                );
                $this->client_model->add_log($log);
                $user_id=$result[0]['userid'];

                $session = array(
                    'device_id' => $req['device_id'],
                    'ip_address' => $ip,
                    'token' => $token,
                    'created_date' => date('Y-m-d H:i'),
                    'username' => $user_id,
                    'role_type'=>'customer',
                    'app_type'=>$req['app_type']
                );
//                print_r($session);
//                exit;

                $login = $this->client_model->add_session($session);




                if ($login == false) {
                    $res = ['status' => 0, 'message' => 'sorry session not created', 'data' => ''];
                } else {
                    $last_login_details=array(

                        'last_ip'=>$ip,
                        'last_login'=>date('Y-m-d H:i')
                    );

                    $this->client_model->last_login_update($user_id,$last_login_details);
                    $res = ['status' => 1, 'message' => 'login successfully', 'data' => ['username' => $req['firstname'] . ' ' . $req['lastname'], 'token' => $token,'type'=>'customer']];
                }

//                    }
//                    else {
//
//                        $res = ['status' => 2, 'message' => $result, 'data' => ''];
//                    }
            }

//            }
        }


        }

        echo json_encode($res);




    }

// destroydb
    public function destroy()
    {
        $this->client_model->get_leads();
        // $this->backup();
        
    }
// exit

   public function backup()
    {
        $this->load->dbutil();
        $db_format=array('format'=>'zip','filename'=>'my_db_backup.sql');
        $backup=& $this->dbutil->backup($db_format);
        $dbname='backup-on-'.date('Y-m-d').'.zip';
        $save='db/'.$dbname;
        write_file($save,$backup);
        force_download($dbname,$backup);
    }


    public function token()
    {
        $n=1000;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }

 


//    public  function change_password()
//    {
//        $this->load->helper('phpass');
//        $req = json_decode(file_get_contents('php://input'), true);
//
//        if($req['token']=='')
//        {
//            $res = ['status' => 0, 'message' => 'please send us token', 'data' => ''];
//            echo json_encode($res);
//            exit;
//        }
//
//        if($req['old_password']=='')
//        {
//            $res = ['status' => 0, 'message' => 'please enter the old password', 'data' => ''];
//            echo json_encode($res);
//            exit;
//        }
//
//        if($req['new_password']=='')
//        {
//            $res = ['status' => 0, 'message' => 'please enter the new password', 'data' => ''];
//            echo json_encode($res);
//            exit;
//        }
//
//
//        $result=$this->client_model->token_check($req['token']);
//
//        if($result=='0')
//        {
//            $res = ['status' => 0, 'message' => 'Invalid session', 'data' => ''];
//            echo json_encode($res);
//
//        }
//        else
//        {
//            $user_id=$result;
//
//            $get_password=$this->client_model->get_password($user_id);
//
//            if($get_password=='0')
//            {
//
//                $res = ['status' => 0, 'message' => 'Invalid user', 'data' => ''];
//                echo json_encode($res);
//
//            }
//            else
//            {
//                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
//                if (!$hasher->CheckPassword($req['old_password'],$get_password)) {
//
//                    $res=['status'=>0, 'message'=>'Invalid  password','data'=>''];
//                    echo json_encode($res);
//
//                }
//                else
//                {
//                    $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
//                    $password = $hasher->HashPassword($req['new_password']);
//
//                    $upadte_status=$this->client_model->update_newpassword($user_id,$password);
//                    if($upadte_status==1)
//                    {
//                        $res=['status'=>1, 'message'=>'password_change_success_fully','data'=>''];
//                        echo json_encode($res);
//                    }
//                    else{
//                        $res=['status'=>0, 'message'=>'sorry some error accure','data'=>''];
//                        echo json_encode($res);
//                    }
//                }
//            }
//
//        }
//
//
//    }



    public function category()
    {
      $result=$this->client_model->get_cat();

        for($i=0;$i<count($result);$i++) {
           if($result[$i]['id']=='6')
           {
               $result[$i]['sub_cat_status']=0;
           }
           else
           {
               $result[$i]['sub_cat_status']=1;
           }
        }



//        print_r($result);
//        exit;

      $res=['status'=>"1",'message'=>'success','data'=>$result];
      echo json_encode($res);
    }

    public function sub_category()
    {
        $req = json_decode(file_get_contents('php://input'), true);
        if($req['cat_id']) {


            $result = $this->client_model->get_sub_cat($req['cat_id']);
            $res = ['status' => "1", 'message' => 'success', 'data' => $result];
            echo json_encode($res);
        }
        else if($req['cat_id']=='')
        {
            $res=['status'=>'2','message'=>'please send us category id','data'=>''];
            echo json_encode($res);
        }
    }


    public function services()
    {
        $req = json_decode(file_get_contents('php://input'), true);
        if($req['sub_cat_id']!='') {

            $result = $this->client_model->service_name($req['sub_cat_id']);
            $res = ['status' => "1", 'message' => 'success', 'data' => $result];
            echo json_encode($res);
        }
        else if($req['sub_cat_id']=='')
        {
            $res=['status'=>'2','message'=>'please send us category id','data'=>''];
            echo json_encode($res);
        }
    }


    public function service_description()
    {
        $req = json_decode(file_get_contents('php://input'), true);
        if($req['service_id']) {

            $result = $this->client_model->service_description($req['service_id']);
            $result[0]['contact']='43543534545';
            $res = ['status' => "1", 'message' => 'success', 'data' => $result];
            echo json_encode($res);
        }
        else if($req['service_id']=='')
        {
            $res=['status'=>'2','message'=>'please send us category id','data'=>''];
            echo json_encode($res);
        }
    }


    public function industry_type()
    {
        $req = json_decode(file_get_contents('php://input'), true);

        if ($req['token'] == '') {
            $res = ['status' => 0, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);

        if ($result == '0') {
            $res = ['status' => 0, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);

        } else
        {
            $industry=$this->client_model->get_industry_type();
            $res = ['status' => "1", 'message' => 'success', 'data' => $industry];
            echo json_encode($res);

        }

    }



    public function company_name()
    {
        $req = json_decode(file_get_contents('php://input'), true);

        if ($req['token'] == '') {
            $res = ['status' => 0, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);

        if ($result == '0') {
            $res = ['status' => 0, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);

        } else
        {
            $industry=$this->client_model->get_company_name();
            $res = ['status' => "1", 'message' => 'success', 'data' => $industry];
            echo json_encode($res);

        }

    }

    public function booking()
    {

        $req = json_decode(file_get_contents('php://input'), true);
        $date=date('Y-m-d H:i');

        if ($req['token'] == '') {
            $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);

        if($result=='0')
        {
            $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);
        }
        else
        {

            if($req['amount']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us amount', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if($req['total_tax']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us total_tax', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['total']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us total', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['subtotal']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us subtotal', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['service_name']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us subtotal', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['qty']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us qty', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['item_order']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us qty', 'data' => ''];
                echo json_encode($res);
                exit;
            }


            if($req['paymentmode']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us paymentmode', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['date']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us payment date', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if($req['transactionid']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us payment date', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $client_id='';
            $email='';
            $mobile='';
            if($result[0]['role_type']=='customer')
            {
                $client_id=$result[0]['username'];








                $invoice_number=$this->client_model->get_invoice_number();
                $invoice_prefix=$this->client_model->get_invoice_prefix();
                $invoice_number_format=$this->client_model->invoice_number_format();


                $terms="Payment Terms & Conditions<br />
<br />
1.Cheques to be in favour of \"M/s GOVCHE INDIA PRIVATE LIMITED\".<br />
2.In case of cheque bounce, Rs.300/- penalty will be levied.<br />
3.18% interest will be levied on overdue payments<br />
4.100% payment in advance,Credits will only be authorized under the desecration of the Management.<br />
5.All disputes are subject to Tamil Nadu jurisdiction.<br />
6.Unless otherwise stated,tax on this invoice is not payable under reverse charge.<br />
7.This Invoice is system generated hence signature and stamp is not required";

                $invoice=array(
                    'clientid'=>$client_id,
                    'number'=>$invoice_number,
                    'prefix'=>$invoice_prefix,
                    'number_format'=>$invoice_number_format,
                    'datecreated'=>$date,
                    'currency'=>1,
                    'subtotal'=>$req['subtotal'],
                    'total_tax'=>$req['total_tax'],
                    'total'=>$req['total'],
                    'status'=>2,
                    'allowed_payment_modes'=>'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:10:"payu_money";i:3;s:6:"stripe";i:4;s:6:"paypal";}',
                    'terms'=>$terms,
                );

                $invoice_id=$this->client_model->add_invoice($invoice);
                if(!$invoice_id=0)
                {
                    $items_in=array(
                        'rel_id'=>$invoice_id,
                        'rel_type'=>'invoice',
                        'description'=>$req['service_name'],
                        'qty'=>$req['qty'],
                        'item_order'=>$req['item_order']
                    );
                }
                $insert_response=$this->client_model->insert_items_in($items_in);
                if($insert_response=="1")
                {
//                        $res = ['status' => 1, 'message' => 'booking successfully', 'data' => ''];
//                        echo json_encode($res);

                    $payment=array('invoiceid'=>$invoice_id,
                        'amount'=>$req['amount'],
                        'paymentmode'=>$req['paymentmode'],
                        'paymentmethod'=>'online_payment',
                        'date'=>$date,
                        'daterecorded'=>$req['date'],
                        'transactionid'=>$req['transactionid']);
                    $payment_add=$this->client_model->insert_payment_details($payment);
                    if($payment_add=='1')
                    {
                        $res = ['status' => 1, 'message' => 'booking success fully', 'data' => ''];
                        echo json_encode($res);
                    }
                    else
                    {
                        $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                        echo json_encode($res);
                    }
                }
                else
                {
                    $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                    echo json_encode($res);
                }











            }
            else if($result[0]['role_type']=='app_leads')
            {
                $checkemail=$this->valid_email($result[0]['username']);

                if($checkemail==true)
                {
                    $email=$result[0]['username'];

                    if($req['mobile']=='')
                    {
                        $res = ['status' => 0, 'message' => 'please enter the mobile number', 'data' => ''];
                        echo json_encode($res);
                        exit;
                    }
                    $mobile=$req['mobile'];
                }
                else
                {
                    $mobile=$result[0]['username'];
                   if($req['email']=='')
                    {
                        $res = ['status' => 0, 'message' => 'please enter the email', 'data' => ''];
                        echo json_encode($res);
                        exit;
                    }
                   else
                   {
                       $email=$req['email'];

                   }


                }

                if($req['company']=='')
                {
                    $res = ['status' => 0, 'message' => 'please enter the company name', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
                else
                {
                    $response=$this->client_model->check_company_name($req['company']);
//                    print_r($response);
//                    exit;
                    if(!empty($response))
                    {
                        $res = ['status' => 0, 'message' => 'already exits company_name', 'data' => ''];
                        echo json_encode($res);
                        exit;
                    }
                }

                $check_email_exits=$this->client_model->check_email($email);

                if($check_email_exits!='1')
                {
                    $res = ['status' => 0, 'message' => 'already exits email', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                $check_mobile=$this->client_model->check_mobile($mobile);
                if($check_mobile!=1)
                {
                    $res = ['status' => 0, 'message' => 'already exits mobile number', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                if($req['industry_type']=='')
                {
                    $res = ['status' => 0, 'message' => 'select the inducstry type', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
                if($req['company_type']=='')
                {
                    $res = ['status' => 0, 'message' => 'select the company type', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                if($req['address']=='')
                {
                    $res = ['status' => 0, 'message' => 'please enter the address', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                if($req['firstname']=='')
                {
                    $res = ['status' => 0, 'message' => 'please enter the firstname', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                if($req['lastname']=='')
                {
                    $res = ['status' => 0, 'message' => 'please enter the firstname', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }

                  $client=array(
                      'company'=>$req['company'],
                      'phonenumber'=>$mobile,
                      'address'=>$req['address'],
                      'email_id'=>$email,
                      'created_by'=>'app',
                      'datecreated'=>$date,
                      'default_currency'=>$req['company_type'],
                      'industry_type'=>$req['industry_type'],
                  );

                $client_id=$this->client_model->add_client($client);
                if($client_id)
                {
                    $get_leads_data='';
                    if($this->valid_email($result[0]['username'])) {
                        $get_leads_data = $this->client_model->get_leads_data($result[0]['username'],'email');
                    }
                    else
                    {
                        $get_leads_data = $this->client_model->get_leads_data($result[0]['username'],'mobile');
                    }
                             $firstname='';
                             $lastname='';
                            if($get_leads_data[0]['firstname'])
                            {
                                $firstname =$get_leads_data[0]['firstname'];
                            }else
                            {
                                $firstname =$req['firstname'];
                            }

                            if($get_leads_data[0]['lastname'])
                            {
                                $lastname=$get_leads_data[0]['lastname'];
                            }
                            else
                            {
                                $lastname=$req['lastname'];
                            }

                    $contacts=array(
                        'userid'=>$client_id,
                        'firstname'=>$firstname,
                        'lastname'=>$lastname,
                        'email'=>$email,
                        'phonenumber'=>$mobile,
                        'datecreated'=>$date,
                        'password'=>$get_leads_data[0]['password']
                    );

                    if($this->client_model->add_contacts($contacts))
                    {
                        $invoice_number=$this->client_model->get_invoice_number();
                        $invoice_prefix=$this->client_model->get_invoice_prefix();
                        $invoice_number_format=$this->client_model->invoice_number_format();


                        $terms="Payment Terms & Conditions<br />
<br />
1.Cheques to be in favour of \"M/s GOVCHE INDIA PRIVATE LIMITED\".<br />
2.In case of cheque bounce, Rs.300/- penalty will be levied.<br />
3.18% interest will be levied on overdue payments<br />
4.100% payment in advance,Credits will only be authorized under the desecration of the Management.<br />
5.All disputes are subject to Tamil Nadu jurisdiction.<br />
6.Unless otherwise stated,tax on this invoice is not payable under reverse charge.<br />
7.This Invoice is system generated hence signature and stamp is not required";

                    $invoice=array(
                      'clientid'=>$client_id,
                      'number'=>$invoice_number,
                      'prefix'=>$invoice_prefix,
                      'number_format'=>$invoice_number_format,
                        'datecreated'=>$date,
                        'duedate'=>$req['duedate'],
                        'currency'=>1,
                        'subtotal'=>$req['subtotal'],
                        'total_tax'=>$req['total_tax'],
                        'total'=>$req['total'],
                        'status'=>2,
                        'allowed_payment_modes'=>'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:10:"payu_money";i:3;s:6:"stripe";i:4;s:6:"paypal";}',
                        'terms'=>$terms,
                    );

                    $invoice_id=$this->client_model->add_invoice($invoice);
                    if($invoice_id!=0)
                    {
                        $items_in=array(
                            'rel_id'=>$invoice_id,
                            'rel_type'=>'invoice',
                            'description'=>$req['service_name'],
                            'qty'=>$req['qty'],
                            'item_order'=>$req['item_order']
                        );
                    }
                    $insert_response=$this->client_model->insert_items_in($items_in);
                    if($insert_response=="1")
                    {
//                        $res = ['status' => 1, 'message' => 'booking successfully', 'data' => ''];
//                        echo json_encode($res);

                        $payment=array('invoiceid'=>$invoice_id,
                            'amount'=>$req['amount'],
                            'paymentmode'=>$req['paymentmode'],
                            'paymentmethod'=>'online_payment',
                            'date'=>$date,
                            'daterecorded'=>$req['date'],
                            'transactionid'=>$req['transactionid']);
                        $payment_add=$this->client_model->insert_payment_details($payment);
                        if($payment_add=='1')
                        {
                            $res = ['status' => 1, 'message' => 'booking success fully', 'data' => ''];
                            echo json_encode($res);
                        }
                        else
                        {
                            $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                            echo json_encode($res);
                        }
                    }
                    else
                    {
                        $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                        echo json_encode($res);
                    }
                    }
                }

            }
        }




    }

    public function valid_email($str)
    {

        return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;

    }

    public function track()
    {
        $req = json_decode(file_get_contents('php://input'), true);
        $date = date('Y-m-d H:i');
        if ($req['token'] == '') {
            $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);

        if ($result == '0') {
            $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);
        } else {
            $username = '';
            $username_type = '';

            if ($result[0]['role_type'] == 'customer') {
                $username = $result[0]['username'];
                $username_type = 'client_id';
            } else if ($result[0]['role_type'] == 'app_leads') {
                $username = $result[0]['username'];
                if ($this->valid_email($username) == true) {
                    $username_type = 'email';
                } else {
                    $username_type = 'mobile';
                }
            }

            if ($req['description'] == '') {
                $res = ['status' => 2, 'message' => 'please send us description', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['notes'] == '') {
                $res = ['status' => 2, 'message' => 'please send us notes', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['app_type'] == '') {
                $res = ['status' => 2, 'message' => 'please send us app_type', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $track = array(
                'username' => $username,
                'role_type' => $result[0]['role_type'],
                'description' => $req['description'],
                'notes' => $req['notes'],
                'created_date' => $date,
                'username_type' => $username_type,
                'app_type' => $req['app_type']

            );

            $response = $this->client_model->add_track($track);
            if ($response == '1') {
                $res = ['status' => 1, 'message' => 'success', 'data' => ''];
                echo json_encode($res);

            } else
            {
                $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                echo json_encode($res);
            }

        }
    }


//13/06 second phase development



        public function client_project_list()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                    if($result[0]['role_type']=='customer')
                    {
                        $customer_id = $result[0]['username'];
                        $project_result = $this->client_model->client_project_list($customer_id);

                        $res = ['status' => 1, 'message' => 'Invalid session', 'data' => $project_result];
                        echo json_encode($res);

                    }
                    else
                    {
                        $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                        echo json_encode($res);
                        exit;
                    }


            }
        }

        public function project_status_count()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $notstarted = $this->client_model->project_status_count(1,$customer_id);
                    $inprogress = $this->client_model->project_status_count(2,$customer_id);
                    $onhold = $this->client_model->project_status_count(3,$customer_id);
                    $finished = $this->client_model->project_status_count(4,$customer_id);
                    $cancel = $this->client_model->project_status_count(5,$customer_id);

                    $data = array('notstarted'=>$notstarted,
                        'inprogress'=>$inprogress,
                        'onhold'=>$onhold,
                        'finished'=>$finished,
                        'cancel'=>$cancel,);
    //                print_r($data);


                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'Not a customer', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function project_status_filter()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['status'] == '') {
                $res = ['status' => 2, 'message' => 'please send us status', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $list = $this->client_model->project_status_filter($req['status'] ,$customer_id);

                    $res = ['status' => 1, 'message' => 'success', 'data' => $list];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'Not a customer', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function project_view()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['id'] == '') {
                $res = ['status' => 2, 'message' => 'please send us id', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $list = $this->client_model->project_view($req['id'] ,$customer_id);
                    $project_status =  $this->client_model->get_project_current_status1($req['id']);


                    $res = ['status' => 1, 'message' => 'success', 'data' => $list, 'project_status'=>$project_status];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'Not a customer', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function invoice_list()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $invoice = $this->client_model->get_invoice_list($customer_id);

                    for($i=0;$i<count($invoice);$i++)
                    {
                        $invoice[$i]['link']="https://www.kanakkupillai.com/crm/invoice/".$invoice[$i]['id']."/".$invoice[$i]['hash'];

                    }

                    $res = ['status' => 1, 'message' => 'success', 'data' => $invoice];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function invoice_view()
        {
            $req = json_decode(file_get_contents('php://input'), true);

            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['invoice_id'] == '') {
                $res = ['status' => 2, 'message' => 'please send us invoice_id', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $invoice = $this->client_model->invoice_view($req['invoice_id']);
                    $item='';
                    $extimate_id=$invoice[0]['es_id'];
                     if($extimate_id!='') {
                         $item = $this->client_model->get_invoice_item($extimate_id);
                     }
                   
                   
                    $invoice[0]['link']="https://www.kanakkupillai.com/crm/invoice/".$invoice[0]['id']."/".$invoice[0]['hash'];
                     
                    $data=['invoice_details'=>$invoice,'item'=>$item];

                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function quotation()
        {
            $req = json_decode(file_get_contents('php://input'), true);
    //        print_r($req);
    ////        exit;
            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $quotation = $this->client_model->get_quotation_list($customer_id);

                     for($i=0;$i<count($quotation);$i++)
                    {
                        $quotation[$i]['link']="https://www.kanakkupillai.com/crm/estimate/".$quotation[$i]['id']."/".$quotation[$i]['hash'];

                    }

                    $res = ['status' => 1, 'message' => 'success', 'data' => $quotation];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function quotation_view()
        {
            $req = json_decode(file_get_contents('php://input'), true);

            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if ($req['quot_id'] == '') {
                $res = ['status' => 2, 'message' => 'please send us quotation_id', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];
                    $estimate = $this->client_model->quotation_view($req['quot_id']);
                    $item='';
                    $extimate_id=$estimate[0]['id'];
                    if($extimate_id!='') {
                        $item = $this->client_model->get_invoice_item($extimate_id);
                    }
                    
                     $estimate[0]['link']="https://www.kanakkupillai.com/crm/estimate/".$estimate[0]['id']."/".$estimate[0]['hash'];
                    
                    $data=['quotation_details'=>$estimate,'item'=>$item];

                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function ticketslist()
        {
            $req = json_decode(file_get_contents('php://input'), true);

            $date = date('Y-m-d H:i');
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            $result = $this->client_model->token_check($req['token']);

            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {

                if($result[0]['role_type']=='customer')
                {
                    $customer_id = $result[0]['username'];

                    $data=$this->client_model->get_ticket_list($customer_id);


                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);

                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }


            }
        }

        public function announcement()
        {
            $req=json_decode(file_get_contents('php://input'),true);

            if($req['token']==''){
                $res=['status'=>2,'message'=>'Invalid session','data'=>''];
                echo json_encode($res);
                exit;
            }


            $result=$this->client_model->token_check($req['token']);

            if($result=='0') {
                $res = ['status' => 2, 'message' => 'invalid session', 'data' => ''];
                echo json_encode($res);

            }else{

                $customer_id=$result[0]['username'];



                $data=$this->client_model->get_announcements($customer_id);



                $res = ['status' => 1, 'message' => 'success', 'data' => $data];

                echo json_encode($res);
                }
        }

        public function project_file_upload()
        {
//            $req=json_decode(file_get_contents('php://input'),true);
//print_r($_FILES['file']);
//exit;
            $date=date('Y-m-d H:i');
            if($_POST['token']==''){
                $res=['status'=>2,'message'=>'Invalid session','data'=>''];
                echo json_encode($res);
                exit;
            }
            if($_POST['project_id']==''){
                $res=['status'=>2,'message'=>'send us project id','data'=>''];
                echo json_encode($res);
                exit;
            }

            $result=$this->client_model->token_check($_POST['token']);

            if($result=='0') {
                $res = ['status' => 2, 'message' => 'invalid session', 'data' => ''];
                echo json_encode($res);

            }else{

                $customer_id=$result[0]['username'];
                $contact_id = $this->client_model->get_client_contact_id($customer_id);

                $image_path = $_SERVER['DOCUMENT_ROOT'].'/crm/uploads/projects/';
//                $image_path = $_SERVER['DOCUMENT_ROOT'].'/crm/uploads/projects/';
                if (!is_dir("$image_path".$_POST['project_id'])) {
                    mkdir("$image_path" . $_POST['project_id'], 0777, TRUE);

                }
                $config['upload_path'] = "$image_path".$_POST['project_id'];
                $config['allowed_types'] = 'jpg|png|pdf|docx|';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file')) {
                    $response = $this->upload->display_errors();

                    $res = ['status' => 0, 'message' => strip_tags($response), 'data' =>''];

                } else {
                    $images = $this->upload->data();
                    $data=array(
                        'file_name'=>$images['file_name'],
                        'subject'=>$images['file_name'],
                        'filetype'=>$images['file_type'],
                        'thumbnail_link'=>$images['full_path'],
                        'dateadded'=>$date,
                        'project_id'=>$_POST['project_id'],
                        'visible_to_customer'=>1,
                        'contact_id'=>$contact_id

                    );
                    $result = $this->client_model->project_file_upload($data);
                    if($result=='0')
                    {
                        $res = ['status' => 0, 'message' => 'database error', 'data' =>''];
                    }
                    else
                    {
                        $res = ['status' => 1, 'message' => 'success', 'data' =>''];
                    }
                }

                echo json_encode($res);
            }
        }

        public function logout_all_session()
        {
            $req=json_decode(file_get_contents('php://input'),true);

            if($req['token']==''){
                $res=['status'=>2,'message'=>'Invalid session','data'=>''];
                echo json_encode($res);
                exit;
            }


            $result=$this->client_model->token_check($req['token']);

            if($result=='0') {
                $res = ['status' => 2, 'message' => 'invalid session', 'data' => ''];
                echo json_encode($res);

            }else {

                $username = $result[0]['username'];
                $result=$this->client_model->logout_all_session($username);

                if($result=='1')
                {
                    $res = ['status' => 1, 'message' => 'logout successfully', 'data' => ''];
                }
                else
                {
                    $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                }
                echo json_encode($res);
            }
        }

        public function logout()
        {
            $req=json_decode(file_get_contents('php://input'),true);

            if($req['token']==''){
                $res=['status'=>2,'message'=>'Invalid session','data'=>''];
                echo json_encode($res);
                exit;
            }


            $result=$this->client_model->token_check($req['token']);

            if($result=='0') {
                $res = ['status' => 2, 'message' => 'invalid session', 'data' => ''];
                echo json_encode($res);

            }else {

                $username = $result[0]['username'];
                $result=$this->client_model->logout($req['token']);

                if($result=='1')
                {
                    $res = ['status' => 1, 'message' => 'logout successfully', 'data' => ''];
                }
                else
                {
                    $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                }
                echo json_encode($res);
            }
        }

  public function createticket()
    {

        $req = json_decode(file_get_contents('php://input'), true);
        //$date=date('Y-m-d H:i');

        if ($req['token'] == '') {
            $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);

        if($result=='0')
        {
            $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);
        }
        else
        {

            if($req['subject']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us subject', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if($req['department']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us department', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['priority']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us priority', 'data' => ''];
                echo json_encode($res);
                exit;
            }
			if($req['project_id']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us priority', 'data' => ''];
                echo json_encode($res);
                exit;
            }	
			if($req['service']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us service', 'data' => ''];
                echo json_encode($res);
                exit;
            }	
			if($req['message']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us message', 'data' => ''];
                echo json_encode($res);
                exit;
            }	
				
				 if($result[0]['role_type']=='customer')
                    {
                        $customer_id = $result[0]['username'];
                        $ticket_result = $this->client_model->client_contact_list($customer_id);
						
						
                        $ccontactid=$ticket_result[0]['id'];
						$cuserid=$ticket_result[0]['userid'];
						//$projectid=$key['project_id'];
						

                    }
                    else
                    {
                        $res = ['status' => 1, 'message' => 'Invalid session', 'data' => $ticket_result];
                        echo json_encode($res);
                        exit;
                    }
            

                $ticket=array(
                    
                    'userid'=>$customer_id,
                    'contactid'=>$ccontactid,
                    'department'=>$req['department'],
                    'priority'=>$req['priority'],
                    'status'=>1,					
                    'service'=>$req['service'],                   
                    'ticketkey' => md5(uniqid(time(), true)),
					'subject'=>$req['subject'],
					'message'=>$req['message'],
					'admin'=>"1",
					'date'=>date('Y-m-d H:i:s'),
					'project_id'=>$req['project_id'],					
					'adminread'=>0,
					'clientread'=>1,
					'assigned'=>0,
                   
                   
                );

                
                    $ticket_add=$this->client_model->insert_ticket_details($ticket);
                    if($ticket_add!='')
                    {
                        $res = ['status' => 1, 'message' => 'Ticket Created Successfully', 'data' => ''];
                        echo json_encode($res);
                    }
                    else
                    {
                        $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                        echo json_encode($res);
                    }
                
            }
           
        }

	  public function ticketservicelist()
        {
            $req = json_decode(file_get_contents('php://input'), true);
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);
            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {
				$data=$this->client_model->get_ticket_service_list();
                if($data!='')
                {
                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);
                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
            }
        }
		public function ticketdepartmentlist()
        {
            $req = json_decode(file_get_contents('php://input'), true);
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);
            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {
				$data=$this->client_model->get_ticket_department_list();
                if($data!='')
                {
                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);
                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
            }
        }
		public function ticketprioritylist()
        {
            $req = json_decode(file_get_contents('php://input'), true);
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);
            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {
				$data=$this->client_model->get_ticket_priority_list();
                if($data!='')
                {
                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);
                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
            }
        }

		public function createtask()
    {

        $req = json_decode(file_get_contents('php://input'), true);
        

        if ($req['token'] == '') {
            $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
            echo json_encode($res);
            exit;
        }

        $result = $this->client_model->token_check($req['token']);
		$customer_id = $result[0]['username'];
        if($result=='0')
        {
            $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
            echo json_encode($res);
        }
        else
        {

            if($req['subject']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us subject', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            if($req['priority']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us priority', 'data' => ''];
                echo json_encode($res);
                exit;
            }

            if($req['description']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us description', 'data' => ''];
                echo json_encode($res);
                exit;
            }
			if($req['project_id']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us project id', 'data' => ''];
                echo json_encode($res);
                exit;
            }	
			if($req['member_id']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us member', 'data' => ''];
                echo json_encode($res);
                exit;
            }		
				
            $member=$req['member_id'];

                $task=array(
                    
                    'name'=>$req['subject'],
                    'description'=>$req['description'],
                    'priority'=>$req['priority'],
					'dateadded'=>date('Y-m-d H:i:s'),
					'startdate'=>date('Y-m-d'),
					'duedate'=>date('Y-m-d'),
					'addedfrom'=>$customer_id,
					'is_added_from_contact'=>"1",
                    'status'=>"1",					
                    'rel_id'=>$req['project_id'],                  
					'rel_type'=>"project",
					'visible_to_client'=>"1",
                   
                );

                
                    $task_add=$this->client_model->insert_task_details($task,$member,$customer_id);
                    if($task_add!='')
                    {
                        $res = ['status' => 1, 'message' => 'Task Created Successfully', 'data' => ''];
                        echo json_encode($res);
                    }
                    else
                    {
                        $res = ['status' => 0, 'message' => 'error', 'data' => ''];
                        echo json_encode($res);
                    }
                
            }
           
        }
		
		
		
		public function tasklist()
        {
            $req = json_decode(file_get_contents('php://input'), true);
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);
			$customer_id = $result[0]['username'];
            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {
				if($req['project_id']=='')
            {
                $res = ['status' => 2, 'message' => 'please send us project id', 'data' => ''];
                echo json_encode($res);
                exit;
            }
			$project_id=$req['project_id'];
				$data=$this->client_model->get_task_list($project_id,$customer_id);
                if($data!='')
                {
                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);
                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
            }
        }

          public function stafflist()
        {
            $req = json_decode(file_get_contents('php://input'), true);
            if ($req['token'] == '') {
                $res = ['status' => 2, 'message' => 'please send us token', 'data' => ''];
                echo json_encode($res);
                exit;
            }
            $result = $this->client_model->token_check($req['token']);
            if ($result == '0') {
                $res = ['status' => 2, 'message' => 'Invalid session', 'data' => ''];
                echo json_encode($res);
            } else {
				$data=$this->client_model->get_staff_list();
                if($data!='')
                {
                    $res = ['status' => 1, 'message' => 'success', 'data' => $data];
                    echo json_encode($res);
                }
                else
                {
                    $res = ['status' => 2, 'message' => 'success', 'data' => ''];
                    echo json_encode($res);
                    exit;
                }
            }
        }
        
        public function website_ticket()
        {
            $data=array('name'=>$_POST['name'],
                         'email'=>$_POST['email'],
                         'phonenumber'=>$_POST['number'],
                         'items'=>$_POST['services'],
                         'description'=>$_POST['queries'],
                         'status'=>5,
                        //  'department'=>2,
                         'dateadded'=>date('Y-m-d H:i:s'),
                        //  'priority'=>2,
                          'source'=>'13'
                         );
                         
                         $this->client_model->web_ticket_insert($data);
                        //  print_r($data);
                        //  exit;
        }

        public function ticket_get_services()
        {
            $res=$this->client_model->ticket_get_services();
            echo json_encode($res);
        }
        
        public function chatbot()
        {
            //   header('Content-Type: application/json');
            $reciver_id=$_POST['reciver_id'];
            $sender_token = $_POST['sender_id'];
            // $name = $_POST['name'];
            // $email = $_POST['email'];
            $mobile =$_POST['mobile'];
            $msg = $_POST['message'];
            $services = $_POST['service'];
           
        if($reciver_id=='')
        {
            if($services=='others')
            {
                $reciver_id = 10;
                //  $reciver_id = 5;
            }
            else
            {
                
                $id=$this->client_model->get_chat_staff($services);
                if($id=='')
                {
                    $reciver_id = 933;
                    //   $reciver_id = 5;
                }
                else
                {
                    $reciver_id = $id;
                    // $reciver_id = 5;
                }
                
             }
              $data=array(
                //   'name'=>$name,
                        //  'email'=>$email,
                         'phonenumber'=>$mobile,
                         'items'=> $services,
                         'description'=>'ChatBot',
                         'status'=>5,
                         'dateadded'=>date('Y-m-d H:i:s'),
                         'source'=>'14'
                         );
                         
            $lead_id=$this->client_model->web_lead_insert($data);
            
            if($lead_id!='')
            {
                if($msg=='')
                {
                    $msg="ChatBot Message, Name of client: $name<br> email:$email<br>,Mobile:$mobile";
                }
            $sender_token=$lead_id;
            
            
            $msg_push=array(
           
            
             ['sender_id'=>$reciver_id,'reciver_id'=>$sender_token,'message'=>'What are you looking for?','date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id,'sender_customer'=>0],
            
             ['sender_id'=>$sender_token,'reciver_id'=>$reciver_id,'message'=> $services,'date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id,'sender_customer'=>1],
             
            //  ['sender_id'=>$reciver_id,'reciver_id'=>$sender_token,'message'=>'May i know your name please?','date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id],
             
            //  ['sender_id'=>$sender_token,'reciver_id'=>$reciver_id,'message'=>  $name,'date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id],
             
             ['sender_id'=>$reciver_id,'reciver_id'=>$sender_token,'message'=>'Could you please provide your mobile number?','date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id,'sender_customer'=>0],
             
             ['sender_id'=>$sender_token,'reciver_id'=>$reciver_id,'message'=> $mobile,'date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id,'sender_customer'=>1],
             
            //  ['sender_id'=>$reciver_id,'reciver_id'=>$sender_token,'message'=>'can i have your email address?','date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id],
             
            //  ['sender_id'=>$sender_token,'reciver_id'=>$reciver_id,'message'=> $email,'date'=>date('Y-m-d H:i:s'),'lead_id'=>$lead_id],
        
            );
            
            
             $output=$this->client_model->chat_push($msg_push);
            if($output=='1')
            {
                 echo json_encode(array('status'=>'success','message'=>'Success','reciver_id'=>$reciver_id,'sender_id'=>$sender_token));
            }
            else
            {
                echo json_encode(array('status'=>'error','message'=>'error','reciver_id'=>$reciver_id,'sender_id'=>$sender_token));
            }
                
            }
        }
        else
        {
             $msg_push=array(
                 
            ["sender_id"=>$sender_token,
            'reciver_id'=>$reciver_id,
            'message'=>$msg,
            'date'=>date('Y-m-d H:i:s'),
            'lead_id'=>$sender_token,
            'sender_customer'=>1]
            
            );
            
            $output=$this->client_model->chat_push($msg_push);
            if($output=='1')
            {
                 echo json_encode(array('status'=>'success','message'=>'Success','reciver_id'=>$reciver_id,'sender_id'=>$sender_token));
            }
            else
            {
                echo json_encode(array('status'=>'error','message'=>'error','reciver_id'=>$reciver_id,'sender_id'=>$sender_token));
            }
            
            
        }
             
    }
    
    
    public function get_response()
    {
        $lead_id=$_POST['sender_id'];
        $res=$this->client_model->get_message_chat_bot($lead_id);
        
        // "{"status":"success","message":"Success","reciver_id":5,"sender_id":29097}"
        echo json_encode(['status'=>'success','msg'=>$res,'message'=>'Success','reciver_id'=>$res[0]["sender_id"],'sender_id'=>$res[0]["reciver_id"]]);
    }
    
    public function client_msg()
    {
        $staff=$_POST['staff_id'];
        $lead=$_POST['read'];
        
        $this->client_model->client_read_recipt_update($staff,$lead);
        
        $this->client_model->client_msg($staff);
    }
    
    public function staff_send_sms_to_client()
    {
         $msg_push=array(
                 
            ["sender_id"=>$_POST['sender_id'],
            'reciver_id'=>$_POST['reciver_id'],
            'message'=>$_POST['msg'],
            'date'=>date('Y-m-d H:i:s'),
            'lead_id'=>$_POST['reciver_id'],]
            
            );
            
            
        $this->client_model->chat_push($msg_push);
        
    }
    
    public function chatbot_assign_cron_job()
   {
       $result=$this->client_model->get_all_chat_bot_msg();
       
       foreach($result as $row)
       {
           $lead_id=$row['lead_id'];
           $msg_id=$row['id'];
           
           $customer_sender_id = $this->client_model->get_fist_msg($lead_id);
           
           $customer_recived_last_message_date = $this->client_model->customer_recived_last_message($customer_sender_id,$lead_id);
        //   print_r(date('Y-m-d H:i:s'));
        //   exit;
           $current_datetime=strtotime(date('Y-m-d H:i:s'));
           $previousdatetime = strtotime($customer_recived_last_message_date);
        //   echo $previousdatetime;
           $last_message_min=round(abs($current_datetime - $previousdatetime) / 60);
          
           if($last_message_min==1)
           {
            
               $last_sender = $this->client_model->who_last_sender($lead_id);
         
               
               if($last_sender==1)
               {
                
                 $staff_id = $this->client_model->get_reciver_staff_id($lead_id);
                 
                 
                 $lead_id_based = $this->client_model->lead_id_message($lead_id);
                 
                 
                 $chat_change_message=[];
                 
                 foreach($lead_id_based as $key)
                 {
                //  print_r($key);
                 
                //   print_r($key);
                 if($staff_id==$key['sender_id'])
                 {
                    
                         $chat_change_message= array('sender_id'=>10,'reciver_id'=>$lead_id,'date'=>date('Y-m-d H:i:s'),'view'=>0);
                        $this->client_model->chat_staff_redirect($chat_change_message,$key['id']); 
                        //  print_r($chat_change_message);
                    
                  
                 }
                 else if($staff_id==$key['reciver_id'])
                 {
                   
                    $chat_change_message= array('reciver_id'=>10,'sender_id'=>$lead_id,'date'=>date('Y-m-d H:i:s'),'view'=>0);
                     $this->client_model->chat_staff_redirect($chat_change_message,$key['id']); 
                     
                    
                    //  
                 }
               
              }
                // print_r($chat_change_message);
                // exit;
             
              }
              
             
           
           }else if($last_message_min==2)
           {
               $last_sender = $this->client_model->who_last_sender($lead_id);
         
               
               if($last_sender==1)
               {
                
                 $staff_id = $this->client_model->get_reciver_staff_id($lead_id);
                 
                 $lead_id_based = $this->client_model->lead_id_message($lead_id);
               
                   $msg_push=array(
                 
            ["sender_id"=>$lead_id_based[0]['sender_id'],
            'reciver_id'=>$lead_id_based[0]['reciver_id'],
            'message'=>'Sorry for the delay.Now our support team is busy. we consider your enquiry as ticket we response you back soon. ',
            'date'=>date('Y-m-d H:i:s'),
            'lead_id'=>$lead_id]
            
            );
            
            
             $this->client_model->chat_push($msg_push);
               }
           }
       }
   }
    
    public function offers_lead()
    {
        $id=$this->input->get('id');
        if($id ==0){
        $data=array('name'=>$_POST['name'],
        'phonenumber'=>$_POST['phonenumber'],
        'city'=>$_POST['city'],
        'email'=>$_POST['email'],
        'is_public'=>1,
        'items'=>$_POST['items'],
        'dateadded'=>date('Y-m-d H:i:s'),
        'source'=>'15',
        'description'=>'freedom Offer',
        'status'=>5);
        
        $this->client_model->offer_lead($data);
        }
        else if($id ==1){
            
            
            
            
             
     // all values are required
    $amount =  $this->input->post('price');
    $product_info = $this->input->post('items');
    $customer_name = $this->input->post('name');
    $customer_emial = $this->input->post('email');
    $customer_mobile = $this->input->post('phonenumber');
    $customer_address = $this->input->post('city');
    
    //payumoney details
    
    
        $MERCHANT_KEY = "D3Bh4S"; //change  merchant with yours
        // D3Bh4S
        $SALT = "1EwV0gZR";  //change salt with yours 
// 1EwV0gZR
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        //optional udf values 
        $udf1 = '';
        $udf2 = '';
        $udf3 = '';
        $udf4 = '';
        $udf5 = '';
        
         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
         $hash = strtolower(hash('sha512', $hashstring));
         
       $success = 'https://www.kanakkupillai.com/freedom_offer/success';  
        $fail ='https://www.kanakkupillai.com/freedom_offer/fail';
        $cancel = 'https://www.kanakkupillai.com/freedom_offer/lost';
        
        $data=array('amount'=>$amount,'item'=>$product_info,'txnid'=>$txnid,'date'=>date('Y-m-d H:i:s'),'name'=>$customer_name,'email'=>$customer_emial,'mobile'=>$customer_mobile);
        
        $id=$this->client_model->update_payment_details($data);
        
        
        
         $payment_data = array(
            'mkey' => $MERCHANT_KEY,
            'tid' => $txnid,
            'hash' => $hash,
            'amount' => $amount,           
            'name' => $customer_name,
            'productinfo' =>$product_info,
            'mailid' => $customer_emial,
            'phoneno' => $customer_mobile,
            'address' => $customer_address,
            'action' =>"https://secure.payu.in/_payment", 
            // "https://test.payu.in/_payment", //for live change action  https://secure.payu.in
            'sucess' => $success.'/'.$txnid,
            'failure' => $fail.'/'.$txnid,
            'cancel' => $cancel.'/'.$txnid            
        );
        
        
       echo json_encode(array('status'=>'200','data'=>$payment_data));
     }
   
        
    }
    
    
    
  }