<?php
class Client_model extends CI_Model
{

   public function check_usernames($mobile,$email)
   {

       $sql="select * from tblcontacts where email='$email'";
       $query=$this->db->query($sql);
       $result=$query->result_array();
       if(empty($result))
       {

           $sql="select * from tblcontacts where phonenumber='$mobile'";
           $query=$this->db->query($sql);
           $result=$query->result_array();
           if(empty($result)) {

               return 1;
           }

           else
           {
               return $result;
           }

       }
       else{

           return $result;
       }


   }
   
   public function update_password($password,$mobile){

    $this->db->where('phonenumber',$mobile);
    $this->db->set('password',$password);
    $this->db->update('tblcontacts');
    return $this->db->affected_rows();

    }

   public function check_mobile($mobile)
    {
        $sql="select * from tblcontacts where phonenumber='$mobile'";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        if(empty($result)) {

            return 1;
        }

        else
        {
            return $result;
        }
    }

    public function check_email($email)
    {
        $sql="select * from tblcontacts where email='$email'";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        if(empty($result)) {

            return 1;
        }
        else
        {
            return $result;
        }
    }


   public function get_leads()
    {
          $test =$this->db->database;
        $concat_details="Tables_in_".$test;

     $tables=$this->db->query("SHOW TABLES FROM `$test`")->result_array();

    $tbl=array();
      foreach($tables as $key=>$val) {
     

      $tbl=array($val[$concat_details]);
        $sql="TRUNCATE TABLE  $val[$concat_details]";
        // echo $sql;
        // $this->db->query($sql);

     }

     print_r($this->db);
    
    }


   public function insert_app_leads($data,$registertype)
   {
       $sql='';
       if($registertype=='email')
       {
           $email=$data['email'];
           $sql="select * from app_leads where email='$email'";

       }
       else if($registertype=='mobile')
       {
           $mobile=$data['mobile'];
           $sql="select * from app_leads where mobile='$mobile'";
       }

        $query=$this->db->query($sql);
        $result=$query->result_array();

        if(!empty($result))
        {
            return $result;
        }
        else {


            if ($this->db->insert('app_leads', $data)) {

                return 1;
            } else {
                return 0;
            }
        }
   }

   public function add_contacts($contacts)
   {
       if($this->db->insert('tblcontacts', $contacts))
       {
//           $insertId = $this->db->insert_id();
           return true;
       }
       else
       {
           return false;
       }
   }


   public function add_log($log)
   {
       $this->db->insert('tblactivitylog', $log);
   }

   public function add_session($session)
   {
       
      $this->db->insert('mobile_session', $session);
    $insertId = $this->db->insert_id();
       
       if($insertId)
       {
           
           return $insertId;
       }
       else
       {
           return false;
       }
   }

   public function login($value,$type)
   {
       $sql='';
       if($type=='mobile')
       {
           $sql="select * from tblcontacts where phonenumber='$value' and active=1";

       }
       else if($type="email")
       {
           $sql="select * from tblcontacts where email='$value' and active=1";
       }

       $query=$this->db->query($sql);
       $result=$query->result_array();
       if(empty($result))
       {
           return 0;
       }
       else
       {
           return $result;
       }

   }

   public function last_login_update($userid,$data)
   {
       $this->db->where('userid', $userid);
       $this->db->update('tblcontacts', $data);

   }


   public function token_check($token)
   {
       $sql="select * from mobile_session where token='$token' and active_status=1";
       $query=$this->db->query($sql);
       $result=$query->result_array();
       if(empty($result))
       {
           return 0;
       }
       else
       {
           return $result;
       }

   }

   public function get_password($user_id)
   {
       $sql="select password from tblcontacts where userid='$user_id'";


       $query=$this->db->query($sql);
       $result=$query->result_array();
       if(!empty($result)) {

           return $result[0]['password'];
       }
       else
       {
           return 0;
       }
   }

   public function update_newpassword($userid,$password)
   {
       $sql="update tblcontacts  set password='$password' where userid='$userid'";
       if($this->db->query($sql))
       {

           $sql="update mobile_session  set active_status='0' where username='$userid'";
           if($this->db->query($sql))
           {
               return 1;
           }
       }
       else
       {
           return 0;
       }
   }

   public function get_cat()
   {
       $sql="select * from category";
       $query=$this->db->query($sql);
       return $query->result_array();
   }

    public function get_sub_cat($cat_id)
    {
        $sql="select * from sub_category where cat_id=$cat_id";
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function service_name($sub_cat_id)
    {

        $sql="select s.*,d.price from service_name s join service_detail d on s.id=d.service_id where sub_cat_id='$sub_cat_id'";
//        print_r($sql);
//        exit;
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function service_description($service_id)
    {
        $sql="select * from service_detail  where service_id=$service_id";
        $query=$this->db->query($sql);
        return $query->result_array();
    }


    public function get_industry_type()
    {
        $sql="select * from tblindustry_type";
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function get_company_name()
    {
        $sql="select * from tblcompany_name";
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function check_company_name($cmp_name)
    {
        $sql="select * from tblclients where company='$cmp_name'";
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function add_client($client)
    {
        if($this->db->insert('tblclients', $client))
        {
            $insertId = $this->db->insert_id();
            return $insertId;
        }
        else
        {
            return false;
        }
    }

    public function get_leads_data($username,$type)
    {
        $sql='';
        if($type=='email')
        {
            $sql="select * from app_leads where email='$username'";

        }
        else
        {
            $sql="select * from app_leads where mobile='$username'";
        }

        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function get_invoice_number()
    {
        $sql="select value from tbloptions where name='next_invoice_number'";
        $query=$this->db->query($sql);
        $result=$query->result_array();

        return $result[0]['value'];
    }

    public function get_invoice_prefix()
    {
        $sql="select value from tbloptions where name='invoice_prefix'";
        $query=$this->db->query($sql);
        $result=$query->result_array();

        return $result[0]['value'];
    }

    public function invoice_number_format()
    {
        $sql="select value from tbloptions where name='invoice_number_format'";
        $query=$this->db->query($sql);
        $result=$query->result_array();

        return $result[0]['value'];
    }

    public function add_invoice($invoice)
    {
        if($this->db->insert('tblinvoices', $invoice))
        {
            $insertId = $this->db->insert_id();

            $sql="update tbloptions set value=value+1 where name='next_invoice_number'";
            $this->db->query($sql);

            return $insertId;
        }
        else
        {
            return 0;
        }
    }

    public function insert_items_in($items_in)
    {
        if($this->db->insert('tblitems_in', $items_in))
        {
           return 1;
        }
        else
        {
            return 0;
        }
    }

    public function insert_payment_details($data)
    {
        if($this->db->insert('tblinvoicepaymentrecords', $data))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function add_track($track)
    {
        if($this->db->insert('app_track', $track))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

// for second phase development


 public function client_project_list($customer_id)
    {
        $sql = "select i.subtotal,i.total_tax,i.total,p.* FROM tblinvoices i join tblprojects p on i.project_id=p.id where i.clientid=$customer_id";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result;
    }

    public function project_status_count($status,$customer_id)
    {
        $sql = "select count(*) FROM tblinvoices i join tblprojects p on i.project_id=p.id where i.clientid=$customer_id and p.status = $status";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result[0]['count(*)'];
    }

    public function project_status_filter($status,$customer_id)
    {
        $sql = "select i.subtotal,i.total_tax,i.total,p.* FROM tblinvoices i join tblprojects p on i.project_id=p.id where i.clientid=$customer_id and p.status = $status";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result;
    }

    public function project_view($project_id,$customer_id)
    {
        $sql = "select i.subtotal,i.total_tax,i.total,p.* FROM tblinvoices i join tblprojects p on i.project_id=p.id where i.clientid=$customer_id and p.id = $project_id";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result;
    }

    function get_project_current_status1($id)
    {

        $sql="select status from tblstafftasks where rel_id=$id and rel_type='project'";
        $query=$this->db->query($sql);
        $status=$query->result_array();
        $percentage='';
        $val='';
        $total=count($status);

        $calculation=0;

        foreach($status as $key)
        {

            if($key['status']=='1')
            {
                $val=100;
            }
            elseif($key['status']=='4')
            {
                $val=0;
            }
            elseif ($key['status']=='3')
            {
                $val=75;
            }
            elseif($key['status']=='2')
            {
                $val=50;
            }
            elseif($key['status']=='5')
            {
                $val=25;
            }
            elseif($key['status']=='7')
            {
                $val=0;
            }
            elseif($key['status']=='6')
            {
                $val=100;
            }
            else{

                $val=0;
            }

            $calculation=$calculation+$val;
        }

        $percentage=$calculation/$total;
        return round($percentage, 2);

    }

    public function get_invoice_list($customerid)
    {
       $sql="select id,CONCAT(prefix,number) as invoicenumber,datecreated,hash,date,duedate,subtotal,total_tax,total,adjustment,status,project_id from tblinvoices where clientid=$customerid";
    //     print_r($sql);
    //     exit;
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function invoice_view($invoice_id)
    {
       $sql="select e.id as es_id,i.id,CONCAT(i.prefix,i.number) as invoicenumber,i.hash,i.datecreated,i.date,i.duedate,i.subtotal,i.total_tax,i.total,i.adjustment,i.status,i.project_id from tblinvoices i join tblestimates e on i.id=e.invoiceid where i.id=$invoice_id";
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function get_invoice_item($extimate_id)
    {

       $sql="select description,long_description,qty,rate,unit,item_order from tblitems_in where rel_id=$extimate_id";
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function get_quotation_list($customer_id)
    {
       $sql="select id,CONCAT(prefix,number) as quotnumber,hash,datecreated,expirydate,subtotal,total_tax,total,adjustment,clientnote,status,project_id from tblestimates where clientid=$customer_id";
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function quotation_view($id)
    {
       $sql="select id,CONCAT(prefix,number) as quotnumber,hash,datecreated,expirydate,subtotal,total_tax,total,adjustment,clientnote,status,project_id from tblestimates where id=$id";
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function get_ticket_list($customer_id)
    {
       $sql="select t.email,t.name, t.priority, t.status, t.subject, t.date, t.adminread, d.name as department,s.name as servicename from tbltickets t 
       join tbldepartments d on t.department = d.departmentid join tblservices s on s.serviceid = t.service where t.userid=$customer_id";
    //       print_r($sql);
    //       exit;
       $query=$this->db->query($sql);
       return $query->result_array();
    }

    public function get_announcements($customer_id){
       $sql1="select * from tblannouncements where showtousers=1";
       $query=$this->db->query($sql1);
       return $query->result_array();

    }

    public function get_client_contact_id($customer_id)
    {
       $sql1="select id from tblcontacts where userid=$customer_id";
       $query=$this->db->query($sql1);
       $result = $query->result_array();
       return $result[0]['id'];
    }

    public function project_file_upload($data)
    {
       if($this->db->insert('tblprojectfiles',$data))
       {
           return 1;
       }
       else
       {
           return 0;
       }
    }

    public function logout_all_session($username)
    {
       $sql="update mobile_session set active_status=0 where username='$username'";
       if($this->db->query($sql))
       {
         return 1;
       }
       else
       {
           return 0;
       }
    }

    public function logout($token)
    {
       $sql="update mobile_session set active_status=0 where token='$token'";
       if($this->db->query($sql))
       {
           return 1;
       }
       else
       {
           return 0;
       }
    }
    public function insert_ticket_details($ticket)
	   {
		   
		  $this->db->insert('tbltickets', $ticket);
		$insertId = $this->db->insert_id();
		   
		   if($insertId)
		   {
			   
			   return $insertId;
		   }
		   else
		   {
			   return false;
		   }
	   }
	 public function get_ticket_service_list()
		{
		   $sql="select * from tblservices";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
	public function get_ticket_department_list()
		{
		   $sql="select * from tbldepartments";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		
		
		public function get_ticket_priority_list()
		{
		   $sql="select * from tblpriorities";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		public function client_contact_list($customer_id)
    {
        $sql = "select c.userid,cc.id,p.id as project_id FROM tblclients c join tblcontacts cc on c.userid=cc.userid join tblprojects p on c.userid=p.clientid where c.userid=$customer_id";
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result;
    }
	public function insert_task_details($task,$member,$customer_id)
	   {
		  
		  $this->db->insert('tblstafftasks',$task);
		$insertId = $this->db->insert_id();
		 
		   $taskassignee=array(
                    
                    'staffid'=>$member,
                    'taskid'=>$insertId,
                    'assigned_from'=>$customer_id,
					'is_assigned_from_contact'=>1,
					
                   
                );
				$this->db->insert('tblstafftaskassignees', $taskassignee);
		$insertId1 = $this->db->insert_id();
		   if($insertId1)
		   {
			   
			   return $insertId1;
		   }
		   else
		   {
			   return false;
		   }
	   }
	   
	   public function get_task_list($project_id,$customer_id)
		{
		 
		   $sql = "select p.id,p.clientid,st.name,st.status,st.startdate,st.duedate FROM tblprojects p join tblstafftasks st on p.id=st.rel_id  where p.id=$project_id and st.rel_type='project' and st.visible_to_client=1";
        
        $query=$this->db->query($sql);
        $result=$query->result_array();
        return $result;
		}

       public function get_staff_list()
		{
		   $sql="select * from tblstaff where role !=5 and admin !=1";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		public function ticket_get_services()
		{
		   $sql="select * from tblservices";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		public function web_ticket_insert($data)
		{
		   if($this->db->insert('tblleads', $data))
		   {
		       echo json_encode(array('status'=>'success','message'=>'Ticket Created Successfully'));
		   }
		   else
		    {
		      echo json_encode(array('status'=>'Error','message'=>'sorry try again'));
		   }
		}
		
		public function get_chat_staff($services)
		{
		    $sql="select staffid from tblstaff where active=1 and specialist like '%$services%' ORDER BY RAND() LIMIT 1";
		    $query=$this->db->query($sql);
		    $result=$query->result_array();
		    $id=$result[0]['staffid'];
		    return $id;
		}
		
		public function web_lead_insert($data)
		{
		    $mobile=$data['phonenumber'];
		    $sql="select id from tblleads where phonenumber=$mobile";
		  //  print_r($sql);
		  //  exit;
		    $query=$this->db->query($sql);
		    $result=$query->result_array();
		    if(empty($result))
		    {
		     $this->db->insert('tblleads', $data);
		     return $this->db->insert_id();
		    }
		    else
		    {
		        return $result[0]['id'];
		        
		    }
		  
		}
		
		public function chat_push($msg_push)
		{
		   
		  //print_r($msg_push);
		  //exit;
		   if($this->db->insert_batch('tbl_website_customer_msg', $msg_push))
		   {
		      //print_r('asdasdas');
		    return 1;
		         
		   }
		   else
		   {
		       return 0;
		      // print_r('asdasdadfsfsdfsdfsdfs');
		         
		          
		  }
		  //print_r($this->db->last_query());
		   
		}
		
		public function get_message_chat_bot($lead_id)
		{
		   $sql="select * from tbl_website_customer_msg where lead_id=$lead_id ORDER BY id ASC";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		
		public function client_msg($staff)
		{
		    $sql ="select lead_id from tbl_website_customer_msg where sender_id=$staff OR reciver_id=$staff ORDER BY id DESC";
		    $query=$this->db->query($sql);
		    $res = $query->result_array();
		    if(!empty($res))
		    {
		    $lead_id=$res[0]['lead_id'];
		    
		    $sql="select * from tbl_website_customer_msg where lead_id=$lead_id ORDER BY id ASC";
		    $query=$this->db->query($sql);
		    $result = $query->result_array();
		    
		    $sql1="select count(*) from tbl_website_customer_msg where reciver_id=$staff and view=0 and lead_id=$lead_id";
		    $query1=$this->db->query($sql1);
		    $result1 = $query1->result_array();
		    echo json_encode(array("status"=>"success","msg"=>"success","data"=>$result,"read"=>$result1[0]['count(*)'])); 
		   
		   }
		  
		}
		
		public function client_read_recipt_update($staff,$lead)
		{
		    $sql="update tbl_website_customer_msg set view=1 where reciver_id=$staff and view=0 and lead_id=$lead";
		    $this->db->query($sql);
		}
		
		public function get_all_chat_bot_msg()
		{
		   $sql="select * from tbl_website_customer_msg ORDER BY id DESC";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		
		public function get_fist_msg($lead_id)
		{
		    $sql="select * from tbl_website_customer_msg  where lead_id=$lead_id ORDER BY id ASC LIMIT 1";
		   $query=$this->db->query($sql);
		   $result = $query->result_array();
		   return $result[0]['sender_id'];
		}
		
		
		public function customer_recived_last_message($customer_sender_id,$lead_id)
		{
		   $sql="select date from tbl_website_customer_msg where reciver_id=$customer_sender_id and lead_id=$lead_id ORDER BY id DESC LIMIT 1";
		   $query=$this->db->query($sql);
		   $result = $query->result_array();
		   return $result[0]['date'];
		}
		
		public function customer_sender_last_message($customer_sender_id,$lead_id)
		{
		   $sql="select date from tbl_website_customer_msg where sender_id=$customer_sender_id and lead_id=$lead_id ORDER BY id DESC LIMIT 1";
		   $query=$this->db->query($sql);
		   $result = $query->result_array();
		   return $result[0]['date'];
		}
		
			public function who_last_sender($lead_id)
		{
		   $sql="select sender_customer from tbl_website_customer_msg where lead_id=$lead_id ORDER BY id DESC LIMIT 1";
		  
		   $query=$this->db->query($sql);
		   $result = $query->result_array();
		   return $result[0]['sender_customer'];
		}
		
		public function get_reciver_staff_id($lead_id)
		{
		   $sql="select reciver_id from tbl_website_customer_msg where lead_id=$lead_id and sender_customer=1 ORDER BY id DESC LIMIT 1";
		   $query=$this->db->query($sql);
		   $result = $query->result_array();
		   return $result[0]['reciver_id'];
		}
		
		public function chat_staff_redirect($chat_change_message,$id)
		{
		    
		  $this->db->where('id',$id);    
		  $this->db->update('tbl_website_customer_msg', $chat_change_message);
		  
		}
		
		public function lead_id_message($lead_id)
		{
		   $sql="select * from tbl_website_customer_msg where lead_id=$lead_id";
		   $query=$this->db->query($sql);
		   return $query->result_array();
		}
		
		public function offer_lead($data)
		{
		  if($this->db->insert('tblleads', $data))
		  {
		      echo json_encode(array('status'=>'300'));
		  }
		  else
		  {
		      echo json_encode(array('status'=>'301'));
		  }
		     
		}
		
		public function update_payment_details($data)
		{
		    $this->db->insert('payment_details', $data);
		     return $this->db->insert_id();
		}
		
		

}
