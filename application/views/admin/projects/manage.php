
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
 
            <div class="panel_s">
              <div class="panel-body">
              <div class="_buttons">
                   <!--by rathina for hide new project-->
			 <div  style="display:none">
              <?php if(has_permission('projects','','create')){ ?>
                <a href="<?php echo admin_url('projects/project'); ?>" class="btn btn-info pull-left display-block">
                  <?php echo _l('new_project'); ?>
                </a>
              <?php } ?>
              </div>
              <input  type="button" class="btn btn-sm btn-primary launch-modal" style="display:none" id="feedback"  value="feedback">
              <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-filter" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right width300">
                  <li>
                    <a href="#" data-cview="all" onclick="dt_custom_view('','.table-projects',''); return false;">
                      <?php echo _l('expenses_list_all'); ?>
                    </a>
                  </li>
                  <?php
                  // Only show this filter if user has permis sion for projects view otherwisde wont need this becuase by default this filter will be applied
                  if(has_permission('projects','','view')){ ?>
                  <li>
                    <a href="#" data-cview="my_projects" onclick="dt_custom_view('my_projects','.table-projects','my_projects'); return false;">
                      <?php echo _l('home_my_projects'); ?>
                    </a>
                  </li>
                  <?php } ?>
                  <li class="divider"></li>
                  <?php foreach($statuses as $status){ ?>
                    <li class="<?php if($status['filter_default'] == true && !$this->input->get('status') || $this->input->get('status') == $status['id']){echo 'active';} ?>">
                      <a href="#" data-cview="<?php echo 'project_status_'.$status['id']; ?>" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>'); return false;">
                        <?php echo $status['name']; ?>
                      </a>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-panel-heading" />
                <img id="call_gif" src="<?php echo base_url();?>assets/Ringing-phone.gif" width="100px" style="margin-left: 40%;display: none">
                      <div id="call_msg">



                      </div>
              </div>
               <div class="row mbot15">
                <div class="col-md-12">
                  <h4 class="no-margin"><?php echo _l('projects_summary'); ?></h4>
                  <?php
                  $_where = '';
                  if(!has_permission('projects','','view')){
                    $_where = 'id IN (SELECT project_id FROM tblprojectmembers WHERE staff_id='.get_staff_user_id().')';
                  }
                  ?>
                </div>
                <div class="_filters _hidden_inputs">
                  <?php
                  echo form_hidden('my_projects');
                  foreach($statuses as $status){
                   $value = $status['id'];
                     if($status['filter_default'] == false && !$this->input->get('status')){
                        $value = '';
                     } else if($this->input->get('status')) {
                        $value = ($this->input->get('status') == $status['id'] ? $status['id'] : "");
                     }
                     echo form_hidden('project_status_'.$status['id'],$value);
                    ?>
                   <div class="col-md-2 col-xs-6 border-right">
                    <?php $where = ($_where == '' ? '' : $_where.' AND ').'status = '.$status['id']; ?>
                    <a href="#" onclick="dt_custom_view('project_status_<?php echo $status['id']; ?>','.table-projects','project_status_<?php echo $status['id']; ?>',true); return false;">
                     <h3 class="bold"><?php echo total_rows('tblprojects',$where); ?></h3>
                     <span style="color:<?php echo $status['color']; ?>" project-status-<?php echo $status['id']; ?>">
                     <?php echo $status['name']; ?>
                     </span>
                   </a>
                 </div>
                 <?php } ?>
               </div>
             </div>
             <div class="clearfix"></div>
             
            
              <hr class="hr-panel-heading" />

             <?php echo form_hidden('custom_view'); ?>
             <?php $this->load->view('admin/projects/table_html'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4>Feed Back form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    
    </div>
    <div class="modal-body">
                     <section>
                     <?php echo form_open('',array('id'=>'feedback_form')); ?>
                    <input type="hidden" name="project_id" id="project_id">
        <div>
            <div>
        Project name: <span class="pname"></span>
        <br>
        Company name: <span class="cname"></span>

        </div>
            <div class="smileybox">
                <h5>1. How likely are you to rate the services given by Customer Support? </h5>
                <label class="check upiq" for="r1">
                <input class="oproce" name="stars1" value="1" id="r1" onchange="ratingStar(event)" type="checkbox">
                </label>

                    <label class="check upiq" for="r2">
                        <input name="stars2" id="r2" value="1" class="oproce" onchange="ratingStar(event)" type="checkbox">
                    </label>

                <label class="check upiq" for="r3"><input  class="oproce"
                    name="stars3" id="r3" value="1" onchange="ratingStar(event)" type="checkbox">
                </label> 

                <label class="check upiq" for="r4">
                    <input class="oproce" value="1" name="stars4" id="r4" onchange="ratingStar(event)" 
                    type="checkbox">
                </label> 

                <label class="check upiq" for="r5">
                    <input class="oproce" value="1" name="stars5" id="r5" onchange="ratingStar(event)" 
                    type="checkbox">
                </label>
            </div>
            
            <div class="smileybox">
                <h5>2. How likely are you to rate Customer Support for handling your Queries technically? </h5>
                <label class="check upiq1" for="r1">
                <input class="oproce1" value="1" name="stars6" id="r1" onchange="ratingStar1(event)"  type="checkbox">
                </label>

                    <label class="check upiq1" for="r2">
                        <input name="stars7" value="1" id="r2" class="oproce1" onchange="ratingStar1(event)" type="checkbox">
                    </label>

                <label class="check upiq1" for="r3"><input  class="oproce1"
                    name="stars8" id="r3" value="1" onchange="ratingStar1(event)" type="checkbox">
                </label> 

                <label class="check upiq1" for="r4">
                    <input class="oproce1" value="1" name="stars9" id="r4" onchange="ratingStar1(event)" 
                    type="checkbox">
                </label> 

                <label class="check upiq1" for="r5">
                    <input class="oproce1" value="1" name="stars10" id="r5" onchange="ratingStar1(event)" 
                    type="checkbox">
                </label>
            </div>
            
            <div class="smileybox">
                <h5>3. How likely are you to do the business with us again?</h5>
                <label class="check upiq2" for="r1">
                <input class="oproce2" value="1" name="stars11" id="r1" onchange="ratingStar2(event)" type="checkbox">
                </label>

                    <label class="check upiq2" for="r2">
                        <input name="stars12" id="r2" value="1" class="oproce2" onchange="ratingStar2(event)" type="checkbox">
                    </label>

                <label class="check upiq2" for="r3"><input  class="oproce2"
                    name="stars13" value="1" id="r3" onchange="ratingStar2(event)" type="checkbox">
                </label> 

                <label class="check upiq2" for="r4">
                    <input class="oproce2" value="1" name="stars14" id="r4" onchange="ratingStar2(event)" 
                    type="checkbox">
                </label> 

                <label class="check upiq2" for="r5">
                    <input class="oproce2" name="stars15" value="1" id="r5" onchange="ratingStar2(event)" 
                    type="checkbox">
                </label>
            </div>
            
             <div class="smileybox">
                <h5>4. How likely are you to recommend our service to a friend or colleague?</h5>
                <label class="check upiq3" for="r1"><input class="oproce3" value="1" name="stars16" id="r1" onchange="ratingStar3(event)" 
                    type="checkbox"><i class="em em-weary"></i>
                </label>

                    <label class="check upiq3" for="r2">
                        <input name="stars17" id="r2" class="oproce3" value="1" onchange="ratingStar3(event)" type="checkbox"><i class="em em-worried"></i>
                    </label>

                <label class="check upiq3" for="r3"><input  class="oproce3"
                    name="stars18" id="r3" onchange="ratingStar3(event)" value="1" type="checkbox"><i class="em em-blush"></i>
                </label> 

                <label class="check upiq3" for="r4">
                    <input class="oproce3" name="stars19" id="r4" value="1" onchange="ratingStar3(event)" 
                    type="checkbox"><i class="em em-smiley"></i>
                </label> 

                <label class="check upiq3" for="r5">
                    <input class="oproce3" name="stars20" value="1" id="r5" onchange="ratingStar3(event)" 
                    type="checkbox"><i class="em em-sunglasses"></i>
                </label>
            </div>
            
                <div class="smileybox">
                <h5>5. If you could change one thing about our service, what would it be?</h5>
                <textarea rows="5" cols="30" name="comment"  placeholder="What did you do today?"></textarea>

            </div>

        </div>
                         <button type="button" class="btn btn-primary" onclick="submitFeedback();" id="submit_feedback">Save Feedback</button>

<?php echo form_close();  ?>
    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
 </div>
        </div>
    </div>
<style>

 .smileybox input[type="checkbox"]{
    width:100%;
    height:100%;
    opacity:0;
    cursor:pointer; 
}

.smileybox label{
    position:relative;
    width: 50px;
    height: 50px;
    display: inline-block;  
    
}

.smileybox .check::before, .rated::after{
    content:'\2605';
    font-size:20px;
    position:absolute;
    color:#777;
    left:0;
    bottom:0;
    line-height: 50px;  
} 
.rated::after{
    color:orange;
}
.check:hover::before{
    color:orange;
}

 .smileybox label i{
    position:absolute;

    font-size:20px;
} 

.smileybox label i.em{
    display:none;   
}

.smileyboxtwo{
   margin-bottom: 100px;
    display: block; 
}


.smileybox {
    margin-bottom: 10px;
    display: block;
}


.rated::after{
    color:orange;
}

</style> 



<?php $number=$number['phonenumber'];
if(!empty($number)){
if(strlen($number) < 13){
  $number1='+91'.$number;
}
else{
   $number1=$number;
}
}
else{
   $number1='';
}
?>

<?php $this->load->view('admin/projects/copy_settings'); ?>
<?php init_tail(); ?>

<script type="text/javascript">
$(document).ready(function(){

	$('.launch-modal').click(function(){
		$('#myModal').modal({
			backdrop: 'static'
		});
	}); 
});
</script>
<script>

function call(mobile,projectid,clientid,staffid) {
var agent_number ='<?php echo $number1; ?>';
if(agent_number !=''){
       $('#call_msg').empty();
       $('#call_gif').css('display','inline');
       $.ajax({
           type: "post",
           url: "<?php echo base_url();?>admin/projects/call",
           data: {"mobile":mobile,"agent_number":agent_number,"project_id":projectid,"client_id":clientid,"staff_id":staffid},
           dataType:"json",
           success: function (response) {
           $('#project_id').val(projectid);
           $('#call_gif').css('display','none');
               console.log(response);
               if (!response.error) {
                   if (response.success.status == 'success') {
                     alert(response.success.message);
                     var call_id=response.success.call_id;
                     $.ajax({
                     type: "post",
                     url: "<?php echo base_url();?>admin/projects/save_call_id",
                     data: {"project_id":projectid,"client_id":clientid,"staff_id":staffid,"call_id":call_id},
                     dataType:"json",
                     success: function (response) {
                     console.log(response);
                     if (response.status == 'success') {
                     console.log()
                     if(response.check_feedback == '' || response.check_feedback == null){
                       $('.cname').html(response.client_name);
                      $('.pname').html(response.project_name);
                     $('#feedback').trigger('click');
                     }
                     } else {
                       console.log(response.message);
          
                    }
               
                    }

                    });
                   } else {
                     alert(response.success.message);
                     $('#feedback').trigger('click');
                   }
               }
               else {
                 alert(response.error.message);
                     $('#feedback').trigger('click');
               }
           }

       });
      }
      else{
         alert('Agent number is Empty');
      }
   }



$(function(){
     var ProjectsServerParams = {};

     $.each($('._hidden_inputs._filters input'),function(){
         ProjectsServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
     });

     initDataTable('.table-projects', admin_url+'projects/table', undefined, undefined, ProjectsServerParams, <?php echo do_action('projects_table_default_order',json_encode(array(5,'asc'))); ?>);

     init_ajax_search('customer', '#clientid_copy_project.ajax-search');
});



function submitFeedback(){

  $.ajax({
                     type: "post",
                     url: "<?php echo base_url();?>admin/projects/save_feedback",
                     data: $("#feedback_form").serialize(),
                     dataType:"json",
                     success: function (response) {
                     console.log(response);
                     if (response.status == 'success') {
                      location.reload();
                     } 
            
                    }

                    });

}

</script>


<script type="text/javascript">
            function ratingStar(event){
                var checkValue = document.querySelectorAll(".oproce");
                var checkStar = document.querySelectorAll(".upiq");
                var checkSmiley = document.querySelectorAll("i");
                var checkCount = 0;
                for(var i=0; i<checkValue.length; i++){
                    if(checkValue[i]==event.target){
                        checkCount = i+1;
                    }
                }
                for(var j=0; j<checkCount; j++){
                    checkValue[j].checked = true;
                    checkStar[j].className = "rated";
                    checkSmiley[j].style.display = "none";
                }
                
                for(var k=checkCount; k<checkValue.length; k++){
                    checkValue[k].checked = false;
                    checkStar[k].className = "check"
                    checkSmiley[k].style.display = "none";  
                }
                if(checkCount == 1){
                    document.querySelectorAll("i")[0].style.display = "block";
                }
                if(checkCount == 2){
                    document.querySelectorAll("i")[1].style.display = "block";
                }
                if(checkCount == 3){
                    document.querySelectorAll("i")[2].style.display = "block";
                }
                if(checkCount == 4){
                    document.querySelectorAll("i")[3].style.display = "block";
                }
                if(checkCount == 5){
                    document.querySelectorAll("i")[4].style.display = "block";
                }
            }
    </script>	
    
    <script type="text/javascript">
            function ratingStar1(event){
                var checkValue = document.querySelectorAll(".oproce1");
                var checkStar = document.querySelectorAll(".upiq1");
                var checkSmiley = document.querySelectorAll("i");
                var checkCount = 0;
                for(var i=0; i<checkValue.length; i++){
                    if(checkValue[i]==event.target){
                        checkCount = i+1;
                    }
                }
                for(var j=0; j<checkCount; j++){
                    checkValue[j].checked = true;
                    checkStar[j].className = "rated";
                    checkSmiley[j].style.display = "none";
                }
                
                for(var k=checkCount; k<checkValue.length; k++){
                    checkValue[k].checked = false;
                    checkStar[k].className = "check"
                    checkSmiley[k].style.display = "none";  
                }
                if(checkCount == 1){
                    document.querySelectorAll("i")[5].style.display = "block";
                }
                if(checkCount == 2){
                    document.querySelectorAll("i")[6].style.display = "block";
                }
                if(checkCount == 3){
                    document.querySelectorAll("i")[7].style.display = "block";
                }
                if(checkCount == 4){
                    document.querySelectorAll("i")[8].style.display = "block";
                }
                if(checkCount == 5){
                    document.querySelectorAll("i")[9].style.display = "block";
                }
            }
    </script>	
    
    <script type="text/javascript">
            function ratingStar2(event){
                var checkValue = document.querySelectorAll(".oproce2");
                var checkStar = document.querySelectorAll(".upiq2");
                var checkSmiley = document.querySelectorAll("i");
                var checkCount = 0;
                for(var i=0; i<checkValue.length; i++){
                    if(checkValue[i]==event.target){
                        checkCount = i+1;
                    }
                }
                for(var j=0; j<checkCount; j++){
                    checkValue[j].checked = true;
                    checkStar[j].className = "rated";
                    checkSmiley[j].style.display = "none";
                }
                
                for(var k=checkCount; k<checkValue.length; k++){
                    checkValue[k].checked = false;
                    checkStar[k].className = "check"
                    checkSmiley[k].style.display = "none";  
                }
                if(checkCount == 1){
                    document.querySelectorAll("i")[10].style.display = "block";
                }
                if(checkCount == 2){
                    document.querySelectorAll("i")[11].style.display = "block";
                }
                if(checkCount == 3){
                    document.querySelectorAll("i")[12].style.display = "block";
                }
                if(checkCount == 4){
                    document.querySelectorAll("i")[13].style.display = "block";
                }
                if(checkCount == 5){
                    document.querySelectorAll("i")[14].style.display = "block";
                }
            }
    </script>	
    
    <script type="text/javascript">
            function ratingStar3(event){
                var checkValue = document.querySelectorAll(".oproce3");
                var checkStar = document.querySelectorAll(".upiq3");
                var checkSmiley = document.querySelectorAll("i");
                var checkCount = 0;
                for(var i=0; i<checkValue.length; i++){
                    if(checkValue[i]==event.target){
                        checkCount = i+1;
                    }
                }
                for(var j=0; j<checkCount; j++){
                    checkValue[j].checked = true;
                    checkStar[j].className = "rated";
                    checkSmiley[j].style.display = "none";
                }
                
                for(var k=checkCount; k<checkValue.length; k++){
                    checkValue[k].checked = false;
                    checkStar[k].className = "check"
                    checkSmiley[k].style.display = "none";  
                }
                if(checkCount == 1){
                    document.querySelectorAll("i")[15].style.display = "block";
                }
                if(checkCount == 2){
                    document.querySelectorAll("i")[16].style.display = "block";
                }
                if(checkCount == 3){
                    document.querySelectorAll("i")[17].style.display = "block";
                }
                if(checkCount == 4){
                    document.querySelectorAll("i")[18].style.display = "block";
                }
                if(checkCount == 5){
                    document.querySelectorAll("i")[19].style.display = "block";
                }
            }
    </script>	

    
    
  
  




</body>
</html>
