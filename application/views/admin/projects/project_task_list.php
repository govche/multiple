<?php
$hasPermissionEdit   = has_permission('tasks', '', 'edit');
$hasPermissionDelete = has_permission('tasks', '', 'delete');

$this->db->select('*,'.get_sql_select_task_asignees_full_names(). ' as assignees, '.get_sql_select_task_created_from_full_name().' as created_by,(SELECT staffid FROM tblstafftaskassignees WHERE taskid=tblstafftasks.id AND staffid=' . get_staff_user_id() . ') as is_assigned,(SELECT GROUP_CONCAT(staffid SEPARATOR ",") FROM tblstafftaskassignees WHERE taskid=tblstafftasks.id ORDER BY tblstafftaskassignees.staffid) as assignees_ids,(SELECT MAX(id) FROM tbltaskstimers WHERE task_id=tblstafftasks.id and staff_id=' . get_staff_user_id() . ' and end_time IS NULL) as not_finished_timer_by_current_staff,(SELECT staffid FROM tblstafftaskassignees WHERE taskid=tblstafftasks.id AND staffid=' . get_staff_user_id() . ') as current_user_is_assigned,(SELECT CASE WHEN addedfrom=' . get_staff_user_id() . ' AND is_added_from_contact=0 THEN 1 ELSE 0 END) as current_user_is_creator');
$this->db->from('tblstafftasks');
$this->db->where('rel_id',$project->id);
$this->db->order_by('dateadded', 'desc');
$result = $this->db->get()->result_array();
//echo $this->db->last_query();
foreach($result as $rows)
{
	$id=$rows['id'];
	$rel_id=$rows['rel_id'];
	$status_id=$rows['status'];
	$status=get_task_status_by_id($status_id);
	$this->db->select('*');
	$this->db->from('tblprojects');
	$this->db->where('id',$rel_id);
	$rel_result = $this->db->get()->result_array();
	//$this->db->last_query();
	$client_id=$rel_result[0]['clientid'];
	$this->db->select('*');
	$this->db->from('tblclients');
	$this->db->where('userid',$client_id);
	$client_result = $this->db->get()->result_array();
	$start_date=$rows['startdate'];
	$due_date=$rows['duedate'];
	$date_added=@date("d-M-Y h:i A",strtotime($rows['dateadded']));
	$priority=task_priority($rows['priority']);
	$this->db->select('content');
	$this->db->where('project_id',$rel_id);
	$notes_result = $this->db->get('tblprojectnotes')->row();
	if($notes_result)
		$notes=$notes_result->content;
	else
		$notes="";
	
	
	$this->db->select('*');
	$this->db->order_by('id', 'asc');
	$this->db->from('tbltaskstimers');
	$this->db->where('task_id',$id);
	$timesheet_result = $this->db->get()->result_array();
	$i=0;$edited_datetime="";
	foreach($timesheet_result as $timesheet_rows)
	{
		$i++;
		if($i==1)
		{
			
		}
		$edited_datetime=$timesheet_rows['end_time'];
	}
	$assigned_staff=$rows['assignees'];
	$assigned_array=explode(",",$assigned_staff);
	$assigned_count=count($assigned_array);
	$assigned_by=$rows['created_by'];
	$assigned_to=$assigned_array[1];
	$count=(string) ($assigned_count-1);
	if($assigned_count>1)
		$assigned_to_name=$assigned_to." + ".$count;
	else
		$assigned_to_name=$assigned_to;
	
	$canChangeStatus = ($rows['current_user_is_creator'] != '0' || $rows['current_user_is_assigned'] || has_permission('tasks', '', 'edit'));
    $status          = get_task_status_by_id($rows['status']);
    $outputStatus    = '';

    $outputStatus .= '<span class="inline-block label" style="color:' . $status['color'] . ';border:1px solid ' . $status['color'] . '" task-status-table="' . $rows['status'] . '">';

    $outputStatus .= $status['name'];
	if ($canChangeStatus) {

        $outputStatus .= '<div class="dropdown inline-block mleft5 status-table-mark">';
        $outputStatus .= '<a href="#" style="font-size:14px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableTaskStatus-' . $rows['id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        $outputStatus .= '<span data-toggle="tooltip" title="' . _l('ticket_single_change_status') . '"><i class="fa fa-caret-down" aria-hidden="true"></i></span>';
        $outputStatus .= '</a>';

        $outputStatus .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableTaskStatus-' . $rows['id'] . '">';
        foreach ($task_statuses as $taskChangeStatus) {
            if ($rows['status'] != $taskChangeStatus['id']) {
                $outputStatus .= '<li>
                  <a href="#" onclick="task_mark_as(' . $taskChangeStatus['id'] . ',' . $rows['id'] . '); return false;">
                     ' . _l('task_mark_as', $taskChangeStatus['name']) . '
                  </a>
               </li>';
            }
        }
        $outputStatus .= '</ul>';
        $outputStatus .= '</div>';
    }

    $outputStatus .= '</span>';
	
	if($rows['billed']==1)
		$bill_status="Billed";
	else if($rows['billable']==1)
		$bill_status="Billable";
	else
		$bill_status="Non-Billable";
	if($rows['billed']==1)
		$inv_no=$rows['invoice_rate'];
	else
		$inv_no="";
	?>
    <div id="div-start" style="pointer-events: all !important;" class="task-list " onclick="RestrictClosedTask(this);">
   <div class="task-list">
      <div id="managetask<?php echo $id;?>" data-parent="<?php echo $id;?>">
         <div id="div<?php echo $id;?>" class="todo-tasklist-main-item todo-tasklist-item-border-red">
            <div id="divedittask<?php echo $id;?>" class="task-item-details col-md-12" style="display:none;"></div>
            <div id="divtask<?php echo $id;?>" class="todo-tasklist-item">
               <div class="todo-tasklist-item-title">
                  <div class="row mc_pd">
                     <div class="col-md-6 change-css"  onclick="init_task_modal(<?php echo $id;?>); return false;">
                        <!--<div><input type="checkbox" id="chkbTask<?php echo $id;?>" class="chkbTask" value="<?php echo $id;?>" onclick="showMultipleDeleteButton();">&nbsp;&nbsp;<a id="lnkNameEdit<?php echo $id;?>" "="" onclick="GetTaskDetails(<?php echo $id;?>,<?php echo $id;?>,'<?php echo $date_added;?>','<?php echo $date_added;?>','0','0')"><span class="id-number"><?php echo $id;?></span></a></div>-->
                        <div class="todo-tasklist-item-text">Client: <a target="_blank" href="<?php echo task_rel_link($client_id, 'customer');?>"><?php echo $client_result[0]['company'];?></a></div>
                        <div class="todo-tasklist-item-text2">Latest Note: <?php echo $notes;?></div>
                        <div class="todo-tasklist-item-text2">Total Estimated Efforts: 00:00 Total Actual Efforts: 00:00</div>
                     </div>
                     <div class="col-md-1" style="padding: 0px;font-size: 12px;"></div>
                     <div class="col-md-3 change-css">
                        <span class="todo-tasklist-date todo-tasklist-item-text" id="taskdates" style="font-size: 81% !important;"><?php echo $date_added;?> <span style="font-weight:400">to</span> <?php echo $date_added;?> </span>
                        <div class="todo-tasklist-controls todo-tasklist-item-text">
                           <div class="row">
                              <div class="col-md-3"></div>
                           </div>
							<br>
                        </div>
                     </div>
                     <div class="col-md-2 change-css mc_pd">
                        <div class="todo-tasklist-item-text">
                           <div class="col-md-12 "><a class="bill "><?php echo $bill_status;?> &nbsp;</a>
						   <?php
						   if($inv_no!="") { ?>
                           <span title="INV -<?php echo $inv_no;?>" style="width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><?php echo $inv_no;?></span><?php } ?>
                           </div>
                           
                           <div class="col-md-12 ">Priority: <span class="label label-sm label-warning"><?php echo $priority;?></span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="row mc_pd">
                     <div class="col-md-7 left">
                        <div>
                        <?php echo $outputStatus;?>
                           &nbsp;
                           <div class="todo-tasklist-item-text2" style="display: inline-block;">Assigned by <a><?php echo $assigned_by;?></a> to <a class="tooltips" data-original-title="<?php echo $assigned_to;?>"><?php echo $assigned_to_name;?></a></div>
                           <div class="todo-tasklist-item-text2">Created By <?php echo $assigned_by;?> on <?php echo $date_added;?>, Last Modified By <?php echo $assigned_by;?> on <?php echo $date_added;?>.</div>
                        </div>
                     </div>
                     <div class="col-md-5 right change-css"><!--<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_notes-->
					 <a class="btn btn-sm fa  fa-file tooltips" onclick="init_task_modal(<?php echo $id;?>); slideToggle('.tasks-comments'); return false;" style="margin-left: 5px;" data-original-title="Notes" href="javascript:;" target="_blank"></a>
					 <a class="btn btn-sm fa fa-users tooltips" style="margin-left: 5px;" data-original-title="Efforts" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_timesheets" target="_blank"></a>
					 <a class="btn btn-sm fa fa-rupee tooltips" style="margin-left: 5px;" data-original-title="Expense" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_expenses" target="_blank"></a>
                     <?php
					 if($hasPermissionEdit)
					 {
						 ?>
					 	<a class="btn btn-sm fa fa-pencil tooltips" style="margin-left: 5px;" data-original-title="Edit Task" onclick="edit_task(<?php echo $id;?>); return false"></a>
                        <?php
					 }
					 else
					 {
						 ?>
					 	<a class="btn btn-sm fa fa-pencil tooltips" style="margin-left: 5px;" data-original-title="Edit Task"></a>
                     <?php } ?>
					 <a class="btn btn-sm fa fa-bell-o  tooltips" style="margin-left: 5px;" data-original-title="Add Reminder" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_timesheets" target="_blank"></a>
					 <a class="btn btn-sm fa  fa-copy tooltips" style="margin-left: 5px;" data-original-title="View Logs" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_activity" target="_blank"></a>
					 <a class="btn btn-sm fa fa-paperclip tooltips" style="margin-left: 5px;" data-original-title="Attach Documents" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_files" target="_blank"></a>
					 <a class="btn btn-sm fa fa-envelope tooltips mail-task" style="margin-left: 5px;" data-original-title="Send Task Details via Mail/SMS" data-id="<?php echo $rel_id;?>" href="<?php echo APP_BASE_URL;?>/admin/projects/view/<?php echo $rel_id;?>?group=project_timesheets" target="_blank"></a>
                     <?php
					 if($hasPermissionEdit)
					 {
						 ?>
                     <a class="btn btn-sm fa fa-trash mc-lnk-danger tooltips _delete task-delete" style="margin-left: 5px;" data-original-title="Delete Task" href="<?php echo APP_BASE_URL;?>/admin/tasks/delete_task/<?php echo $id;?>"></a>
                        <?php
					 }
					 else
					 {
						 ?>
                     <a class="btn btn-sm fa fa-trash mc-lnk-danger tooltips" style="margin-left: 5px;" data-original-title="Delete Task"></a>
                     <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
            
            <div style="clear: both;"></div>
         </div>
      </div>
      </div>
      </div>
	<?php } ?>
