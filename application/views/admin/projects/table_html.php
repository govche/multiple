<style>
.text-danger {
    color: #fc2d42;
    display: none;
}
</style>
<?php

$table_data = [
   '#',
   _l('project_name'),
    [
         'name'     => _l('project_customer'),
         'th_attrs' => ['class' => isset($client) ? 'not_visible' : ''],
    ],
   //_l('tags'),
   _l('project_start_date'),
   _l('project_deadline'),
   _l('Completed Date'),
  _l('project_members'),
  _l('Project Current Status'),
   _l('project_status'),
    _l('Notes'),
];

$custom_fields = get_custom_fields('projects', ['show_on_table' => 1]);
foreach ($custom_fields as $field) {
    array_push($table_data, $field['name']);
}

if($this->session->userdata('staff_agent') == true){
  if(has_permission('project_call','','view')){
  array_push($table_data, 'Call');
  }
  array_push($table_data, 'Feedback');
}

$table_data = do_action('projects_table_columns', $table_data);

render_datatable($table_data, isset($class) ?  $class : 'projects', [], [
  'data-last-order-identifier' => 'projects',
  'data-default-order'  => get_table_last_order('projects'),
]);
