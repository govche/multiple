<style>

.lead1{
	width:200px;
}
.modal-sm1 {
		position:absolute;
  top:50% !important;
  transform: translate(0, -50%) !important;
  -ms-transform: translate(0, -50%) !important;
  -webkit-transform: translate(0, -50%) !important;
  margin:auto 35%;
  width:30%;
  
		}
	
.panel_s .panel-body {
    background: #fff;
    border: 1px solid #dce1ef;
    border-radius: 4px;
    padding: 20px;
    position: relative;
    width: 1000px;
}		
#txt {
    text-transform:lowercase;
}		
.gr{
width: 515px;
    margin-left: 178px;
    margin-right: -91px;
}
ul li.selected {
    background-color:red;
}
</style>


<h4 class="customer-profile-group-heading"><?php echo _l('client_add_edit_profile'); ?></h4>
<div class="row">
   <?php echo form_open($this->uri->uri_string(),array('class'=>'client-form','autocomplete'=>'off')); ?>
   <div class="additional"></div>
   <div class="col-md-12">
      <div class="horizontal-scrollable-tabs">
<div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
<div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
<div class="horizontal-tabs">
      <ul class="nav nav-tabs profile-tabs row customer-profile-tabs nav-tabs-horizontal" role="tablist">
         <li role="presentation" class="<?php if(!$this->input->get('tab')){echo 'active';}; ?>">
            <a href="#contact_info" aria-controls="contact_info" role="tab" data-toggle="tab">
            <?php echo _l( 'customer_profile_details'); ?>
            </a>
         </li>
         <?php
            $customer_custom_fields = false;
            if(total_rows('tblcustomfields',array('fieldto'=>'customers','active'=>1)) > 0 ){
                 $customer_custom_fields = true;
             ?>
         <!--<li role="presentation" class="<?php if($this->input->get('tab') == 'custom_fields'){echo 'active';}; ?>">
            <a href="#custom_fields" aria-controls="custom_fields" role="tab" data-toggle="tab">
            <?php //echo do_action('customer_profile_tab_custom_fields_text',_l( 'custom_fields')); ?>
            </a>
         </li>-->
         <?php } ?>
         <!--<li role="presentation">
            <a href="#billing_and_shipping" aria-controls="billing_and_shipping" role="tab" data-toggle="tab">
            <?php echo _l( 'billing_shipping'); ?>
            </a>
         </li>-->
         <?php do_action('after_customer_billing_and_shipping_tab',isset($client) ? $client : false); ?>
         <?php if(isset($client)){ ?>
         <!--<li role="presentation">
            <a href="#customer_admins" aria-controls="customer_admins" role="tab" data-toggle="tab">
            <?php echo _l( 'customer_admins' ); ?>
            </a>
         </li>-->
         <?php do_action('after_customer_admins_tab',$client); ?>
         <?php } ?>
      </ul>
   </div>
</div>
      <div class="tab-content">
         <?php do_action('after_custom_profile_tab_content',isset($client) ? $client : false); ?>
         <?php if($customer_custom_fields) { ?>
         <div role="tabpanel" class="tab-pane <?php if($this->input->get('tab') == 'custom_fields'){echo ' active';}; ?>" id="custom_fields">
            <?php $rel_id=( isset($client) ? $client->userid : false); ?>
            <?php //echo render_custom_fields( 'customers',$rel_id); ?>
         </div>
         <?php } ?>
         <div role="tabpanel" class="tab-pane<?php if(!$this->input->get('tab')){echo ' active';}; ?>" id="contact_info">
            <div class="row">
               <div class="col-md-12<?php if(isset($client) && (!is_empty_customer_company($client->userid) && total_rows('tblcontacts',array('userid'=>$client->userid,'is_primary'=>1)) > 0)) { echo ''; } else {echo ' hide';} ?>" id="client-show-primary-contact-wrapper">
                  <div class="checkbox checkbox-info mbot20 no-mtop">
                     <input type="checkbox" name="show_primary_contact"<?php if(isset($client) && $client->show_primary_contact == 1){echo ' checked';}?> value="1" id="show_primary_contact">
                     <label for="show_primary_contact"><?php echo _l('show_primary_contact',_l('invoices').', '._l('estimates').', '._l('payments').', '._l('credit_notes')); ?></label>
                  </div>
               </div>
			   
			   
			 
               <div class="col-md-6">
			  
                  <?php $value=( isset($client) ? $client->company : ''); ?>
                  <?php $attrs = (isset($client) ? array() : array('autofocus'=>true)); ?>
                  <?php echo render_input1( 'company', 'Company/Trade Name',$value,'text',$attrs,array('onkeypress'=>"check_if_exists();"),array('onchange'=>"get_cmp_details();")) ?><br>
<!--                   <label class="col-md-4" for="company"></label>-->
<!--                   <div class="col-md-8"><input type="text" id="company" name="company" class="form-control1" autofocus="1" value="" aria-invalid="false"></div>-->
                   <div class="col-md-4"></div>
                   <div class="col-md-8">
					 <div id="result">

                     </div>
                   </div>
<!--               </div>-->
					 <?php
               $company_type= get_all_company();
               $customer_default_company = get_option('customer_default_company');
               $selected =( isset($client) ? $client->company_type : $customer_default_country  );
              
               if(empty($selected)) { $selected =array("name" => "AOP Trust" 
			   			   
			   );}

               echo render_select1( 'company_type',$company_type,array( 'id',array( 'company_name')), 'Company Type',$selected ,array('data-custom-field-required' => 1 ),array('required'=>true));
              


			  ?>
			
			   <br>

				<?php
               $industry_type= get_all_industry();
               $customer_default_industry = get_option('customer_default_company');
               $selected =( isset($client) ? $client->industry_type : $customer_default_country  );
              
               if(empty($selected)) { $selected =array("name" => "AOP Trust" 
			   			   
			   );}

               echo render_select1('industry_type',$industry_type,array( 'id',array( 'industry_type')), 'Industry Type',$selected,array('data-custom-field-required' => 1 ),array('required'=>true));
              


			  ?>
			   <br>



                  <?php if(get_option('company_requires_vat_number_field') == 1){
                     $value=( isset($client) ? $client->vat : '');
					 
                     echo render_input1( 'vat', 'client_vat_number',$value,array('data-custom-field-required' => 1),array('maxlength'=>15,'minlength'=>15));
                     } ?><br>
					
					 <!-- <?php $value=( isset($client) ? $client->PAN : ''); ?>
                  <?php echo render_input1( 'PAN', 'PAN Number',$value); ?><br>-->
                  <?php $value=( isset($client) ? $client->PAN : ''); ?>
				 <?php echo render_input1( 'PAN', 'PAN Number',$value,array('data-custom-field-required' => 1),array('maxlength'=>10,'minlength'=>10)); ?><br>
					 
					 
					 <?php $value=( isset($client) ? $client->dob_type : ''); ?>
                  <?php echo render_date_input1( 'DOB', 'Date Of Birth/Date Of Reg',$value); ?><br>
                  
                   <!--<?php $value = (isset($client) ? _d($estimate->date) : _d(date('Y-m-d'))); ?>
                  <?php echo render_date_input1('DOB','Date Of Birth/Date Of Reg',$value); ?><br>-->
				  
				 
				  
                  
                  
                  
                  
      
				 <!-- else { ?>
                  <div class="form-group">
                     <label for="website"><?php echo _l('client_website'); ?></label>
                     <div class="input-group">
                        <input type="text" name="website" id="website" value="<?php echo $client->website; ?>" class="form-control"><br>
                        <div class="input-group-addon">
                           <span><a href="<?php echo maybe_add_http($client->website); ?>" target="_blank" tabindex="-1"><i class="fa fa-globe"></i></a></span><br>
                        </div>
                     </div>
                  </div>}-->
				  
				  
				 
				  
				 <?php 
				 
                     $selected = array();
                     if(isset($customer_groups)){
                       foreach($customer_groups as $group){
                          array_push($selected,$group['groupid']);
                       }
                     }
                     if(is_admin() || get_option('staff_members_create_inline_customer_groups') == '1'){
                      /*echo render_select_with_input_group1('groups_in[]',$groups,array('id','name'),'customer_groups',$selected,'<a href="#" data-toggle="modal" data-target="#customer_group_modal"><i class="fa fa-plus"></i></a>',array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);*/
					  echo render_select1('groups_in[]',$groups,array('id','name'),'customer_groups',$selected,array('multiple'=>true,'data-actions-box'=>true),array('onchange'=>"gethide(this);"),'','',false,4);
                      } else {
                        echo render_select1('groups_in[]',$groups,array('id','name'),'customer_groups',$selected,array('multiple'=>true,'data-actions-box'=>true),array('onchange'=>"gethide(this);"),'','',false,4);
                      }
                     ?><br>
					
					 <!--<?php $value=( isset($client) ? $client->groups_in : ''); ?>
					  <div class="form-group row" >
                        <label for="groups" class="col-md-4"><?php echo "Groups"?></label>
                        <div class="gr">
						<select class="selectpicker" name="groups_in" id="groups" data-width="60%"  data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" onkeyup="gethide()">
                            <option value="<?php echo $value;?>"></option>
                            <?php foreach($groups as $mode){ ?>
                           
                            <option value="<?php echo $mode['id']; ?>"><?php echo $mode['name']; ?></option>
                            
                            <?php } ?>
                        </select>
						</div>
                    </div><br>-->
					 
					 <div  id="note_date"  style="display:none" >
					 <?php $value=( isset($client) ? $client->g_contactno : ''); ?>
                  <?php echo render_input1( 'g_contactno', 'Groups Contact No',$value,array('data-custom-field-required' => 1 ),array('minlength'=>10)); ?><br>
					


                     <?php $value=( isset($client) ? $client->g_email : ''); ?>
                   <?php echo render_input1( 'g_email', 'Groups Email',$value,array('data-custom-field-required' => 1 )) ?><br>
					</div>
					
					 
					 
					 <div class="hide">
                  <?php if(!isset($client)){ ?>
                  <!--<i class="fa fa-question-circle pull-left" data-toggle="tooltip" data-title="<?php echo _l('customer_currency_change_notice'); ?>"></i>-->
                  <?php }
                     $s_attrs = array('data-none-selected-text'=>_l('system_default_string'));
                     $selected = '';
                     if(isset($client) && client_have_transactions($client->userid)){
                        $s_attrs['disabled'] = true;
                     }
                     foreach($currencies as $currency){
                        if(isset($client)){
                          if($currency['id'] == $client->default_currency){
                            $selected = $currency['id'];
                         }
                      }
                     }
                            // Do not remove the currency field from the customer profile!
                     echo render_select1('default_currency',$currencies,array('id','name','symbol'),'invoice_add_edit_currency',$selected,$s_attrs,array(),'','',false,4); ?><br>
                 </div>
<!-- --><?php //$value=( isset($client) ? $client->zip : '');?>
 <!--by rathina for change zip code to pin code-->
<!--                  --><?php //echo render_input1( 'zip', 'client_postal_code',$value,array('data-custom-field-required' => 1 ),array('required'=>true)); ?><!--<br>-->
<!--                   <label class="col-md-4" for="zip">Pin Code</label>-->
<!--                   <input type="number" name="zip" id="zip" onkeyup="getLocation()" required value=--><?php //echo "$value";?><!-->


				 <?php if(get_option('disable_language') == 0){ ?>
                  <!--<div class="form-group row select-placeholder">
                     <label for="default_language" class="col-md-4 control-label"><?php echo _l('localization_default_language'); ?>
                     </label>
                     <div class="col-md-8">
                     <select name="default_language" id="default_language" class="form-control selectpicker" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                        <option value=""><?php echo _l('system_default_string'); ?></option>
                        <?php foreach(list_folders(APPPATH .'language') as $language){
                           $selected = '';
                           if(isset($client)){
                              if($client->default_language == $language){
                                 $selected = 'selected';
                              }
                           }
                           ?>
                        <option value="<?php echo $language; ?>" <?php echo $selected; ?>><?php echo ucfirst($language); ?></option>
                        <?php } ?>
                     </select>
                     </div>
                  </div>-->
                  <?php } ?>
                  <?php //echo render_custom_fields1( 'customers',$rel_id,'slug="customers_type_of_company"','add_edit_preview'); ?>
               </div>
			   
			   
			   
			   
               <div class="col-md-6">
			   
			  <?php $value=( isset($client) ? $client->phonenumber : ''); ?>
                  <?php echo render_input1( 'phonenumber', 'client_phonenumber',$value,array('data-custom-field-required' => 1 ),array('required'=>true)); ?><br>



                     <?php $value=( isset($client) ? $client->address : ''); ?>
                   <?php echo render_input1( 'address', 'Address',$value,array('data-custom-field-required' => 1 ),array('required'=>true, 'minlength'=>30, 'onkeyup'=>"return forceLower(this);")); ?><br>




                 <!-- <?php $value=( isset($client) ? $client->address : ''); ?>
                   <div class="form-group row" app-field-wrapper="city">
                       <label class="col-md-4" for="address">Address</label>
                       <div class="col-md-8"><textarea type="text" id="txt" name="address" class="form-control1" onkeyup="return forceLower(this);" value="<?php echo $value;?>" required></textarea>
                       </div>
                   </div><br>-->
				 
			   
                  
				   <?php $value=( isset($client) ? $client->email_id : ''); ?>
                  <?php echo render_input1( 'email_id', 'Email_Id',$value,array('data-custom-field-required' => 1 ),array('required'=>true)); ?><br>

                   <?php $value=( isset($client) ? $client->zip : ''); ?>
                   <div class="form-group row" app-field-wrapper="zip">
                       <label class="col-md-4" for="zip">Pin Code</label>
                       <div class="col-md-8"><input type="text" id="zip1" name="zip" class="form-control1" value="<?php echo $value;?>" onkeyup="getLocation()">
                       </div>
                   </div><br>

                   <?php $value=( isset($client) ? $client->city : ''); ?>
                   <div class="form-group row" app-field-wrapper="city">
                       <label class="col-md-4" for="city">City</label>
                       <div class="col-md-8"><input type="text" id="city" name="city" class="form-control1" value="<?php echo $value;?>">
                       </div>
                   </div><br>
<!--                 -->
                   <?php $value=( isset($client) ? $client->state : ''); ?>
                   <div class="form-group row" app-field-wrapper="state">
                       <label class="col-md-4" for="state">State</label>
                       <div class="col-md-8"><input type="text" id="state" name="state" class="form-control1" value="<?php echo $value;?>">
                       </div>
                   </div><br>

                   <?php $value=( isset($client) ? $client->country : ''); ?>
                   <div class="form-group row" app-field-wrapper="country">
                       <label class="col-md-4" for="country">Country</label>
                       <div class="col-md-8"><input type="text" id="country" name="country" class="form-control1" value="<?php echo $value;?>">
                       </div>
                   </div><br>
				   
<?php if((isset($client) && empty($client->website)) || !isset($client)){
                     $value=( isset($client) ? $client->website : '');
                     echo render_input1( 'website', 'client_website',$value);
                     } ?>
<!--                  --><?php //$countries= get_all_countries();
//                     $customer_default_country = get_option('customer_default_country');
//                     $selected =( isset($client) ? $client->country : $customer_default_country);
//                     echo render_select1( 'country',$countries,array( 'country_id',array( 'short_name')), 'clients_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex')),array(),'','',false,4);
//                     ?><!--<br>-->
<!--					 -->

<!--					 -->
<!--					 -->
<!--                      --><?php
//               $state= get_all_states();
//               $customer_default_state = get_option('customer_default_state');
//               $selected =( isset($client) ? $client->state : $customer_default_state );
//
//               if(empty($selected)) { $selected =array("name" => "Tamil Nadu" );}
//
//               echo render_select1( 'state',$state,array( 'name',array( 'name')), 'State',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex')));
//
//
//
//			  ?><!--<br>-->
<!--			   --><?php
//               $cities= get_all_cities();
//               $customer_default_cities = get_option('customer_default_cities');
//               $selected =( isset($client) ? $client->city : $customer_default_cities);
//               echo render_select1('city',$cities,array( 'name',array( 'name')), 'City',$selected,array('data-custom-field-required' => 1 ),array('required'=>true));
//               ?><!--<br>-->
<!--                     --><?php ////echo render_custom_fields1( 'customers',$rel_id,'slug="customers_industry_type"'); ?>
<!--                     --><?php ////echo render_custom_fields1( 'customers',$rel_id,'slug="customers_file_number"'); ?>
<!--					  -->
               </div>
		 
				  <div class="col-md-6">
			  
			   
			    
			   </div>
				   <div class="col-md-6">
			   
				  
				  
				   </div>
				   <div style="display:none" class="col-md-6">
				  
				 
				  
				  <?php $value=( isset($client) ? $client->group_name : ''); ?>
                  <?php echo render_input1( 'group_name', 'Group Name',$value); ?><br>
				  
				  
				   </div>
				   <div style="display:none" class="col-md-6">
				  
				   
			    <?php $value=( isset($client) ? $client->refer_by : ''); ?>
                  <?php echo render_input1( 'refer_by', 'Referred By',$value); ?><br>
				  
			   
			   </div><br />
            </div>
               <div  class="row">
               		<div class="col-md-12">
					<?php $text=render_custom_fields('customers',$rel_id); ?>
					
					
					<?php $text1 = nl2br($text);?>
		               <?php echo   $text1 ?>
                    </div>
               </div>
			  
			   
         </div>
         <?php if(isset($client)){ ?>
         <div role="tabpanel" class="tab-pane" id="customer_admins">
            <?php if (has_permission('customers', '', 'create') || has_permission('customers', '', 'edit')) { ?>
            <a href="#" data-toggle="modal" data-target="#customer_admins_assign" class="btn btn-info mbot30"><?php echo _l('assign_admin'); ?></a>
            <?php } ?>
            <table class="table dt-table">
               <thead>
                  <tr>
                     <th><?php echo _l('staff_member'); ?></th>
                     <th><?php echo _l('customer_admin_date_assigned'); ?></th>
                     <?php if(has_permission('customers','','create') || has_permission('customers','','edit')){ ?>
                     <th><?php echo _l('options'); ?></th>
                     <?php } ?>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($customer_admins as $c_admin){ ?>
                  <tr>
                     <td><a href="<?php echo admin_url('profile/'.$c_admin['staff_id']); ?>">
                        <?php echo staff_profile_image($c_admin['staff_id'], array(
                           'staff-profile-image-small',
                           'mright5'
                           ));
                           echo get_staff_full_name($c_admin['staff_id']); ?></a>
                     </td>
                     <td data-order="<?php echo $c_admin['date_assigned']; ?>"><?php echo _dt($c_admin['date_assigned']); ?></td>
                     <?php if(has_permission('customers','','create') || has_permission('customers','','edit')){ ?>
                     <td>
                        <a href="<?php echo admin_url('clients/delete_customer_admin/'.$client->userid.'/'.$c_admin['staff_id']); ?>" class="btn btn-danger _delete btn-icon"><i class="fa fa-remove"></i></a>
                     </td>
                     <?php } ?>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
         <?php } ?>
         <div role="tabpanel" class="tab-pane" id="billing_and_shipping">
            <div class="row">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <h4 class="no-mtop"><?php echo _l('billing_address'); ?> <a href="#" class="pull-right billing-same-as-customer"><small class="font-medium-xs"><?php echo _l('customer_billing_same_as_profile'); ?></small></a></h4>
                        <hr />
                        <?php $value=( isset($client) ? $client->billing_street : ''); ?>
                        <?php echo render_textarea( 'billing_street', 'billing_street',$value); ?>
                        <?php $value=( isset($client) ? $client->billing_city : ''); ?>
                        <?php echo render_input( 'billing_city', 'billing_city',$value); ?>
                        <?php $value=( isset($client) ? $client->billing_state : ''); ?>
                        <?php echo render_input( 'billing_state', 'billing_state',$value); ?>
                        <?php $value=( isset($client) ? $client->billing_zip : ''); ?>
                        <?php echo render_input( 'billing_zip', 'billing_zip',$value); ?>
                        <?php $selected=( isset($client) ? $client->billing_country : '' ); ?>
                        <?php echo render_select( 'billing_country',$countries,array( 'country_id',array( 'short_name')), 'billing_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex'))); ?>
                     </div>
                     <div class="col-md-6">
                        <h4 class="no-mtop">
                           <i class="fa fa-question-circle" data-toggle="tooltip" data-title="<?php echo _l('customer_shipping_address_notice'); ?>"></i>
                           <?php echo _l('shipping_address'); ?> <a href="#" class="pull-right customer-copy-billing-address"><small class="font-medium-xs"><?php echo _l('customer_billing_copy'); ?></small></a>
                        </h4>
                        <hr />
                        <?php $value=( isset($client) ? $client->shipping_street : ''); ?>
                        <?php echo render_textarea( 'shipping_street', 'shipping_street',$value); ?>
                        <?php $value=( isset($client) ? $client->shipping_city : ''); ?>
                        <?php echo render_input( 'shipping_city', 'shipping_city',$value); ?>
                        <?php $value=( isset($client) ? $client->shipping_state : ''); ?>
                        <?php echo render_input( 'shipping_state', 'shipping_state',$value); ?>
                        <?php $value=( isset($client) ? $client->shipping_zip : ''); ?>
                        <?php echo render_input( 'shipping_zip', 'shipping_zip',$value); ?>
                        <?php $selected=( isset($client) ? $client->shipping_country : '' ); ?>
                        <?php echo render_select( 'shipping_country',$countries,array( 'country_id',array( 'short_name')), 'shipping_country',$selected,array('data-none-selected-text'=>_l('dropdown_non_selected_tex'))); ?>
                     </div>
                     <?php if(isset($client) &&
                        (total_rows('tblinvoices',array('clientid'=>$client->userid)) > 0 || total_rows('tblestimates',array('clientid'=>$client->userid)) > 0 || total_rows('tblcreditnotes',array('clientid'=>$client->userid)) > 0)){ ?>
                     <div class="col-md-12">
                        <div class="alert alert-warning">
                           <div class="checkbox checkbox-default">
                              <input type="checkbox" name="update_all_other_transactions" id="update_all_other_transactions">
                              <label for="update_all_other_transactions">
                              <?php echo _l('customer_update_address_info_on_invoices'); ?><br />
                              </label>
                           </div>
                           <b><?php echo _l('customer_update_address_info_on_invoices_help'); ?></b>
                           <div class="checkbox checkbox-default">
                              <input type="checkbox" name="update_credit_notes" id="update_credit_notes">
                              <label for="update_credit_notes">
                              <?php echo _l('customer_profile_update_credit_notes'); ?><br />
                              </label>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <?php echo form_close(); ?>
</div>
<?php if(isset($client)){ ?>
<?php if (has_permission('customers', '', 'create') || has_permission('customers', '', 'edit')) { ?>
<div class="modal fade" id="customer_admins_assign" tabindex="-1" role="dialog">
   <div class="modal-dialog">
      <?php echo form_open(admin_url('clients/assign_admins/'.$client->userid)); ?>
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('assign_admin'); ?></h4>
         </div>
         <div class="modal-body">
            <?php
               $selected = array();
               foreach($customer_admins as $c_admin){
                  array_push($selected,$c_admin['staff_id']);
               }
               echo render_select('customer_admins[]',$staff,array('staffid',array('firstname','lastname')),'',$selected,array('multiple'=>true),array(),'','',false); ?>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
            <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
      <?php echo form_close(); ?>
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<?php } ?>
<?php $this->load->view('admin/clients/client_group'); ?>

<script language="javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyARJjzkKsDI7BMSF97s1jkXEyxYBsRR7sI"></script>
<script type="text/javascript">

	function gethide(select)
	{
		//var grouped = select.value;
//alert(this.select);
		
		if(select.value!=''){
    document.getElementById('note_date').style.display = "block";
   } else{
    document.getElementById('note_date').style.display = "none";
   }
   $.ajax(
    {
        type:"post",
        url: "<?php echo base_url(); ?>admin/clients/filename_exists/",
        data:{ company:company},
        success:function(response)
        {
			//console.log(response);
            if (response == 0) 
            {
//alert("sdaf");
            }
			else{
				alert("Company Name already exists");
				var res=JSON.parse(response);
				//console.log(res);
				//$('#livesearch').val(res[0].company);
				document.getElementById("livesearch").innerHTML=this.responseText;
				//$('#phonenumber').val(res[0].phonenumber);
				//$('#address').val(res[0].address);
				//$('#vat').val(res[0].vat);
				//$('#PAN').val(res[0].PAN);
				//$('#email_id').val(res[0].email_id);
				
				
            }
        }
    });
   
   
		}
		
	
</script>
<script>

function check_if_exists() {

var company = $("#company").val();
//alert(company);

var msg;

    if(company.length>=2) {
        $('#result').html('');
        $.ajax(
            {
                type: "post",
                url: "<?php echo base_url(); ?>admin/clients/filename_exists/",
                data: {company: company},
                dataType:'json',
                success: function (response) {
                    if(response!='') {
                     console.log(response);
                   var output='<select class="form-control" id="company_select"  style="margin-bottom: 12px;\n' +
                       'height: 200px;">';
                        var i=0;
                     for (i=0;i<response.length;i++)
                     {
                         console.log(response[i].company);
                         output+='<option value="'+response[i].userid+'" onclick="fetch_company_name(this)">'+response[i].company+'</option>';
                     }

                     output+='</select>';

                        if($('#result').html(output))
                        {
                            // $('#company_select').click();
                            $("#company_select").attr("size",10);
                        }



                    }
                    else {
                        $('#result').html('');
                        $('#result').html('No data');
                    }

                }
            });
    }
    else {
        $('#result').empty()
    }
}










   function fetch_company_name(get) {

    console.log(get.innerHTML);
       $("#company").val(get.innerHTML);

       console.log(get.value);
       $('#result').html('');
       $.ajax({
               type: "post",
               url: "<?php echo base_url(); ?>admin/clients/get_company_details/",
               data: {user_id: get.value},
               dataType: 'json',
               success: function (response) {

                   console.log(response);

                   $('#vat').val(response.vat);
                   $('#PAN').val(response.PAN);
                   $('#DOB').val(response.DOB);
                   $('#phonenumber').val(response.phonenumber);
                   $('#address').val(response.address);
                   $('#email_id').val(response.email_id);
                   $('#zip1').val(response.zip);
                   $('#city').val(response.city);
                   $('#state').val(response.state);
                   $('#country').val(response.country);
                   $('#website').val(response.website);
                   $('#company_type').val(response.company_type);
                   $('#industry_type').val(response.industry_type);

               }
           });

   }





</script>
<script>

function forceLower(strInput) 
{
strInput.value=strInput.value.toLowerCase();
}



    function getLocation(){

        var a =$('#zip1').val();

        if(a.length==6)
        {

            getAddressInfoByZip(a);
        }
    }

    function response(obj){

            // console.log(obj.city);
           $('#state').val(obj.state);
           $('#city').val(obj.city);
           $('#country').val(obj.country);


    }
    function getAddressInfoByZip(zip){
        if(zip.length >= 5 && typeof google != 'undefined'){
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': zip }, function(results, status){
                if (status == google.maps.GeocoderStatus.OK){


                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++){
                            var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "street_number"){
                                addr.street_number = results[0].address_components[ii].long_name;
                            }
                            if (types == "route" || types == "point_of_interest,establishment"){
                                addr.route = results[0].address_components[ii].long_name;
                            }
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political"){
                                addr.city = (city == '' || types == "locality,political") ? results[0].address_components[ii].long_name : city;
                            }
                            if (types == "administrative_area_level_1,political"){
                                addr.state = results[0].address_components[ii].short_name;
                            }
                            if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                                addr.zipcode = results[0].address_components[ii].long_name;
                            }
                            if (types == "country,political"){
                                addr.country = results[0].address_components[ii].long_name;
                            }
                        }
                        addr.success = true;
                        for (name in addr){
                            console.log('### google maps api ### ' + name + ': ' + addr[name] );
                        }
                        response(addr);
                    } else {
                        response({success:false});
                    }
                } else {
                    response({success:false});
                }
            });
        } else {
            response({success:false});
        }
    }

    </script>
	<script>
function showResult(str) {
	
  if (str.length==0) { 
  //alert(str);
    document.getElementById("company").innerHTML="";
    document.getElementById("company").style.border="0px";
    return;
  }
  //alert(str);
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("company").innerHTML=this.responseText;
      document.getElementById("company").style.border="1px solid #A5ACB2";
    }
  }
  //xmlhttp.open("GET","livesearch.php?q="+str,true); 
  
  xmlhttp.open("POST","<?php echo base_url(); ?>admin/clients/cname_exists/",true);
  xmlhttp.send();
}
</script>
	
