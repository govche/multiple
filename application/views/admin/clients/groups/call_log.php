
<?php if(isset($client)){ ?>
<h4 class="customer-profile-group-heading"><?php echo 'Call Logs'; ?></h4>
<div class="col-md-12">

 <!-- <a href="#" class="btn btn-success mtop15 mbot10" onclick="slideToggle('.usernote'); return false;"><?php echo _l('new_note'); ?></a> -->
 <div class="clearfix"></div>
<div class="row">
     <hr class="hr-panel-heading" />
</div>
 <div class="clearfix"></div>
 <div class="usernote hide">
    <?php echo form_open(admin_url( 'misc/add_note/'.$client->userid.'/customer')); ?>
    <?php echo render_textarea( 'description', 'note_description', '',array( 'rows'=>5)); ?>
    <button class="btn btn-info pull-right mbot15">
        <?php echo _l( 'submit'); ?>
    </button>
    <?php echo form_close(); ?>
</div>
<div class="clearfix"></div>
<div class="mtop15">
    <table class="table dt-table scroll-responsive" data-order-col="2" data-order-type="desc">
        <thead>
            <tr>
                <th width="50%">
                    <?php echo 'Agent Number'; ?>
                </th>
                <th>
                    <?php echo 'Call Duration'; ?>
                </th>
                <th>
                    <?php echo 'Call Recording'; ?>
                </th>
                <th>
                    <?php echo 'Call Start Time'; ?>
                </th>
          
            </tr>
        </thead>
        <tbody>
            <?php foreach($call_log as $call){ ?>
            <tr>
                <td width="50%">
                  <div data-note-description="<?php echo $note['id']; ?>">
                    <?php echo $call['agent_number']; ?>
                </div>
          </td>
          <td>
          <?php echo $call['call_duration']; ?>
        </td>
        <td>
        <audio src="<?php echo $call['call_recording'] ?>
" controls>
Your browser does not support the audio element.
</audio>

    
        </td>
        <td>
        <?php echo $call['start_time']; ?>
        </td>
    </tr>
    <?php } ?>
</tbody>
</table>
</div>
<?php } ?>


