<?php if(!is_admin()) {?>
	<style>
	.btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
	display:none;
}
.btn-group>.btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
    display: none;
}
	</style>
<?php }
?>
<div style="display:none">
<?php if(isset($client)){ ?>
<h4 class="customer-profile-group-heading"><?php echo _l('client_expenses_tab'); ?></h4>
<?php if(has_permission('expenses','','create')){ ?>
<a href="<?php echo admin_url('expenses/expense?customer_id='.$client->userid); ?>" class="btn btn-info mbot15<?php if($client->active == 0){echo ' disabled';} ?>">
    <?php echo _l('new_expense'); ?>
</a>
<?php } ?>
</div>
<div id="expenses_total" class="mbot25"></div>
<?php $this->load->view('admin/expenses/table_html', array('class'=>'expenses-single-client')); ?>
<?php } ?>
