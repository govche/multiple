<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

<style>

.lead1{
  width:200px;
}
.modal-sm1 {
    position:absolute;
  top:50% !important;
  transform: translate(0, -50%) !important;
  -ms-transform: translate(0, -50%) !important;
  -webkit-transform: translate(0, -50%) !important;
  margin:auto 35%;
  width:30%;
  
    }
  
.panel_s .panel-body {
    background: #fff;
    border: 1px solid #dce1ef;
    border-radius: 4px;
    padding: 20px;
    position: relative;
    width: 1000px;
}   
#txt {
    text-transform:lowercase;
}   
.gr{
width: 515px;
    margin-left: 178px;
    margin-right: -91px;
}
#sect label{
  font-size: 12px;
  font-weight: 59   0;


} 
#sect span{
  font-size: 12px;
   font-weight: 500;
}

</style>


<h4 class="customer-profile-group-heading">View Profile</h4>
<div class="row">
   <?php echo form_open($this->uri->uri_string(),array('class'=>'client-form','autocomplete'=>'off')); ?>
   <div class="additional"></div>
   <img id="call_gif" src="<?php echo base_url();?>assets/Ringing-phone.gif" width="40px" height="40px" style="margin-left: 40%;display: none">

   <div id="call_msg"></div>
   <div class="col-md-12">
      <div class="horizontal-scrollable-tabs">
<div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
<div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
<div class="horizontal-tabs">
      <ul class="nav nav-tabs profile-tabs row customer-profile-tabs nav-tabs-horizontal" role="tablist">
         <li role="presentation" class="<?php if(!$this->input->get('tab')){echo 'active';}; ?>">
            <a href="#contact_info" aria-controls="contact_info" role="tab" data-toggle="tab">
            <?php echo _l( 'customer_profile_details'); ?>
            </a>
         </li>
         <?php
            $customer_custom_fields = false;
            if(total_rows('tblcustomfields',array('fieldto'=>'customers','active'=>1)) > 0 ){
                 $customer_custom_fields = true;
             ?>
         <!--<li role="presentation" class="<?php if($this->input->get('tab') == 'custom_fields'){echo 'active';}; ?>">
            <a href="#custom_fields" aria-controls="custom_fields" role="tab" data-toggle="tab">
            <?php //echo do_action('customer_profile_tab_custom_fields_text',_l( 'custom_fields')); ?>
            </a>
         </li>-->
         <?php } ?>
         <!--<li role="presentation">
            <a href="#billing_and_shipping" aria-controls="billing_and_shipping" role="tab" data-toggle="tab">
            <?php echo _l( 'billing_shipping'); ?>
            </a>
         </li>-->
         <?php do_action('after_customer_billing_and_shipping_tab',isset($client) ? $client : false); ?>
         <?php if(isset($client)){ ?>
         <!--<li role="presentation">
            <a href="#customer_admins" aria-controls="customer_admins" role="tab" data-toggle="tab">
            <?php echo _l( 'customer_admins' ); ?>
            </a>
         </li>-->
         <?php do_action('after_customer_admins_tab',$client); ?>
         <?php } ?>
      </ul>
   </div>
</div>
      <div class="tab-content" >
         <?php do_action('after_custom_profile_tab_content',isset($client) ? $client : false); ?>
         <?php if($customer_custom_fields) { ?>
         <div role="tabpanel" class="tab-pane <?php if($this->input->get('tab') == 'custom_fields'){echo ' active';}; ?>" id="custom_fields">
            <?php $rel_id=( isset($client) ? $client->userid : false); ?>
            <?php //echo render_custom_fields( 'customers',$rel_id); ?>
         </div>
         <?php } ?>
         <div role="tabpanel" class="tab-pane<?php if(!$this->input->get('tab')){echo ' active';}; ?>" id="contact_info">
            <div class="row">
               <!-- <div class="col-md-12<?php if(isset($client) && (!is_empty_customer_company($client->userid) && total_rows('tblcontacts',array('userid'=>$client->userid,'is_primary'=>1)) > 0)) { echo ''; } else {echo ' hide';} ?>" id="client-show-primary-contact-wrapper">
                  <div class="checkbox checkbox-info mbot20 no-mtop">
                     <input disabled type="checkbox" name="show_primary_contact"<?php if(isset($client) && $client->show_primary_contact == 1){echo ' checked';}?> value="1" id="show_primary_contact">
                     <label for="show_primary_contact"><?php echo _l('show_primary_contact',_l('invoices').', '._l('estimates').', '._l('payments').', '._l('credit_notes')); ?></label>
                  </div>
               </div> -->
               
                <section id="sect">
                <div class="row">
               <div class="col-md-6">
      
                  <?php $value=( isset($client) ? $client->company : ''); ?>
                  <?php $attrs = (isset($client) ? array() : array('autofocus'=>true,)); ?>
                  <?php //echo render_input5( 'Company', 'Company/Trade Name:',$value,'text','','','','','',true) ?>
                  <div>
                  <label for="Comapny">COMPANY/TRADE NAME<span style="word-spacing: 2em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>
                  <div id="result"></div>
           <?php
               $company_type= get_all_company();
               $customer_default_company = get_option('customer_default_company');
               $selected =( isset($client) ? $client->company_type: $customer_default_country  );
              
               if(empty($selected)) { $selected = "AOP Trust"; 
                 
         }
         else{
            foreach($company_type as  $row){
               if($selected == $row['id']){
                  $company_name=$row['company_name'];
               }
            
            }
         }


            //echo render_input5( 'Company', 'Company-type:',$company_name,'text','','','','','',true)              


        ?>
        <div>
      <label for="Comapny">COMPANY TYPE<span style="word-spacing: 6.25em;"> :</span></label>
                  <span><?php echo $company_name ?></span>
                  
         </div>
        <br>

    <?php
               $industry_type= get_all_industry();
               $customer_default_industry = get_option('customer_default_company');
               $selected =( isset($client) ? $client->industry_type : $customer_default_country  );
              
               if(empty($selected)) { $industry_type = "AOP Trust"; 
                 
               }
               else{
                  foreach($industry_type as  $row){
                     if($selected == $row['id']){
                        $industry_type=$row['industry_type'];
                     }
                  
                  }
               }
      
      
                 // echo render_input5( 'industry_type', 'Industry_type:',$industry_type,'text','','','','','',true)  ?>
                 <div>
               <label for="industry_type">INDUSTRY TYPE<span style="word-spacing: 6.25em;"> :</span></label>
                  <span><?php echo $industry_type ?></span>
                </div>
                  <br>
                  <?php if(get_option('company_requires_vat_number_field') == 1){
                     $value=( isset($client) ? $client->vat : '');
           
                     //echo render_input5( 'vat', 'Client_vat_number:',$value,array('data-custom-field-required' => 1),array('maxlength'=>15,'minlength'=>15),'','','','',true);
                     } ?>
                      <div>
               <label for="vat">GST NUMBER<span style="word-spacing: 7.5em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>
        
                  <?php $value=( isset($client) ? $client->PAN : ''); ?>
         <?php //echo render_input5( 'PAN', 'PAN Number:',$value,array('data-custom-field-required' => 1),array('maxlength'=>10,'minlength'=>10),'','','','','',true); ?>
                <div>
               <label for="PAN">PAN NUMBER<span style="word-spacing: 7.5em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>
           
           <?php $value=( isset($client) ? $client->DOB : ''); ?>
                  <?php //echo render_input5( 'DOB', 'Date Of Birth/Date Of Reg:',$value,'','','','','','',true); ?>

                  <div>
               <label for="DOB">DATE OF BIRTH/DATE OF REG<span style="word-spacing: 0.15em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>
    
          
              
          
         
          <div>
               <?php 
      $client_id=$client->userid;
      $this->db->select('*');
      $this->db->where('customer_id',$client_id);
      $group = $this->db->get('tblcustomergroups_in')->result_array();
      ?>
      <label for="group">GROUP<span style="word-spacing: 10.5em;"> :</span></label>
      <?php
      if($group){
          $group_count=count($group);
          $i=1;
         foreach($group as $row){
          
            $group_id=$row['groupid'];
            $this->db->select('name');
            $this->db->where('id',$group_id);
            $group = $this->db->get(' tblcustomersgroups')->row_array();
            if( $i < $group_count)
            {?>
            <span ><?php echo $group['name'];
            ?>,</span>
            <?php
            }
            else
            {
              ?>
              <span ><?php echo $group['name'];
            ?></span>
            <?php
            }
            $i=$i+1;
         }
        }
      
             ?>
            
                  
                </div>
                  <br>





           
           <div  id="note_date" >
           <?php $value=( isset($client) ? $client->g_contactno : ''); ?>
                  <?php //echo render_input5( 'g_contactno', 'Groups Contact No:',$value,array('data-custom-field-required' => 1 ),array('minlength'=>10),'','','','',true); ?>
           <label for="g_contactno">GROUPS CONTACT NUMBER<span style="word-spacing: 0.5em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                <br>


                     <?php $value=( isset($client) ? $client->g_email : ''); ?>
                   <?php //echo render_input5( 'g_email', 'Groups Email:',$value,array('data-custom-field-required' => 1 ),'','','','',true) ?>
                   <div>
               <label for="g_email">GROUPS E-MAIL<span style="word-spacing: 6.25em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
            <div class="hide">
                  <?php if(!isset($client)){ ?>
                  <!--<i class="fa fa-question-circle pull-left" data-toggle="tooltip" data-title="<?php echo _l('customer_currency_change_notice'); ?>"></i>-->
                  <?php }
                     $s_attrs = array('data-none-selected-text'=>_l('system_default_string'));
                     $selected = '';
                     if(isset($client) && client_have_transactions($client->userid)){
                        $s_attrs['disabled'] = true;
                     }
                     foreach($currencies as $currency){
                        if(isset($client)){
                          if($currency['id'] == $client->default_currency){
                            $selected = $currency['id'];
                         }
                      }
                     }
                            // Do not remove the currency field from the customer profile!
                     echo render_select5('default_currency',$currencies,array('id','name','symbol'),'invoice_add_edit_currency',$selected,$s_attrs,array(),'','',false,4); ?><br>
                 </div>
<!-- --><?php //$value=( isset($client) ? $client->zip : '');?>
 <!--by rathina for change zip code to pin code-->
<!--                  --><?php //echo render_input5( 'zip', 'client_postal_code',$value,array('data-custom-field-required' => 1 ),array('required'=>true)); ?><!--<br>-->
<!--                   <label class="col-md-4" for="zip">Pin Code</label>-->
<!--                   <input type="number" name="zip" id="zip" onkeyup="getLocation()" required value=--><?php //echo "$value";?><!-->


         <?php if(get_option('disable_language') == 0){ ?>
                  <!--<div class="form-group row select-placeholder">
                     <label for="default_language" class="col-md-4 control-label"><?php echo _l('localization_default_language'); ?>
                     </label>
                     <div class="col-md-8">
                     <select name="default_language" id="default_language" class="form-control selectpicker" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                        <option value=""><?php echo _l('system_default_string'); ?></option>
                        <?php foreach(list_folders(APPPATH .'language') as $language){
                           $selected = '';
                           if(isset($client)){
                              if($client->default_language == $language){
                                 $selected = 'selected';
                              }
                           }
                           ?>
                        <option value="<?php echo $language; ?>" <?php echo $selected; ?>><?php echo ucfirst($language); ?></option>
                        <?php } ?>
                     </select>
                     </div>
                  </div>-->
                  <?php } ?>
                  <?php //echo render_custom_fields1( 'customers',$rel_id,'slug="customers_type_of_company"','add_edit_preview'); ?>
               </div>
         
         
         
         
               <div class="col-md-6">
         
        <?php $value=( isset($client) ? $client->phonenumber : ''); ?>
                  <?php //echo render_input5( 'phonenumber', 'Client_Phonenumber:',$value,array('data-custom-field-required' => 1 ),array('required'=>true),'','','','',true); ?>
                  <div>
               <label for="phonenumber">PHONE NUMBER<span style="word-spacing: 0.25em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>



                     <?php $value=( isset($client) ? $client->address : ''); ?>
                   <?php //echo render_input5( 'address', 'Address:',$value,array('data-custom-field-required' => 1 ),array('required'=>true, 'minlength'=>30, 'onkeyup'=>"return forceLower(this);"),'','','','',true); ?>
                   <div>
               <label for="Address">ADDRESS<span style="word-spacing: 3.65em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>


         
                  
           <?php $value=( isset($client) ? $client->email_id : ''); ?>
                  <?php // echo render_input5( 'email_id', 'Email_Id:',$value,array('data-custom-field-required' => 1 ),array('required'=>true),'','','','',true); ?>
                  <div>
               <label for="email">E-MAIL<span style="word-spacing: 4.65em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                  <br>

                   <?php $value=( isset($client) ? $client->zip : ''); ?>
                   <div >
                       <label >PIN CODE<span style="word-spacing: 3.35em;"> :</span></label>
                       <span > <?php echo $value;?> </span>
                       </div>
                   <br>

                   <?php $value=( isset($client) ? $client->city : ''); ?>
                   <div >
                       <label  for="city">CITY<span style="word-spacing: 5.75em;"> :</span></label>
                       <span><?php echo $value;?></span>
    
                   </div>
                   <br>
<!--                 -->
                   <?php $value=( isset($client) ? $client->state : ''); ?>
                   <div >
                       <label >STATE<span style="word-spacing: 5em;"> :</span></label>
                      <span> <?php echo $value;?></span>
                  
                   </div>
                   <br>

                   <?php $value=( isset($client) ? $client->country : ''); ?>
                   <div  >
                       <label >COUNTRY<span style="word-spacing: 3.05em;"> :</span></label>
                       <span><?php echo $value;?></span>
                   </div>
                   <br>
           
                   <?php if((isset($client) && empty($client->website)) || !isset($client)){
                     $value=( isset($client) ? $client->website : '');
                     //echo render_input5( 'website', 'Client_website:',$value,'','','','','','',true);
                     } ?>
                      <div>
               <label for="website">CLIENT WEBSITE<span style="word-spacing: 0.05em;"> :</span></label>
                  <span><?php echo $value ?></span>
                </div>
                </div>
               </div>
             </section>
          <div class="col-md-6">
        
         
          
         </div>
           <div class="col-md-6">
         
          
          
           </div>
           <div style="display:none" class="col-md-6">
          
         
          
          <?php $value=( isset($client) ? $client->group_name : ''); ?>
                  <?php echo render_input5( 'group_name', 'Group Name',$value); ?><br>
          
          
           </div>
           <div style="display:none" class="col-md-6">
          
           
          <?php $value=( isset($client) ? $client->refer_by : ''); ?>
                  <?php echo render_input5( 'refer_by', 'Referred By:',$value); ?><br>
          
         
         </div><br />
            </div>
               <div  class="row">
                  <div class="col-md-12">
          <?php $text=render_custom_fields('customers',$rel_id); ?>
          <?php $text1 = nl2br($text);?>
                   <?php echo   $text1 ?>
                    </div>
               </div>
        
         
         </div>
         <?php if(isset($client)){ ?>
         <div role="tabpanel" class="tab-pane" id="customer_admins">
            <?php if (has_permission('customers', '', 'create') || has_permission('customers', '', 'edit')) { ?>
            <a href="#" data-toggle="modal" data-target="#customer_admins_assign" class="btn btn-info mbot30"><?php echo _l('assign_admin'); ?></a>
            <?php } ?>
            <table class="table dt-table">
               <thead>
                  <tr>
                     <th><?php echo _l('staff_member'); ?></th>
                     <th><?php echo _l('customer_admin_date_assigned'); ?></th>
                     <?php if(has_permission('customers','','create') || has_permission('customers','','edit')){ ?>
                     <th><?php echo _l('options'); ?></th>
                     <?php } ?>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($customer_admins as $c_admin){ ?>
                  <tr>
                     <td><a href="<?php echo admin_url('profile/'.$c_admin['staff_id']); ?>">
                        <?php echo staff_profile_image($c_admin['staff_id'], array(
                           'staff-profile-image-small',
                           'mright5'
                           ));
                           echo get_staff_full_name($c_admin['staff_id']); ?></a>
                     </td>
                     <td data-order="<?php echo $c_admin['date_assigned']; ?>"><?php echo _dt($c_admin['date_assigned']); ?></td>
                     <?php if(has_permission('customers','','create') || has_permission('customers','','edit')){ ?>
                     <td>
                        <a href="<?php echo admin_url('clients/delete_customer_admin/'.$client->userid.'/'.$c_admin['staff_id']); ?>" class="btn btn-danger _delete btn-icon"><i class="fa fa-remove"></i></a>
                     </td>
                     <?php } ?>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
         <?php } ?>
         
   <?php echo form_close(); ?>

<?php if(has_permission('customers', '', 'edit') || customer_created($this->session->userdata('staff_user_id'),$client->userid) == true){ ?>
   <a href="<?php echo admin_url('clients/client/' . $client->userid . '?group=edit ') ?>" ><button  type="button" class="btn btn-lg btn-primary"><i class="fa fa-edit"></i>Edit</button> </a>   
<?php } ?>
<?php if($client->premium ==0){ ?>
      <button id="call" type="button" class="btn btn-lg btn-success" onClick="callAgent('<?php echo $client->phonenumber; ?>','<?php echo $number['phonenumber']; ?>')" ><i class=" fa fa-phone"></i>Call</button>

<?php  } ?>
</div>

<?php $this->load->view('admin/clients/client_group'); ?>


<script>

function callAgent(customer,user) {
   var  user_id ='<?php echo $client->userid; ?>';
$('#call_msg').empty();
$('#call_gif').css('display','inline');



$.ajax({
    type: "post",
    url: "<?php echo base_url();?>admin/clients/call",
   data: {"mobile":user,"customer":customer},
    dataType:"json",
    success: function (response) {
      $('#call_gif').css('display','none');
      
      console.log(response);
        if (!response.error) {
            if (response.success.status == 'success') {
               setInterval(function () {
             
                  $('#call_msg').html(' <div class="alert alert-success">\n' +
                    '    <strong>Success!</strong> '+response.success.message+'\n' +
                    '  </div>')
                    window.location.href = "<?php echo admin_url()?>clients/client/"+user_id+"?group=notes";

    },1000);
               
            } 
            
            

        }
        else {
            $('#call_msg').html(' <div class="alert alert-danger">\n' +
                '    <strong>Error! </strong>' + response.error.message + '\n' +
                '  </div>')
             
        }
    }

});

}

</script>



