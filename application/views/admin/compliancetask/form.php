<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<?php init_head(); ?>
<style>
    /*.dropdown-toggle{*/
    /*    display: none!important;*/
    /*}*/
    .mleft10 {
    margin-left: 10px!important;
    float: right;
}

 

 
.error {
  color: red;
  margin-left: 5px;
}
 

    </style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<link href="<?php echo APP_BASE_URL;?>/assets/css/task_css.css" rel="stylesheet">
  
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                <div class="row _buttons">
                    
                     <h4 class="no-margin" style="color: #03a9f4"><u><?php echo _l('Create Compliance'); ?></u></h4>    
                      <br>
                        <?php echo form_open('admin/tasks/list_compliance_tasks'); ?>
					   <div class="col-md-4">
			    <?php echo render_input('name','Name',$values,array('data-custom-field-required' => 1 ),array('required'=>true)); ?>
				</div>
								<!--<div class="col-md-4">
								<?php 
									
									echo render_select('service',$service,array('id','description'),'ticket_settings_service');
								
								?>
							</div>-->
							  
					
				  <div class="col-md-4">
				 <label for="frequency" ><?php echo _l('Frequency'); ?></label>
                        <select name="frequency" required class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <option value="" ><?php echo _l('Select Frequency'); ?></option>
                           <option value="Once" ><?php echo _l('Once'); ?></option>
                           <option value="Monthly"  ><?php echo _l('Monthly'); ?></option>
						   <option value="Quartely" ><?php echo _l('Quartely'); ?></option>
         <!--                  <option value="Half-Annual"  ><?php echo _l('Half-Annual'); ?></option>-->
						   <!--<option value="Annual" ><?php echo _l('Annual'); ?></option>-->
                        </select>
                     </div>
						
						
							<div class="col-md-4">
                   <label for="duedate" ><?php echo _l('Due Date'); ?></label><br>
                  
                   <input type="date" class="date" name="duedate" required>
                  
               </div> 	
               
               
	            <!-- <div class="col-md-4">
								<?php 
									
									echo render_select('company_type',$company,array('id','company_name'),'Type Of Company');
								
								?>
							</div>
							  
				  <div class="col-md-4">
				  <label for="compliance_type" ><?php echo _l('Compliance Type'); ?></label>
                        <select name="compliance_type"  class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <option value="" ><?php echo _l('Select Type'); ?></option>
                           <option value="Corporate and Secretarial Compliance" ><?php echo _l('Corporate and Secretarial Compliance'); ?></option>
                           <option value="HR Compliance"  ><?php echo _l('HR Compliance'); ?></option>
						   <option value="Other" ><?php echo _l('Other'); ?></option>
                          <option value="Tax Compliances" ><?php echo _l('Tax Compliances'); ?></option>
                        </select>
                     </div>
				 
				  
				  <div class="col-md-4">
				  <label for="category" class="control-label"><?php echo _l('Category'); ?></label>
                        <select name="category" id="repeat_type_custom" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value="" ><?php echo _l('Select Category'); ?></option>
                           <option value="Board Compliance"  ><?php echo _l('Board Compliance'); ?></option>
						   <option value="Central Excise" ><?php echo _l('Central Excise'); ?></option>
                           <option value="Employees State Insurance" ><?php echo _l('Employees State Insurance'); ?></option>
							<option value="Employment Exchange Notification" ><?php echo _l('Employment Exchange Notification'); ?></option>
                           <option value="Income Tax"  ><?php echo _l('Income Tax'); ?></option>
						   <option value="Labour Welfare Fund" ><?php echo _l('Labour Welfare Fund'); ?></option>
                           <option value="Minimum Wages"  ><?php echo _l('Minimum Wages'); ?></option>
						   <option value="Payment Of Bonus" ><?php echo _l('Payment Of Bonus'); ?></option>
                        <option value="Payment Of Wages" ><?php echo _l('Payment Of Wages'); ?></option>
                           <option value="Professional Tax"  ><?php echo _l('Professional Tax'); ?></option>
						   <option value="Provident Fund" ><?php echo _l('Provident Fund'); ?></option>
                        </select>
                     </div>
				  
				  
				  <div class="col-md-4">
								<?php 
									
									echo render_select('department',$department,array('departmentid','name'),'Department');
								
								?>
							</div>-->
							   <div class="col-md-12">
						      <button class="btn-tr btn btn-info ">
                  <?php echo _l('submit'); ?>
                </button>
				</div>
				
				 <div class="row mbot15">
                            <div class="col-md-12">
                                <h4 class="no-margin" style="color: #03a9f4"><center><u><?php echo _l('Compliance List'); ?></center></u></h4>

                            </div>

                        </div>
                       
                        <?php echo form_hidden('custom_view'); ?>
                        <?php $this->load->view('admin/tasks/table_html'); ?>
				
                           <?php  echo form_close(); ?>
					
                  </div>
                 
                
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>

</body>
</html>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    // $.noConflict();
    $(document).ready( function () {
        var table = $('#table_data').DataTable();
    });



    
</script>

</body>

</html>