<style>
.col-lg-1 {
    width: 6%;
    margin:0;
    padding:0;
}
.col-lg-11 {
    width: 92.5%;
    margin:0;
    padding:0;
}
.test-knowlarity{margin-top:1%;}
.col-lg-1 input{width:100%}
</style>
<ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active">
        <a href="#misc" aria-controls="misc" role="tab" data-toggle="tab">
          <i class="fa fa-cog"></i> <?php echo 'Knowlarity'; ?></a>
        </li>
          </ul>
          <div class="tab-content mtop30">
            <div role="tabpanel" class="tab-pane active" id="misc">
            <div class="form-group" app-field-wrapper="knowlarity_api_url"><label for="knowlarity_api_url" class="control-label">ApI Url</label><input type="text" id="knowlarity_api_url" name="knowlarity_api_url" class="form-control" value="<?php echo get_option('knowlarity_api_url'); ?>" placeholder="https://example.knowlarity.com/"></div>
            <hr />
              <?php echo render_input('knowlarity_api_key','knowlarity_api_key',get_option('knowlarity_api_key')); ?>
              <hr />
              <?php echo render_input('knowlarity_authorization_key','knowlarity_authorization_key',get_option('knowlarity_authorization_key')); ?>
              <hr />
              
              <?php echo get_option('knowlarity_sr'); ?>
              
              <div style="margin-bottom:1%">Knowlarity SR number</div>
              <div class="form-group" app-field-wrapper="knowlarity_sr_number">
            
             <div class="col-lg-1"> <input type="text" value="+91" name="indiacode" readonly class="form-control ss" ></div>
             <div class="col-lg-11" > 
             <input type="number" id="knowlarity_sr_number" name="knowlarity_sr_number" class="form-control inputnumber" value="<?php echo get_option('knowlarity_sr_number'); ?>">

             <hr/>
            
             </div>
              </div>
           
              <label>Please Verify your knowlarity Account</label>
           
              <div style="margin-bottom:1%">Knowlarity Agent number</div>
              <div class="form-group" app-field-wrapper="knowlarity_agent_number">
             <div class="col-lg-1"> <input type="text" name="indiacode" value="+91" readonly class="form-control ss" ></div>
             <div class="col-lg-11" > 
             <input type="number" id="knowlarity_agent_number" name="knowlarity_agent_number" class="form-control inputnumber"  >
             </div>
              </div>
              
               <hr /> 
             <div style="margin-bottom:1%">Testing Contact</div>
              <div class="form-group" app-field-wrapper="testing_contact">
            
             <div class="col-lg-1"> <input type="text" value="+91" name="indiacode" readonly class="form-control ss" ></div>
             <div class="col-lg-11" > 
             <input type="number"  id="testing_contact" name="testing_contact" class="form-control inputnumber"  >
             </div>
              </div>
              <hr />
              <button class="btn btn-primary test-knowlarity"> <?php echo _l('knowlarity_verify'); ?></button>
           <hr />
    <div class="debug">
    </div>
	      </div>
           
            </div>
        </div>
