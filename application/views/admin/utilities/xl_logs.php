<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="row">
						    <?php
						    foreach($file as $key)
						    {
						    ?>
					    <div class="col-md-2">
					        <a href ="<?php echo base_url();?>logs_xl/<?php echo $key;?>" download>
					    <i class="fa fa-file-excel-o" style="font-size: 100px;
                        color: green;margin-bottom: 9px;margin-left: -5px;" aria-hidden="true"></i><br> 
                        <b style="text-transform: capitalize;font-weight: 600;"><?php echo pathinfo($key, PATHINFO_FILENAME);?> <span><i class="fa fa-download" aria-hidden="true"></i></span></b>
                        </a>
					    </div>
					    
					     <?php
						    }
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php init_tail(); ?>
</body>
</html>
