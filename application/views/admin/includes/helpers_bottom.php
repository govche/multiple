<?php include_once(APPPATH . 'views/admin/includes/modals/post_likes.php'); ?>
<?php include_once(APPPATH . 'views/admin/includes/modals/post_comment_likes.php'); ?>







<style>

.textarea4
      {
        padding: 11px 29px 16px 16px;
        width: 100%;
        height: 56px;
        resize: none;
        overflow: auto;
        border: none;
      }


    #chatbot_icon {
        position: fixed;
        top: 93%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 10px;
        z-index: 1;

    }
    #chatbot_icon:hover {
        /*-ms-transform: scale(1.5); !* IE 9 *!*/
        /*-webkit-transform: scale(1.5); !* Safari 3-8 *!*/
        /*transform: scale(1.5);*/

    }

    .chat_icon
    {
        cursor: pointer;
        width: 48px;
    }
    .chat_icon:hover
    {
        /*border: 1px solid;*/
        padding: 3px;
        box-shadow: 3px 3px transparent;
        border-radius: 50%;
    }
    .chat-popup {
        display: none;
        position: fixed;
        left: 58px;
        /*z-index: 9;*/
        /*height: 500px;*/
        /*overflow: scroll;*/
        /*: ;*/
    }
    .card1{
        width: 350px!important;

    }

    .option.bubble {
        background-color: #fff;
        border: 1px solid;
    }

    .theme-border {
        border-color: #c6cac8;
    }

    .theme-color {
        color: #2dc464;
    }

    .bubble-inline {
        display: inline-block;
        cursor: pointer;

    }
    .bubble-inline:hover
    {
        /*padding: 3px;*/
        box-shadow: 3px 3px #2dc464;
        /*border-radius: 50%;*/
    }

    .bubble-inline:active
    {
        transform: translateY(4px);
    }

    .bubble {
        border-radius: 1.3em;
        border: 1px solid transparent;
        padding: 7px 11px;
        transition: width 2s;
        word-break: break-word;
        line-height: 20px;

    }

    .option {
        /*position: relative;
        padding: 7px 14px 11px 17px;*/
        margin: 5px;
    }



    .chat_img {
        float: left;
        width: 11%;
    }

    .chat_img img {
        width: 100%
    }

    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people {
        overflow: hidden;
        clear: both;
    }

    .chat_list {
        border-bottom: 1px solid #ddd;
        margin: 0;
        padding: 18px 16px 10px;
    }

    .inbox_chat {
        height: 550px;
        overflow-y: scroll;
    }

    .active_chat {
        background: #e8f6ff;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .incoming_msg_img img {
        width: 40px;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 0 15px 15px 15px;
        color: #646464;
        font-size: 14px;
        margin:0px 0px 0px 18px;
        padding: 10px 17px 11px 18px;
        width: 230px;
        font-family: lato;
    }


    .received_withd_msg {
        width: 57%;
    }

    .mesgs{
        float: left;
        padding: 12px 0px 0 0px;
        width:100%;
    }

    .sent_msg p {
        background:#8FC737;
        border-radius: 10px 0px 9px 8px;
        font-size: 14px;
        margin: 0px 0px 0px -78px;
        color: #fff;
        padding: 7px 16px 11px 14px;
        width: 230px;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px -75px;
    }

    .sent_msg {
        float: right;
        width: 46%;
        /*height: 80px;*/

    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        padding: 0px 0px 0px 20px;
        width: 100%;
        outline:none;
    }

    .type_msg {
        border-top: 1px solid #c4c4c4;
        position: relative;
    }

    .msg_send_btn {
        background: #8FC737 none repeat scroll 0 0;
        border:none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 15px;
        height: 33px;
        position: absolute;
        right: 14px;
        top: 8px;
        width: 33px;
    }

    .messaging {
        /*padding: 0 0 50px 0;*/
    }

    .msg_history {
        height: 516px;
        overflow-y: auto;
    }


    .refresh{
        z-index: 2147483001;
        cursor: pointer;
        background-size: 15px;
        background-repeat: no-repeat;
        background-position: center;
        position: absolute;
        top: 10px;
        right: 50px;
        width: 30px;
        height: 30px;
        background-color: #666;
        border-radius: 50%;
        padding: 4px 0px 0px 8px;
    }

    .close{
        z-index: 2147483001;
        cursor: pointer;
        background-size: 15px;
        background-repeat: no-repeat;
        background-position: center;
        position: absolute;
        top: 10px;
        right: 10px;
        width: 30px;
        height: 30px;
        background-color: #666;
        border-radius: 50%;
        padding: 4px 0px 0px 8px;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }
    .header
    {
        width: 100%;
        height: 50px;
        padding: 10px;
        border-bottom: 3px solid;
        color: #8FC737;
        background: #fff;
        border-radius: 9px 9px 0px 0px;


    }



</style>

<!--chat bot icon-->
<div id="chatbot_icon">

</div>
<input type="hidden" id="read">
<!--chat bot icon end-->

<!--chat bot popup-->

<div class="chat-popup" id="popup" style="z-index: 999; bottom: 80px;  border-radius: 10px 10px 10px 10px !important; box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2);
}; background: #fff">
    <div class="header">
        <img src="<?php echo base_url();?>assets/chat_img/kanakkupillai-logo.png"  style="width:120px;" />
<!--        <div class="refresh"  onclick="refresh()">-->
<!--            <img src="--><?php //echo base_url();?><!--assets/chat_img/refresh.png" width="15" height="15" />-->
<!--        </div>-->
        <div class="close" onclick="chatpoup()">
            <img src="<?php echo base_url();?>assets/chat_img/close.png" width="15" height="15" />
        </div>
    </div>
    <div class="card card1">

        <div class="card-body" style="height:220px; overflow-y:scroll !important;">

                <div class="bubble no-border" style="display: table; direction: unset;overflow: scroll;">

                    <div class="option-wrapper">





                        <div id="chatbot_covesation">

                        </div>
                    </div>


                </div>


        </div>
        <div class="messaging">


            <div class="mesgs">
                <div class="type_msg">
                    <div class="input_msg_write" style="background:#fff!important; border-radius: 9px 9px 9px 9px;">
                        <form id="send_sms">
                            <input type="hidden" name="lead_id" value="" id="lead_id">
                            <textarea class="write_msg textarea4" id="text" placeholder="Type an answer" required></textarea>
                            <button class="msg_send_btn" id="submit_id" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>


            </div>

        </div>

        <input type="hidden" id="shows">
    </div>

</div>

<!--chat bot pop up-->













<div id="event"></div>
<div id="newsfeed" class="animated fadeIn hide" <?php if($this->session->flashdata('newsfeed_auto')){echo 'data-newsfeed-auto';} ?>>
</div>
<!-- Task modal view -->
<div class="modal fade task-modal-single" id="task-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog <?php echo get_option('lead_modal_class'); ?>">
    <div class="modal-content data">

    </div>
  </div>
</div>

<!--Add/edit task modal-->
<div id="_task"></div>

<!-- Lead Data Add/Edit-->
<div class="modal fade lead-modal" id="lead-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog <?php echo get_option('lead_modal_class'); ?>">
    <div class="modal-content data">

    </div>
  </div>
</div>

<div id="timers-logout-template-warning" class="hide">
  <h2 class="bold"><?php echo _l('timers_started_confirm_logout'); ?></h2>
  <hr />
  <a href="<?php echo site_url('authentication/logout'); ?>" class="btn btn-danger"><?php echo _l('confirm_logout'); ?></a>
</div>

<!--Lead convert to customer modal-->
<div id="lead_convert_to_customer"></div>

<!--Lead reminder modal-->
<div id="lead_reminder_modal"></div>
