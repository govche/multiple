<style>
#menu {
   width: 210px;
   bottom: 0;
   float: left;
   left: 0;
   position: fixed;
   top: 63px;
   -webkit-transition: all .5s ease 0s;
   transition: all .5s ease 0s;
}
#header{
 border-bottom:1px solid #03a9f4;
  }
/* .sidebar .fa{  box-shadow: 0px 1px 3px 2px #03a9f4;} */
.sidebar  .arrow {  box-shadow:none;}
.sidebar  .fa-power-off {  box-shadow:none;}
.sidebar{
  scroll-behavior: smooth;
  overflow-y: scroll;
  position:fixed;
  padding-bottom:2%;
}
#header{
   position: fixed;
   width: 100%;
}
.content {
   padding: 70px 25px 25px 25px;
   min-width: 320px;
}
.sidebar::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px #ccc;
  -webkit-box-shadow: outset 0 0 6px #ccc;
  border-radius: 10px;
}
.sidebar::-webkit-scrollbar
{
    width: 5px;
    /* background-color: #F5F5F5; */
}
.sidebar::-webkit-scrollbar-thumb
{
    /* background-color: #3366FF; */
    border-radius: 5px;
    /* background-image: -webkit-linear-gradient(0deg,
                                              rgba(255, 255, 255, 0.5) 25%,
                                              transparent 25%,
                                              transparent 50%,
                                              rgba(255, 255, 255, 0.5) 50%,
                                              rgba(255, 255, 255, 0.5) 75%,
                                              transparent 75%,
                                              transparent) */
}
#logo a:hover img {
border: none !important;
display: block;
}
a:hover {
color:#ffffff!important;
background-color:#0095e6!important;
}
</style>


<?php
   ob_start();
   ?>
<li id="top_search" class="dropdown" data-toggle="tooltip" data-placement="bottom" data-title="<?php echo _l('search_by_tags'); ?>">
   <input type="search" id="search_input" class="form-control" placeholder="<?php echo _l('top_search_placeholder'); ?>">
   <div id="search_results">
   </div>
</li>
<li id="top_search_button">
   <button class="btn"><i class="fa fa-search"></i></button>
</li>
<?php
   $top_search_area = ob_get_contents();
   ob_end_clean();
   ?>
<div id="header">
   <div class="hide-menu"><i class="fa fa-bars"></i></div>
   <div id="logo">
      <?php get_company_logo(get_admin_uri().'/') ?>
   </div>
   <nav>
      <div class="small-logo">
         <span class="text-primary">
         <?php get_company_logo(get_admin_uri().'/') ?>
         </span>
      </div>
      <div class="mobile-menu">
         <button type="button" class="navbar-toggle visible-md visible-sm visible-xs mobile-menu-toggle collapsed" data-toggle="collapse" data-target="#mobile-collapse" aria-expanded="false">
         <i class="fa fa-chevron-down"></i>
         </button>
         <ul class="mobile-icon-menu">
            <?php
               // To prevent not loading the timers twice
               if(is_mobile()){ ?>
            <li class="dropdown notifications-wrapper header-notifications">
               <?php $this->load->view('admin/includes/notifications'); ?>
            </li>
            <li class="header-timers">
               <a href="#" id="top-timers" class="dropdown-toggle top-timers" data-toggle="dropdown"><i class="fa fa-clock-o fa-fw fa-lg"></i>
               <span class="label bg-success icon-total-indicator icon-started-timers<?php if ($totalTimers = count($startedTimers) == 0){ echo ' hide'; }?>"><?php echo count($startedTimers); ?></span>
               </a>
               <ul class="dropdown-menu animated fadeIn started-timers-top width300" id="started-timers-top">
                  <?php $this->load->view('admin/tasks/started_timers',array('startedTimers'=>$startedTimers)); ?>
               </ul>
            </li>
            <?php } ?>
         </ul>
         <div class="mobile-navbar collapse" id="mobile-collapse" aria-expanded="false" style="height: 0px;" role="navigation" >
            <ul class="nav navbar-nav">
               <li class="header-my-profile"><a href="<?php echo admin_url('profile'); ?>"><?php echo _l('nav_my_profile'); ?></a></li>
                <li class="header-my-profile"><a href="<?php echo admin_url('profile'); ?>"><?php echo _l('nav_my_profile'); ?></a></li>
               <li class="header-my-timesheets"><a href="<?php echo admin_url('staff/timesheets'); ?>"><?php echo _l('my_timesheets'); ?></a></li>
               <li class="header-edit-profile"><a href="<?php echo admin_url('staff/edit_profile'); ?>"><?php echo _l('nav_edit_profile'); ?></a></li>
              <?php if(is_staff_member()){ ?>
               <li class="header-newsfeed">
                    <a href="#" class="open_newsfeed">
                        <?php echo _l('whats_on_your_mind'); ?>
                    </a>
                 </li>
               <?php } ?>
               <li class="header-logout"><a href="#" onclick="logout(); return false;"><?php echo _l('nav_logout'); ?></a></li>
            </ul>
         </div>
      </div>
      <ul class="nav navbar-nav navbar-right">
         <?php
            if(!is_mobile()){
              echo $top_search_area;
            } ?>
         <?php do_action('after_render_top_search'); ?>
         <li class="icon header-user-profile" data-toggle="tooltip" title="<?php echo get_staff_full_name(); ?>" data-placement="bottom">
            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="false">
            <?php echo staff_profile_image($current_user->staffid,array('img','img-responsive','staff-profile-image-small','pull-left')); ?>
            </a>
            <ul class="dropdown-menu animated fadeIn">
               <li class="header-my-profile"><a href="<?php echo admin_url('profile'); ?>"><?php echo _l('nav_my_profile'); ?></a></li>
               
               <?php if($this->session->userdata('staff_role_id') ==5){ ?>
			   <li class="header-my-timesheets"><a href="<?php echo admin_url('staff/kycform'); ?>"><?php echo _l('KYC Form'); ?></a></li>
               
			   <?php  } ?>
               
               
               <li class="header-my-timesheets"><a href="<?php echo admin_url('staff/timesheets'); ?>"><?php echo _l('my_timesheets'); ?></a></li>
               <li class="header-edit-profile"><a href="<?php echo admin_url('staff/edit_profile'); ?>"><?php echo _l('nav_edit_profile'); ?></a></li>
               <!--<?php if(get_option('disable_language') == 0){ ?>
               <li class="dropdown-submenu pull-left header-languages">
                  <a href="#" tabindex="-1"><?php echo _l('language'); ?></a>
                  <ul class="dropdown-menu dropdown-menu">
                     <li class="<?php if($current_user->default_language == ""){echo 'active';} ?>"><a href="<?php echo admin_url('staff/change_language'); ?>"><?php echo _l('system_default_string'); ?></a></li>
                     <?php foreach($this->app->get_available_languages() as $user_lang) { ?>
                     <li<?php if($current_user->default_language == $user_lang){echo ' class="active"';} ?>>
                        <a href="<?php echo admin_url('staff/change_language/'.$user_lang); ?>"><?php echo ucfirst($user_lang); ?></a>
                        <?php } ?>
                  </ul>
               </li>
               <?php } ?>-->
               <li class="header-logout">
                  <a href="#" id="logout" onclick="logout(); return false;"><?php echo _l('nav_logout'); ?></a>
               </li>
            </ul>
         </li>
         <?php if(is_staff_member()){ ?>
         <li class="icon header-newsfeed">
            <a href="#" class="open_newsfeed" data-toggle="tooltip" title="<?php echo _l('whats_on_your_mind'); ?>" data-placement="bottom"><i class="fa fa-share fa-fw fa-lg" aria-hidden="true"></i></a>
         </li>
         <?php } ?>
        <?php if($this->session->userdata('staff_role_id') !=5){ ?>
         <li class="icon header-todo">
            <a href="<?php echo admin_url('todo'); ?>" data-toggle="tooltip" title="<?php echo _l('nav_todo_items'); ?>" data-placement="bottom"><i class="fa fa-check-square-o fa-fw fa-lg"></i>
            <span class="label bg-warning icon-total-indicator nav-total-todos<?php if($current_user->total_unfinished_todos == 0){echo ' hide';} ?>"><?php echo $current_user->total_unfinished_todos; ?></span>
            </a>
         </li>
         <li class="icon header-timers timer-button" data-placement="bottom" data-toggle="tooltip" data-title="<?php echo _l('my_timesheets'); ?>">
            <a href="#" id="top-timers" class="dropdown-toggle top-timers" data-toggle="dropdown">
            <i class="fa fa-clock-o fa-fw fa-lg" aria-hidden="true"></i>
            <span class="label bg-success icon-total-indicator icon-started-timers<?php if ($totalTimers = count($startedTimers) == 0){ echo ' hide'; }?>">
            <?php echo count($startedTimers); ?>
            </span>
            </a>
            <ul class="dropdown-menu animated fadeIn started-timers-top width350" id="started-timers-top">
               <?php $this->load->view('admin/tasks/started_timers',array('startedTimers'=>$startedTimers)); ?>
            </ul>
         </li>
         <li class="dropdown notifications-wrapper header-notifications" data-toggle="tooltip" title="<?php echo _l('nav_notifications'); ?>" data-placement="bottom">
            <?php $this->load->view('admin/includes/notifications'); ?>
         </li>
      </ul>
         <?php  } ?>
   </nav>
</div>
<div id="mobile-search" class="<?php if(!is_mobile()){echo 'hide';} ?>">
   <ul>
      <?php
         if(is_mobile()){
           echo $top_search_area;
         } ?>
   </ul>
</div>


<script>
var inactivityTime = function () {
    var time;
    window.onload = resetTimer;
    document.onmousemove = resetTimer;
    document.onkeypress = resetTimer;
    document.onclick = resetTimer;     
    document.onscroll = resetTimer; 

    function logout() {
    $('#logout').trigger('click');
      }
    function resetTimer() {
        clearTimeout(time);
        time = setTimeout(logout, 900000)
    }
};

    inactivityTime(); 
</script>
<!--<script type="text/javascript">
if (document.layers) {
    //Capture the MouseDown event.
    document.captureEvents(Event.MOUSEDOWN);
 
    //Disable the OnMouseDown event handler.
    document.onmousedown = function () {
        return false;
    };
}
else {
    //Disable the OnMouseUp event handler.
    document.onmouseup = function (e) {
        if (e != null && e.type == "mouseup") {
            //Check the Mouse Button which is clicked.
            if (e.which == 2 || e.which == 3) {
                //If the Button is middle or right then disable.
                return false;
            }
        }
    };
}
 
//Disable the Context Menu event.
document.oncontextmenu = function () {
    return false;
};
</script>
<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>-->
