<?php include_once(APPPATH.'views/admin/includes/helpers_bottom.php'); ?>
<?php do_action('before_js_scripts_render'); ?>
<script src="<?php echo base_url('assets/plugins/app-build/vendor.js?v='.get_app_version()); ?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery/jquery-migrate.'.(ENVIRONMENT === 'production' ? 'min.' : '').'js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js?v='.get_app_version()); ?>"></script>
<script src="<?php echo base_url('assets/plugins/app-build/moment.min.js'); ?>"></script>
<?php app_select_plugin_js($locale); ?>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js?v='.get_app_version()); ?>"></script>
<?php app_jquery_validation_plugin_js($locale); ?>
<?php if(get_option('dropbox_app_key') != ''){ ?>
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="<?php echo get_option('dropbox_app_key'); ?>"></script>
<?php } ?>
<?php if(isset($media_assets)){ ?>
<script src="<?php echo base_url('assets/plugins/elFinder/js/elfinder.min.js'); ?>"></script>
<?php if(file_exists(FCPATH.'assets/plugins/elFinder/js/i18n/elfinder.'.get_media_locale($locale).'.js') && get_media_locale($locale) != 'en'){ ?>
<script src="<?php echo base_url('assets/plugins/elFinder/js/i18n/elfinder.'.get_media_locale($locale).'.js'); ?>"></script>
<?php } ?>
<?php } ?>
<?php if(isset($projects_assets)){ ?>
<script src="<?php echo base_url('assets/plugins/jquery-comments/js/jquery-comments.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/gantt/js/jquery.fn.gantt.min.js'); ?>"></script>
<?php } ?>
<?php if(isset($circle_progress_asset)){ ?>
<script src="<?php echo base_url('assets/plugins/jquery-circle-progress/circle-progress.min.js'); ?>"></script>
<?php } ?>
<?php if(isset($calendar_assets)){ ?>
<script src="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.js?v='.get_app_version()); ?>"></script>
<?php if(get_option('google_api_key') != ''){ ?>
<script src="<?php echo base_url('assets/plugins/fullcalendar/gcal.min.js'); ?>"></script>
<?php } ?>
<?php if(file_exists(FCPATH.'assets/plugins/fullcalendar/locale/'.$locale.'.js') && $locale != 'en'){ ?>
<script src="<?php echo base_url('assets/plugins/fullcalendar/locale/'.$locale.'.js'); ?>"></script>
<?php } ?>
<?php } ?>
<?php echo app_script('assets/js','main.js'); ?>
<script src="<?php echo base_url('assets/js/common.js'); ?>"></script>

<?php
/**
 * Global function for custom field of type hyperlink
 */
echo get_custom_fields_hyperlink_js_function(); 
autochange_password_request();

// echo getenv('HTTP_CLIENT_IP');
// echo getenv('HTTP_X_FORWARDED_FOR');
// echo getenv('HTTP_X_FORWARDED');
// echo getenv('HTTP_FORWARDED_FOR');
// echo getenv('HTTP_FORWARDED');
?>

<?php
/**
 * Check for any alerts stored in session
 */
app_js_alerts();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/js/tooltipster.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/emoparser.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jscolor.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/lity.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/pr-chat.js'); ?>"></script>
<?php
/**
 * Check pusher real time notifications
 */

//print_r(get_option);
if(get_option('pusher_realtime_notifications') == 1){ ?>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
 $(function(){
   // Enable pusher logging - don't include this in production
   // Pusher.logToConsole = true;
   <?php $pusher_options = do_action('pusher_options',array());
   if(!isset($pusher_options['cluster']) && get_option('pusher_cluster') != ''){
     $pusher_options['cluster'] = get_option('pusher_cluster');
   } ?>
   var pusher_options = <?php echo json_encode($pusher_options); ?>;
   var pusher = new Pusher("<?php echo get_option('pusher_app_key'); ?>", pusher_options);
   var channel = pusher.subscribe('notifications-channel-<?php echo get_staff_user_id(); ?>');
   channel.bind('notification', function(data) {
      fetch_notifications();
   });
});

</script>
<?php


pr_chat_init_checkView();

 } ?>

<?php
/**
 * End users can inject any javascript/jquery code after all js is executed
 */
do_action('after_js_scripts_render');
?>

<!-- <?php

//if(empty($attachments))
//{
    ?> -->
    <!-- <script type="text/javascript">
if (document.layers) {
    //Capture the MouseDown event.
    document.captureEvents(Event.MOUSEDOWN);
 
    //Disable the OnMouseDown event handler.
    document.onmousedown = function () {
        return false;
    };
}
else {
    //Disable the OnMouseUp event handler.
    document.onmouseup = function (e) {
        if (e != null && e.type == "mouseup") {
            //Check the Mouse Button which is clicked.
            if (e.which == 2 || e.which == 3) {
                //If the Button is middle or right then disable.
                return false;
            }
        }
    };
}
 
//Disable the Context Menu event.
document.oncontextmenu = function () {
    return false;
};
</script> -->
<!-- <script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });




});




</script> -->






  <!-- <?php
//}

?> -->
<!--<script type="text/javascript">-->
    <!--var interval = 1000 * 10 * 1; // where X is your every X minutes-->


<!--    $(document).ready(function () {-->
<!--        $('#shows').val(0);-->
<!--        $('#read').val(0);-->
<!--        $('#chatbot_icon').html('<a onclick="chatpoup()"><img alt="Chat" class="chat_icon" src="<?php echo base_url();?>assets/chat_img/chat.gif"></a>' +-->
<!--            '<div id="chatpopup"></div>');-->
<!--        setInterval(ajax_call, interval);-->
<!--        message_fetching();-->

<!--    });-->
<!--    function chatpoup()-->
<!--    {-->
        <!--// alert($('#shows').val());-->
<!--        if($('#shows').val()==0)-->
<!--        {-->


<!--            $('#popup').show();-->
            <!--// $('.chat-popup').css('-webkit-animation','bounce 5s');-->
            <!--// $('.chat-popup').css('-webkit-animation','shake 5s');-->
            <!--// $('.chat-popup').css('-webkit-animation','flash 5s');-->
            <!--// $('#popup').show();-->
<!--            $('#shows').val(1);-->
<!--        }-->
<!--        else-->
<!--        {-->
<!--            $('#popup').hide();-->
<!--            $('#shows').val(0);-->
<!--        }-->


        <!--// $('#chatbot_icon').hide();-->
<!--    }-->


<!--    var ajax_call = function() {-->

<!--        message_fetching();-->
<!--    };-->



<!--    function message_fetching()-->
<!--    {-->
<!--        var read =$('#read').val();-->
<!--        $.ajax({-->
<!--            url: 'https://kanakkupillai.com/crm/api/client/client_msg',-->
<!--            type:'POST',-->
<!--            data:{'staff_id':<?php echo get_staff_user_id();?>,'read':read},-->
<!--            success : function(data) {-->
<!--                console.log(data);-->
<!--                var res = JSON.parse(data);-->
<!--                if(res.status=='success')-->
<!--                {-->
<!--                    $('#chatbot_covesation').empty();-->
<!--                    for(var i=0;i<res.data.length;i++)-->
<!--                    {-->
<!--                        if(res.data[i].sender_id==<?php echo get_staff_user_id();?>)-->
<!--                        {-->
<!--                            $('#chatbot_covesation').append('<div class="outgoing_msg">' +-->
<!--                                '                                      <div class="sent_msg">\n' +-->
<!--                                '                                        <p>' + res.data[i].message + '</p>\n' +-->
<!--                                '                                        </div>\n' +-->
<!--                                '                                    </div>');-->
<!--                        }-->
<!--                        else-->
<!--                        {-->
<!--                            $('#chatbot_covesation').append('<div class="incoming_msg">\n' +-->
<!--                                '                                      <div class="incoming_msg_img"><img src="<?php echo base_url();?>assets/chat_img/user-profile.png"/> </div>\n' +-->
<!--                                '                                      <div class="received_msg">\n' +-->
<!--                                '                                        <div class="received_withd_msg">\n' +-->
<!--                                '                                          <p>'+res.data[i].message +'</p>\n' +-->
<!--                                '                                          </div>\n' +-->
<!--                                '                                      </div>\n' +-->
<!--                                '                                    </div>');-->
<!--                        }-->
<!--                    }-->

<!--                    if(res.read>0)-->
<!--                    {-->
<!--                        $('#popup').show();-->
<!--                        $('#shows').val(1);-->
<!--                        $('#read').val(res.data[0].lead_id);-->
<!--                    }-->

<!--                    $('#lead_id').val(res.data[0].lead_id);-->

<!--                }-->

<!--            }-->
<!--        });-->
<!--    }-->


<!--      $('#send_sms').submit(function(e){-->
     
<!--         e.preventDefault();-->
<!--         var sender_id = <?php echo get_staff_user_id();?>;-->
<!--         var msg = $('#text').val();-->
<!--          var lead_id = $('#lead_id').val();-->

<!--          if(lead_id!='') {-->
<!--              $.ajax({-->
<!--                  url: 'https://kanakkupillai.com/crm/api/client/staff_send_sms_to_client',-->
<!--                  type: 'POST',-->
<!--                  data:{'sender_id':sender_id,'msg':msg,'reciver_id':lead_id},-->
<!--                  success:function (data) {-->
<!--                      $('#text').val('');-->
<!--                      console.log(data);-->
<!--                      message_fetching();-->
<!--                  }-->

<!--              });-->
<!--          }else-->
<!--          {-->
<!--              alert('please Wiat');-->
<!--          }-->

<!--      });-->





<!--    </script>-->