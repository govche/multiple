<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">

				<div class="panel_s">
					<div class="panel-body">
						<?php if(has_permission('staff','','create')){ ?>
						<div class="_buttons">
							<a href="<?php echo admin_url('staff/member'); ?>" class="btn btn-info pull-left display-block"><?php echo _l('new_staff'); ?></a>
						</div>
						
												<?php echo form_open('admin/staff'); ?>
						<select style="width:150px;height:33px;margin-left:10px;font-size:15px" id="staff_selected"  name="staff_selected">
                        <option value="all" <?php if($this->session->userdata('staff') == ''){ echo 'selected'; } ?>>All</option>
						<option value="staff" <?php if($this->session->userdata('staff') == 'staff'){ echo 'selected'; } ?>>Staff</option>
                        <option value="affiliate" <?php if($this->session->userdata('staff') == 'affiliate'){ echo 'selected'; } ?>>Affiliate</option>
						
						
						</select>
						<input type="submit" value="submit" style="display:none" id="submit">
						<?php echo form_close(); ?>
						
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php } ?>
						<div class="clearfix"></div>
						<?php
						$table_data = array(
							_l('staff_dt_name'),
							_l('staff_dt_email'),
							_l('staff_dt_last_Login'),
							_l('staff_dt_active'),
							_l('Staff Services'),
							);
						$custom_fields = get_custom_fields('staff',array('show_on_table'=>1));
						foreach($custom_fields as $field){
							array_push($table_data,$field['name']);
						}
						render_datatable($table_data,'staff');
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="staffspecial"></div>
<div class="modal fade" id="delete_staff" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<?php echo form_open(admin_url('staff/delete',array('delete_staff_form'))); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo _l('delete_staff'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="delete_id">
					<?php echo form_hidden('id'); ?>
				</div>
				<p><?php echo _l('delete_staff_info'); ?></p>
				<?php
				echo render_select('transfer_data_to',$staff_members,array('staffid',array('firstname','lastname')),'staff_member',get_staff_user_id(),array(),array(),'','',false);
				?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-danger _delete"><?php echo _l('confirm'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		initDataTable('.table-staff', window.location.href);
		
		  $("#staff_selected").change(function(){
	  $('#submit').trigger('click');
  })
	});
	function delete_staff_member(id){
		$('#delete_staff').modal('show');
		$('#transfer_data_to').find('option').prop('disabled',false);
		$('#transfer_data_to').find('option[value="'+id+'"]').prop('disabled',true);
		$('#delete_staff .delete_id input').val(id);
		$('#transfer_data_to').selectpicker('refresh');
	}
	function add(id)
{
var id=id;
$.ajax({

url:"<?php echo admin_url(); ?>Staff/specialistadd",
method:"post",
data:{id:id},
success:function(data)
{
$('#staffspecial').html(data);
$('#speciallmodel').modal({
show : true,


});
$('#specialist').selectpicker('refresh');



}

});

}
</script>
</body>
</html>
