<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-7">
                <div class="panel_s">
                    <div class="panel-body">
                        <h4 class="no-margin">
                           <?php echo $title; ?>
                       </h4>
                       <hr class="hr-panel-heading" />
                       <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'staff_profile_table','autocomplete'=>'off')); ?>
                       <?php if(total_rows('tblemailtemplates',array('slug'=>'two-factor-authentication','active'=>0)) == 0){ ?>
                      
                     
                     <?php } ?>
					 
					 <div class="form-group">
                        <label for="PAN_No" class="control-label"><?php echo _l('PAN NO'); ?></label>
                        <input type="text" class="form-control" name="PAN_No" value="<?php if(isset($member)){echo $member;} ?>">
                    </div>
					<br>
					 
					 <div class="form-group">
                        <label for="aff_address" class="control-label"><?php echo _l('Present Address'); ?></label>
                       
						 <input type="text" class="form-control" name="aff_address" value="<?php if(isset($member1)){echo $member1;} ?>">
                    </div>
					<br>
					 
					
                   <?php if($current_user->address_image == NULL){ ?>
                     <div class="form-groupd">
                        <label for="address_image" class="profile-image"><?php echo _l('Address Proof Image'); ?></label>
                        <input type="file" name="address_image" class="form-control" id="address_image">
                    </div>
                   <?php } ?>
                   <?php if($current_user->address_image != NULL){ ?>
                    <div class="form-groupd">
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo "ADDRESS PROOF" ?>
                                <a href="<?php echo admin_url('staff/edit_profile'); ?>"><i class="fa fa-edit"></i></a>
                            <br>
                                <?php echo staff_address_image($current_user->staffid,array('img','img-responsive','staff-profile-image-thumb'),'thumb'); ?>
                            </div>
                            
                       
                   
                    <?php } ?>
					
					
					
                    <?php if($current_user->profile_image == NULL){ ?>
                     <div class="form-groupd">
                        <label for="profile_image" class="profile-image"><?php echo _l('staff_edit_profile_image'); ?></label>
                        <input type="file" name="profile_image" class="form-control" id="profile_image">
                    </div>
                    <?php } ?>
                    <?php if($current_user->profile_image != NULL){ ?>
                    <div class="form-groupd">
                       
                               <?php echo "UPLOADED PHOTO" ?>
                                <a href="<?php echo admin_url('staff/edit_profile'); ?>"><i class="fa fa-edit"></i></a>
                            <br>
                                <?php echo staff_kyc_profile_image($current_user->staffid,array('img','img-responsive','staff-profile-image-thumb'),'thumb'); ?>
                            </div>
                            <!--<div class="col-md-3 text-right">
                                <a href="<?php echo admin_url('staff/edit_profile'); ?>"><i class="fa fa-edit"></i></a>
                            </div>-->
                       
                    <?php } ?>
                   
                  
                   </div>
                     </div> 
                 
               
             <button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
             <?php echo form_close(); ?>
         </div>
     </div>
 </div>
 
</div>
</div>
</div>
<?php init_tail(); ?>
<script>
 $(function(){
   _validate_form($('#staff_profile_table'),{PAN_No:'required',address_image:'required',profile_image:'required'});
   //_validate_form($('#staff_password_change_form'),{oldpassword:'required',newpassword:'required',newpasswordr: { equalTo: "#newpassword"}});
 });
</script>
</body>
</html>
