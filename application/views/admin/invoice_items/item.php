 <style>
  
		.modal-sm1 {
		position:absolute;
  top:50% !important;
  transform: translate(0, -50%) !important;
  -ms-transform: translate(0, -50%) !important;
  -webkit-transform: translate(0, -50%) !important;
  margin:auto 35%;
  width:30%;
  
		}
		

  </style>



<div class="modal fade" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-sm1" role="document" style="width:60%;  margin: auto 25%; padding:5px 5px 5px 5px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('invoice_item_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('invoice_item_add_heading'); ?></span>
                </h4>
            </div>
            <?php echo form_open('admin/invoice_items/manage',array('id'=>'invoice_item_form')); ?>
            <?php echo form_hidden('itemid'); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                     <div class="row">
                         <div class="alert alert-warning affect-warning hide" style="margin:0px 20px 0px 20px">
                            <?php echo _l('changing_items_affect_warning'); ?>
                        </div>
                            <div class="col-md-6">

                        <?php echo render_input('description','invoice_item_add_edit_description'); ?>
                        <?php echo render_input('long_description','invoice_item_long_description'); ?>


                         <div  class="form-group">
                        <label for="rate1" class="control-label">
                            <?php echo _l('Internal Estimate days') ?></label>
                            <input type="number" name="beforehand" class="form-control" value="">
                        </div>


                        <div style="display:none"class="form-group">
                            <?php echo render_select('frequency',array(array('data' => 1),array('data' => 2)),array('data','data'),'Frequency'); ?>

                        </div>
                        <div class="form-group">
                        <label for="rate" class="control-label">
                            <?php echo _l('invoice_item_add_edit_rate_currency',$base_currency->name . ' <small>('._l('base_currency_string').')</small>'); ?></label>
                            <input type="number" id="rate" name="rate" class="form-control" value="">
                        </div>
                    </div>
                     <div class="col-md-6">
                        <?php
                            foreach($currencies as $currency){
                                if($currency['isdefault'] == 0 && total_rows('tblclients',array('default_currency'=>$currency['id'])) > 0){ ?>
                                <div class="form-group">
                                    <label for="rate_currency_<?php echo $currency['id']; ?>" class="control-label">
                                        <?php echo _l('invoice_item_add_edit_rate_currency',$currency['name']); ?></label>
                                        <input type="number" id="rate_currency_<?php echo $currency['id']; ?>" name="rate_currency_<?php echo $currency['id']; ?>" class="form-control" value="">
                                    </div>
                             <?php   }
                            }
                        ?>
                    </div>
                       
                            <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="tax"><?php echo _l('tax_1'); ?></label>
                                <select class="selectpicker display-block" data-width="80%" name="tax" data-none-selected-text="<?php echo _l('no_tax'); ?>">
                                    <option value=""></option>
                                    <?php foreach($taxes as $tax){ ?>
                                    <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div style="display:none" class="col-md-6">
                         <div class="form-group">
                            <label class="control-label" for="tax2"><?php echo _l('tax_2'); ?></label>
                            <select class="selectpicker display-block" disabled data-width="80%" name="tax2" data-none-selected-text="<?php echo _l('no_tax'); ?>">
                                <option value=""></option>
                                <?php foreach($taxes as $tax){ ?>
                                <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
               <div style="display:none" class="col-md-6">
                  <?php $value = (isset($estimate) ? _d($estimate->date) : _d(date('Y-m-d'))); ?>
                  <?php echo render_date_input('renewal_date','Renewal Date',$value); ?>
               </div>

                <div class="col-md-6">
                        <label for="rate1" class="control-label">
                            <?php echo _l('Estimate Days') ?></label>
                            <input type="number" name="estimate_days" class="form-control" value="">
                        </div>

					
                
           <div class="col-md-6">
               
				  <?php $value ="1" ; ?>
				
                <?php echo render_input('unit','unit',$value); ?>
                <div id="custom_fields_items">
                    <?php echo render_custom_fields('items'); ?>
                </div>

                <div style="width:80%;">
                <?php echo render_select('group_id',$items_groups,array('id','name'),'item_group'); ?>
                </div>
</div>
 <div class="col-md-12" style="overflow-y: scroll; height:150px; width: 98%;">
   <br /> 
 <h5>Select Compliance:</h5>
  
<?php foreach($compliance as $item) { ?>
<div class="col-md-3" style="padding-bottom:10px; "> 
<input type="checkbox" name="compliance[]" value="<?php echo $item['id']; ?>"  id="compliance-<?php echo $item['id']; ?>"><?php echo $item['name']; ?>
</div>

<?php  } ?>

</div>
<div class="col-md-8">
<div class="checkbox checkbox-primary">
<input type="checkbox" name="send_created_email" id="send_created_email">
<label for="send_created_email"><?php echo _l('Send Item Created Email'); ?></label>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>

<?php $com =json_encode($compliance);  ?>
<script>


    // Maybe in modal? Eq convert to invoice or convert proposal to estimate/invoice
    if(typeof(jQuery) != 'undefined'){
        init_item_js();
    } else {
     window.addEventListener('load', function () {
       var initItemsJsInterval = setInterval(function(){
            if(typeof(jQuery) != 'undefined') {
                init_item_js();
                clearInterval(initItemsJsInterval);
            }
         }, 1000);
     });
  }



// Items add/edit
function manage_invoice_items(form) {
    var data = $(form).serialize();

    var url = form.action;
    $.post(url, data).done(function (response) {
        response = JSON.parse(response);
        if (response.success == true) {
            var item_select = $('#item_select');
            if ($("body").find('.accounting-template').length > 0) {
                if (!item_select.hasClass('ajax-search')) {
                    var group = item_select.find('[data-group-id="' + response.item.group_id + '"]');
                    if (group.length == 0) {
                        var _option = '<optgroup label="' + (response.item.group_name == null ? '' : response.item.group_name) + '" data-group-id="' + response.item.group_id + '">' + _option + '</optgroup>';
                        if (item_select.find('[data-group-id="0"]').length == 0) {
                            item_select.find('option:first-child').after(_option);
                        } else {
                            item_select.find('[data-group-id="0"]').after(_option);
                        }
                    } else {
                        group.prepend('<option data-subtext="' + response.item.long_description + '" value="' + response.item.itemid + '">(' + accounting.formatNumber(response.item.rate) + ') ' + response.item.description + '</option>');
                    }
                }
                if (!item_select.hasClass('ajax-search')) {
                    item_select.selectpicker('refresh');
                } else {

                    item_select.contents().filter(function () {
                        return !$(this).is('.newitem') && !$(this).is('.newitem-divider');
                    }).remove();

                    var clonedItemsAjaxSearchSelect = item_select.clone();
                    item_select.selectpicker('destroy').remove();
                    $("body").find('.items-select-wrapper').append(clonedItemsAjaxSearchSelect);
                    init_ajax_search('items', '#item_select.ajax-search', undefined, admin_url + 'items/search');
                }

                add_item_to_preview(response.item.itemid);
            } else {
                // Is general items view
                $('.table-invoice-items').DataTable().ajax.reload(null, false);
            }
            alert_float('success', response.message);
        }
        $('#sales_item_modal').modal('hide');
    }).fail(function (data) {
        alert_float('danger', data.responseText);
    });
    return false;
}
function init_item_js() {
     // Add item to preview from the dropdown for invoices estimates
    $("body").on('change', 'select[name="item_select"]', function () {
        var itemid = $(this).selectpicker('val');
        if (itemid != '') {
            add_item_to_preview(itemid);
        }
    });

    // Items modal show action
    $("body").on('show.bs.modal', '#sales_item_modal', function (event) {

        $('.affect-warning').addClass('hide');

        var $itemModal = $('#sales_item_modal');
        $('input[name="itemid"]').val('');
        $itemModal.find('input').not('input[type="hidden"]').val('');
        $itemModal.find('textarea').val('');
        $itemModal.find('select').selectpicker('val', '').selectpicker('refresh');
        $('select[name="tax2"]').selectpicker('val', '').change();
        $('select[name="tax"]').selectpicker('val', '').change();
        $itemModal.find('.add-title').removeClass('hide');
        $itemModal.find('.edit-title').addClass('hide');

var compliance=<?php echo $com; ?>;
        for(var i=0; i < compliance.length; i++){
            $('#compliance-'+compliance[i].id).val(compliance[i].id);
        }



        var id = $(event.relatedTarget).data('id');
        // If id found get the text from the datatable
        if (typeof (id) !== 'undefined') {

            $('.affect-warning').removeClass('hide');
            $('input[name="itemid"]').val(id);

            requestGetJSON('invoice_items/get_item_by_id_service/' + id).done(function (response) {
             
         var compliance=<?php echo $com; ?>;
        for(var i=0; i < compliance.length; i++){
            $('#compliance-'+compliance[i].id).val(compliance[i].id);
            for(var j=0; j < response.service.length; j++ ){
               if(compliance[i].id == response.service[j].id){
                   $('#compliance-'+compliance[i].id).attr('checked',true);
                 
               }

            }

        }
                $itemModal.find('input[name="description"]').val(response.items.description);
                $itemModal.find('input[name="long_description"]').val(response.items.long_description.replace(/(<|<)br\s*\/*(>|>)/g, " "));
                $itemModal.find('input[name="rate"]').val(response.items.rate);
                $itemModal.find('input[name="unit"]').val(response.items.unit);
                $itemModal.find('input[name="beforehand"]').val(response.items.beforehand);
                $('select[name="frequency"]').selectpicker('val', response.items.frequency).change();
                // $itemModal.find('input[name="frequency"]').val(response.frequency);
                $itemModal.find('input[name="renewal_date"]').val(response.items.renewal_date);
                $itemModal.find('input[name="estimate_days"]').val(response.items.estimate_days);
                $('select[name="tax"]').selectpicker('val', response.items.taxid).change();
                $('select[name="tax2"]').selectpicker('val', response.items.taxid_2).change();
                $itemModal.find('#group_id').selectpicker('val', response.items.group_id);
               
                $.each(response, function (column, value) {
                    if (column.indexOf('rate_currency_') > -1) {
                        $itemModal.find('input[name="' + column + '"]').val(value);
                    }
                });

                $('#custom_fields_items').html(response.items.custom_fields_html);

                init_selectpicker();
                init_color_pickers();
                init_datepicker();

                $itemModal.find('.add-title').addClass('hide');
                $itemModal.find('.edit-title').removeClass('hide');
                validate_item_form();
           
            });

 

        }
    });

    $("body").on("hidden.bs.modal", '#sales_item_modal', function (event) {
        $('#item_select').selectpicker('val', '');
    });

   validate_item_form();
}
function validate_item_form(){
    // Set validation for invoice item form
    _validate_form($('#invoice_item_form'), {
        description: 'required',
        rate: {
            required: true,
        }
    }, manage_invoice_items);
}
</script>
