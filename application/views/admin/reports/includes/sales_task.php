    <div id="invoicesedtask-report" class="hide">
      <div class="row">
         <div class="col-md-4">
            <div class="form-group">
               <label for="invoice_status"><?php echo _l('report_invoice_status'); ?></label>
               <select name="invoice_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('invoice_status_report_all'); ?>">
                  <?php foreach($invoice_statuses as $status){ if($status ==5){continue;} ?>
                  <option value="<?php echo $status; ?>"><?php echo format_invoice_status($status,'',false) ?></option>
                  <?php } ?>
               </select>
            </div>
         </div>
         <?php if(count($invoices_sale_agents) > 0 ) { ?>
         <div class="col-md-4">
              <?php   if (has_permission('reports', '', 'view') || is_admin())  { ?>
            <div class="form-group">
               <label for="sale_agent_invoices"><?php echo _l('sale_agent_string'); ?></label>
               <select name="sale_agent_invoices" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('invoice_status_report_all'); ?>">
                  <?php foreach($invoices_sale_agents as $agent){ ?>
                  <option value="<?php echo $agent['sale_agent']; ?>"><?php echo get_staff_full_name($agent['sale_agent']); ?></option>
                  <?php } ?>
               </select>
            </div>
            <?php }?>
         </div>
         <?php } ?>
         <div class="clearfix"></div>
      </div>
         <table class="table table-invoicesedtask-report scroll-responsive">
            <thead>
               <tr>
                  <th><?php echo _l('report_invoice_number'); ?></th>
				 <th><?php echo _l('report_invoice_customer'); ?></th>
                  <th><?php echo _l('year'); ?></th>
                  <th><?php echo _l('Task No'); ?></th>
                  <th><?php echo _l('Invoice Date'); ?></th>
                  <th><?php echo _l('report_invoice_duedate'); ?></th>
				  <th>Agent name</th>
               <th>Service name</th>
                
                 <th>Task Assigned To</th> 
                  
				 <th>Status Of Task</th>
				 
				  <th>Date of Completion</th>
				  <th>No. of days delayed</th>
				  <th>Last Modified Date</th>
                
                  
               </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
               <tr>
                  <td></td>
				  <td></td>
                  <td></td>
                  <td></td>
                
                
                  <td></td>
				   <td></td>
				  <td></td>
				  <td></td><td></td>
				   <td></td> <td></td>
              <td></td>
              <td></td>
               </tr>
            </tfoot>
         </table>
   </div>
