<div id="payments-received-report" class="hide">
      <table class="table table-payments-received-report scroll-responsive">
         <thead>
            <tr>
              <th><?php echo _l('payments_table_number_heading'); ?></th>
               <th><?php echo _l('payments_table_date_heading'); ?></th>
               <th><?php echo _l('payments_table_invoicenumber_heading'); ?></th>
               <th>Ledger Name</th>
			   <th>Amount Received</th>
			   <th>Agent Name</th>
			   <!--<th>Service Name</th>-->
			   <!--<th>Service Value</th>-->
			   <!--<th>CGST</th>-->
			   <!--<th>SGST</th>-->
			   <th>PhoneNumber</th>
			   <th>Email</th>
               <th><?php echo _l('payments_table_mode_heading'); ?></th>
               <th><?php echo _l('payment_transaction_id'); ?></th>
               <th><?php echo _l('note'); ?></th>
               <th>Approved Status</th>
			   <th>Approved By</th>
            </tr>
         </thead>
         <tbody></tbody>
         <tfoot>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
     <!--       <td></td>-->
			  <!--<td></td>-->
			   <td></td>
            <td class="total"></td>
			<!--<td></td>-->
			<!--<td></td>-->
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
         </tfoot>
      </table>
   </div>
