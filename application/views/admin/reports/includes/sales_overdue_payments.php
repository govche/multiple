    <div id="invoicesed-report" class="hide">
      <div class="row" style="display:none">
         <div class="col-md-4">
            <div class="form-group">
               <label for="invoice_status"><?php echo _l('report_invoice_status'); ?></label>
               <select name="invoice_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('invoice_status_report_all'); ?>">
                  <?php foreach($invoice_statuses as $status){ if($status ==5){continue;} ?>
                  <option value="<?php echo $status; ?>"><?php echo format_invoice_status($status,'',false) ?></option>
                  <?php } ?>
               </select>
            </div>
         </div>
         <?php if(count($invoices_sale_agents) > 0 ) { ?>
         <div class="col-md-4">
            <div class="form-group">
               <label for="sale_agent_invoices"><?php echo _l('sale_agent_string'); ?></label>
               <select name="sale_agent_invoices" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('invoice_status_report_all'); ?>">
                  <?php foreach($invoices_sale_agents as $agent){ ?>
                  <option value="<?php echo $agent['sale_agent']; ?>"><?php echo get_staff_full_name($agent['sale_agent']); ?></option>
                  <?php } ?>
               </select>
            </div>
         </div>
         <?php } ?>
         <div class="clearfix"></div>
      </div>
         <table class="table table-invoicesed-report scroll-responsive">
            <thead>
                <tr>
			   
                  <th><?php echo _l('report_invoice_number'); ?></th>
                  <th><?php echo _l('report_invoice_customer'); ?></th>
				   
                  <th><?php echo _l('invoice_estimate_year'); ?></th>
				  <th><?php echo _l('report_invoice_date'); ?></th>
					<th>Service name</th>
					<th>Descriptions</th>
				<th>BDE name</th>
					  <th>Taxable Value</th>
					 <th>CGST</th>
				  <th>SGST</th>
				  <th>IGST</th>
				   <th>Invoice Value</th>
				   <th>Payment received till Date</th>
				    <th>Outstanding balance</th>
					 <th>Project Assigned TO </th>
                <th>Task and Status</th>
                  <th><?php echo _l('report_invoice_duedate'); ?></th>
				 <th>Project completion Status</th>
                <th>Project completed date</th>
                  <th>Remarks</th>
					  <th>over due days</th>
                
                  <th><?php echo _l('report_invoice_status'); ?></th>
				 
				  
			    
               </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
               <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <!-- <td></td>  -->
				  <td></td>
				   <td></td>
				   <td></td>
                <td></td>
                  <td></td>
				   <td></td>
				  <td></td>
				  <td class="total"></td>
				  <td class="adjustment"></td>
				  <td class="subtotal"></td>
				   <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            <td></td>
            <td></td>
               </tr>
            </tfoot>
         </table>
   </div>
