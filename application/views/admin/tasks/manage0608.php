<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<?php init_head(); ?>
<style>
    /*.dropdown-toggle{*/
    /*    display: none!important;*/
    /*}*/
    
    </style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<link href="<?php echo APP_BASE_URL;?>/assets/css/task_css.css" rel="stylesheet">
  
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                <div class="row _buttons">
                     <div class="col-md-8">
                        <?php if(has_permission('tasks','','create')){ ?>
                        <a href="#" onclick="new_task(<?php if($this->input->get('project_id')){ echo "'".admin_url('tasks/task?rel_id='.$this->input->get('project_id').'&rel_type=project')."'";} ?>); return false;" class="btn btn-info pull-left new"><?php echo _l('new_task'); ?></a>
                        <?php } ?>
                        
                        
                        
						 <?php $custom_values= check_staff_manager($this->session->userdata('staff_user_id'));
                        
                        // print_r($custom_values);
                        $this->db->select('*');
                        $this->db->like('value',$custom_values);
                        $this->db->where('fieldid','141');
                        $this->db->where('fieldto','staff');
                        $result = $this->db->get('tblcustomfieldsvalues')->result_array();
                        ?>
                        <?php echo form_open('admin/tasks/list_tasks'); ?>
                        
                  
            
<?php if(!empty($result) || is_admin()) { ?>


						      <select style="width:300px;height:33px;margin-left:10px;font-size:15px" id="staff_selected"  name="staff_selected">
                       

                        <?php if(is_admin()) { ?>

<?php $this->db->select('*');
$this->db->where('active',1);
$this->db->where('admin !=',1);
$this->db->where('role !=',5);
   $staff1=  $this->db->get('tblstaff')->result_array();
  ?>
 <option selected disabled>Select Staff </option>
<?php  foreach($staff1 as $staff){ ?>
<option value="<?php echo $staff['staffid'] ?>"><?php echo $staff['firstname'].' '.$staff['lastname'];  ?></option>
<?php } } else{ ?>

   <option selected disabled>Select Your Department Staff </option>
                        <?php foreach($result as $rows){ 
                     
                        //   if($rows['value'] == $custom_values){
                            $this->db->where('staffid',$rows['relid']);
                           $staff = $this->db->get('tblstaff')->row_array();
                   
                           if($staff > 0){
                           ?>
                         <option value="<?php echo $staff['staffid'] ?>"><?php echo $staff['firstname'].' '.$staff['lastname'];  ?></option>
						     
                        <?php }  } } ?>
						      </select>
						      <input type="submit" value="submit" style="display:none" id="submit">
                           <?php } echo form_close(); ?>
						
						
                        
                        
                        
                        
                       <!--correct by rathina for hide switch to kanban-->
                       <div style="display:none">
                        <a href="<?php if(!$this->input->get('project_id')){ echo admin_url('tasks/switch_kanban/'.$switch_kanban); } else { echo admin_url('projects/view/'.$this->input->get('project_id').'?group=project_tasks'); }; ?>" class="btn btn-default mleft10 pull-left hidden-xs">
                           <?php if($switch_kanban == 1){ echo _l('switch_to_list_view');}else{echo _l('leads_switch_to_kanban');}; ?>
                        </a>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <?php if($this->session->has_userdata('tasks_kanban_view') && $this->session->userdata('tasks_kanban_view') == 'true') { ?>
                        <div data-toggle="tooltip" data-placement="bottom" data-title="<?php echo _l('search_by_tags'); ?>">
                           <?php echo render_input('search','','','search',array('data-name'=>'search','onkeyup'=>'tasks_kanban();','placeholder'=>_l('search_tasks')),array(),'no-margin') ?>
                        </div>
                        <?php } else { ?>
                        <?php $this->load->view('admin/tasks/tasks_filter_by',array('view_table_name'=>'.table-tasks')); ?>
                        <!--<a href="<?php echo admin_url('tasks/detailed_overview'); ?>" class="btn btn-success pull-right mright5">Filter</a>-->
                        <?php } ?>
                     </div>
                  </div>
                  <hr class="hr-panel-heading hr-10" />
                  <div class="clearfix"></div>
                  <?php
                  if($this->session->has_userdata('tasks_kanban_view') && $this->session->userdata('tasks_kanban_view') == 'true') { ?>
                  <div class="kan-ban-tab" id="kan-ban-tab" style="overflow:auto;">
                     <div class="row">
                        <div id="kanban-params">
                           <?php echo form_hidden('project_id',$this->input->get('project_id')); ?>
                        </div>
                        <div class="container-fluid">
                           <div id="kan-ban"></div>
                        </div>
                     </div>
                  </div>
                  <?php } else { ?>
                 <?php $this->load->view('admin/tasks/_summary',array('table'=>'.table-tasks')); ?>
                  <?php $this->load->view('admin/tasks/_task_list'); ?>
                  <!--<a href="#" data-toggle="modal" data-target="#tasks_bulk_actions" class="hide bulk-actions-btn table-btn" data-table=".table-tasks"><?php echo _l('bulk_actions'); ?></a>-->
                  <?php //$this->load->view('admin/tasks/_table',array('bulk_actions'=>true)); ?>
               </div>
               <?php //$this->load->view('admin/tasks/_bulk_actions'); ?>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>
<script>
   taskid = '<?php echo $taskid; ?>';
   $(function(){
       tasks_kanban();
   });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        
            $('#staff_selected').change(function(){
       $('#submit').trigger('click');
      });
        $("#myInput").on("keyup keypress blur change", function() {
            var value = $(this).val().toLowerCase();
            $("#testst1 time").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
</body>
</html>
