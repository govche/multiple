
<style>
    li.li_hover:hover {
        outline: 0 !important;
        background: #e4e8f1;
        margin-right: 20px;
        /*padding: 15px;*/
    }
    li.li_hover{
        height: 35px;
        margin-left: 17px;
    }
    </style>
<div class="_hidden_inputs _filters _tasks_filters">
    <?php
    $tasks_filter_assignees = $this->misc_model->get_tasks_distinct_assignees();
    do_action('tasks_filters_hidden_html');
    echo form_hidden('my_tasks',(!has_permission('tasks','','view') ? 'true' : ''));
    echo form_hidden('my_following_tasks');
    echo form_hidden('billable');
    echo form_hidden('billed');
    echo form_hidden('not_billed');
    echo form_hidden('not_assigned');
    echo form_hidden('due_date_passed');
    echo form_hidden('upcoming_tasks');
    echo form_hidden('recurring_tasks');
    echo form_hidden('today_tasks');

    /* Related task filter - used in customer profile */
    echo form_hidden('tasks_related_to');

    if(has_permission('tasks','','view')){
        foreach($tasks_filter_assignees as $tf_assignee){
            echo form_hidden('task_assigned_'.$tf_assignee['assigneeid']);
        }
    }
    foreach($task_statuses as $status){
        $val = 'true';
        if($status['filter_default'] == false){
            $val = '';
        }
        echo form_hidden('task_status_'.$status['id'],$val);
    }

    ?>
</div>

<div style=""class="btn-group pull-right mleft4 mbot25 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       <i class="fa fa-filter" aria-hidden="true"></i>
   </button>

   <ul class="dropdown-menu width300">
       <li class="li_hover">
       <?php echo form_open('admin/tasks/list_tasks'); ?>
       <input type="hidden" value="all" name="status">
       <button type="submit" class="frm">ALL</button>
       <?php echo form_close(); ?>
       </li>
       <?php

       foreach(tasks_summary_data((isset($rel_id) ? $rel_id : null),(isset($rel_type) ? $rel_type : null)) as $summary) {
           echo form_open('admin/tasks/list_tasks');
           ?>

           <li class="li_hover">


                   <input type="hidden" value="<?php

                   if($summary['name']=='Payment Pending')
                   {
                       echo 1;
                   }
                   else if($summary['name']=='Assigned')
                   {
                       echo 4;
                   }
                   else if($summary['name']=='Pending From Department')
                   {
                       echo 3;
                   }
                   else if($summary['name']=='Pending From Client')
                   {
                       echo 2;
                   }
                   else if($summary['name']=='Work In Progress')
                   {
                       echo 5;
                   }
                   else if($summary['name']=='Completed')
                   {
                       echo 6;
                   }
                   else if($summary['name']=='Cancelled')
                   {
                       echo 7;
                   }
                   ?>" name="status"><button class="frm" type="submit"><?php echo $summary['name']; ?></button>



           </li>
           <?php echo form_close(); ?>
           <?php
       }
       ?>
       <li class="li_hover">

                   <?php echo form_open('admin/tasks/list_tasks'); ?>
                   <input type="hidden" value="today" name="status">
                  <button type="submit" class="frm">Today's tasks</button>
           <?php echo form_close(); ?>
       </li>
       <li class="li_hover">
           <?php echo form_open('admin/tasks/list_tasks');?>
           <input type="hidden" value="due" name="status">
           <button type="submit" class="frm">Overdue</button>
           <?php echo form_close(); ?>
       </li>
       <li class="li_hover">
           <?php echo form_open('admin/tasks/list_tasks');?>
           <input type="hidden" value="staff_id" name="status">
           <button type="submit" class="frm">Task assigned to me</button>
           <?php echo form_close(); ?>
       </li>
       <li class="li_hover">
           <?php echo form_open('admin/tasks/list_tasks');?>
           <input type="hidden" value="assigned_from" name="status">
           <button type="submit" class="frm">Task assigned by</button>
           <?php echo form_close(); ?>
       </li>
   </ul>

</div>
<script>
    function reload() {

        location.reload();
    }
    </script>