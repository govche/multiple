<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                <div class="row _buttons">
                     <div class="col-md-8">
                        <?php if(has_permission('tasks','','create')){ ?>
                        <a href="#" onclick="new_task(<?php if($this->input->get('project_id')){ echo "'".admin_url('tasks/task?rel_id='.$this->input->get('project_id').'&rel_type=project')."'";} ?>); return false;" class="btn btn-info pull-left new"><?php echo _l('new_task'); ?></a>
                        <?php } ?>
                        <a href="<?php if(!$this->input->get('project_id')){ echo admin_url('tasks/switch_kanban/'.$switch_kanban); } else { echo admin_url('projects/view/'.$this->input->get('project_id').'?group=project_tasks'); }; ?>" class="btn btn-default mleft10 pull-left hidden-xs">
                           <?php if($switch_kanban == 1){ echo _l('switch_to_list_view');}else{echo _l('leads_switch_to_kanban');}; ?>
                        </a>
                     </div>
                     <div class="col-md-4">
                        <?php if($this->session->has_userdata('tasks_kanban_view') && $this->session->userdata('tasks_kanban_view') == 'true') { ?>
                        <div data-toggle="tooltip" data-placement="bottom" data-title="<?php echo _l('search_by_tags'); ?>">
                           <?php echo render_input('search','','','search',array('data-name'=>'search','onkeyup'=>'tasks_kanban();','placeholder'=>_l('search_tasks')),array(),'no-margin') ?>
                        </div>
                        <?php } else { ?>
                        <?php $this->load->view('admin/tasks/tasks_filter_by',array('view_table_name'=>'.table-tasks')); ?>
                        <a href="<?php echo admin_url('tasks/detailed_overview'); ?>" class="btn btn-success pull-right mright5"><?php echo _l('detailed_overview'); ?></a>
                        <?php } ?>
                     </div>
                  </div>
                  <hr class="hr-panel-heading hr-10" />
                  <div class="clearfix"></div>
                  <?php
                  if($this->session->has_userdata('tasks_kanban_view') && $this->session->userdata('tasks_kanban_view') == 'true') { ?>
                  <div class="kan-ban-tab" id="kan-ban-tab" style="overflow:auto;">
                     <div class="row">
                        <div id="kanban-params">
                           <?php echo form_hidden('project_id',$this->input->get('project_id')); ?>
                        </div>
                        <div class="container-fluid">
                           <div id="kan-ban"></div>
                        </div>
                     </div>
                  </div>
                  <?php } else { ?>
                  <?php $this->load->view('admin/tasks/_summary',array('table'=>'.table-tasks')); ?>
				
                  <div id="div-start" style="pointer-events: all !important;" class="task-list " onclick="RestrictClosedTask(this);">
   <div class="task-list">
      <div id="managetask974494" data-parent="974494">
         <div id="div974494" class="todo-tasklist-main-item todo-tasklist-item-border-red" style="    border-left: 2px solid #FF9900">
            <div id="divedittask974494" class="task-item-details col-md-12" style="display:none;"></div>
            <div id="divtask974494" class="todo-tasklist-item">
               <div class="todo-tasklist-item-title">
                  <div class="row mc_pd">
                     <div class="col-md-6 change-css">
                        <div><input type="checkbox" id="chkbTask974494" class="chkbTask" value="974494" onclick="showMultipleDeleteButton();">&nbsp;&nbsp;<a id="lnkNameEdit974494" "="" onclick="GetTaskDetails(974494,974494,'2018-12-04T20:21:00','2018-12-18T20:21:00','0','0')"><span class="id-number">7478 ( FY: 2018-19 )</span>NSC</a></div>
                        <div class="todo-tasklist-item-text">Client: CK Bakery Shollinganallur Sri Vari Agency</div>
                        <div class="todo-tasklist-item-text2">Latest Note: </div>
                        <div class="todo-tasklist-item-text2">Total Estimated Efforts: 00:00 Total Actual Efforts: 00:00</div>
                     </div>
                     <div class="col-md-1" style="padding: 0px;font-size: 12px;"></div>
                     <div class="col-md-3 change-css">
                        <span class="todo-tasklist-date todo-tasklist-item-text" id="taskdates" style="font-size: 81% !important;">04-Dec-2018 08:21 PM <span style="font-weight:400">to</span> 18-Dec-2018 08:21 PM </span>
                        <div class="todo-tasklist-controls todo-tasklist-item-text">
                           <div class="row">
                              <div class="col-md-3"></div>
                           </div>
                           <a data-toggle="modal" data-target="#divAddTask" class="btn-add-subtask " onclick="AddTask(974494,326854,137421,'2018-12-04T20:21:00','2018-12-18T20:21:00','0','263020,271348','27794')">Add Sub-Task</a><br>
                        </div>
                     </div>
                     <div class="col-md-2 change-css mc_pd">
                        <div class="todo-tasklist-item-text">
                           <div class="col-md-12 "><a class="bill "><span data-toggle="confirmation" data-placement="bottom" data-html="true" data-content="<div style='font-size: 12px;font-weight: 400;'>Do you want to make this task Non-billable?<br><br>
						   <button class='btn btn-xs btn-success btn-change-taskbillable' onclick='ChangeTaskNonBillable(974494)'>Yes</button>&nbsp;<button class='btn btn-xs btn-success ' onclick='CloseBillablePopover()'>No</button></div>" data-original-title="Change Billable Status" title="">Billable &nbsp;</span></a>
						   <span title="INV -2301" style="width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">2301</span></div>
                           <div class="col-md-12 ">Priority: <span class="label label-sm label-warning">Medium</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="row mc_pd">
                     <div class="col-md-7 left">
                        <div>
                           <select id="ddlSetStatus974494" onchange="TaskStatusChange(974494)" class="bs-select input-small " data-style="btn-success task-status" data-css="background-color:#FF9900" style="display: none;">
                              <option selected="selected" value="1">Assigned</option>
                              <option class="" value="5">Closed</option>
                              <option value="4">Completed</option>
                              <option value="3">Need Approval</option>
                              <option value="300">Pending From Client</option>
                              <option value="302">Pending From Department</option>
                              <option value="2">Work In Progress</option>
                           </select>
                           <div class="btn-group bootstrap-select bs-select input-small">
                              <button type="button" class="btn dropdown-toggle selectpicker btn-success task-status" data-toggle="dropdown" data-id="ddlSetStatus974494" title="Assigned" style="background-color:#FF9900"><span class="filter-option pull-left">Assigned</span>&nbsp;<span class="caret"></span></button>
                              <div class="dropdown-menu open">
                                 <ul class="dropdown-menu inner selectpicker" role="menu">
                                    <li data-original-index="0" class="selected"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Assigned</span>"><span class="text">Assigned</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="1"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Closed</span>"><span class="text">Closed</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="2"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Completed</span>"><span class="text">Completed</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="3"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Need Approval</span>"><span class="text">Need Approval</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="4"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Pending From Client</span>"><span class="text">Pending From Client</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="5"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Pending From Department</span>"><span class="text">Pending From Department</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                    <li data-original-index="6"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Work In Progress</span>"><span class="text">Work In Progress</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                 </ul>
                              </div>
                           </div>
                           &nbsp;
                           <div class="todo-tasklist-item-text2" style="display: inline-block;">Assigned by <a>Inthumathy s</a> to <a class="tooltips" data-original-title="Inthumathy s,Manikandan D">Inthumathy + 1</a></div>
                           <div class="todo-tasklist-item-text2">Created By Inthumathy s on 04-Dec-2018 08:22 PM, Last Modified By Inthumathy s on 04-Dec-2018 08:22 PM.</div>
                        </div>
                     </div>
                     <div class="col-md-5 right change-css"><a class="btn btn-sm fa fa-th-list tooltips show-subtask" style="margin-left: 0px; display:none;" data-original-title="Show Sub-Task" onclick="FillSubTaskGridList(974494)"></a>
					<!-- <a class="btn btn-sm fa fa-plus mc_mrg_rt tooltips btn-add-subtask" style="margin-left: 5px; " data-toggle="modal" data-target="#divAddTask" data-original-title="Add Sub-Task" onclick="AddTask(974494,326854,137421,'2018-12-04T20:21:00','2018-12-18T20:21:00','0','263020,271348','27794')"></a>-->
					 <a class="btn btn-sm fa  fa-file tooltips" style="margin-left: 5px;" data-original-title="Notes" onclick="ShowNoteTab(974494)"></a>
					 <a class="btn btn-sm fa fa-users tooltips" style="margin-left: 5px;" data-original-title="Efforts" onclick="ShowEffortTab(974494)"></a>
					 <a class="btn btn-sm fa fa-rupee tooltips" style="margin-left: 5px;" data-original-title="Expense" onclick="ShowExpenseTab(974494)"></a>
					 <a class="btn btn-sm fa fa-pencil tooltips" style="margin-left: 5px;" data-original-title="Edit Task" onclick="GetTaskDetails(974494,974494,'2018-12-04T20:21:00','2018-12-18T20:21:00','0','0')"></a>
					 <a class="btn btn-sm fa fa-bell-o  tooltips" style="margin-left: 5px;" data-original-title="Add Reminder" onclick="ShowReminderTab(974494)"></a>
					 <a class="btn btn-sm fa  fa-copy tooltips" style="margin-left: 5px;" data-original-title="View Logs" onclick="ShowLogTab(974494)"></a>
					 <a class="btn btn-sm fa fa-paperclip tooltips" style="margin-left: 5px;" data-original-title="Attach Documents" onclick="ShowAttachmentTab(974494)"></a>
					 <a class="btn btn-sm fa fa-envelope tooltips mail-task" style="margin-left: 5px;" data-original-title="Send Task Details via Mail/SMS" data-id="974494"></a><a class="btn btn-sm fa fa-trash mc-lnk-danger tooltips" style="margin-left: 5px;" data-original-title="Delete Task" onclick="DeleteTask(974494)"></a></div>
                  </div>
               </div>
            </div>
            <div style="clear: both;"></div>
         </div>
      </div>
      <div id="divSubTask974494" class="margintask5" style="display:none;"></div>
   </div>
   <div class="hide" id="tempTaskData974494"></div>
   <input type="hidden" id="tempParentTaskID974494" value="0">
</div>


				  <a href="#" data-toggle="modal" data-target="#tasks_bulk_actions" class="hide bulk-actions-btn table-btn" data-table=".table-tasks"><?php echo _l('bulk_actions'); ?></a>
                  <?php $this->load->view('admin/tasks/_table',array('bulk_actions'=>true)); ?>
               </div>
               <?php $this->load->view('admin/tasks/_bulk_actions'); ?>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<?php init_tail(); ?>
<script>
   taskid = '<?php echo $taskid; ?>';
   $(function(){
       tasks_kanban();
   });
</script>
</body>
</html>
