<style>

.h32 {
   
    width: 180px;
  clear: both;
    display: inline-block;
    overflow: hidden;
    white-space: nowrap;
}
.font-medium {
    font-size: 15px!important;
    font-family: lato;
}
button.frm {
    background: transparent;
    border: none;
    color: #008ece;
}
</style>

<h4 class="mbot15"><?php echo _l('tasks_summary'); ?></h4>
<div class="row">
  <?php foreach(tasks_summary_data((isset($rel_id) ? $rel_id : null),(isset($rel_type) ? $rel_type : null)) as $summary){ ?>
   <!-- <div class="col-md-2 col-xs-6 border-right">-->
<!-- my correction-->  <div class="h32">
      <h3 class="bold no-mtop"><?php if(is_admin()) {
              echo $summary['total_tasks'];
          }
          else{
if(!has_permission('tasks','','view') ? 'true' : '')
          {
              echo $summary['total_my_tasks'];
          }
else
{
    echo $summary['total_tasks'];
}
          }
          ?></h3>
      <p style="color:<?php echo $summary['color']; ?>" class="font-medium no-mbot">
<!--          <form method="post" action="--><?php //echo base_url();?><!--admin/tasks/list_task">-->
              <?php echo form_open('admin/tasks/list_tasks'); ?>
              <input type="hidden" value="<?php

              if($summary['name']=='Payment Pending')
              {
                  echo 1;
              }
              else if($summary['name']=='Assigned')
              {
                  echo 4;
              }
              else if($summary['name']=='Pending From Department')
              {
                  echo 3;
              }
              else if($summary['name']=='Pending From Client')
              {
                  echo 2;
              }
              else if($summary['name']=='Work In Progress')
              {
                  echo 5;
              }
              else if($summary['name']=='Completed')
              {
                  echo 6;
              }
              else if($summary['name']=='Cancelled')
              {
                  echo 7;
              }
                  ?>" name="status"><button class="frm" type="submit"><?php echo $summary['name']; ?></button>
          <?php echo form_close(); ?>
      </p>
      <p class="font-medium-xs no-mbot text-muted">
        <?php echo _l('tasks_view_assigned_to_user'); ?>: <?php echo $summary['total_my_tasks']; ?>
      </p>
    </div>
    <?php } ?>
    <?php
    if(has_permission('tasks','','view') ? 'true' : '') {
        ?>
        <div class="h32" style="margin: 12px;">
            <h3 class="bold no-mtop">
                <?php

                echo $due_date_count;
                ?>

            </h3>
            <p style="color:#989898" class="font-medium no-mbot">
                <!--          <form method="post" action="--><!--admin/tasks/list_task">-->
            </p>
            <?php echo form_open('admin/tasks/list_tasks'); ?>
            <input type="hidden" value="due" name="status">
            <button class="frm" type="submit">Overdue</button>
            <?php echo form_close(); ?>
            <p></p>
            <p class="font-medium-xs no-mbot text-muted">
            </p>
        </div>
        <?php
    }

    ?>


  </div>
  <hr class="hr-panel-heading"/>
