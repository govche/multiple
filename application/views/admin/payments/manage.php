<?php if(!is_admin()) {?>
	<style>
	.btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
	display:none;
}
.btn-group>.btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
    display: none;
}
	</style>
<?php }
?>

<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="panel_s">
			<div class="panel-body">
				<?php $this->load->view('admin/payments/table_html'); ?>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
<script>
	$(function(){
		initDataTable('.table-payments', admin_url+'payments/table', undefined, undefined,'undefined',<?php echo do_action('payments_table_default_order',json_encode(array(0,'desc'))); ?>);
	});
</script>
</body>
</html>
