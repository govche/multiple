<div class="panel_s accounting-template estimate">
   <div class="panel-body">
      <?php if(isset($estimate)){ ?>
      <?php echo format_estimate_status($estimate->status); ?>
      <hr class="hr-panel-heading" />
      <?php } ?>
      <div class="row">
         <div class="col-md-6">
            <div class="f_client_id col-md-6">
             <div class="form-group select-placeholder">
                <label for="clientid" class="control-label"><?php echo _l('estimate_select_customer'); ?></label>
                <select id="clientid" name="clientid" data-live-search="true" data-width="100%" class="ajax-search<?php if(isset($estimate) && empty($estimate->clientid)){echo ' customer-removed';} ?>" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
               <?php $selected = (isset($estimate) ? $estimate->clientid : '');
                 if($selected == ''){
                   $selected = (isset($customer_id) ? $customer_id: '');
                 }
                 if($selected != ''){
                    $rel_data = get_relation_data('customer',$selected);
                    $rel_val = get_relation_values($rel_data,'customer');
                    echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
                 } ?>
                </select>
              </div>
            </div>
            
           
            <?php
               $next_estimate_number = get_option('next_estimate_number');
               $format = get_option('estimate_number_format');

                if(isset($estimate)){
                  $format = $estimate->number_format;
                }

               $prefix = get_option('estimate_prefix');

               if ($format == 1) {
                 $__number = $next_estimate_number;
                 if(isset($estimate)){
                   $__number = $estimate->number;
                   $prefix = '<span id="prefix">' . $estimate->prefix . '</span>';
                 }
               } else if($format == 2) {
                 if(isset($estimate)){
                   $__number = $estimate->number;
                   $prefix = $estimate->prefix;
                   $prefix = '<span id="prefix">'. $prefix . '</span><span id="prefix_year">' . date('Y',strtotime($estimate->date)).'</span>/';
                 } else {
                   $__number = $next_estimate_number;
                   $prefix = $prefix.'<span id="prefix_year">'.date('Y').'</span>/';
                 }
               } else if($format == 3) {
                  if(isset($estimate)){
                   $yy = date('y',strtotime($estimate->date));
                   $__number = $estimate->number;
                   $prefix = '<span id="prefix">'. $estimate->prefix . '</span>';
                 } else {
                  $yy = date('y');
                  $__number = $next_estimate_number;
                }
               } else if($format == 4) {
                  if(isset($estimate)){
                   $yyyy = date('Y',strtotime($estimate->date));
                   $mm = date('m',strtotime($estimate->date));
                   $__number = $estimate->number;
                   $prefix = '<span id="prefix">'. $estimate->prefix . '</span>';
                 } else {
                  $yyyy = date('Y');
                  $mm = date('m');
                  $__number = $next_estimate_number;
                }
               }
               $_estimate_number = str_pad($__number, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT);
               if($estimate->reneval != 0){
                $_estimate_number = str_pad($__number, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT).'.0'.$estimate->reneval;
               }
               
               $isedit = isset($estimate) ? 'true' : 'false';
               $data_original_number = isset($estimate) ? $estimate->number : 'false';
               ?>
            <div class="form-group ">
               <label for="number"><?php echo _l('estimate_add_edit_number'); ?></label>
               <div class="input-group">
                  <span class="input-group-addon">
                  <?php if(isset($estimate)){ ?>
                  <a href="#" onclick="return false;" data-toggle="popover" data-container='._transaction_form' data-html="true" data-content="<label class='control-label'><?php echo _l('settings_sales_estimate_prefix'); ?></label><div class='input-group'><input name='s_prefix' type='text' class='form-control' value='<?php echo $estimate->prefix; ?>'></div><button type='button' onclick='save_sales_number_settings(this); return false;' data-url='<?php echo admin_url('estimates/update_number_settings/'.$estimate->id); ?>' class='btn btn-info btn-block mtop15'><?php echo _l('submit'); ?></button>"><i class=""></i></a>
                   <?php }
                    echo $prefix;
                  ?>
                 </span>
                  <input type="text" readonly name="number" class="form-control" value="<?php echo $_estimate_number; ?>" data-isedit="<?php echo $isedit; ?>" data-original-number="<?php echo $data_original_number; ?>">
                  <?php if($format == 3) { ?>
                  <span class="input-group-addon">
                     <span id="prefix_year" class="format-n-yy"><?php echo $yy; ?></span>
                  </span>
                  <?php } else if($format == 4) { ?>
                   <span class="input-group-addon">
                     <span id="prefix_month" class="format-mm-yyyy"><?php echo $mm; ?></span>
                     /
                     <span id="prefix_year" class="format-mm-yyyy"><?php echo $yyyy; ?></span>
                  </span>
                  <?php } ?>
               </div>
            </div>

            <div class="row col-md-12 no-padding-right">
               <div class="col-md-6 no-padding-right">
                  <?php $value = (isset($estimate) ? _d($estimate->date) : _d(date('Y-m-d'))); ?>
                  <?php echo render_date_input('date','estimate_add_edit_date',$value); ?>
               </div>
               <div class="col-md-6 no-padding-right">
                  <?php
                  $value = '';
                  if(isset($estimate)){
                    $value = _d($estimate->expirydate);
                  } else {
                      if(get_option('estimate_due_after') != 0){
                          $value = _d(date('Y-m-d', strtotime('+' . get_option('estimate_due_after') . ' DAY', strtotime(date('Y-m-d')))));
                      }
                  }
                  echo render_date_input('expirydate','estimate_add_edit_expirydate',$value); ?>
               </div>
            </div>
            <div class="clearfix mbot15"></div>
            <?php $rel_id = (isset($estimate) ? $estimate->id : false); ?>
            <?php
                  if(isset($custom_fields_rel_transfer)) {
                      $rel_id = $custom_fields_rel_transfer;
                  }
             ?>
         </div>
         <div class="col-md-6">
            <div class="panel_s no-shadow">
               
               <div class="row">
                  <div class="col-md-6">
                     <?php
                        $s_attrs = array('disabled'=>true,'data-show-subtext'=>true);
                        $s_attrs = do_action('estimate_currency_disabled',$s_attrs);
                        foreach($currencies as $currency){
                          if($currency['isdefault'] == 1){
                            $s_attrs['data-base'] = $currency['id'];
                          }
                          if(isset($estimate)){
                            if($currency['id'] == $estimate->currency){
                              $selected = $currency['id'];
                            }
                          } else{
                           if($currency['isdefault'] == 1){
                            $selected = $currency['id'];
                          }
                        }
                        }
                        ?>
                     <?php echo render_select('currency',$currencies,array('id','name','symbol'),'estimate_add_edit_currency',$selected,$s_attrs); ?>
                  </div>
                   <div class="col-md-6">
                     <div class="form-group select-placeholder">
                        <label class="control-label"><?php echo _l('estimate_status'); ?></label>
                        <select class="selectpicker display-block mbot15" name="status" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <?php foreach($estimate_statuses as $status){ ?>
                           <option value="<?php echo $status; ?>" <?php if(isset($estimate) && $estimate->status == $status){echo 'selected';} ?>><?php echo format_estimate_status($status,'',false); ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                    <?php $value = (isset($estimate) ? $estimate->reference_no : ''); ?>
                    <?php echo render_input('reference_no','reference_no',$value); ?>
                  </div>
                  <div class="col-md-6">
                         <?php
                       $id=get_staff_user_id();

                                   $selected = $id;
                        echo render_select('sale_agent',$staff,array('staffid',array('firstname','lastname')),'sale_agent_string',$selected);
                        ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12">
         <hr>
            <div class="panel_s no-shadow">
                <?php $value = (isset($estimate) ? $estimate->adminnote : ''); ?>
                  <?php echo render_textarea('adminnote','estimate_add_edit_admin_note',$value,array('rows'=>1),array(),'col-md-3'); ?>
                <?php echo render_custom_fields('estimate',$rel_id); ?> 
            </div>
         </div>   
      </div>
   </div>
   <?php $this->load->view('admin/estimates/_add_edit_items'); ?>
   <div class="row">
      <div class="col-md-12 mtop15">
         <div class="panel-body bottom-transaction">
         <a href="#estimateNotes"><button type="button" class="btn-tr btn btn-info mleft10 notes-view" onclick="noteview()">View Note <i class="arrow down"></i></button></a>
         <div class="estimateNotes d-block" id="estimateNotes">
           <?php $value = (isset($estimate) ? $estimate->clientnote : get_option('predefined_clientnote_estimate')); ?>
            <?php echo render_textarea('clientnote','estimate_add_edit_client_note',$value,array(),array(),'mtop15'); ?>
            <?php $value = (isset($estimate) ? $estimate->terms : get_option('predefined_terms_estimate')); ?>
            <?php echo render_textarea('terms','terms_and_conditions',$value,array(),array(),'mtop15'); ?>
            
         </div>
            <div class="btn-bottom-toolbar text-left">
             <div style="display:none">
			 <button type="button" class="btn-tr btn btn-info mleft10 estimate-form-submit save-and-send transaction-submit">
              <?php echo _l('save_and_send'); ?>
              </button>
			  </div>
              <button type="button" class="btn-tr btn btn-info mleft10 estimate-form-submit transaction-submit">
              <?php echo _l('submit'); ?>
              </button>
            </div>

             <!-- <div class="row"> -->
                <div class="col-md-12 mtop15">
                  <div class="panel_s">
                    <div class="panel-heading">
                      <?php echo _l('E-Mail'); ?>
                    </div>
                    <div class="panel-body">

                      <div class="row">
                        <div class="col-md-12 mbot20 before-ticket-message">
                          <select id="insert_predefined_reply" data-live-search="true" class="selectpicker mleft10 pull-right" data-title="<?php echo _l('ticket_single_insert_predefined_reply'); ?>">
                            <?php foreach($predefined_replies as $predefined_reply){ ?>
                            <option value="<?php echo $predefined_reply['id']; ?>"><?php echo $predefined_reply['name']; ?></option>
                            <?php } ?>
                          </select>
                          <?php if(get_option('use_knowledge_base') == 1){ ?>
                          <?php $groups = get_all_knowledge_base_articles_grouped(); ?>
                          <select id="insert_knowledge_base_link" class="selectpicker pull-right" data-live-search="true" onchange="insert_ticket_knowledgebase_link(this);" data-title="<?php echo _l('ticket_single_insert_knowledge_base_link'); ?>">
                            <option value=""></option>
                            <?php foreach($groups as $group){ ?>
                            <?php if(count($group['articles']) > 0){ ?>
                            <optgroup label="<?php echo $group['name']; ?>">
                              <?php foreach($group['articles'] as $article) { ?>
                              <option value="<?php echo $article['articleid']; ?>">
                                <?php echo $article['subject']; ?>
                              </option>
                              <?php } ?>
                            </optgroup>
                            <?php } ?>
                            <?php } ?>
                          </select>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <?php echo render_textarea('message','','',array(),array(),'','tinymce'); ?>
                    </div>
                    <div class="panel-footer attachments_area">
                      <div class="row attachments">
                        <div class="attachment">
                          <div class="col-md-4 col-md-offset-4 mbot15">
                            <div class="form-group">
                              <label for="attachment" class="control-label"><?php echo _l('ticket_add_attachments'); ?></label>
                              <div class="input-group">
                                <input type="file" extension="<?php echo str_replace('.','',get_option('ticket_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
                                <span class="input-group-btn">
                                  <button class="btn btn-success add_more_attachments p8-half" data-ticket="true" type="button"><i class="fa fa-plus"></i></button>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <!-- </div> -->

         </div>
           <div class="btn-bottom-pusher"></div>
      </div>
   </div>
</div>