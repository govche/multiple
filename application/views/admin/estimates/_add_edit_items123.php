<div class="panel-body mtop10">
   <div class="row">
      <div class="col-md-4">
          <?php $this->load->view('admin/invoice_items/item_select'); ?>
      </div>
      <div class="col-md-8 text-right show_quantity_as_wrapper">
<!--         <div class="mtop10">-->
<!--            <span>--><?php //echo _l('show_quantity_as'); ?><!--</span>-->
<!--            <div class="radio radio-primary radio-inline">-->
<!--               <input type="radio" value="1" id="1" name="show_quantity_as" data-text="--><?php //echo _l('estimate_table_quantity_heading'); ?><!--" --><?php //if(isset($estimate) && $estimate->show_quantity_as == 1){echo 'checked';}else{echo'checked';} ?><!-->
<!--               <label for="1">--><?php //echo _l('quantity_as_qty'); ?><!--</label>-->
<!--            </div>-->
<!--            <div class="radio radio-primary radio-inline">-->
<!--               <input type="radio" value="2" id="2" name="show_quantity_as" data-text="--><?php //echo _l('estimate_table_hours_heading'); ?><!--" --><?php //if(isset($estimate) && $estimate->show_quantity_as == 2){echo 'checked';} ?><!-->
<!--               <label for="2">--><?php //echo _l('quantity_as_hours'); ?><!--</label>-->
<!--            </div>-->
<!--            <div class="radio radio-primary radio-inline">-->
<!--               <input type="radio" id="3" value="3" name="show_quantity_as" data-text="--><?php //echo _l('estimate_table_quantity_heading'); ?><!--/--><?php //echo _l('estimate_table_hours_heading'); ?><!--" --><?php //if(isset($estimate) && $estimate->show_quantity_as == 3){echo 'checked';} ?><!-->
<!--               <label for="3">--><?php //echo _l('estimate_table_quantity_heading'); ?><!--/--><?php //echo _l('estimate_table_hours_heading'); ?><!--</label>-->
<!--            </div>-->
<!--         </div>-->
      </div>
   </div>
   <div class="table-responsive s_table">
      <table class="table estimate-items-table items table-main-estimate-edit no-mtop">
         <thead>
            <tr>
               <th></th>
               <th width="20%" align="left"><i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" data-title="<?php echo _l('item_description_new_lines_notice'); ?>"></i> <?php echo _l('estimate_table_item_heading'); ?></th>
               <th width="25%" align="left"><?php echo _l('estimate_table_item_description'); ?></th>
               <?php
                  $custom_fields = get_custom_fields('items');

                  foreach($custom_fields as $cf){
                   echo '<th width="15%" align="left" class="custom_field">' . $cf['name'] . '</th>';
                  }

                  $qty_heading = _l('estimate_table_quantity_heading');
                  if(isset($estimate) && $estimate->show_quantity_as == 2){
                  $qty_heading = _l('estimate_table_hours_heading');
                  } else if(isset($estimate) && $estimate->show_quantity_as == 3){
                  $qty_heading = _l('estimate_table_quantity_heading') . '/' . _l('estimate_table_hours_heading');
                  }
                  ?>
               <th width="10%" class="qty" align="right"><?php echo $qty_heading; ?></th>
               <th width="15%" align="right"><?php echo _l('estimate_table_rate_heading'); ?></th>
               <th width="20%" align="right"><?php echo _l('estimate_table_tax_heading'); ?></th>
               <th width="10%" align="right"><?php echo _l('estimate_table_amount_heading'); ?></th>
               <th align="center"><i class="fa fa-cog"></i></th>
            </tr>
         </thead>
         <tbody>
   
            <tr class="main" id="main2">
               <td></td>
               <td>
                  <textarea readonly name="description" rows="4" class="form-control" placeholder="<?php echo _l('item_description_placeholder'); ?>"></textarea>
               </td>
               <td>
                  <textarea  name="long_description" rows="4" class="form-control" placeholder="<?php echo _l('item_long_description_placeholder'); ?>"></textarea>
               </td>
               <?php echo render_custom_fields_items_table_add_edit_preview(); ?>
               <td>
                    <!-- by rathina for hide unit and qty readonly-->
                  <input  type="number" name="quantity" min="0" value="1" class="form-control" placeholder="<?php echo _l('item_quantity_placeholder'); ?>">
                  <div style="display:none"> <input readonly type="text" placeholder="<?php echo _l('unit'); ?>" name="unit" class="form-control input-transparent text-right"></div>
               </td>
               <td>
                  <input type="number" name="rate" class="form-control" placeholder="<?php echo _l('item_rate_placeholder'); ?>">
               </td>
               <td>
                  <?php
                     $default_tax = unserialize(get_option('default_tax'));
                     $select = '<select class="selectpicker display-block tax main-tax" data-width="100%" name="taxname" multiple data-none-selected-text="'._l('no_tax').'">';
                     foreach($taxes as $tax){
                       $selected = '';
                       if(is_array($default_tax)){
                         if(in_array($tax['name'] . '|' . $tax['taxrate'],$default_tax)){
                           $selected = ' selected ';
                         }
                       }
                       $select .= '<option value="'.$tax['name'].'|'.$tax['taxrate'].'"'.$selected.'data-taxrate="'.$tax['taxrate'].'" data-taxname="'.$tax['name'].'" data-subtext="'.$tax['name'].'">'.$tax['taxrate'].'%</option>';
                     }
                     $select .= '</select>';
                     echo $select;
                     ?>
               </td>
               <td></td>
               <td>
                  <?php
                     $new_item = 'undefined';
                     if(isset($estimate)){
                       $new_item = true;
                     }
                     ?>
                  <button type="button" onclick="add_item_to_table('undefined','undefined',<?php echo $new_item; ?>); " class="btn pull-right btn-info"><i class="fa fa-check"></i></button>
               </td>
            </tr>

            <tr class="main" id="main1">
               <td></td>
               <td>
                  <textarea readonly name="description" id="description" rows="4" class="form-control" placeholder="<?php echo _l('item_description_placeholder'); ?>"></textarea>
               </td>
               <td>
                  <textarea  name="long_description" id="long_description"   rows="4" class="form-control" placeholder="<?php echo _l('item_long_description_placeholder'); ?>"></textarea>
               </td>
               <?php echo render_custom_fields_items_table_add_edit_preview(); ?>
               <td>
                    <!-- by rathina for hide unit and qty readonly-->
                  <input  type="number" name="quantity" id="quantity" min="0" value="1" class="form-control" placeholder="<?php echo _l('item_quantity_placeholder'); ?>">
                  <div style="display:none"> <input readonly type="text" placeholder="<?php echo _l('unit'); ?>" name="unit" name="unit" class="form-control input-transparent text-right"></div>
               </td>
               <td>
                  <input type="number" name="rate" id="rate" class="form-control" placeholder="<?php echo _l('item_rate_placeholder'); ?>">
               </td>
               <td>
                  <?php
                     $default_tax = unserialize(get_option('default_tax'));
                     $select = '<select class="selectpicker display-block tax main-tax" data-width="100%" name="taxname" id="taxname" multiple data-none-selected-text="'._l('no_tax').'">';
                     foreach($taxes as $tax){
                       $selected = '';
                       if(is_array($default_tax)){
                         if(in_array($tax['name'] . '|' . $tax['taxrate'],$default_tax)){
                           $selected = ' selected ';
                         }
                       }
                       $select .= '<option value="'.$tax['name'].'|'.$tax['taxrate'].'"'.$selected.'data-taxrate="'.$tax['taxrate'].'" data-taxname="'.$tax['name'].'" data-subtext="'.$tax['name'].'">'.$tax['taxrate'].'%</option>';
                     }
                     $select .= '</select>';
                     echo $select;
                     ?>
               </td>
               <td></td>
               <td>
                  <?php
                     $new_item = 'undefined';
                     if(isset($estimate)){
                       $new_item = true;
                     }
                     ?>
                  <button type="button" onclick="add_item_to_table('undefined','undefined',<?php echo $new_item; ?>,'id'); " class="btn pull-right btn-info"><i class="fa fa-check"></i></button>
               </td>
            </tr>


          
         </tbody>
      </table>
   </div>
   <div class="col-md-8 col-md-offset-4">
      <table class="table text-right">
         <tbody>
            <tr id="subtotal">
               <td><span class="bold"><?php echo _l('estimate_subtotal'); ?> :</span>
               </td>
               <td class="subtotal">
               </td>
            </tr>
            <tr id="discount_area">
               <td>
                  <div class="row">
                     <div class="col-md-7">
                        <span class="bold"><?php echo _l('estimate_discount'); ?></span>
                     </div>
                     <div class="col-md-5">
                        <div class="input-group" id="discount-total">

                           <input type="number" value="<?php echo (isset($estimate) ? $estimate->discount_percent : 0); ?>" class="form-control pull-left input-discount-percent<?php if(isset($estimate) && !is_sale_discount($estimate,'percent') && is_sale_discount_applied($estimate)){echo ' hide';} ?>" min="0" max="100" name="discount_percent">

                           <input type="number" data-toggle="tooltip" data-title="<?php echo _l('numbers_not_formatted_while_editing'); ?>" value="<?php echo (isset($estimate) ? $estimate->discount_total : 0); ?>" class="form-control pull-left input-discount-fixed<?php if(!isset($estimate) || (isset($estimate) && !is_sale_discount($estimate,'fixed'))){echo ' hide';} ?>" min="0" name="discount_total">

                           <div class="input-group-addon">
                              <div class="dropdown">
                                 <a class="dropdown-toggle" href="#" id="dropdown_menu_tax_total_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                 <span class="discount-total-type-selected">
                                  <?php if(!isset($estimate) || isset($estimate) && (is_sale_discount($estimate,'percent') || !is_sale_discount_applied($estimate))) {
                                    echo '%';
                                    } else {
                                    echo _l('discount_fixed_amount');
                                    }
                                    ?>
                                 </span>
                                 <span class="caret"></span>
                                 </a>
                                 <ul class="dropdown-menu" id="discount-total-type-dropdown" aria-labelledby="dropdown_menu_tax_total_type">
                                   <li>
                                    <a href="#" class="discount-total-type discount-type-percent<?php if(!isset($estimate) || (isset($estimate) && is_sale_discount($estimate,'percent')) || (isset($estimate) && !is_sale_discount_applied($estimate))){echo ' selected';} ?>">%</a>
                                  </li>
                                  <li>
                                    <a href="#" class="discount-total-type discount-type-fixed<?php if(isset($estimate) && is_sale_discount($estimate,'fixed')){echo ' selected';} ?>">
                                      <?php echo _l('discount_fixed_amount'); ?>
                                    </a>
                                  </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
               <td class="discount-total"></td>
            </tr>
            <tr>
               <td>
                  <div class="row">
                     <div class="col-md-7">
                        <span class="bold"><?php echo _l('estimate_adjustment'); ?></span>
                     </div>
                     <div class="col-md-5">
                        <input type="number" data-toggle="tooltip" data-title="<?php echo _l('numbers_not_formatted_while_editing'); ?>" value="<?php if(isset($estimate)){echo $estimate->adjustment; } else { echo 0; } ?>" class="form-control pull-left" name="adjustment">
                     </div>
                  </div>
               </td>
               <td class="adjustment"></td>
            </tr>
             <tr>
               <td>
                  <div class="row">
                     <div class="col-md-7">
                        <label for="discount_type" class="control-label bold"><?php echo _l('discount_type'); ?></label>
                     </div>
                     <div class="col-md-5">
                         <select name="discount_type" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value="" selected><?php echo _l('no_discount'); ?></option>
                           <option value="before_tax" <?php
                              if(isset($estimate)){ if($estimate->discount_type == 'before_tax'){ echo 'selected'; }}?>><?php echo _l('discount_type_before_tax'); ?></option>
                           <option value="after_tax" <?php if(isset($estimate)){if($estimate->discount_type == 'after_tax'){echo 'selected';}} ?>><?php echo _l('discount_type_after_tax'); ?></option>
                        </select>
                     </div>
                  </div>
               </td>
               <td class="adjustment"></td>
            </tr>
            <tr>
               <td><span class="bold"><?php echo _l('estimate_total'); ?> :</span>
               </td>
               <td class="total">
               </td>
            </tr>
         </tbody>
      </table>
   </div>
   <div id="removed-items"></div>
</div>
