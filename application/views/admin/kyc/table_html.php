
<!--<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>-->
<!--<link href="//datatables.net/download/build/nightly/jquery.dataTables.css" rel="stylesheet" type="text/css" />-->
<!--<script src="//datatables.net/download/build/nightly/jquery.dataTables.js"></script>-->


    <table id="table_data" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th class="th-sm">#
        </th>
        <th class="th-sm">Profile
        </th>
        <th class="th-sm">KYC NUMBER
        </th>
        <th class="th-sm">Name
        </th>
        <th class="th-sm">Present Address
        </th>
         <th class="th-sm">Address Proof
        </th>
        <th class="th-sm">Mobile
        </th>
        <th class="th-sm">Pan
        </th>
        <th class="th-sm">Date
        </th>
        <!--<th class="th-sm">Status-->
        <!--</th>-->
        <!--<th class="th-sm">Action-->
        <!--</th>-->
    </tr>
    </thead>
    <tbody>
    <?php
    $i=0;
    foreach($kyc_list as $key) {
        $i++;
        ?>
        <tr>
            <td><?php echo $i;?></td>


            <td>
                <?php
                if($key['profile_image']!='') {
                    ?>
                    <img
                         style="    border-radius: 50px;
    height: 100px;
    width: 100px;" src="<?php echo base_url(); ?>uploads/staff_profile_images/<?php echo $key['staffid']; ?>/<?php echo 'thumb_' . $key['profile_image']; ?>">
                    <?php
                }
                else {
                    ?>
                    <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="false">
                        <img src="<?php echo base_url();?>assets/images/user-placeholder.jpg"
                             class="img img-responsive staff-profile-image-small pull-left">
                    </a>
                    <?php
                }
                ?>
            </td>

            <td><?php echo $key['kyc_id'];?></td>
            <td><span style="text-transform: capitalize"><?php echo $key['firstname'].'' .$key['lastname'];?></span></td>
            <td><?php echo $key['aff_address'];?></td>
            <td>
                <?php
                if($key['address_image']!='') {
                    ?>
                                   <a href="#" onclick="show_address(<?php echo $key['staffid']; ?>,'<?php echo 'thumb_' . $key['address_image']; ?>')" >View address</a>
                                   <?php
                }
                else {
                    ?>
                    <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="false">
                        <img src="<?php echo base_url();?>assets/images/user-placeholder.jpg"
                             class="img img-responsive staff-profile-image-small pull-left">
                    </a>
                    <?php
                }
                ?>
            </td>
                <td><?php echo $key['phonenumber'];?></td>
            <td><?php echo $key['PAN_No'];?></td>
            <td><?php echo $key['datecreated'];?></td>
   <!-- <td><span style="color: deepskyblue"><?php if ($key['approve_status']==1){echo 'Approved';}else{echo "Waiting";};?></span></td>

            <td><?php
        //if ($key['approve_status']==1)
              //  {
                ?>
                <button style="color: #ffffff;
    background: red;
    border-color: red;" onclick="approve(0,<?php //echo $key['id'];?>)"><i class="fa fa-times" aria-hidden="true"></i></button>
            </td>
            <?php
            //}
       // else if($key['approve_status']==0){
            ?>
            <button style="    color: #ffffff;
    background: #407d19;
    border-color: #37651a   " onclick="approve(1,<?php //echo $key['id'];?>)"><i class="fa fa-check" aria-hidden="true"></i></button>
            <?php
       // }
            ?>-->
        </tr>
        <?php
    }
    ?>

</table>
<div id="myModalz" class="modal" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Remainder</h4>
            </div>
            <div class="modal-body" id="add_address">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
