<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="_buttons">

                            <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
<!--                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                    <i class="fa fa-filter" aria-hidden="true"></i>-->
<!--                                </button>-->

                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr-panel-heading" />
                        </div>
                        <div class="row mbot15">
                            <div class="col-md-12">
                                <h4 class="no-margin"><?php echo _l('KYC List'); ?></h4>

                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php echo form_hidden('custom_view'); ?>
                        <?php $this->load->view('admin/kyc/table_html'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php init_tail(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    // $.noConflict();
    $(document).ready( function () {
        var table = $('#table_data').DataTable();
    });




    function approve(val,id) {

        $.ajax({
            url: '<?php echo base_url();?>admin/staff/kyc_approve',
            type: 'POST',
            data:{val:val,id:id},
            success: function (data) {
            //   alert(data);
              
                location.reload();
            }

        });
    }

    function show_address(id,img)
    {
        var img_path='<?php echo base_url();?>uploads/staff_profile_images/'+id+'/'+img;
        $('#add_address').html('<img style="width:100%" src="'+img_path+'">');

        $('#myModalz').modal('show');
    }

</script>

</body>

</html>
