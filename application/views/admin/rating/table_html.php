
<style>
.checked {
  color: orange;
}
</style>
    <table id="table_data" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
   
        <th class="th-sm">S.No
        </th>
        <th class="th-sm">Project
        </th>
        <th class="th-sm">Client 
        </th>
        <th class="th-sm">Staff name
        </th>
              <th class="th-sm">Date
        </th>
        <th class="th-sm">Comment
        </th>
        <th class="th-sm">Q1
     </th> 
        <th class="th-sm">Q2
        </th>
        <th class="th-sm">Q3
        </th>
        <th class="th-sm">Q4
        </th>
        <th class="th-sm">Total Rating
        </th>
      
 

    </tr>
    </thead>
   
    <tbody>
    <?php
    $x=1;
  foreach($ratings as $rate){ ?>
   <?php $total= $rate['q1'] + $rate['q2'] +$rate['q3'] + $rate['q4']; ?>
     <tr>
   
     <td><?php echo $x; ?></td>
     <td><a href="<?php echo admin_url('projects/view/').$rate['pid']; ?>"><?php echo $rate['name']; ?></td>
     <td><a href="<?php echo admin_url('clients/client/').$rate['userid']; ?>"><?php echo $rate['company']; ?></td>

     <td><a href="<?php echo admin_url('staff/member/').$rate['project_convert_staff']; ?>"><?php echo $rate['firstname'].' '.$rate['lastname']; ?></td>
        <td><?php echo $rate['created_by']; ?></td>
    <td><?php echo $rate['comment']; ?></td>
     <td><?php echo $rate['q1']; ?><br>
<?php for($i=1; $i <= $rate['q1']; $i++){ ?>
    <span class="fa fa-star checked"></span>
<?php  } ?>

     

     </td>
     <td><?php echo $rate['q2']; ?>
     <br>

     <?php for($i=1; $i <= $rate['q2']; $i++){ ?>
     <span class="fa fa-star checked"></span>
     <?php  }?>
     </td>
     <td><?php echo $rate['q3']; ?>
     
     <br>
     <?php for($i=1; $i <= $rate['q3']; $i++){ ?>
     <span class="fa fa-star checked"></span>
     <?php } ?>
     </td>
     <td><?php echo $rate['q4']; ?>
     <br>
     <?php for($i=1; $i <= $rate['q4']; $i++){ ?>
     <span class="fa fa-star checked"></span>
     <?php } ?>
     
     </td>
     <td><?php echo $total; ?></td>

     </tr>


<?php  

$x++;   } 
    ?>

</table>
