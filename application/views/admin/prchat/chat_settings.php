<style type="text/css" media="screen">
    #pusherChat #membersContent a:hover { background: <?php echo $getColor; ?>; }
    #pusherChat .pusherChatBox .msgTxt p.you { background: <?php echo $getColor; ?>; }
    #pusherChat .chatBoxWrap #slideRight .fa-angle-double-right { color: <?php echo $getColor; ?> }
    #pusherChat .chatBoxWrap #slideLeft .fa-angle-double-left { color: <?php echo $getColor; ?> }
</style>


<!-- Check if user has permission to delete own messages enabled -->
<?php $chat_delete_option = get_option('chat_staff_can_delete_messages'); ?>

<script>
    // <script>

    /*---------------* Start of main Chat helper function  *---------------*/
    var prchatSettings =
        {
            'usersList' : '<?php echo site_url('admin/prchat_controller/users'); ?>',
            'getMessages' : '<?php echo site_url('admin/prchat_controller/getMessages'); ?>',
            'updateUnread' : '<?php echo site_url('admin/prchat_controller/updateUnread'); ?>',
            'serverPath' : '<?php echo site_url('admin/prchat_controller/initiateChat'); ?>',
            'chatLastSeenText' : "<?php echo _l('chat_last_seen'); ?>",
            'noMoreMessagesText' : "<?php echo _l('chat_no_more_messages_to_show'); ?>",
            'hasComeOnlineText' : "<?php echo _l('chat_user_is_online'); ?>",
            'sayHiText' : "<?php echo _l('chat_say_hi'); ?>",
            'deleteMessage' : "<?php echo site_url('admin/prchat_controller/deleteMessage'); ?>",
            'deleteChatMessage' : "<?php echo _l('chat_delete_message'); ?>",
            'messageIsDeleted' : "<?php echo _l('chat_message_deleted'); ?>",
            'getUnread' : '<?php echo site_url('admin/prchat_controller/getUnread'); ?>',
            'switchTheme' : '<?php echo site_url('admin/prchat_controller/switchTheme'); ?>',
            'debug':<?php if(ENVIRONMENT != 'production') { ?> true <?php } else { ?> false <?php } ; ?>,
        };
</script>

