<?php
//defined('BASEPATH') or exit('No direct script access allowed');
init_head();
?>
<div id="wrapper" class="desktop_chat">
  <div id="frame">
    <div id="sidepanel">
      <div id="profile">
        <div class="wrap">
          <?php echo staff_profile_image($current_user->staffid, array('img','img-responsive','staff-profile-image-small','pull-left'),'small',['id'=>'profile-img']); ?>
          <p>
            <?php echo get_staff_full_name(); ?>
          </p>
        </div>
      </div>
      <div class="connection_field">
        <i class="fa fa-wifi blink"></i>
      </div>
      <div id="search">
        <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
        <input type="text" id="search_field" placeholder="<?php echo _l('chat_search_chat_members'); ?>" data-container="body" data-toggle="tooltip" data-placement="top" title="<?php echo _l('chat_search_chat_members'); ?>" />
      </div>
      <div id="contacts">
        <div id="bottom-bar">
          <button id="switchTheme"><i class="fa fa-ioxhost" aria-hidden="true"></i> <span>
           <div class="dropdown" id="theme_options">
            <a href="#" class="dropbtn"><?php echo _l('chat_theme_name'); ?></a>
            <div class="dropdown-content">
              <a id="light" onClick="chatSwitchTheme('light')" href="#"><?php echo _l('chat_theme_options_light'); ?></a>
              <a id="dark" onClick="chatSwitchTheme('dark')" href="#"><?php echo _l('chat_theme_options_dark'); ?></a>
            </div>
          </div>
        </span></button>
<!--        <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>-->
      </div>
      <ul>
        <li class="contact">

        </li>
      </ul>
    </div>
  </div>
  <div class="content">
    <div id="sharedFiles">
      <i class="fa fa-times-circle-o" aria-hidden="true"></i>
      <div class="history_slider" style="    float: left;
      position: absolute;
      left: 6px;">
    </div>
  </div>
  <div class="contact-profile">
    <img src="" class="img img-responsive staff-profile-image-small pull-left" alt="" />
    <p></p>
    <div class="social-media mright15">
      <i data-toggle="tooltip" data-container="body" title="<?php echo _l('chat_shared_files'); ?>" data-placement="left" class="fa fa-share-alt" id="shared_user_files"></i>
      <a href="" id="fa-skype" data-toggle="tooltip" data-container="body" class="mright5" title="<?php echo _l('chat_call_on_skype'); ?>"><i class="fa fa-skype" aria-hidden="true"></i></a>
      <a href="" id="fa-facebook" target="_blank" class="mright5"><i class="fa fa-facebook" aria-hidden="true"></i></a>
      <a href="" id="fa-linkedin" target="_blank" class="mright5"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    </div>
  </div>
  <div class="messages">
    <svg class="message_loader" viewBox="0 0 50 50">
      <circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
    </svg>
    <span class="userIsTyping bounce" id="">
      <img src="<?php echo base_url('assets/chat_implements/userIsTyping.gif'); ?>"/>
    </span>
    <ul>
    </ul>
  </div>
  <form hidden enctype="multipart/form-data" name="fileForm" method="post" onsubmit="postform(this);return false;">
    <input type="file" class="file" name="userfile" required />
    <input type="submit" name="submit" class="save" value="save" />
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  </form>
  <form method="post" enctype="multipart/form-data" name="pusherMessagesForm" onsubmit="return false;">
    <div class="message-input">
     <div class="wrap">
      <textarea type="text" disabled name="msg" class="chatbox ays-ignore" placeholder="Write your message..."></textarea>
      <input type="hidden" class="ays-ignore from" name="from" value="" />
      <input type="hidden" class="ays-ignore to" name="to" value="" />
      <input type="hidden" class="ays-ignore typing" name="typing" value="false" />
      <input type="hidden" class="ays-ignore" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
      <i class="fa fa-paperclip attachment fileUpload" data-container="body" data-toggle="tooltip" title="<?php echo _l('chat_file_upload'); ?>" aria-hidden="true"></i>
      <button class="submit enterBtn"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
    </div>
  </div>
</form>
</div>
</div>


    <?php init_tail(); ?>
    <?php $this->load->view('admin/prchat/chat_settings'); ?>


<script>



var wentOffline, wentOnline;
window.addEventListener('online', handleConnectionChange);
window.addEventListener('offline', handleConnectionChange);
monitorWindowActivity();

/*---------------* Main first thing get users/staff from database *---------------*/
var users = $.get(prchatSettings.usersList);

/*---------------* Parse emojies in chat area do not touch *---------------*/
    emojify.setConfig({emojify_tag_type:'div','img_dir':site_url+'/assets/chat_implements/emojis'});
emojify.run();

var offsetPush = {};
var endOfScroll = {};
var friendUsername = '';
var unreadMessages = '';
var pusherKey = "<?php echo get_option('pusher_app_key') ?>";
var appCluster = "<?php echo get_option('pusher_cluster') ?>";
var staffFullName = "<?php echo get_staff_full_name(); ?>";
var userSessionId = "<?php echo get_staff_user_id(); ?>";

/*---------------* Handles input form sending *---------------*/
$('#frame').on('click','.fileUpload',function()
{
    $('#frame').find('form[name="fileForm"] input:first').click();
});

$('#frame').on('change','input[type=file]',function()
{
    $(this).parent('form').submit();
});

function postform(file)
{
    var formData = new FormData();
    var fileForm = $(file).children('input[type=file]')[0].files[0];
    var sentTo = $(file).attr('id').replace('id_','');
    var token_name = $(file).children('input:nth-child(3)').val();
    var formId = $(file).attr('id');

    formData.append('userfile',fileForm);
    formData.append('send_to',sentTo);
    formData.append('send_from',userSessionId);
    formData.append('csrf_token_name',token_name);

    $.ajax({
        type: 'POST',
        url: '<?php echo site_url('admin/prchat_controller/uploadMethod'); ?>',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function() {
            $('.content').prepend('<div class="chat-module-loader"><div></div><div></div><div></div></div>');
        },
        success: function(r){
            if(!r.error){
                const uploadSend = $.Event( "keypress", {  which:13 } );
                var basePath = "<?php echo base_url('uploads/'); ?>";
                $('form#'+formId).trigger("reset");
                $('#frame textarea.chatbox').val(basePath+r.upload_data.file_name);
                setTimeout(function(){
                    if($('#frame textarea.chatbox').trigger(uploadSend)){
                        alert_float('info', 'File '+r.upload_data.file_name+' sent.');
                        $('.content .chat-module-loader').fadeOut();
                    }
                },100);
                getSharedFiles(userSessionId, sentTo);
            } else {
                alert_float('danger',r.error);
            }
        }
    });
}

/*---------------* Capitalize first string of letter *---------------*/
function strCapitalize(string)
{
    if(string != undefined) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}
/*---------------* Update user lastes message into dabatase *---------------*/
function updateLatestMessages(id)
{
    $.post(prchatSettings.updateUnread,{id:id}).done(function(r){
        if(r != 'true'){
            return false;
        }
    });
}
/*---------------* Get user avatar picture *---------------*/
function fetchUserAvatar(id, image_name) {
    var type = 'small';
    var url = site_url + '/assets/images/user-placeholder.jpg';
    if (image_name == false) {
        return url;
    }
    if (image_name != null) {
        url = site_url + '/uploads/staff_profile_images/' + id + '/' + type + '_' + image_name;
    } else {
        url = site_url + '/assets/images/user-placeholder.jpg';
    }
    return url;
}
function images_search(data)
{
    var regex_pic = /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)$/i;
    if(data.match(regex_pic)) {
        return true;
    }
    return false;
}
/*---------------* Check messages if contains any links or images *---------------*/
function createTextLinks_(text)
{
    var regex = (/\.(gif|jpg|jpeg|tiff|png|swf)$/i);
    return (text || "").replace(/([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,function(match, string, url){
        var hyperlink = url;
        if (!hyperlink.match('^https?:\/\/')) {
            hyperlink = '//' + hyperlink;
        }
        if (hyperlink.match('^http?:\/\/')) {
            hyperlink = hyperlink.replace('http://','//');
        }
        if(hyperlink.match(regex)){
            return string + '<a href="'+ hyperlink +'" target="blank" data-lity><img class="prchat_convertedImage" src="' + hyperlink + '"/></a>';
        } else {
            return string + '<a data-lity target="blank" href="' + hyperlink + '">' + url + '</a>';
        }
    });
}

/*---------------* Chat shortcuts for better user experience Ctrl+Alt+Z and Ctrl+Alt+S *---------------*/
$(document).keydown(function(e)
{
    if ((e.which === 90 || e.keyCode == 90) && e.ctrlKey && e.altKey) {
        $(".messages").animate({ scrollTop: $('.messages').prop("scrollHeight")}, 500);
    }
    if((e.which === 83 || e.keyCode == 83) && e.ctrlKey && e.altKey) {
        $('#frame').find('#search_field').focus();
    }
    if((e.which === 70 || e.keyCode == 70) && e.ctrlKey && e.altKey) {
        if($('#shared_user_files').is(':visible')){
            $('#shared_user_files').click();
        }
    }
});

/*---------------* Set no more messages if not found in database *---------------*/
function prchat_setNoMoreMessages()
{
    if ($("#no_messages").length == 0){
        $('.messages').prepend('<div class="text-center" style="margin-top:5px;" id="no_messages">' + prchatSettings.noMoreMessagesText + '</div>');
    }
}

/*---------------* Check for messages history and append to main chat window *---------------*/
function loadMessages(el)
{
    var pos = $(el).scrollTop();
    var id = $(el).attr("id");
    var to = $('#contacts ul li').children('a.active_chat').attr('id');
    var from = userSessionId;

    if (endOfScroll[to] == true) {
        prchat_setNoMoreMessages();
        return false;
    }

    if (pos == 0) {
        $.get(prchatSettings.getMessages,
            {
                from: from,
                to: to,
                offset: offsetPush[to]
            })
            .done(function(message) {
                message = JSON.parse(message);
                if (Array.isArray(message) == false) {
                    endOfScroll[to] = true;
                    if($(el).hasScrollBar && endOfScroll[to] == true && offsetPush[to] != 10){
                        prchat_setNoMoreMessages();
                    }
                } else {
                    offsetPush[to] += 10;
                    activateLoader();
                }

                $(message).each(function(key, value) {
                    value.message = emojify.replace(value.message);
                    var element =  $('.messages#id_'+to).find('ul');
                    if (value.is_deleted == 1) {
                        value.message = '<span>' + prchatSettings.messageIsDeleted + '</span>';
                    } else {
                        value.message = emojify.replace(value.message);
                    }
                    if(value.reciever_id == from){
                        element.prepend('<li class="replies"><img class="friendProfilePic" src="'+ fetchUserAvatar(value.sender_id,value.user_image) +'"/><p data-toggle="tooltip" data-container="body" title="'+ value.time_sent_formatted +'" class="friend">'+ value.message +'</p></li>');
                    } else {
                        element.prepend('<li class="sent" id="'+value.id+'"><img class="myProfilePic" src="'+ fetchUserAvatar(value.sender_id,value.user_image) +'"/><p data-toggle="tooltip" data-container="body" title="'+ value.time_sent_formatted +'" class="you" id="msg_'+value.id+'">'+ value.message +'</p></li>');
                        <?php if($chat_delete_option == '1' || is_admin()) :  ?>
                        if (value.is_deleted == 0) {
                            $('#msg_'+value.id).tooltipster({
                                content: $("<span id='"+value.id+"' class='prchat_message_delete' onClick='delete_chat_message(this)'>"+prchatSettings.deleteChatMessage+"</span>"),
                                interactive: true,
                                side: 'left'
                            });
                        }
                        <?php endif; ?>
                    }
                });
                if (endOfScroll[to] == false && offsetPush[to] > 20) {
                    $(el).scrollTop(200);
                }
            });
    }
}

/*---------------* Check for unread messages *---------------*/
var checkforNewMessages = $.getJSON(prchatSettings.getUnread).done(function(new_messages)
{
    if(new_messages.success == true) {
        return JSON.parse(new_messages);
    }
});

/*---------------* Put prchatSettings.debug for debug mode for Pusher *---------------*/
if (prchatSettings.debug)
{
    try {
        Pusher.log = function(message) {
            if (window.console && window.console.log) {
                window.console.log(message);
            }
        };
    } catch (e) {
        if (e instanceof ReferenceError) {
            alert_float('danger',e);
        }
    }
}
/*---------------* Init pusher library, and register *---------------*/
var pusher = new Pusher(pusherKey, {
    authEndpoint: "<?php echo site_url('admin/prchat_controller/pusher_auth'); ?>",
    authTransport: 'jsonp',
    'cluster':appCluster,
});

/*---------------* Pusher Trigger accessing channel *---------------*/
var presenceChannel = pusher.subscribe('presence-mychanel');
pusher.config.unavailable_timeout = 5000;
pusher.connection.bind('state_change', function(states) {
    var prevState = states.previous;
    var currState = states.current;
    var conn_tracker = $('.connection_field');
    if(currState == 'unavailable'){
        conn_tracker.fadeIn();
        conn_tracker.children('i.fa-wifi').fadeIn();
        conn_tracker.css('background','#f03d25');
    } else if(currState == 'connected'){
        if(conn_tracker.is(':visible')){
            conn_tracker.children('i.fa-wifi').removeClass('blink');
            conn_tracker.css('background','#04cc04',function(){
                conn_tracker.fadeOut(2000);
            });
        }
    }

});

/*---------------* Pusher Trigger subscription succeeded *---------------*/
presenceChannel.bind('pusher:subscription_succeeded', function(members){
    chatMemberUpdate(members);
    users.then(function(){
        if(localStorage.staff_to_redirect){
            $('#contacts a#'+localStorage.staff_to_redirect).click();
            localStorage.staff_to_redirect = '';
        } else {
            setTimeout(function(){
                $('#frame #contacts ul li:first').find('a').click();
            },600);
        }
    });
});

/*---------------* Pusher Trigger user connected *---------------*/
presenceChannel.bind('pusher:member_added', function(member) {
    addChatMember(member);
    if(member.info.justLoggedIn){
        var message_selector = $('#contacts .contact a#'+member.id).find('.wrap .meta .preview');
        var old_message_content = message_selector.html();
        message_selector.html('<strong class="contact_role">'+member.info.name + "<?php echo _l('chat_user_is_online'); ?>"+'</strong>');
        setTimeout(function(){
            message_selector.html(old_message_content);
        },7000);
        $.notify('', {
            'title': app.lang.new_notification,
            'body': member.info.name + ' ' + prchatSettings.hasComeOnlineText,
            'requireInteraction': true,
            'icon': $('#header').find('img').attr('src'),
            'tag': 'user-join-'+member.id,
            'closeTime': 5000,
        });
    }
});
/*---------------* Pusher Trigger user logout *---------------*/
presenceChannel.bind('pusher:member_removed', function(members) {
    removeChatMember(members);
});

/*---------------*  Monitor members join and leave channels for better UI  *---------------*/
var pendingRemoves = [];

/*---------------* Members joing/leave channel live tracking *---------------*/
function addChatMember(member)
{
    var pendingRemoveTimeout = pendingRemoves[member.id];
    $('a#'+member.id+' .wrap span').addClass('online').removeClass('offline');
    if(member.info.justLoggedIn == true ){
        appendMemberToTop(member.id);
    } else {
        if($('#contacts li.contact#'+member.id).find('.unread-notifications').attr('data-badge') != 0){
            appendMemberToTop(member.id);
        }
    }
    if(pendingRemoveTimeout) {
        clearTimeout(pendingRemoveTimeout);
    }
}
function removeChatMember(members)
{
    pendingRemoves[members.id] = setTimeout(function() {
        //  if(presenceChannel.members.count > 0 ){
        //   $("#count").html(presenceChannel.members.count - 1);
        // }
        $('a#'+members.id+' .wrap span').addClass('online').removeClass('offline');
        chatMemberUpdate(members);
    }, 5000);
}

function appendMemberToTop(member){
    var $cloned = $('#contacts li.contact#'+member).clone();
    $('#contacts li.contact#'+member).remove();
    $cloned.prependTo('#contacts ul')
}

/*---------------* Bind the 'send-event' & update the chat box message log *---------------*/
presenceChannel.bind('send-event', function(data)
{
    $('#frame .messages').find('span.userIsTyping').fadeOut(500);
    if(data.last_insert_id){
        $('.messages').find('li.sent .you#'+userSessionId).attr('id','msg_'+data.last_insert_id)
        $('.messages').find('li.sent#'+userSessionId).attr('id',data.last_insert_id)
    }
    <?php if($chat_delete_option == '1' || is_admin()) :  ?>
    $('li#'+data.last_insert_id+' #msg_'+data.last_insert_id).tooltipster({
        content: $("<span id='"+data.last_insert_id+"' class='prchat_message_delete' onClick='delete_chat_message(this)'>"+prchatSettings.deleteChatMessage+"</span>"),
        interactive: true,
        side: 'left'
    });
    <?php endif; ?>
    if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id){

        data.message = createTextLinks_(emojify.replace(data.message));
        $('.messages#id_'+data.from+' ul').append('<li class="replies"><img class="friendProfilePic" src="'+fetchUserAvatar(data.from,data.sender_image)+'"/><p class="friend">'+ data.message+'</p></li>');
        $('#contacts .contact a#'+data.from).find('.wrap .meta .preview').html(data.message);
        $('#contacts .contact a#'+data.from).find('.wrap .meta .pull-right.time_ago').html(moment().format('hh:mm A'));
    }
    if (presenceChannel.members.me.id == data.to){
        var old_data = emojify.replace(data.message);
        data.message = escapeHtml(data.message);

        var firstname = presenceChannel.members.members[data.from].name.replace(/ .*/,'');

        if(data.message.includes('class="prchat_convertedImage"')){
            data.message = '<p class="tb">' + firstname + ' ' + '<?php echo _l('chat_new_file_uploaded'); ?></p>';
        }

        if(data.message.includes('data-lity target="blank" href')){
            data.message = '<p class="tb">' + firstname + ' ' + '<?php echo _l('chat_new_link_shared'); ?></p>';
        }

        var truncated_message = '';
        if(old_data.includes('emoji') && !old_data.includes('href')){
            $('#contacts .contact a#'+data.from).find('.wrap .meta .preview').html(old_data);
            scroll_event();
            return false;
        }

        if(!data.message.includes('class="tb"')){
            truncated_message = data.message.trunc(36);
        } else {
            truncated_message = data.message.trunc(46);
        }

        if ($(window).width() > 733) {
            $('#contacts .contact a#'+data.from).find('.unread-notifications').hide();
        }
        $('#contacts .contact a#'+data.from).find('.wrap .meta .preview').html(truncated_message);
    }
    scroll_event();
});
/*---------------* Detect when a user is typing a message *---------------*/
presenceChannel.bind('typing-event', function(data)
{
    if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id && data.message == 'true'){
        $('#frame .messages').find('span.userIsTyping#'+data.from).fadeIn(500);
    }
    else if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id && data.message == 'null'){
        $('#frame .messages').find('span.userIsTyping#'+data.from).fadeOut(500);
    }
});

/*---------------* Trigger notification popup increment*---------------*/
presenceChannel.bind('notify-event', function(data)
{
    if ($(window).width() < 733) {
        if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id){
            var notiBox = $('body').find('li.contact#'+data.from+' a#'+data.from);
            if(!$(notiBox).hasClass("active_chat")){
                $(notiBox).find('img').addClass('shaking');
                var notification = parseInt($(notiBox).find('.unread-notifications#'+data.from).attr('data-badge'));
                var badge = $(notiBox).find('.unread-notifications#'+data.from);
                badge.attr('data-badge',notification+1);
                $(notiBox).find('.unread-notifications#'+data.from).show();
            }
            delay(function(){
                $(notiBox).find('img').removeClass('shaking');
            },600);
        }
    }
});

/*---------------* On click send message button trigger post message *---------------*/
$('#frame').on('click','.enterBtn',function()
{
    const eventEnter = $.Event( "keypress", {  which:13 } );
    $('#frame').find('.chatbox').trigger(eventEnter);
});

/*---------------* chatMemberUpdate() place & update users on user page, unred messages notifications *---------------*/
function chatMemberUpdate()
{
    checkforNewMessages.then(function(r){
        if(!checkforNewMessages.responseText.includes('false')){
            var newUnreadMessages = JSON.parse(checkforNewMessages.responseText);
            users.then(function(){
                $.each(newUnreadMessages, function(i, sender) {
                    var insertId = $('#contacts a#'+sender.sender_id);
                    notifications = $('#contacts a#'+sender.sender_id).find('.unread-notifications#'+sender.sender_id);
                    if(sender.sender_id === $(insertId).attr('id')){
                        notifications.attr('data-badge',sender.count_messages);
                        notifications.show();
                    }
                });
            });
        }
    });

    users.then(function(data) {
        var offlineUser = '';
        var onlineUser = '';
        data = JSON.parse(data);
        $('.chatbox').prop('disabled','');
        $.each(data, function(user_id, value) {
            if (value.staffid != presenceChannel.members.me.id) {
                user = presenceChannel.members.get(value.staffid);
                if(value.message == undefined) value.message = prchatSettings.sayHiText + ' ' + strCapitalize(value.firstname + ' ' +value.lastname);
                if(value.time_sent_formatted == undefined) value.time_sent_formatted = '';
                if (user != null){
                    onlineUser += '<li class="contact" id="'+value.staffid+'" data-toggle="tooltip" data-container="body" title="<?php echo _l('chat_user_active_now'); ?>">';
                    onlineUser += '<a href="#'+value.staffid+'" id="'+value.staffid+'" class="on"><div class="wrap"><span class="online">';
                    onlineUser += '</span><img src="'+fetchUserAvatar(value.staffid,value.profile_image)+'" class="imgFriend" /><div class="meta"><p role="'+value.admin+'" class="name">'+strCapitalize(value.firstname + ' ' +value.lastname)+'</p><social_info skype="'+value.skype+'" facebook="'+value.facebook+'" linkedin="'+value.linkedin+'"></social_info>';
                    onlineUser += '<p class="preview">'+value.message+'</p><p class="pull-right time_ago">'+value.time_sent_formatted+'</p>';
                    onlineUser += '</div></div>';
                    onlineUser += '<span class="unread-notifications" id="'+value.staffid+'" data-badge="0"></span></a></li>';
                    // if(presenceChannel.members.count > 0 ){
                    // $("#count").html(presenceChannel.members.count - 1);
                    // }
                } else {
                    offlineUser += '<li class="contact" id="'+value.staffid+'"';
                    var lastLoginText = '';
                    if(value.last_login) {
                        lastLoginText = moment(value.last_login, "YYYYMMDD h:mm:ss").fromNow();
                    } else {
                        lastLoginText ='Never';
                    }
                    offlineUser += ' data-toggle="tooltip" data-container="body" title="<?php echo _l('chat_last_seen'); ?>: '+lastLoginText+'">';
                    offlineUser += '<a href="#'+value.staffid+'" id="'+value.staffid+'" class="off"><div class="wrap"><span class="offline"></span>';
                    offlineUser += '<img src="'+fetchUserAvatar(value.staffid,value.profile_image)+'" class="imgFriend" /><div class="meta"><p role="'+value.admin+'" class="name">'+strCapitalize(value.firstname + ' ' +value.lastname)+'</p>';
                    offlineUser += '<p class="preview">'+value.message+'</p><p class="pull-right time_ago">'+value.time_sent_formatted+'</p><social_info skype="'+value.skype+'" facebook="'+value.facebook+'" linkedin="'+value.linkedin+'"></social_info>';
                    offlineUser += '</div></div><span class="unread-notifications" id="'+value.staffid+'" data-badge="0"></span></a></li>';
                }
            }
        });
        $('#frame #contacts ul').html('');
        $('#frame #contacts ul').prepend(onlineUser+offlineUser);
        return false;
    });
}

/*---------------* Trigger click on user & create chat box and check for messages *---------------*/
$('#frame #contacts').on("click", "li a", function (event)
{
    $('#frame .chatbox').val('');
    var obj = $(this);
    var id = obj.attr('id').replace('id_', '');
    var contact_selector = $('#contacts a#' + id);
    $('#contacts li a').removeClass('active_chat');
    $('#contacts .contact').removeClass('active');
    contact_selector.parent('.contact').addClass('active');
    $(this).addClass('active_chat');
    if(contact_selector.parent('.contact').find('.tb')){
        contact_selector.parent('.contact').find('.tb').css({'font-weight':'normal','color':'rgba(153, 153, 153, 1)'});
    }
    createChatBox(obj);
    updateUnreadMessages($(this));
    if($('#search_field').val() !== ''){
        clearSearchValues();
    }
    setTimeout(function () {
        obj.find('.unread-notifications#'+id).attr('data-badge','0').hide();
    }, 1000);
});

/*---------------* Creating chat box from the html template to the DOM *---------------*/
var createChatBoxRequest = null;
function createChatBox(obj)
{
    $('.messages ul').html('');
    var id = obj.attr('href');
    var fullName = obj.find('.meta').children('p:first-child').text();
    var contactRole = obj.find('.meta').children('p:first-child').attr('role');

    var contact_id = id.replace("#", "");
    endOfScroll = [];
    offsetPush = [];
    id = id.replace("#", "id_");
    $('.messages').find('span.userIsTyping').attr('id',contact_id);

    $('#frame .content .contact-profile p').html(fullName+'<br><a target="_blank" href="'+site_url+'admin/profile/'+contact_id+'"><small class="contact_role"></small></a>');

    checkContactRole(contactRole);

    var currentActiveChatWindow = obj.hasClass('active');

    var dfd = $.Deferred();
    var promise = dfd.promise();

    if (!currentActiveChatWindow) {
        if(createChatBoxRequest) {
            createChatBoxRequest.abort();

        }
        createChatBoxRequest = $.get(prchatSettings.getMessages,
            {
                from:userSessionId,
                to:contact_id,
                offset:0,
                limit:20
            })
            .done(function(r){

                r = JSON.parse(r);
                message = r;

                if (typeof(offsetPush[contact_id]) == 'undefined') {
                    offsetPush[contact_id] = 0;
                }

                if (typeof(endOfScroll[contact_id]) == 'undefined') {
                    endOfScroll[contact_id] = 0;
                }

                offsetPush[contact_id] += 10;
                dfd.resolve(message);

            }).always(function(){
                if ($("#no_messages").length) {
                    $("#no_messages").remove();
                }
                createChatBoxRequest = null;
            });
    } else {
        dfd.resolve([]);
    }

    /*---------------* After users are fetched from database -> continue with loading *---------------*/
    promise.then(function(message)
    {
        var sliderimg = obj.find('img').prop("currentSrc");
        $('#frame .content .contact-profile img').prop("src",sliderimg);

        $('#frame form:hidden').attr('id', id);
        $('.messages#' + id).parent('.content').find('.to:hidden').val(id.replace("id_", ""));
        $('.messages#' + id).parent('.content').find('.from:hidden').val(userSessionId);

        $(message).each(function( key, value) {
            if (value.is_deleted == 1) {
                value.message = '<span>' + prchatSettings.messageIsDeleted + '</span>';
            } else {
                value.message = emojify.replace(value.message);
            }
            if(value.sender_id == userSessionId){
                $('.messages ul').prepend('<li class="sent" id="'+value.id+'"><img data-toggle="tooltip" data-container="body" data-placement="left" title="'+ value.time_sent_formatted +'" class="myProfilePic" src="'+ fetchUserAvatar(userSessionId,value.user_image) +'"/><p class="you" id="msg_'+value.id+'">'+ value.message +'</p></li>');
                <?php if($chat_delete_option == '1' || is_admin()) :  ?>
                if (value.is_deleted == 0) {
                    $('#msg_'+value.id).tooltipster({
                        content: $("<span id='"+value.id+"' class='prchat_message_delete' onClick='delete_chat_message(this)'>"+prchatSettings.deleteChatMessage+"</span>"),
                        interactive: true,
                        side: 'left'
                    });
                }
                <?php endif; ?>

            } else {
                $('.messages ul').prepend('<li class="replies"><img data-toggle="tooltip" data-container="body" data-placement="right" title="'+ value.time_sent_formatted +'" class="friendProfilePic" src="'+ fetchUserAvatar(value.sender_id,value.user_image) +'"/><p  class="friend">'+ value.message +'</p></li>');
            }
        });
    });
    fillSocialIconsData(obj);
    $('.messages').attr('id',id);
    $('.messages#'+id).attr('onscroll','loadMessages(this)');

    activateLoader();

    $.when(promise.then())
        .then(function () {
            if($(".messages").hasScrollBar && $(window).width() > 733){
                scroll_event();
                $('.message-input textarea.chatbox').focus();
            } else if($(window).width() < 733){
                // Due to mobile devices bug and toading time
                scroll_event();
                scroll_event();
            } else {
                // One last check for mobile devices
                scroll_event();
            }
        });

    $("#contacts").animate({ scrollTop: "0px" });
    $('.contact #'+id+' .from').val(presenceChannel.members.me.id);
    $('.contact #'+id+' .to').val(obj.attr('href'));
    getSharedFiles(userSessionId, contact_id);
    return false;
}
/*--------------------  * send message & typing event to server  * ------------------- */
$("#frame").on('keypress','textarea',function(e)
{
    var form = $(this).parents('form');
    var imgPath = $('#sidepanel #profile .wrap img').prop('currentSrc');

    if ( e.which == 13 ) {
        e.preventDefault();
        var message = $.trim($(this).val());
        if(message == '' || internetConnectionCheck() === false) {
            return false;
        }
        var current_active_user_status = $('.contact.active a.on.active_chat').length;

        message = createTextLinks_(emojify.replace(message));

        $('#contacts .contact.active').find('.wrap .meta .preview').html('<?php echo _l('chat_message_you'); ?>' + ' ' + escapeHtml(message));
        $('.messages ul').append('<li class="sent" id="'+userSessionId+'"><img class="myProfilePic" src="'+imgPath+'"/><p class="you" id="'+userSessionId+'">'+ message+'</p></li>');
        $(this).next().next().next().val('false');
        message = escapeHtml(message);
        // send event
        var formData = form.serializeArray();
        formData.push({ name: "is_member_active", value: current_active_user_status });
        $.post(prchatSettings.serverPath,formData);
        $(this).val('');
        $(this).focus();
        scroll_event();
    }
    else if (!$(this).val() || ($(this).next().next().next().val() == 'null' && $(this).val()))
    {
        // typing event
        $(this).next().next().next().val('true');
        $.post(prchatSettings.serverPath, form.serialize());
    }
});

/*---------------* updating unread messages trigger and notification trigger *---------------*/
function updateUnreadMessages(chat)
{
    var timeOut = 2000;
    var linkId = $(chat).attr('id');
    setTimeout(function(){
        if(linkId){
            updateLatestMessages(linkId);
        }
    },timeOut)
    $('.unread-notifications#'+linkId).hide();
    return false;
}

/*---------------* Additional checks for chatbox and unread message update control *---------------*/
$('#frame').on( "click", "textarea", function() { updateUnreadMessages(this); });

/*---------------* prevent showing dots if user is not typing *---------------*/
$("#frame").on("focus",".chatbox, .messages",function()
{
    $('.messages').find('span.userIsTyping').fadeOut(500);
    if($('.tb')){
        $('.tb').css({'font-weight':'normal','color':'rgba(153, 153, 153, 1)'});
    }
});

/*---------------* Switch user chat theme *---------------*/
function chatSwitchTheme(theme_name){
    alert(theme_name);
    $.post(prchatSettings.switchTheme,{theme_name:theme_name}).done(function(r){
        if(r.success !== false){
            location.reload();
        }
    });
}

/*---------------* Search users *---------------*/
$("#frame #search").on("keyup", function()
{
    var value = $(this).find('#search_field').val().toLowerCase();
    $("#frame  #contacts ul li").filter(function() {
        $(this).toggle($(this).children('a').find('p.name').text().toLowerCase().indexOf(value) > -1);
    });
});

/*---------------* Security escaping html in chatboxes prevent database injection *---------------*/
function escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/'/g, "&#039;");
}
/*---------------* On focus out clear out input field and show all users if not found in searchbox *---------------*/
function clearSearchValues()
{
    $('#frame #search_field').val('');
    $("#contacts ul li").filter(function() {
        $(this).css('display','block');
        $('#profile').click();
    });
}
$('#frame').keyup('#search_field',function(e)
{
    if (e.keyCode === 27) {
        clearSearchValues();
    }
});

/*---------------* Check if element has scrollbar *---------------*/
(function($)
{
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})
(jQuery);
/*---------------* UI Track chat monitor current load activity for mobile version *---------------*/
if ($(window).width() > 733) {
    $('#switchTheme').show();
    $('#shared_user_files').show();
    $('body').removeClass('hide-sidebar').addClass('show-sidebar');
} else {
    $('#sharedFiles').css('display','none');
    $('#switchTheme').hide();
    $('#shared_user_files').hide();
    $('body').removeClass('show-sidebar').addClass('hide-sidebar');
}

/*---------------* UI Track chat monitor resize activity *---------------*/
function monitorWindowActivity()
{
// Menu toggle under 900px * for better UI *
    $(window).resize(function() {
        if ($(window).width() > 733) {
            $('#switchTheme').show();
            $('#shared_user_files').show();
            $('body').removeClass('hide-sidebar').addClass('show-sidebar');
        } else {
            $('#sharedFiles').css('display','none');
            $('#switchTheme').hide();
            $('#shared_user_files').hide();
            $('body').removeClass('show-sidebar').addClass('hide-sidebar');
        }
        if($('#frame #sidepanel #contacts li').length > 10){
            $('#frame #sidepanel #contacts').css({ 'overflow-y':'scroll' });
        }
    });
}

/*---------------* Fill Social Iconds with data *---------------*/
function fillSocialIconsData(obj)
{
    var social_info_attributes = $(obj).find('social_info');

    var socialMedia = [
        {
            type:'skype',
            value: social_info_attributes[0].attributes.skype.value,
            link: 'skype:'+social_info_attributes[0].attributes.skype.value+'?call'
        },
        {
            type:'facebook',
            value: social_info_attributes[0].attributes.facebook.value,
            link: 'http://www.facebook.com/'+social_info_attributes[0].attributes.facebook.value
        },
        {
            type:'linkedin',
            value: social_info_attributes[0].attributes.linkedin.value,
            link: 'http://www.linkedin.com/in/'+social_info_attributes[0].attributes.linkedin.value
        },
    ];

    for(var i in socialMedia) {
        var element = $('#frame').find('.contact-profile #fa-'+socialMedia[i].type);
        socialMedia[i].value == ''
            ? element.hide()
            : element.attr('href', socialMedia[i].link).show()
    }

}

/*---------------* Delete own messages function *---------------*/
function delete_chat_message(msg_id)
{
    msg_id = $(msg_id).attr('id');
    var contact_id = $('#contacts ul li').children('a.active_chat').attr('id');
    var paragraph = "<p class='you message_was_deleted'>";
    var selector = $("li#"+msg_id);

    $.post(prchatSettings.deleteMessage,{id:msg_id,contact_id:contact_id}).done(function(response){
        if(response == 'true'){
            $('.tooltipster-base').hide();
            selector.find("p#msg_"+msg_id).remove();
            selector.append(paragraph);
            selector.find("p.you.message_was_deleted").html(prchatSettings.messageIsDeleted).removeClass('tooltipstered');
            getSharedFiles(userSessionId,contact_id);
        } else {
            alert_float('danger', '<?php echo _l('chat_error_float'); ?>');
        }
    });
}

/*---------------* Check contact/staff role and append *---------------*/
function checkContactRole(contactRole)
{
    if(contactRole == '1'){
        $('#frame .content .contact-profile p small').html("<?= _l('chat_role_administrator'); ?>")
    } else {
        $('#frame .content .contact-profile p small').html("<?= _l('chat_role_employee'); ?>")
    }
}

/*---------------* UI Loader for messages *---------------*/
function activateLoader()
{
    var initLoader = $('#frame .messages');
    $(initLoader).find('.message_loader').show(function() {
        delay(function(){
            $(initLoader).find('.message_loader').hide();
        },600);
    });
}

function getSharedFiles(own_id, contact_id){
    $.post("<?php echo site_url('admin/prchat_controller/getSharedFiles'); ?>",{own_id:own_id,contact_id:contact_id}).done(function(data){
        $('.history_slider').html('');
        $('.history_slider').html(JSON.parse(data));
    })
}
/*---------------* Truncate text message to contacts view left side *---------------*/
String.prototype.trunc = String.prototype.trunc ||
    function(n){
        return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
    };

/*---------------* Scroll bottom *---------------*/
function scroll_event(){
    $('.messages').scrollTop($('.messages')[0].scrollHeight);
}

/*---------------* Internet connection navigator tracker *---------------*/
function internetConnectionCheck() { return navigator.onLine ? true : false; }

/*---------------* Live internet connection tracking *---------------*/
function handleConnectionChange(event){
    var conn_tracker = $('.connection_field');
    if(event.type == "offline"){
        conn_tracker.fadeIn();
        conn_tracker.children('i.fa-wifi').addClass('blink');
        conn_tracker.css('background','#f03d25');
        conn_tracker.children('i.fa-wifi').fadeIn();
    }
    if(event.type == "online"){
        conn_tracker.css('background','#04cc04');
        conn_tracker.children('i.fa-wifi').fadeIn();
        conn_tracker.children('i.fa-wifi').removeClass('blink');
        conn_tracker.delay(4000).fadeOut(0,function(){
            conn_tracker.children('i.fa-wifi').fadeOut();
        });
    }
}

/*---------------* For mobile devices vh ports adjust for better UX *---------------*/
var vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty("--vh", vh+ "px");

window.addEventListener("resize",function(){
    var vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", vh+ "px");
});

/*---------------* Theme events  *---------------*/
$('#bottom-bar').on('click','#switchTheme',function(){
    $('.dropdown-content').toggle("slide");
});

// $('#settings').on('click',function(){
//
// alert(1);
// });

$('#sharedFiles').on('click','i.fa-times-circle-o',function(){
    $('#sharedFiles').css('display','none');
});

$('#shared_user_files').on('click',function(){
    if(!$('#sharedFiles').is(':visible')){
        $('#sharedFiles').css('display','block');
    } else {
        $('#sharedFiles').css('display','none');
    }
});

</script>

