<?php

defined('BASEPATH') or exit('No direct script access allowed');
$aColumns = [
    'sid',
    'staffname',
	'staff_emailid',
	'system_forward_ip',
	'ipaddress',
	'browser',
	'browser_version',
	'os',
	'date',
	'logout_time',
    ];

$sWhere = [];
if ($this->ci->input->post('activity_log_date')) {
    array_push($sWhere, 'AND date LIKE "' . to_sql_date($this->ci->input->post('activity_log_date')) . '%"');
}
$sIndexColumn = 'sid';
$sTable       = 'tblstaff_session';
$result       = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $sWhere);
$output       = $result['output'];
$rResult      = $result['rResult'];
foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];
        if ($aColumns[$i] == 'date') {
            $_data = _dt($_data);
        }
        $row[] = $_data;
    }
    $output['aaData'][] = $row;
}
