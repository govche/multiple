<?php

defined('BASEPATH') or exit('No direct script access allowed');

$has_permission_delete = has_permission('staff', '', 'delete');

$custom_fields = get_custom_fields('staff', [
    'show_on_table' => 1,
    ]);
$aColumns = [
    'firstname',
    'email',
    'last_login',
    'active',
    'specialist',
    ];
$sIndexColumn = 'staffid';
$sTable       = 'tblstaff';
$join         = [];
$i            = 0;
foreach ($custom_fields as $field) {
    $select_as = 'cvalue_' . $i;
    if ($field['type'] == 'date_picker' || $field['type'] == 'date_picker_time') {
        $select_as = 'date_picker_cvalue_' . $i;
    }
    array_push($aColumns, 'ctable_' . $i . '.value as ' . $select_as);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $i . ' ON tblstaff.staffid = ctable_' . $i . '.relid AND ctable_' . $i . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $i . '.fieldid=' . $field['id']);
    $i++;
}
            // Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

if(check_session_is_there() == true){
    if(check_session() == 'affiliate'){
$where=[
    'AND role=5',
    ];
}
else if(check_session() == 'staff'){
    $where=[
        'AND role !=5',
        ];
}
}
else{
$where = do_action('staff_table_sql_where', []);
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'profile_image',
    'lastname',
    'staffid',
    'admin',
    ]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
        }
        if ($aColumns[$i] == 'last_login') {
            if ($_data != null) {
                $_data = '<span class="text-has-action" data-toggle="tooltip" data-title="' . _dt($_data) . '">' . time_ago($_data) . '</span>';
            } else {
                $_data = 'Never';
            }
        } elseif ($aColumns[$i] == 'specialist') {
           
             $data=array();
                $data=explode(',', $_data);
                 $increment = 0;
                 if(count($data)>2)
                {
                    $_data='';
                    $_d='';
                     while($increment<count($data)-1)
                    {
                        if($increment <1)
                        {
                        $_data.=$data[$increment].',';
                         
                        }
                        elseif($increment==1)
                        {
                           $_data.=$data[$increment]; 
                        }
                        else
                        {
                            $_d.=$data[$increment].',';
                        }
                        $increment=$increment+1;
                     }
                    $_d.=$data[$increment];
                    $count=count($data)-2;
                     $_data .= '<span style="color:#84c529;" data-toggle="tooltip"  title="'.$_d.'" data-placement="bottom">'.'&nbsp;&nbsp;'.'+'.$count.'&nbsp;Services'.'</span>'.'&nbsp;&nbsp;<a class="btn btn-info" style=" border-radius: 80%; padding: 2px 3px; background-color:#03a9f4;font-size: 12px;" onclick=(add("'. $aRow['staffid'] .'")) data-toggle="tooltip" title=" Add Services" data-placement="top"> ' . _l(' ADD') . '</a>';
                }
                else
                {
            $_data .= '    <a class="btn btn-info" style=" border-radius: 80%; padding: 2px 3px; background-color:#008ece;font-size: 12px;" onclick=(add("'. $aRow['staffid'] .'")) data-toggle="tooltip" title=" Add Services" data-placement="top"> ' . _l(' ADD') . '</a>';
        }
         } elseif ($aColumns[$i] == 'active') {
            $checked = '';
            if ($aRow['active'] == 1) {
                $checked = 'checked';
            }

            $_data = '<div class="onoffswitch">
                <input type="checkbox" ' . ($aRow['staffid'] == get_staff_user_id() ? 'disabled' : '') . ' data-switch-url="' . admin_url() . 'staff/change_staff_status" name="onoffswitch" class="onoffswitch-checkbox" id="c_' . $aRow['staffid'] . '" data-id="' . $aRow['staffid'] . '" ' . $checked . '>
                <label class="onoffswitch-label" for="c_' . $aRow['staffid'] . '"></label>
            </div>';

            // For exporting
            $_data .= '<span class="hide">' . ($checked == 'checked' ? _l('is_active_export') : _l('is_not_active_export')) . '</span>';
        } elseif ($aColumns[$i] == 'firstname') {
            $_data = '<a href="' . admin_url('staff/profile/' . $aRow['staffid']) . '">' . staff_profile_image($aRow['staffid'], [
                'staff-profile-image-small',
                ]) . '</a>';
            $_data .= ' <a href="' . admin_url('staff/member/' . $aRow['staffid']) . '">' . $aRow['firstname'] . ' ' . $aRow['lastname'] . '</a>';

            $_data .= '<div class="row-options">';
            $_data .= '<a href="' . admin_url('staff/member/' . $aRow['staffid']) . '">' . _l('view') . '</a>';

            if (($has_permission_delete && ($has_permission_delete && !is_admin($aRow['staffid']))) || is_admin()) {
                if ($has_permission_delete && $output['iTotalRecords'] > 1 && $aRow['staffid'] != get_staff_user_id()) {
                    $_data .= ' | <a href="#" onclick="delete_staff_member(' . $aRow['staffid'] . '); return false;" class="text-danger">' . _l('delete') . '</a>';
                }
            }

            $_data .= '</div>';
        } elseif ($aColumns[$i] == 'email') {
            $_data = '<a href="mailto:' . $_data . '">' . $_data . '</a>';
        } else {
            if (strpos($aColumns[$i], 'date_picker_') !== false) {
                $_data = (strpos($_data, ' ') !== false ? _dt($_data) : _d($_data));
            }
        }
        $row[] = $_data;
    }

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
