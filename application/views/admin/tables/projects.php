<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionEdit   = has_permission('projects', '', 'edit');
$hasPermissionDelete = has_permission('projects', '', 'delete');
$hasPermissionCreate = has_permission('projects', '', 'create');

$aColumns = [
    'tblprojects.id as id',
    'name',
    get_sql_select_client_company(),
    //'(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM tbltags_in JOIN tbltags ON tbltags_in.tag_id = tbltags.id WHERE rel_id = tblprojects.id and rel_type="project" ORDER by tag_order ASC) as tags',
    'start_date',
    'deadline',
    '(SELECT GROUP_CONCAT(CONCAT(firstname, \' \', lastname) SEPARATOR ",") FROM tblprojectmembers JOIN tblstaff on tblstaff.staffid = tblprojectmembers.staff_id WHERE project_id=tblprojects.id ORDER BY staff_id) as members',
     'date_finished',
    'status',
    'tblclients.phonenumber as number',
    'tblclients.premium as premium_user',

    
    ];


$sIndexColumn = 'id';
$sTable       = 'tblprojects';

$join = [
    'JOIN tblclients ON tblclients.userid = tblprojects.clientid',
];

$where  = [];
$filter = [];

if ($clientid != '') {
    array_push($where, ' AND clientid=' . $clientid);
}

if (!has_permission('projects', '', 'view') || $this->ci->input->post('my_projects')) {
    array_push($where, ' AND tblprojects.id IN (SELECT project_id FROM tblprojectmembers WHERE staff_id=' . get_staff_user_id() . ')');
}

$statusIds = [];

foreach ($this->ci->projects_model->get_project_statuses() as $status) {
    if ($this->ci->input->post('project_status_' . $status['id'])) {
        array_push($statusIds, $status['id']);
    }
}

if (count($statusIds) > 0) {
    array_push($filter, 'OR status IN (' . implode(', ', $statusIds) . ')');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

$custom_fields = get_table_custom_fields('projects');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblprojects.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$aColumns = do_action('projects_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'clientid',
    '(SELECT GROUP_CONCAT(staff_id SEPARATOR ",") FROM tblprojectmembers WHERE project_id=tblprojects.id ORDER BY staff_id) as members_ids',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $link = admin_url('projects/view/' . $aRow['id']);

    $row[] = '<a href="' . $link . '">' . $aRow['id'] . '</a>';

    $name = '<a href="' . $link . '">' . $aRow['name'] . '</a>';

    $name .= '<div class="row-options">';

    $name .= '<a href="'.$link.'">'._l('view').'</a>';

   /* if ($hasPermissionCreate && !$clientid) {
        $name .= ' | <a href="#" onclick="copy_project(' . $aRow['id'] . ');return false;">'._l('copy_project').'</a>';
    }*/

    if ($hasPermissionEdit) {
        $name .= ' | <a href="'.admin_url('projects/project/'. $aRow['id']).'">'._l('edit').'</a>';
    }

    if ($hasPermissionDelete) {
        $name .= ' | <a href="'.admin_url('projects/delete/'. $aRow['id']).'" class="text-danger _delete">'._l('delete').'</a>';
    }

    $name .= '</div>';

    $row[] = $name;
    
        if($aRow['premium_user'] == 1){
        $content = '-<span style="color:red;font-size:15px"> Premium Client</span>';
    }
    else{
        $content ='';
    }

    

    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] .$content. '</a>';

    //$row[] = render_tags($aRow['tags']);

    $row[] = _d($aRow['start_date']);

    $row[] = _d($aRow['deadline']);
    $row[] = _d($aRow['date_finished']);

    $membersOutput = '';

    $members       = explode(',', $aRow['members']);
    $exportMembers = '';
    foreach ($members as $key => $member) {
        if ($member != '') {
            $members_ids = explode(',', $aRow['members_ids']);
            $member_id   = $members_ids[$key];
            $membersOutput .= '<a href="' . admin_url('profile/' . $member_id) . '">' .
            staff_profile_image($member_id, [
                'staff-profile-image-small mright5',
                ], 'small', [
                'data-toggle' => 'tooltip',
                'data-title'  => $member,
                ]) . '</a>';
            // For exporting
            $exportMembers .= $member . ', ';
        }
    }

    $membersOutput .= '<span class="hide">' . trim($exportMembers, ', ') . '</span>';
    $row[] = $membersOutput;




    if($aRow['id']) {

        $status=get_project_current_status($aRow['id']);

//        foreach ($status as $key)
//        {
//
//        }
        $row[] = $status;
    }
    else{
        $row[] = 'Not Project Assigned';
    }


    $status = get_project_status_by_id($aRow['status']);
    $row[]  = '<span class="label label inline-block project-status-' . $aRow['status'] . '" style="color:' . $status['color'] . ';border:1px solid ' . $status['color'] . '">' . $status['name'] . '</span>';

$row[] = get_project_notes_by_id($aRow['id']);





 if(is_agent() == true){
     
      $pro_fee_com=get_project_feedback_status($aRow['id']);
	 if($pro_fee_com==$aRow['id'])
	 {
		 $row[] = '<div style=color:green>Feedback Call Completed </div>';		
		$row[] = '<div style=color:green>Feedback Rating Completed </div>';		 
		 
	 }
	 
	 else{

if($aRow['premium_user'] == 1){

    if(has_permission('project_call','','view')){
        $row[] = '<i class="fa fa-ban fa-lg" aria-hidden="true"></i>';
        }

$row[] = '<a style="color:black"><button class="btn btn-primary btm-sm disabled" >Feedback</button></a>';
}


       else if(get_project_status($aRow['id']) == 4){
            $staff_id =get_staff_user_id();


  if(has_permission('project_call','','view')){
        $row[] = '<a   style="cursor:pointer!important"  onclick="call('.$aRow['number'].','.$aRow['id'].','.$aRow['clientid'].','.$staff_id.')"><i class="fa fa-phone-square fa-lg" aria-hidden="true"></i></a>';
  }
        $row[] = '<a   style="cursor:pointer!important;color:black"  href="' . admin_url('Projects/feedback/' . $aRow['id']) . '"><button class="btn btn-primary">Feedback</button></a>';

        }
        else{
            if(has_permission('project_call','','view')){
            $row[] = '<i class="fa fa-ban fa-lg" aria-hidden="true"></i>';
            }
            $row[] = '<a style="color:black"><button class="btn btn-primary btm-sm disabled" >Feedback</button></a>';

        }
    }

}







//     if(is_agent() == true){

//         if(get_project_status($aRow['id']) == 4){
       
//             $staff_id =get_staff_user_id();
//   if(has_permission('project_call','','view')){
//         $row[] = '<a   style="cursor:pointer!important"  onclick="call('.$aRow['number'].','.$aRow['id'].','.$aRow['clientid'].','.$staff_id.')"><i class="fa fa-phone-square fa-lg" aria-hidden="true"></i></a>';
//   }
//         $row[] = '<a   style="cursor:pointer!important;color:black"  href="' . admin_url('Projects/feedback/' . $aRow['id']) . '"><button class="btn btn-primary">Feedback</button></a>';

//         }
//         else{
//             if(has_permission('project_call','','view')){
//             $row[] = '<i class="fa fa-ban fa-lg" aria-hidden="true"></i>';
//             }
//             $row[] = '<a style="color:black"><button class="btn btn-primary btm-sm disabled" >Feedback</button></a>';

//         }
//     }


    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $hook = do_action('projects_table_row_data', [
        'output' => $row,
        'row'    => $aRow,
    ]);

    $row = $hook['output'];
    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
