<?php

defined('BASEPATH') or exit('No direct script access allowed');
$aColumns = [
	'id',
	'time_sent',
	'sender_id',
	'reciever_id',
	'message',
	
	
    ];

$sWhere = [];

$sIndexColumn = 'id';
$sTable       = 'tblchatmessages';
$result       = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $sWhere);
$output       = $result['output'];
$rResult      = $result['rResult'];
foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];
        if ($aColumns[$i] == 'time_sent') {
            $_data = _dt($_data);
        }elseif($aColumns[$i] == 'sender_id'){
			$_data = $aRow[$aColumns[$i]];
			$sendername=get_staff_sender_name_chart($_data);
			$_data = $sendername;
		}elseif($aColumns[$i] == 'reciever_id'){
			$_data = $aRow[$aColumns[$i]];
			$receivername=get_staff_sender_name_chart($_data);
			$_data = $receivername;
		}
        $row[] = $_data;
    }
    $output['aaData'][] = $row;
}
