<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<?php
			echo form_open($this->uri->uri_string(),array('id'=>'invoice-form','class'=>'_transaction_form invoice-form'));
			if(isset($invoice)){
				echo form_hidden('isedit');
			}
			?>
			<div class="col-md-12">
				<?php $this->load->view('admin/invoices/invoice_template'); ?>
			</div>
			<?php echo form_close(); ?>
			<?php $this->load->view('admin/invoice_items/item'); ?>
		</div>
	</div>
</div>
<?php init_tail(); ?>
<script>

	var predefined_replies_est = <?php echo  json_encode($predefined_replies) ;?>;
	var invoiceItems_est = <?php echo json_encode($invoice->items);?>;
	var add_items_est = <?php echo json_encode($add_items);?>;

	var selected_items = add_items_est || estimateItems_est;


	function arrayUnique(array) {
	    var a = array.concat();
	    for(var i=0; i<a.length; ++i) {
	        for(var j=i+1; j<a.length; ++j) {
	            if(a[i] === a[j])
	                a.splice(j--, 1);
	        }
	    }

	    return a;
	}

	$(function(){
		validate_invoice_form();
		// Init accountacy currency symbol
		init_currency_symbol();
		// Project ajax search
		init_ajax_project_search_by_customer_id();
		// Maybe items ajax search
	    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');
	  
	    if(selected_items){
	    	add_email_item_content(predefined_replies_est, selected_items,true);
	    }
	    

	    // Add predefined reply click
	    $('#insert_predefined_reply').on('change', function(e) {
	        e.preventDefault();
	        var selectpicker = $(this);
	        var id = selectpicker.val();
	        if (id != '') {
	            requestGetJSON('tickets/get_predefined_reply_ajax/' + id).done(function(response) {
	                tinymce.activeEditor.execCommand('mceInsertContent', true, response.message);
	                selectpicker.selectpicker('val', '');
	            });
	        }
	    });
	});

	function delete_item_est(row, itemid) {

    	//remove deleted items on table
    	selected_items = $.grep(selected_items, function(e){ 
		     return e.id != itemid; 
		});

    	// Set estimate mail content
    	add_email_item_content(predefined_replies_est, selected_items,false);


	    // delete item on table
	    $(row).parents('tr').addClass('animated fadeOut', function() {
	        setTimeout(function() {
	            $(row).parents('tr').remove();
	            calculate_total();
	        }, 50);
	    });
	    // If is edit we need to add to input removed_items to track activity
	    if ($('input[name="isedit"]').length > 0) {
	        $('#removed-items').append(hidden_input('removed_items[]', itemid));
	    }
	}

	function add_email_item_content(predefined_replies_est, selected_items, scroll = false){
// alert();
		var count = 1;
		var temp = [];
		
		predefined_replies_est.forEach(function(elemet){
	    	if(selected_items.some(function(o){return o["description"] === elemet.item_description;})){
	    		temp = arrayUnique(temp.concat(elemet.message.split("\r\n")));
	    	}
	    	if(count == predefined_replies_est.length){
	    		var message = temp.join('\r\n');
	    		setTimeout(function(){
	    			tinyMCE.activeEditor.setContent('');
                	tinymce.activeEditor.execCommand('mceInsertContent', false, message); 
                	if(scroll){
                		window.scrollTo( 0, 0 );
                	}               	
                }, 1000)
                temp = [];
                count_est = 1;
            }    
	    	count ++;
	    });
	}
</script>
<!--<script>
	$(function(){
		validate_invoice_form();
	    // Init accountacy currency symbol
	    init_currency_symbol();
	    // Project ajax search
	    init_ajax_project_search_by_customer_id();
	    // Maybe items ajax search
	    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');
	});
</script>-->
</body>
</html>
