<div class="col-md-12 no-padding animated fadeIn">
    <div class="panel_s">
        <?php echo form_open('admin/invoices/record_payment',array('id'=>'record_payment_form')); ?>
        <?php echo form_hidden('invoiceid',$invoice->id); ?>
        <div class="panel-body">
            <h4 class="no-margin"><?php echo _l('record_payment_for_invoice'); ?> <?php echo format_invoice_number($invoice->id); ?></h4>
           <hr class="hr-panel-heading" />
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $amount = $invoice->total_left_to_pay;
                    echo render_input('amount','record_payment_amount_received',$amount,'number',array('max'=>$amount)); ?>
                   <div  id="note_date">
                    <?php echo render_input('date','record_payment_date',_d(date('Y-m-d'))); ?>
                    </div>
                    <div class="form-group">
                        <label for="paymentmode" class="control-label"><?php echo _l('payment_mode'); ?></label>
                        <select class="selectpicker" name="paymentmode" id="paymentmode" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <option value=""></option>
                            <?php foreach($payment_modes as $mode){ ?>
                            <?php if(is_payment_mode_allowed_for_invoice($mode['id'],$invoice->id)){ ?>
                            <option value="<?php echo $mode['id']; ?>"><?php echo $mode['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <?php
                    $pr_template = total_rows('tblemailtemplates',array('slug'=>'invoice-payment-recorded','active'=>0)) == 0;
                    $sms_trigger = is_sms_trigger_active(SMS_TRIGGER_PAYMENT_RECORDED);
                    if($pr_template || $sms_trigger){ ?>
                    <div class="checkbox checkbox-primary mtop15 inline-block">
                        <input type="checkbox" name="do_not_send_email_template" id="do_not_send_email_template">
                        <label for="do_not_send_email_template">
                            <?php
                            if($pr_template){
                                echo _l('do_not_send_invoice_payment_email_template_contact');
                                if($sms_trigger) {
                                    echo '/';
                                }
                            }
                            if($sms_trigger) {
                                echo 'SMS' . ' ' . _l('invoice_payment_recorded');
                            }
                            ?>
                            </label>
                    </div>
                    <?php } ?>
                    <div class="checkbox checkbox-primary mtop15 do_not_redirect hide inline-block">
                        <input type="checkbox" name="do_not_redirect" id="do_not_redirect" checked>
                        <label for="do_not_redirect"><?php echo _l('do_not_redirect_payment'); ?></label>
                    </div>

                </div>
                <div class="col-md-6">
                    <?php echo render_input('transactionid','payment_transaction_id'); ?>
                       <div class="form-gruoup">
                            <label for="note" class="control-label"><?php echo _l('record_payment_leave_note'); ?></label>
                            <textarea name="note" class="form-control" rows="8" placeholder="<?php echo _l('invoice_record_payment_note_placeholder'); ?>" id="note"></textarea>
                        </div>
                </div>
            </div>
            <div class="pull-right mtop15">
                <a href="#" class="btn btn-danger" onclick="init_invoice(<?php echo $invoice->id; ?>); return false;"><?php echo _l('cancel'); ?></a>
                <a href="#" onclick="getamount();"  data-target="#confirm_popup" data-toggle="modal"><button type="button" class="btn btn-success" data-dismiss="modal"><?php echo _l('submit'); ?></button></a>                
            </div>
            <?php
            if($payments){ ?>
            <div class="mtop25 inline-block full-width">
                <h5 class="bold"><?php echo _l('invoice_payments_received'); ?></h5>
                <?php include_once(APPPATH . 'views/admin/invoices/invoice_payments_table.php'); ?>
            </div>
            <?php } ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div class="modal" id="confirm_popup">
    <div class="modal-dialog">
      <div class="modal-content">        
        <!-- Modal body -->
        <div class="modal-body text-center">
          <p><h4> Are you received <h3 id="disamt"><?php echo $invoice->total_left_to_pay ?></h3> </h4></p>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
           <button type="submit" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" data-form="#record_payment_form" class="btn btn-success submit_form"><?php echo _l('submit'); ?></button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
</div>

<script>
function getamount()
{ 
	var amount = $("#amount").val();//alert(amount.toFixed(2));
	$("#disamt").html(amount);
}
   $(function(){
     init_selectpicker();
     init_datepicker();
     _validate_form($('#record_payment_form'),{amount:'required',date:'required',paymentmode:'required'});
     var $sMode = $('select[name="paymentmode"]');
     var total_available_payment_modes = $sMode.find('option').length - 1;
     if(total_available_payment_modes == 1) {
        $sMode.selectpicker('val',$sMode.find('option').eq(1).attr('value'));
        $sMode.trigger('change');
     }
     $('.submit_form').on('click',function(){
        $('#record_payment_form').submit();
     });
    
 });
</script>
<script>
		
            $(document).ready(function(){
                var id=$('#paymentmode').val();
				
				 
                if(id==2) {
                    $('#note_date').hide();
                }
                else {
                    $('#note_date').show();
                }
            });
            $('#paymentmode').on('change',function(){
				
                // alert(this.value);
                if(this.value==2)
                {
                    $('#note_date').hide();
                }
                else
                {
                    $('#note_date').show();
                }
                if(this.value==1)
        {
            $('#transactionid').attr('required',true);
        }

            });

		</script>
		<script>
$(document).ready(function(){
    $("#date").prop('readonly', true)
	$("#date").datepicker({
        dateFormat: 'dd-mm-yy',
		 minDate: '-3',

	});
});


</script>

