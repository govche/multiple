 <style type="text/css">
table{width:100%;}
tr{height:50px;}
 .invoice_delete:hover {
  cursor:pointer;
 }
 input[type='checkbox']{
    margin-left:2%;
    margin-right:2%;
 }
 input[type='radio']{
    margin-left:2%;
    margin-right:2%;
 }
 .col-md-12{
     padding:7px;
 }
 .col-xs-6{
     padding:5px;
 }
</style> 

<?php
          // Get customer details in invoice helper
          $customer_details=customer_details($invoice->clientid); 
      
          //GET verification details in invoice helper
          $verification_details=verification_details($invoice->id);
          foreach($verification_details as $verification){
          if($verification['service'] == 'GST Registration - Proprietorship'){
          $gst_reg_pro=json_decode($verification['document'],true);
          $gst_reg_id=$verification['id'];
          $status=$verification['type'];
          }
}


?>
  
 <!-- GST registration proprietor verification modal -->
  
   <div class="modal fade " id="GSTRegistration-Proprietorship" role="dialog" style="">
    <div class="modal-dialog" style="margin-left: 18%;">
    <div class="modal-content" style="width: 150%;">
      <!-- Modal content-->
      
           <div class="modal-header" >
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">GST Registration - Proprietorship</h4>
           </div>
          
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
            <?php echo form_open(admin_url( 'invoices/add_verification_gstregistrationproprietor/'),array('class'=>'document','id'=>'customer_document')); ?>

            <div><h4 style="font-weight:bold"> Form</h4></div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     Name of the properitor
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="text" name="name_properitor" value="<?php echo $customer_details['firstname'].' '.$customer_details['lastname']; ?>">
                     <input type="hidden" name="verification_id" value="<?php echo $gst_reg_id; ?>">
                     </div>
                     </div>
                    
                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     Business place registeroffice address
                     <label>
                     </div>
                     <div class="col-sm-4">
                     <textarea name="ofcregaddress" class="ofcregaddress"><?php echo $customer_details['address']; ?> </textarea>
                     </div>
                     <div class="col-sm-4">
                     <input type="checkbox" name="address_verified" <?php if($gst_reg_pro['office_registration_address']['address_verified'] == 'on'){ echo 'checked'; } ?> >Verified
                     </div>
                     </div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     Proprietor's residential address
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <textarea name="residentialaddress" class="residentialaddress"><?php echo $gst_reg_pro['resitendial_address']['res_address'];   ?></textarea>
                     </div>
                     <div class="col-sm-4">
                     <input type="radio" value="1" name="same_as_ofcaddress" class="same_as_ofcaddress" <?php if($gst_reg_pro['resitendial_address']['same_as_office_address'] == '1'){ echo 'checked'; } ?>>Same as office address
                     </div>
                     </div>

                   
                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Contact Number
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="number" value="<?php echo $customer_details['phonenumber'];  ?>" name="contact_number" class="contact_number"> 
                    </div>
                    <div class="col-sm-4">
                    <input type="radio" name="contact_number_primary" class="contact_number_primary"  <?php if($gst_reg_pro['contact_number']['contact_number_primary'] =='on'){ echo 'checked'; }  ?> >Primary    
                    </div>
                    </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                     Alternate Number
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="number" name="alternative_number" class="alternative_number" value="<?php echo $gst_reg_pro['alternative_number']['alternative_number'] ?>"> 
                     </div>
                     <div class="col-sm-4">
                     <input  type="radio" name="alternative_number_primary" class="alternative_number_primary"  <?php if($gst_reg_pro['alternative_number']['alternative_number_primary'] =='on'){ echo 'checked'; }  ?>>Primary
                     </div>
                     </div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     Landline
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="number" name="landline_number" class="landline_number" value="<?php echo $gst_reg_pro['landline_number']['landline_number'] ?>"> 
                     </div>
                     <div class="col-sm-4">
                     <input type="radio" name="landline_number_primary" class="landline_number_primary" <?php if($gst_reg_pro['landline']['landline_primary'] =='on'){ echo 'checked'; } ?> >Primary
                     </div>
                     </div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     Email 
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="email" value="<?php echo $customer_details['email_id'];  ?>" name="email" class="email">
                     </div>
                     <div class="col-sm-4">
                     <input type="radio" name="email_primary" class="email_primary"   <?php if($gst_reg_pro['email']['email_primary'] =='on'){ echo 'checked'; } ?> >Primary
                     </div>
                     </div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     OTP Purpose
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="email" name="otp_email" class="otp_email" value="<?php echo $gst_reg_pro['otp_email']['otp_email'];  ?>">
                     </div>
                     <div class="col-sm-4">
                     <input type="radio" name="otp_email_primary" class="otp_email_primary" <?php if($gst_reg_pro['otp_email']['otp_email_primary'] =='on'){ echo 'checked'; } ?> >Primary
                     </div>
                     </div>
                     
                     <div class="col-md-12">
                     <div class="col-sm-6">
                     <label>
                      Nature Of Business
                     </label>
                     </div>
                     
                     <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="bonded_warehouse" <?php if($gst_reg_pro['nature_business']['bonded_warehouse'] =='on'){ echo 'checked';  } ?>>Bonded Warehouse</div>
                      <div class="col-xs-6"> <input type="checkbox" name="eop_stp_ehtp" <?php if($gst_reg_pro['nature_business']['eop_stp_ehtp'] =='on'){ echo 'checked';  } ?> >EOP/STP/EHTP </div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="export" class="export" <?php if($gst_reg_pro['nature_business']['export'] =='on'){ echo 'checked';  } ?> >Export</div>
                      <div class="col-xs-6"> <input type="checkbox" name="factory_manufacturing" <?php if($gst_reg_pro['nature_business']['factory_manufacturing'] =='on'){ echo 'checked';  } ?>>Factory / Manufacturing</div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="import" class="import" <?php if($gst_reg_pro['nature_business']['import'] =='on'){ echo 'checked';  } ?> >Import</div>
                      <div class="col-xs-6"> <input type="checkbox" name="supplier_services" <?php if($gst_reg_pro['nature_business']['supplier_services'] =='on'){ echo 'checked';  } ?> >Supplier / Services</div>
                      </div>
                      
                      <?php if($gst_reg_pro['nature_business']['lut'] ==1) { ?>
                      <div class="col-xs-12">
                      <div class="col-xs-4 lutlabel">If you have submitted LUT form ?</div>
                      <div class="col-xs-4 lutyes"><input type="radio" name="lut" checked>Yes</div>
                      <div class="col-xs-4 lutno"><input type="radio" name="lut" >No </div>
                      </div>
                      <?php } ?>
                      <div class="col-xs-12">
                      <div class="col-xs-4 lutlabel"></div>
                      <div class="col-xs-4 lutyes"></div>
                      <div class="col-xs-4 lutno"></div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="leasing_business" <?php if($gst_reg_pro['nature_business']['leasing_business'] =='on'){ echo 'checked';  } ?> >Leasing Business</div>
                      <div class="col-xs-6"> <input type="checkbox" name="office_saleoffice" <?php if($gst_reg_pro['nature_business']['office_saleoffice'] =='on'){ echo 'checked';  } ?> >Office / Sale office</div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="recipients_of_goods_services" <?php if($gst_reg_pro['nature_business']['recipients_of_goods_services'] =='on'){ echo 'checked';  } ?>>Recipient of goods or services</div>
                      <div class="col-xs-6"> <input type="checkbox" name="retail_business" <?php if($gst_reg_pro['nature_business']['retail_business'] =='on'){ echo 'checked';  } ?> >Retail Business</div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="warehouse_depot" <?php if($gst_reg_pro['nature_business']['warehouse_depot'] =='on'){ echo 'checked';  } ?>>Warehouse / Depot</div>
                      <div class="col-xs-6"> <input type="checkbox" name="wholesale_business" <?php if($gst_reg_pro['nature_business']['wholesale_business'] =='on'){ echo 'checked';  } ?>>Wholesale Business</div>
                      </div>
                      <div class="col-xs-12">
                      <div class="col-xs-6"><input type="checkbox" name="work_contract" <?php if($gst_reg_pro['nature_business']['work_contract'] =='on'){ echo 'checked';  } ?>> Works Contract</div>
                      <div class="col-xs-6"> <input type="checkbox" name="others" <?php if($gst_reg_pro['nature_business']['others'] =='on'){ echo 'checked';  } ?>>Others (Please Specify)</div></div>
                      <div class="col-xs-12">
                      <textarea cols="40" rows='5' name="nature_of_business_comment" placeholder="Comment about nature of business"><?php echo $gst_reg_pro['nature_business']['nature_of_business_comment'] ?></textarea>
                      </div>
                     </div>
                     
                     <div class="col-md-12">
                     <div class="col-sm-6">
                    <b style="font-weight:bold;font-size:15px">Document Required</b> </div>
  
                    </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                     PAN
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="pan_verified" <?php if($gst_reg_pro['document_verified']['pan_verified']){ echo 'checked'; } ?>>Verified
                    </div>
                    </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Aadhaar
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="aadhar_verified" <?php if($gst_reg_pro['document_verified']['aadhar_verified']){ echo 'checked'; } ?>>Verified
                    </div>
                    </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                     Driving License
                     </label>
                     </div>
                     <div class="col-sm-4">
                     <input type="checkbox" name="driving_license_verified" <?php if($gst_reg_pro['document_verified']['driving_license_verified']){ echo 'checked'; } ?>>Verified
                     </div>
                     </div>

                     <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Passport
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox" name="passport" <?php if($gst_reg_pro['document_verified']['passport']){ echo 'checked'; } ?>>Verified
                    </div>
                    </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Voter ID
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="voterid_verified" <?php if($gst_reg_pro['document_verified']['voterid_verified']){ echo 'checked'; } ?>>Verified</td></tr>
                   </div>
                   </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Bank Statement
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="bankstatement_verified" <?php if($gst_reg_pro['document_verified']['bankstatement_verified']){ echo 'checked'; } ?>>Verified
                     </div>
                     </div>

                    <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                    Photograph
                    </label>
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="photo_verified" <?php if($gst_reg_pro['document_verified']['photo_verified']){ echo 'checked'; } ?>>Verified
                    </div>
                    </div>
                    <div class="col-md-12">
                    <textarea cols="40" rows='5' name="verified_comment" placeholder="Comment about document verification"><?php echo $gst_reg_pro['document_verified']['verified_comment'] ?></textarea>
                    </div>
                     
                      <div class="col-md-12">
                    <div class="col-sm-4">
                    <label>
                     Office Address
                     </label>
                     </div>
                     <div class="col-sm-4">
                        <input type="radio" name="ofcaddress"  class="afcaddress_own" value="1" <?php if($gst_reg_pro['office_address']['ofcaddress'] == 1){ echo 'checked'; } ?>>Own Property
                         </div>
                         <div class="col-sm-4">
                        <input  type="radio" name="ofcaddress"  class="afcaddress_own" value="0" <?php if($gst_reg_pro['office_address']['ofcaddress'] == 0){ echo 'checked'; } ?>>Rent
                        </div>
                        <input type="hidden" name="type" id="type">
                        <input type="hidden" name="invoiceid" value="<?php echo $invoice->id; ?>">
                        <input type="hidden" name="service" value="GST Registration - Proprietorship">
                        <input type="hidden" name="clientid" value="<?php echo $invoice->clientid; ?>">
                       </div>


                       <div class="col-md-12">
                       <?php if($gst_reg_pro['office_address']['ofcaddress'] == 1){ ?>
                     <div class="col-sm-4"> 
               
                     </div>
                     <div class="col-sm-4 taxreceipt" > 
                     <input type="checkbox" name="properity_tax_receipt" value="1" <?php if($gst_reg_pro['office_address']['properity_tax_receipt'] ==1){ echo 'checked'; } ?> >Properity tax receipt
                     </div>
                     <div class="col-sm-4 eb"> 
                     <input type="checkbox" name="ebcard" value="1" <?php if($gst_reg_pro['office_address']['ebcard'] ==1){ echo 'checked'; } ?>>EB card
                     </div>
                   <?php  } else if($gst_reg_pro['office_address']['ofcaddress'] == 0){ ?>
                     <div class="col-sm-4"> 
               
               </div>
               <div class="col-sm-4 taxreceipt" > 
          
               </div>
               <div class="col-sm-4 eb"> 
               <input type="checkbox" name="rental_agreement" value="1" <?php if($gst_reg_pro['office_address']['rental_agreement'] ==1){ echo 'checked'; } ?>>rental Agreement
               </div>
                   <?php } ?>
                     </div>

                   
                  
                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     GST Filling Frequency
                     </label>
                     </div>
                     <div class="col-sm-4">
                        <input type="radio" name="frequency" value='1' <?php if($gst_reg_pro['frequency'] == 1){ echo 'checked'; } ?>>Yes
                        </div>
                        <div class="col-sm-4">
                         <input type="radio" name="frequency" value="0" <?php if($gst_reg_pro['frequency'] == 0){ echo 'checked'; } ?>>No
                         </div>
                         </div>

                     <div class="col-md-12">
                     <div class="col-sm-4">
                     <label>
                     If existing Registration
                     </label>
                     </div>
                     <div class="col-sm-4">
                        <input type="radio" name="exregistration" class="exregistration" value='1' <?php if($gst_reg_pro['exreg']['exr'] ==1){  echo 'checked'; } ?> >Yes
                        </div>
                        <div class="col-sm-4">
                         <input type="radio" name="exregistration" class="exregistration" value="0" <?php if($gst_reg_pro['exreg']['exr'] ==0){  echo 'checked'; } ?> >No
                         </div>
                         </div>

                         <div class="col-md-12">
                     <div class="col-sm-4"> 
                     </div>
 
                     <?php if($gst_reg_pro['exreg']['exr'] ==1){ ?>
                     <div class="col-sm-4 exregistration_msme " > 
                     <input type="checkbox" name="msme" value="1" <?php if($gst_reg_pro['exreg']['msme'] ==1){  echo 'checked'; } ?>>MSME
                     </div>
                     <?php } ?>
                     </div>
                     <div class="col-md-12">
                     <div class="col-sm-4"> 
                     </div>
                     <div class="col-sm-4 exregistration_msme" > 
                  
                     </div>
                     </div>
                    
                      
                 
                  <?php echo form_close(); ?>
                  <div class="col-md-12">
                  <div class="col-sm-4">
                  </div>

                  <div class="col-sm-4">
<?php if($status !=1){ ?>
                   <button class="btn btn-primary" onclick="save();">save</button>
<?php } ?>
                   </div>
                   <div class="col-sm-4">

                    <input type="submit" value="Submit" class="btn btn-primary" onclick="submit();" >
                    </div>
                    </div>                 
                    
                  </div>
                 
               
                         <div class="col-lg-6">
                         <div><h4 style="font-weight:bold">Documentation</h4></div> <table> 
                                
                                <?php
                        
                                foreach(customer_attachments($invoice->clientid) as $type => $_att){



                                   echo "<tr><td style='vertical-align: middle;'>";
                                          $download_indicator = 'id';
                                          $key_indicator = 'rel_id';
                                          $upload_path = get_upload_path_by_type('customer');
                             
                                              $url = site_url() .'download/file/client/';
                                              $download_indicator = 'attachment_key';
                                          
                                          
                        
                                            ?>
                             
                                        
                                                    <?php
                                                    $path = $upload_path . $_att[$key_indicator] . '/' . $_att['file_name'];
                                                   $is_image = false;
                        
                                           
                        
                                                 
                                              if(!isset($_att['external'])) {
                                                        $attachment_url = $url . $_att[$download_indicator];
                                                     $is_image = is_image($path);
                                                    $img_url = site_url('download/preview_image?path='.protected_file_url_by_path($path,true).'&type='.$_att['filetype']);
                                                      $lightBoxUrl = site_url('download/preview_image?path='.protected_file_url_by_path($path).'&type='.$_att['filetype']);
                                                   } else if(isset($_att['external']) && !empty($_att['external'])){
                        
                                                    if(!empty($_att['thumbnail_link'])){
                                                          $is_image = true;
                                                           $img_url = optimize_dropbox_thumbnail($_att['thumbnail_link']);
                                                        }
                        
                                                      $attachment_url = $_att['external_link'];
                                               }
                                                       if($is_image){
                                                       echo '<div class="preview_image">';
                                                     }
                                        
                                                   ?>
                                                        <a href="<?php if($is_image){ echo isset($lightBoxUrl) ? $lightBoxUrl : $img_url; } else {echo $attachment_url; } ?>"<?php if($is_image){ ?> data-lightbox="customer-profile" <?php } ?> class="display-block mbot5">
                                                        <?php if($is_image){ ?>
                                                      
                                                        <div class="table-image">
                                                            <div class="text-center"></div>
                                                            <img src="<?php echo $img_url; ?>" class="img-table-loading" data-orig="<?php echo $img_url; ?>">
                                                            
                                                        </div></td></tr><tr><td>
                                                        <div>
                                                        <span ><?php echo $_att['file_name']; ?></span>
                                                        </div>
                                                       <?php } else { ?>
                                                       <i class="<?php echo get_mime_class($_att['filetype']); ?>"></i> <?php echo $_att['file_name']; ?>
                                                       <?php } ?>
                                                   </a>
                                                   <?php if($is_image){ echo '</div>'; }  echo "</td></tr>"; } ?>
                                                   </table>
                        
                      </div>
               </div>
               
               
               
         </div>
               
   </div>


    <!-- GST registration proprietor  -->


<script>
$('.afcaddress_own').click(function(){
if($(this).val() == '1'){
   $('.taxreceipt').empty();
   $('.eb').empty();
   $('.taxreceipt').append('<input type="checkbox" name="properity_tax_receipt" value="1">Properity tax receipt');
   $('.eb').append('<input type="checkbox" name="ebcard" value="1">EB card');


}
else if($(this).val() == '0'){
   $('.taxreceipt').empty();
   $('.eb').empty();
   $('.taxreceipt').append('<input type="checkbox" name="rental_agreement" value="1">Rental Agreement');
}
});

$('.exregistration').click(function(){
if($(this).val() == '1'){
   $('.exregistration_msme').empty();

   $('.exregistration_msme').html('<input type="checkbox" name="msme" value="1" >MSME ');

}
else if($(this).val() == '0'){
   $('.exregistration_msme').empty();
   
}
});

function save(){
   $('#type').val('0');
   $('#customer_document').submit();
   }
   function submit(){
   $('#type').val('1');
   $('#customer_document').submit();
   }

   $('.same_as_ofcaddress').click(function(){

    if($(this).val() ==1){
    var ofcregaddress = $('.ofcregaddress').val();
    $('.residentialaddress').empty();
    $('.residentialaddress').html(ofcregaddress);
}
   });

   $('.import').click(function(){
    
      if($(this).val() == 'on'){
         $('.lutlabel').empty();
         $('.lutyes').empty();
         $('.lutno').empty();
         $('.lutlabel').append('<label>If you have submit LUT ?</label>');
         $('.lutyes').append('<input type="radio" name="lut" value="1" class="lut">Yes');
         $('.lutno').append('<input type="radio" name="lut" value="0" class="lut">No');

      

      }
 
   });

   $('.export').click(function(){

      if($(this).val() == 'on'){
         $('.lutlabel').empty();
         $('.lutyes').empty();
         $('.lutno').empty();
         $('.lutlabel').append('<label>If you have submit LUT ?</label>');
         $('.lutyes').append('<input type="radio" name="lut" value="1" class="lut">Yes');
         $('.lutno').append('<input type="radio" name="lut" value="0" class="lut">No');

      

      }
 
   });

   $('.alternative_number_primary').click(function(){

      if($('.alternative_number').val() !=''){
      $('.contact_number_primary').prop('checked',false);
      $('.landline_number_primary').prop('checked',false);
      }
      else{
         $('.alternative_number_primary').prop('checked',false);
         alert('Alternative number is Empty');
      }

   });
   $('.contact_number_primary').click(function(){
      if($('.contact_number').val() !=''){
      $('.alternative_number_primary').prop('checked',false);
      $('.landline_number_primary').prop('checked',false);
      }
      else{
         $('.contact_number_primary').prop('checked',false);
         alert('Contact Number is empty');
      }

   });
   $('.landline_number_primary').click(function(){
      if($('.landline_number').val() !=''){
      $('.contact_number_primary').prop('checked',false);
      $('.alternative_number_primary').prop('checked',false);
      }
      else{
         $('.landline_number_primary').prop('checked',false);
         alert('Landline number is empty');
      }

   });

   $('.otp_email_primary').click(function(){
      if($('.otp_email').val() !=''){
         $('.email_primary').prop('checked',false);

      }
      else{
         $('.otp_email_primary').prop('checked',false);
         alert('OTP purpose email is empty');
      }

   });
   $('.email_primary').click(function(){
      if($('.email').val() !=''){
         $('.otp_email_primary').prop('checked',false);

      }
      else{
         $('.email_primary').prop('checked',false);
         alert('Email address is empty');
      }

   });

    </script>