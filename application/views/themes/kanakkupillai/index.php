<?php
echo $head;
if($use_navigation == true){
	get_template_part('navigation');
}
?>
<div id="wrapper">
	<div id="content">
		<div class="container">
			<div class="row">
				<?php get_template_part('alerts'); ?>
			</div>
		</div>
		<?php if(isset($knowledge_base_search)){ ?>
		<?php get_template_part('knowledge_base/search'); ?>
		<?php } ?>
		<div class="container">
			<div class="row">
					<?php // Dont show calendar for invoices,estimates,proposals etc.. views where no navigation is included or in kb area
					if(is_client_logged_in() && $use_submenu == true && !isset($knowledge_base_search)){ ?>
					<ul class="submenu customer-top-submenu">
						<li class="customers-top-submenu-files"><a href="<?php echo site_url('clients/files'); ?>"><i class="fa fa-file" aria-hidden="true"></i> <?php echo _l('customer_profile_files'); ?></a></li>
						<li class="customers-top-submenu-calendar"><a href="<?php echo site_url('clients/calendar'); ?>"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> <?php echo _l('calendar'); ?></a></li>
					</ul>
					<div class="clearfix"></div>
					<?php } ?>
					<?php echo $view; ?>
				</div>
			</div>
		</div>
		<?php
		echo $footer;
		echo $scripts;
		?>
	</div>
</body>

 <script type="text/javascript">
if (document.layers) {
    //Capture the MouseDown event.
    document.captureEvents(Event.MOUSEDOWN);
 
    //Disable the OnMouseDown event handler.
    document.onmousedown = function () {
        return false;
    };
}
else {
    //Disable the OnMouseUp event handler.
    document.onmouseup = function (e) {
        if (e != null && e.type == "mouseup") {
            //Check the Mouse Button which is clicked.
            if (e.which == 2 || e.which == 3) {
                //If the Button is middle or right then disable.
                return false;
            }
        }
    };
}
 
//Disable the Context Menu event.
document.oncontextmenu = function () {
    return false;
};
</script>

<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

</html>
