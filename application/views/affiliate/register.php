
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

<?php $this->load->view('authentication/includes/head.php'); ?>
<body class="login_admin"<?php if(is_rtl()){ echo ' dir="rtl"'; } ?>>
 <div class="container">
  <div class="row">
   <div class="col-md-6 col-md-offset-3 authentication-form-wrapper">
    <div class="company-logo">
      <?php get_company_logo(); ?>
    </div>
    <div class="mtop40 authentication-form">
      <h1><?php echo "Register"; ?></h1>
      <?php $this->load->view('authentication/includes/alerts'); ?>

      <?php echo form_open(site_url('authentication/register')); ?>
      <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
      <?php do_action('after_admin_login_form_start'); ?>

        <div class="row">
               <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 col-xl-6">
                  <div class="form-group">
                      <label for="email" class="control-label">First name </label>
                      <input type="text" id="fname" name="fname"  class="form-control" required >
                  </div>
               </div>

               <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label for="email" class="control-label">Last name </label>
                    <input type="text" id="lname" name="lname"  class="form-control" required >
                  </div>
               </div>

               <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 col-xl-6">
                  <div class="form-group">
                      <label for="email" class="control-label">Mobile </label>
                      <input type="number" id="number" name="number" pattern=".{10,10}"  class="form-control" required >
                     <input type="hidden" id="numbervali" value="">
                      <span id="match_number" style="color:red;font-size:15px"></span>
                      <span id="notmatch_number" style="color:green;font-size:15px"></span>
                  </div>
               </div>
               <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label for="email" class="control-label"><?php echo _l('admin_auth_login_email'); ?></label>
                    <input type="email" id="email" name="email"  class="form-control" required  >
                    <input type="hidden" id="emailvali" value="">
                    <span id="match_email" style="color:red;font-size:15px"></span>
                    <span id="notmatch_email" style="color:green;font-size:15px"></span>

                  </div>
               </div>

               <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
                      <div class="form-group">
                        <label for="password" class="control-label"><?php echo _l('admin_auth_login_password'); ?></label>
                        <input type="password" id="password" pattern=".{6,12}"  name="password" class="form-control" required>
                    </div>
               </div>

               <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                <button type="submit" id="submit" class="btn btn-info btn-block _register">Register</button>
              </div>
               </div>
              
        </div>
     
      <div class="form-group">
      <a class="_login  btn" href="<?php echo site_url('authentication/admin'); ?>">Already An Affiliate! Login Here</a>
          <?php if(get_option('recaptcha_secret_key') != '' && get_option('recaptcha_site_key') != ''){ ?>
      <div class="g-recaptcha" data-sitekey="<?php echo get_option('recaptcha_site_key'); ?>"></div>
      <?php } ?>
      <?php do_action('before_admin_login_form_close'); ?>
      <?php echo form_close(); ?>
      </div>
     
    </div>
  </div>
</div>
</div>
<!--<script>-->
<!--$('#email').keyup(function(){-->
<!--    var email=$(this).val();-->
<!--    $.ajax({-->
<!--            url: '<?php echo base_url();?>admin/staff/check_mail/'+email,-->
<!--            type: 'GET',-->
<!--            dataType:'json',-->
          
<!--            success: function (data) {-->
        
<!--                if(data.status=='success') {-->
<!--                  $('#notmatch_email').html('');-->
<!--                 $('#match_email').html('Already Mail is exist');-->
<!--                 $('#emailvali').val('1');-->

<!--                 if($('#emailvali').val() == 1 || $('#numbervali').val() == 1){-->
<!--                   $('#submit').attr('disabled',true);-->

<!--                 }-->
<!--                 else{-->
<!--                  $('#submit').attr('disabled',false);-->
<!--                 }-->

<!--                }-->
<!--                else{-->
<!--                  $('#match_email').html('');-->
<!--                  $('#notmatch_email').html(data.data);-->

<!--                  $('#emailvali').val(0);-->
<!--                  if($('#emailvali').val() == 0 && $('#numbervali').val() == 0){-->
<!--                   $('#submit').attr('disabled',false);-->

<!--                 }-->
<!--                 else{-->
<!--                  $('#submit').attr('disabled',true);-->
<!--                 }-->
<!--                }-->
               
<!--            }-->
<!--        });-->
<!--})-->

<!--$('#number').keyup(function(){-->
<!--    var number=$(this).val();-->
<!--    $.ajax({-->
<!--            url: '<?php echo site_url();?>affiliate/register/check_number/'+number,-->
<!--            type: 'GET',-->
<!--            dataType:'json',-->
<!--            success: function (data) {-->
<!--                if(data.status=='success') {-->
<!--                  $('#notmatch_number').html('');-->
<!--                 $('#match_number').html('Already Mobile number is exist');-->
<!--                 $('#numbervali').val('1');-->
<!--                 if($('#emailvali').val() == 1 || $('#numbervali').val() == 1){-->
<!--                   $('#submit').attr('disabled',true);-->

<!--                 }-->
<!--                 else{-->
<!--                  $('#submit').attr('disabled',false);-->
<!--                 }-->
<!--                }-->
<!--                else{-->

<!--                  $('#match_number').html('');-->
<!--                  $('#notmatch_number').html(data.data);-->
<!--                  $('#numbervali').val(0);-->
<!--                  if($('#emailvali').val() == 0 && $('#numbervali').val() == 0){-->
<!--                   $('#submit').attr('disabled',false);-->

<!--                 }-->
<!--                 else{-->
<!--                  $('#submit').attr('disabled',true);-->
<!--                 }-->
<!--                }-->
            
<!--            }-->

<!--        });-->
<!--})-->
<!--</script>-->


</body>
</html>

