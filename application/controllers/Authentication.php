<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authentication extends CRM_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->app->is_db_upgrade_required()) {
            redirect(admin_url());
        }
        load_admin_language();
        $this->load->model('Authentication_model');
        $this->load->library('form_validation');
             $this->load->model('Affiliate_model','affiliate');
        $this->load->model('roles_model');

        $this->form_validation->set_message('required', _l('form_validation_required'));
        $this->form_validation->set_message('valid_email', _l('form_validation_valid_email'));
        $this->form_validation->set_message('matches', _l('form_validation_matches'));
    }

    public function index()
    {
        $this->admin();
    }

    public function admin()
    {
        if (is_staff_logged_in()) {
            redirect(admin_url());
        }

        $this->form_validation->set_rules('password', _l('admin_auth_login_password'), 'required');
        $this->form_validation->set_rules('email', _l('admin_auth_login_email'), 'trim|required|valid_email');
        if (get_option('recaptcha_secret_key') != '' && get_option('recaptcha_site_key') != '') {
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');
        }
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $email    = $this->input->post('email');
                $password = $this->input->post('password', false);
                $remember = $this->input->post('remember');

                $data = $this->Authentication_model->login($email, $password, $remember, true);

                if (is_array($data) && isset($data['memberinactive'])) {
                    set_alert('danger', _l('admin_auth_inactive_account'));
                    redirect(site_url('authentication/admin'));
                } elseif (is_array($data) && isset($data['two_factor_auth'])) {
                    $this->Authentication_model->set_two_factor_auth_code($data['user']->staffid);

                    $this->load->model('emails_model');

                    $sent = $this->emails_model->send_email_template(
                        'two-factor-authentication',
                        $email,
                        get_staff_merge_fields($data['user']->staffid)
                    );

                    if (!$sent) {
                        set_alert('danger', _l('two_factor_auth_failed_to_send_code'));
                        redirect(site_url('authentication/admin'));
                    } else {
                        set_alert('success', _l('two_factor_auth_code_sent_successfully', $email));
                    }
                    redirect(site_url('authentication/two_factor'));
                } elseif ($data == false) {
                    set_alert('danger', _l('admin_auth_invalid_email_or_password'));
                    redirect(site_url('authentication/admin'));
                } else {
                    
                    	//for storing staff session 
					$this->db->select('*');
            $this->db->where('email', $email);
            $staff_id = $this->db->get('tblstaff')->result_array();
			$stid=$staff_id[0]['staffid'];
			$stname=$staff_id[0]['firstname'];
			
			$ip_address = $_SERVER['REMOTE_ADDR'];
		
			//$myIp = getHostByName(getHostName());
			//$ip_address = $myIp;
			$browser =  $_SERVER['HTTP_USER_AGENT'];

			
			
			$this->load->library('user_agent');
			  $databrowser = $this->agent->browser();
			  $databrowser_version = $this->agent->version();
			  $dataos = $this->agent->platform();
			  
              $sessid=$this->session->userdata('staff_logged_in');
  
					 $this->db->insert('tblstaff_session', [
                        'staffid' => $stid,
						'staffname' => $stname,
						 'ipaddress' => $ip_address,
						 'system_forward_ip'=> getenv('HTTP_X_FORWARDED_FOR'),
						  'date' => date('Y-m-d H:i:s'),
						  'browser' => $databrowser,
						  'browser_version' => $databrowser_version,
						  'os' => $dataos,
						   'staff_emailid' => $email,
						    'staff_logged_in' => $sessid,
                        
                    ]);
					//for storing staff session end
                    
                    
                    
                    // is logged in
                    maybe_redirect_to_previous_url();
                }
                do_action('after_staff_login');
                redirect(admin_url());
            }
        }

        $data['title'] = _l('admin_auth_login_heading');
        $this->load->view('authentication/login_admin', $data);
    }

    public function two_factor()
    {
        $this->form_validation->set_rules('code', _l('two_factor_authentication_code'), 'required');

        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $code = $this->input->post('code');
                $code = trim($code);
                if ($this->Authentication_model->is_two_factor_code_valid($code)) {
                    $user = $this->Authentication_model->get_user_by_two_factor_auth_code($code);
                    $this->Authentication_model->clear_two_factor_auth_code($user->staffid);
                    $this->Authentication_model->two_factor_auth_login($user);
                    maybe_redirect_to_previous_url();
                    do_action('after_staff_login');
                    redirect(admin_url());
                } else {
                    set_alert('danger', _l('two_factor_code_not_valid'));
                    redirect(site_url('authentication/two_factor'));
                }
            }
        }
        $this->load->view('authentication/set_two_factor_auth_code');
    }

    public function recaptcha($str = '')
    {
        return do_recaptcha_validation($str);
    }

    public function forgot_password()
    {
        if (is_staff_logged_in()) {
            redirect(admin_url());
        }
        $this->form_validation->set_rules('email', _l('admin_auth_login_email'), 'trim|required|valid_email|callback_email_exists');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->forgot_password($this->input->post('email'), true);
                if (is_array($success) && isset($success['memberinactive'])) {
                    set_alert('danger', _l('inactive_account'));
                    redirect(site_url('authentication/forgot_password'));
                } elseif ($success == true) {
                    set_alert('success', _l('check_email_for_resetting_password'));
                    redirect(site_url('authentication/admin'));
                } else {
                    set_alert('danger', _l('error_setting_new_password_key'));
                    redirect(site_url('authentication/forgot_password'));
                }
            }
        }
        $this->load->view('authentication/forgot_password');
    }

    public function reset_password($staff, $userid, $new_pass_key)
    {
        if (!$this->Authentication_model->can_reset_password($staff, $userid, $new_pass_key)) {
            set_alert('danger', _l('password_reset_key_expired'));
            redirect(site_url('authentication/admin'));
        }
        $this->form_validation->set_rules('password', _l('admin_auth_reset_password'), 'required');
        $this->form_validation->set_rules('passwordr', _l('admin_auth_reset_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                do_action('before_user_reset_password', [
                    'staff'  => $staff,
                    'userid' => $userid,
                ]);
                $success = $this->Authentication_model->reset_password($staff, $userid, $new_pass_key, $this->input->post('passwordr', false));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', _l('password_reset_key_expired'));
                } elseif ($success == true) {
                    do_action('after_user_reset_password', [
                        'staff'  => $staff,
                        'userid' => $userid,
                    ]);
                    set_alert('success', _l('password_reset_message'));
                } else {
                    set_alert('danger', _l('password_reset_message_fail'));
                }
                redirect(site_url('authentication/admin'));
            }
        }
        $this->load->view('authentication/reset_password');
    }

    public function set_password($staff, $userid, $new_pass_key)
    {
        if (!$this->Authentication_model->can_set_password($staff, $userid, $new_pass_key)) {
            set_alert('danger', _l('password_reset_key_expired'));
            redirect(site_url('authentication/admin'));
            if ($staff == 1) {
                redirect(site_url('authentication/admin'));
            } else {
                redirect(site_url());
            }
        }
        $this->form_validation->set_rules('password', _l('admin_auth_set_password'), 'required');
        $this->form_validation->set_rules('passwordr', _l('admin_auth_set_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->set_password($staff, $userid, $new_pass_key, $this->input->post('passwordr', false));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', _l('password_reset_key_expired'));
                } elseif ($success == true) {
                    set_alert('success', _l('password_reset_message'));
                } else {
                    set_alert('danger', _l('password_reset_message_fail'));
                }
                if ($staff == 1) {
                    redirect(site_url('authentication/admin'));
                } else {
                    redirect(site_url());
                }
            }
        }
        $this->load->view('authentication/set_password');
    }

    public function logout()
    {
        $this->Authentication_model->logout();
        do_action('after_user_logout');
        redirect(site_url('authentication/admin'));
    }

    public function email_exists($email)
    {
        $total_rows = total_rows('tblstaff', [
            'email' => $email,
        ]);
        if ($total_rows == 0) {
            $this->form_validation->set_message('email_exists', _l('auth_reset_pass_email_not_found'));

            return false;
        }

        return true;
    }
    
        /*public function register(){
     
        if($_POST['fname'] !='' && $_POST['lname'] !='' && $_POST['number'] !='' &&  $_POST['email'] !='' && $_POST['password'] !=''){
            $this->load->helper('phpass');
            $hasher              = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password']    = $hasher->HashPassword($_POST['password']);
            $data=array(
           'firstname'=>$_POST['fname'],
           'lastname'=>$_POST['lname'],
           'phonenumber'=>$_POST['number'],
           'last_ip'=>$_SERVER['REMOTE_ADDR'],
           'email'=>$_POST['email'],
           'password'=>$data['password'],
           'datecreated'=>date('Y-m-d H:i:s'),
           'admin'=>0,
           'role'=>5,
           'active'=>1
         );
         $response = $this->affiliate->add_affiliate($data);
         if($response){
            set_alert('success', 'Register Successfully');
             redirect('authentication/admin');
         }
         else{
            $this->load->view('affiliate/register');   
         }
        }
        else{
            $this->load->view('affiliate/register');
        }
    }*/
    
    public function register(){
     
        if($_POST['fname'] !='' && $_POST['lname'] !='' && $_POST['number'] !='' &&  $_POST['email'] !='' && $_POST['password'] !=''){
           
  
				  $this->db->select('email');
            $this->db->where('email', $_POST['email']);
            $email_id = $this->db->get('tblstaff')->result_array();
			
                 foreach ($email_id as $eid) {
                 $emailid=$eid['email'];
				 }
				 
				 
				
			if($emailid=='')
			{
				$this->db->select('phonenumber');
            $this->db->where('phonenumber', $_POST['number']);
            $num_id = $this->db->get('tblstaff')->result_array();
			
                 foreach ($num_id as $nid) {
                 $numid=$nid['phonenumber'];
				 }
				 
				if($numid=='')
				{
				
				
		   $this->load->helper('phpass');
            $hasher              = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password']    = $hasher->HashPassword($_POST['password']);
            $data=array(
           'firstname'=>$_POST['fname'],
           'lastname'=>$_POST['lname'],
           'phonenumber'=>$_POST['number'],
           'last_ip'=>$_SERVER['REMOTE_ADDR'],
           'email'=>$_POST['email'],
           'password'=>$data['password'],
           'datecreated'=>date('Y-m-d H:i:s'),
           'admin'=>0,
           'role'=>5,
           'active'=>1
         );
         $response = $this->affiliate->add_affiliate($data);
         if($response){
             
             //for automatic kyc id
             $this->db->insert('tblkyc', [
                        'staffid' => $response,
                        
                    ]);
					
				$sql="select value from tbloptions where name='kyc_id_count'";
        $query=$this->db->query($sql);
        $data=$query->result_array();

        $key=$data[0]['value'];
        
      
        $id_valu='KA'.$key;

       
            $sql="update tblkyc set kyc_id='$id_valu',approve_status=1 where staffid=$response";
            if($this->db->query($sql))
            {
                $sql="update tbloptions set value=value+1 where id=399";
               
                $this->db->query($sql);
            }
             
             
             
             
             
             
             
            set_alert('success', 'Register Successfully');
             redirect('authentication/admin');
         }
         else{
            $this->load->view('affiliate/register');   
         }
		 
				}else{
					set_alert('danger', 'Mobile Number Already Exists');
             redirect('authentication/register');
				}
		 
		 
		 
		}else{
			set_alert('danger', 'Email-Id Already Exists');
             redirect('authentication/register');
		}
		 
		 
        }
        else{
            $this->load->view('affiliate/register');
        }
    }
    
    
}
