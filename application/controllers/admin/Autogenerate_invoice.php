

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Autogenerate_invoice extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('autogenerate_model');
        // $this->load->helper('autogenerate_helper');
    }


    public function index(){
    $recent_invoice=$this->autogenerate_model->recent_invocie();

 
    if($recent_invoice){
    foreach($recent_invoice as $invoice){
    $get_items=$this->autogenerate_model->get_items($invoice['id']);
 
// continue;
    foreach($get_items as $items){
    $max_number=$this->autogenerate_model->get_maxnumber();


//     }
if($items){
    $current_date = date('Y-m-d');
    $first_dt=date_create($current_date.'first day of last month');
    $last_dt=date_create($current_date.'last day of last month');
    $first=$first_dt->format('Y-m-d');
    $last=$last_dt->format('Y-m-d');

$check_convertedNot=$this->autogenerate_model->check_convertNot($items['description'],$invoice['clientid']);
if($check_convertedNot == true){
 continue;
}

    else if($items['frequency'] =='Monthly'){
        $current_date=date('d');
       if($current_date == '01'){
        $invoiceGenerate_date=date('Y-m-d');
        $date = strtotime($invoiceGenerate_date); 
        $invoiceGenerate_correctformat = date('Y-m-d', $date);
        $start_date = strtotime($invoiceGenerate_correctformat);
        $duedate = strtotime("+7 day", $start_date);
        $duedateFormat = date('Y-m-d', $duedate);
        $check_alreadyInvoiced=$this->autogenerate_model->check_invoices($invoice['clientid'],$items['item_name'],$invoiceGenerate_correctformat,$items['compliance_id']);

      if($check_alreadyInvoiced == ''){
      $service_description=$this->autogenerate_model->service_details($items['rel_id'],$items['item_name']);
      $data=array(
     'clientid'=>$invoice['clientid'],
     'prefix'=>'QUOT-',
     'number'=>$max_number,
     'number_format'=>1,
     'datecreated'=>date('Y-m-d H:i:s'),
     'date'=>$invoiceGenerate_correctformat,
     'expirydate'=>$duedateFormat,
     'currency'=>1,
     'subtotal'=>$service_description['rate'] * $service_description['qty'] ,
     'total_tax'=>0.00,
     'total'=>$service_description['rate'] * $service_description['qty'],
     'adjustment'=>0.00,
     'hash'=>app_generate_hash(),
     'status'=>1,
     'show_quantity_as'=>1,
     'addedfrom'=>$invoice['addedfrom'],
     'sale_agent'=>$invoice['sale_agent'],
     'type'=>'auto'
     );

     $this->db->insert('tblestimates',$data);
     if($this->db->insert_id()){
       $data=array(
       'rel_id'=>$this->db->insert_id(),
       'rel_type'=>'estimate',
       'description'=>$service_description['description'],
       'long_description'=>$service_description['long_description'],
       'qty'=>$service_description['qty'],
       'rate'=>$service_description['rate'],
       'item_order'=>$service_description['item_order'],
       'compliance_id'=>$items['compliance_id']
       );
       $this->db->insert('tblitems_in',$data);
       if($this->db->insert_id()){
           $this->db->query('update tbloptions set value=value+1 where id=51');
           echo 'success';
           echo '<br>';
       } 
           
     }
    }
    else{
     echo 'Sorry Already Estimate created';
        echo '<br>';
    }
// }
    }


    else if($items['frequency'] =='Quartely'){
        $month=date('F');
        if($month == 'June' || $month == 'May' || $month == 'April'){
            $current_month='07';
        }
        else if($month == 'August' || $month == 'September' || $month == 'July'){
          $current_month='10';
      }
      else if($month == 'October' || $month == 'November' || $month == 'December'){
          $current_month='02';
      }
      else{
          $current_month='04';
      }


      
    $current_year=date('Y');
    $invoiceGenerate_date=$current_year.'-'.$current_month.-01;
    $date = strtotime($invoiceGenerate_date); 
    $invoiceGenerate_correctformat = date('Y-m-d', $date);
    $start_date = strtotime($invoiceGenerate_correctformat);
    $duedate = strtotime("+7 day", $start_date);
    $duedateFormat = date('Y-m-d', $duedate);
    $check_alreadyInvoiced=$this->autogenerate_model->check_invoices($invoice['clientid'],$items['item_name'],$invoiceGenerate_correctformat,$items['compliance_id'],$first,$current_date);
   
    if($invoiceGenerate_correctformat == date('Y-m-d')){

      if($check_alreadyInvoiced == ''){
      $service_description=$this->autogenerate_model->service_details($items['rel_id'],$items['item_name']);
      $data=array(
     'clientid'=>$invoice['clientid'],
     'prefix'=>'QUOT-',
     'number'=>$max_number,
     'number_format'=>1,
     'datecreated'=>date('Y-m-d H:i:s'),
     'date'=>$invoiceGenerate_correctformat,
     'expirydate'=>$duedateFormat,
     'currency'=>1,
     'subtotal'=>$service_description['rate'] * $service_description['qty'] ,
     'total_tax'=>0.00,
     'total'=>$service_description['rate'] * $service_description['qty'],
     'adjustment'=>0.00,
     'hash'=>app_generate_hash(),
     'status'=>1,
     'show_quantity_as'=>1,
     'addedfrom'=>$invoice['addedfrom'],
     'sale_agent'=>$invoice['sale_agent'],
     'type'=>'auto'
     );

     $this->db->insert('tblestimates',$data);
     if($this->db->insert_id()){
       $data=array(
       'rel_id'=>$this->db->insert_id(),
       'rel_type'=>'estimate',
       'description'=>$service_description['description'],
       'long_description'=>$service_description['long_description'],
       'qty'=>$service_description['qty'],
       'rate'=>$service_description['rate'],
       'item_order'=>$service_description['item_order'],
       'compliance_id'=>$items['compliance_id']
       );
       $this->db->insert('tblitems_in',$data);
       if($this->db->insert_id()){
           $this->db->query('update tbloptions set value=value+1 where id=51');
           echo 'success';
           echo '<br>';
       } 
           
     }
    }
    else{
     echo 'Sorry Already Estimate created';
     echo '<br>';
     echo $items['rel_id'];
        echo '<br>';
    }

    }
    }
    else if($items['frequency'] == 'Once'){

    
    
        $current_date=date('d');
        if($current_date == '01'){   
            
    
            $invoiceGenerate_date=date('Y-m-d');
            $date = strtotime($invoiceGenerate_date); 
            $invoiceGenerate_correctformat = date('Y-m-d', $date);
            $start_date = strtotime($invoiceGenerate_correctformat);
            $duedate = strtotime("+7 day", $start_date);
            $duedateFormat = date('Y-m-d', $duedate);
            $check_alreadyQuoted=$this->autogenerate_model->check_quotes($invoice['clientid'],$items['item_name'],$items['compliance_id']);

          if($check_alreadyQuoted ==''){
            $service_description=$this->autogenerate_model->service_details($items['rel_id'],$items['item_name']);
            $data=array(
           'clientid'=>$invoice['clientid'],
           'prefix'=>'QUOT-',
           'number'=>$max_number,
           'number_format'=>1,
           'datecreated'=>date('Y-m-d H:i:s'),
           'date'=>$invoiceGenerate_correctformat,
           'expirydate'=>$duedateFormat,
           'currency'=>1,
           'subtotal'=>$service_description['rate'] * $service_description['qty'] ,
           'total_tax'=>0.00,
           'total'=>$service_description['rate'] * $service_description['qty'],
           'adjustment'=>0.00,
           'hash'=>app_generate_hash(),
           'status'=>1,
           'show_quantity_as'=>1,
           'addedfrom'=>$invoice['addedfrom'],
           'sale_agent'=>$invoice['sale_agent'],
           'type'=>'auto'
           );
      
           $this->db->insert('tblestimates',$data);
           if($this->db->insert_id()){
             $data=array(
             'rel_id'=>$this->db->insert_id(),
             'rel_type'=>'estimate',
             'description'=>$service_description['description'],
             'long_description'=>$service_description['long_description'],
             'qty'=>$service_description['qty'],
             'rate'=>$service_description['rate'],
             'item_order'=>$service_description['item_order'],
             'compliance_id'=>$items['compliance_id']
             );
             $this->db->insert('tblitems_in',$data);
             if($this->db->insert_id()){
                 $this->db->query('update tbloptions set value=value+1 where id=51');
                 echo 'success';
                 echo '<br>';
             } 
                 
           }
          }
      


        }
        else{
            echo 'sorry already quote created once';
            echo '<br>';
        }

    }

    // else{
    //     echo "Sorry this is one time quote create method your quote is created";
    //     echo'<br>';
    // }



    }

else{
    echo 'no data found';
}

}

    }
}


    }    
      
}

    public function deletequotes(){
    $recent_invoice=$this->autogenerate_model->recent_invocie_distinct();
    foreach($recent_invoice as $invoice){
    $checkInvoiceconverted=$this->autogenerate_model->checkInvoiceconverted($invoice['clientid']);
    }
    }


          public function send_notification(){
            $current_month=date('m');
            $current_year=date('Y');
            $date1=strtotime($current_year.'-'.$current_month.-01);
            $date_format=date('Y-m-d',$date1);
            $get_estimate_details=$this->autogenerate_model->get_estimates($date_format);

            foreach($get_estimate_details as $details){
            $created_date=$details['datecreated'];
            $current_date = strtotime($created_date);
            $present_date = date('Y-m-d',$current_date);
         
            // 2nd day
            $date_2=date_create($created_date);
            date_modify($date_2,"+1 days");
            $two_days = date_format($date_2,"Y-m-d");
            // 5th day
            $date_5=date_create($created_date);
            date_modify($date_5,"+4 days");
            $five_days = date_format($date_5,"Y-m-d");
            //  6th day
            $date_6=date_create($created_date);
            date_modify($date_6,"+5 days");
            $six_days = date_format($date_6,"Y-m-d");
          //  10th day
            $date_10=date_create($created_date);
            date_modify($date_10,"+9 days");
            $ten_days = date_format($date_10,"Y-m-d");
          //  11th day
          $date_11=date_create($created_date);
          date_modify($date_11,"+10 days");
          $eleven_days = date_format($date_11,"Y-m-d");
          //current date
          $currentDate=date('Y-m-d');
      $date_notification=strtotime($current_year.'-'.$current_month.-20);
      $date_format_notification=date('Y-m-d',$date_notification);
    
     
        //   //send notification

          if($currentDate == $present_date ){
            send_email($details['email_id'],$date_format_notification);
            echo 'Day one email is send';
            echo '<br>';

          }
          else if($currentDate == $two_days){
            sendSms($details['phonenumber']);
            echo 'Day two sms is send';
            echo '<br>';
          }
          else if($currentDate == $five_days){
                    //  $count[]=$details['email_id'];
            send_email($details['email_id'],$date_format_notification);
            echo 'Day five email is send';
            echo '<br>';
            break;
          }
          else if($currentDate == $six_days){
            sendSms($details['phonenumber']);
            echo 'Day six sms is send';
            echo '<br>';
          }
          else if($currentDate == $ten_days){
            send_email($details['email_id'],$date_format_notification);
            echo 'Day ten email is send';
            echo '<br>';
          }
          else if($currentDate == $eleven_days){
            sendSms($details['phonenumber']);
            echo 'Day eleven sms is send';
            echo '<br>';
          }
          else{
              echo 'Sorry there is no data found';
              echo '<br>';
          }

            }
            
            // print_r($count);
        }
//auto backup logs and delete

        public function logs(){
            $date=date('Y-m-d');
            $last_start_date = date('Y-m-d', strtotime('-7 days', strtotime($date))); 
            $ts = strtotime($date);
            $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
            $start_date = date('Y-m-d', $start);
            $end_date = date('Y-m-d', strtotime('next sunday', $start));

            if($date == $start_date){
                $this->load->dbutil();
                $this->load->helper('file');
                $this->load->helper('download');

                //task logs 
                
                $query = $this->db->query("SELECT concat(tblstaff.firstname,' ',tblstaff.lastname) as staff_name,tbltaskactivity.task_id as TaskID,tbltaskactivity.description as Activity, DATE(tbltaskactivity.created_date) as date FROM tbltaskactivity LEFT JOIN tblstaff ON tblstaff.staffid=tbltaskactivity.staff_id WHERE DATE(tbltaskactivity.created_date) >= '$last_start_date' AND DATE(tbltaskactivity.created_date) <= '$date'");
                $data=$this->dbutil->csv_from_result($query);
                $dbname='task_'.$last_start_date.'_'.$date.'.csv';
                $save='logs_xl/'.$dbname;
                write_file($save,$data);

            
                //project logs 
                
                $query = $this->db->query("SELECT concat(tblstaff.firstname,' ',tblstaff.lastname) as staff_name,tblprojectactivity.additional_data as Additional_data,tblprojectactivity.project_id as ProjectID,tblprojectactivity.description_key as Activity, DATE(tblprojectactivity.dateadded) as date FROM tblprojectactivity LEFT JOIN tblstaff ON tblstaff.staffid=tblprojectactivity.staff_id WHERE DATE(tblprojectactivity.dateadded) >= '$last_start_date' AND DATE(tblprojectactivity.dateadded) <= '$date'");
                $data=$this->dbutil->csv_from_result($query);
                $dbname='projects_'.$last_start_date.'_'.$date.'.csv';
                $save='logs_xl/'.$dbname;
                write_file($save,$data);
              //lead logs 
                
                $query = $this->db->query("SELECT concat(tblstaff.firstname,' ',tblstaff.lastname) as staff_name,tblleadactivitylog.additional_data	as Additional_data,tblleadactivitylog.leadid as LeadID,tblleadactivitylog.description as Activity, DATE(tblleadactivitylog.date) as date FROM tblleadactivitylog LEFT JOIN tblstaff ON tblstaff.staffid=tblleadactivitylog.staffid WHERE DATE(tblleadactivitylog.date) >= '$last_start_date' AND DATE(tblleadactivitylog.date) <= '$date'");
                $data=$this->dbutil->csv_from_result($query);
                $dbname='leads_'.$last_start_date.'_'.$date.'.csv';
                $save='logs_xl/'.$dbname;
                write_file($save,$data);
                 //sale logs 
                
                $query = $this->db->query("SELECT concat(tblstaff.firstname,' ',tblstaff.lastname) as staff_name,tblsalesactivity.additional_data as Additional_data,tblsalesactivity.rel_id as ID,tblsalesactivity.rel_type ,tblsalesactivity.description as Activity, DATE(tblsalesactivity.date) as date FROM tblsalesactivity LEFT JOIN tblstaff ON tblstaff.staffid=tblsalesactivity.staffid WHERE DATE(tblsalesactivity.date) >= '$last_start_date' AND DATE(tblsalesactivity.date) <= '$date'");
                $data=$this->dbutil->csv_from_result($query);
                $dbname='sales_'.$last_start_date.'_'.$date.'.csv';
                $save='logs_xl/'.$dbname;
                write_file($save,$data);
                
                //login details
                
                $query = $this->db->query("SELECT * from tblstaff_session WHERE DATE(date) >= '$last_start_date' AND DATE(date) <= '$date'");
                $data=$this->dbutil->csv_from_result($query);
                $dbname='Login_'.$last_start_date.'_'.$date.'.csv';
                $save='logs_xl/'.$dbname;
                if(write_file($save,$data)){
                 
                //Delete old data after get backup
                 
                $this->db->where('DATE(date) >=',$last_start_date);
                $this->db->where('DATE(date) <=',$end_date);
                $this->db->delete('tblstaff_session');
            
         //
                }


        }
        }


// public function excel(){
//                 $this->load->dbutil();
//                 $this->load->helper('file');
//                 $this->load->helper('download');
                
//                 $query = $this->db->query("SELECT tblestimates.id,tblestimates.clientid,tblclients.company from tblestimates join tblclients on tblestimates.clientid =tblclients.userid WHERE tblestimates.type='auto'");
//                 $data=$this->dbutil->csv_from_result($query);
//                 $dbname='compliance_quote.csv';
//                      $save='logs_xl/'.$dbname;
//               if(write_file($save,$data)){
//                   echo 'success';
//               }
// }

        
}
?>