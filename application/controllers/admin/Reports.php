<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Reports extends Admin_controller
{
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

    public function __construct()
    {
        parent::__construct();
        if (!has_permission('reports', '', 'view')) {
            access_denied('reports');
        }
        $this->ci = &get_instance();
        $this->load->model('reports_model');
    }

    /* No access on this url */
    public function index()
    {
        redirect(admin_url());
    }

    /* See knowledge base article reports*/
    public function knowledge_base_articles()
    {
        $this->load->model('knowledge_base_model');
        $data['groups'] = $this->knowledge_base_model->get_kbg();
        $data['title']  = _l('kb_reports');
        $this->load->view('admin/reports/knowledge_base_articles', $data);
    }

    /*
        public function tax_summary(){
           $this->load->model('taxes_model');
           $this->load->model('payments_model');
           $this->load->model('invoices_model');
           $data['taxes'] = $this->db->query("SELECT DISTINCT taxname,taxrate FROM tblitemstax WHERE rel_type='invoice'")->result_array();
            $this->load->view('admin/reports/tax_summary',$data);
        }*/
    /* Repoert leads conversions */
    public function leads()
    {
        if (has_permission('reports', '', 'view') || is_admin())  { 
        $type = 'leads';
        if ($this->input->get('type')) {
            $type                       = $type . '_' . $this->input->get('type');
            $data['leads_staff_report'] = json_encode($this->reports_model->leads_staff_report());
        }
        $this->load->model('leads_model');
        $data['statuses']               = $this->leads_model->get_status();
        $data['leads_this_week_report'] = json_encode($this->reports_model->leads_this_week_report());
        $data['leads_sources_report']   = json_encode($this->reports_model->leads_sources_report());
        $this->load->view('admin/reports/' . $type, $data);
        
        }else{
		$this->sales();
	}
    }

    /* Sales reportts */
    public function sales()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode']      = $this->db->query('SELECT @@sql_mode as mode')->row();

        if (is_using_multiple_currencies() || is_using_multiple_currencies('tblcreditnotes') || is_using_multiple_currencies('tblestimates') || is_using_multiple_currencies('tblproposals')) {
            $this->load->model('currencies_model');
            $data['currencies'] = $this->currencies_model->get();
        }
        $this->load->model('invoices_model');
        $this->load->model('estimates_model');
        $this->load->model('proposals_model');
        $this->load->model('credit_notes_model');

        $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
        $data['invoice_statuses']      = $this->invoices_model->get_statuses();
        $data['estimate_statuses']     = $this->estimates_model->get_statuses();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();
        $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();

        $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();

        $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
        $data['proposals_statuses']    = $this->proposals_model->get_statuses();

        $data['invoice_taxes']     = $this->distinct_taxes('invoice');
        $data['estimate_taxes']    = $this->distinct_taxes('estimate');
        $data['proposal_taxes']    = $this->distinct_taxes('proposal');
        $data['credit_note_taxes'] = $this->distinct_taxes('credit_note');


        $data['title'] = _l('sales_reports');
        $this->load->view('admin/reports/sales', $data);
    }

    /* Customer report */
    public function customers_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $select = [
                get_sql_select_client_company(),
                '(SELECT COUNT(clientid) FROM tblinvoices WHERE tblinvoices.clientid = tblclients.userid AND status != 5)',
                '(SELECT SUM(subtotal) - SUM(discount_total) FROM tblinvoices WHERE tblinvoices.clientid = tblclients.userid AND status != 5)',
                '(SELECT SUM(total) FROM tblinvoices WHERE tblinvoices.clientid = tblclients.userid AND status != 5)',
            ];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' ' . $custom_date_select . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
            }
            $by_currency     = $this->input->post('report_currency');
            $currency        = $this->currencies_model->get_base_currency();
            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);
            if ($by_currency) {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' AND currency =' . $by_currency . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
                $currency        = $this->currencies_model->get($by_currency);
                $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);
            }
            $aColumns     = $select;
            $sIndexColumn = 'userid';
            $sTable       = 'tblclients';
            $where        = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, [
                'userid',
            ]);
            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($i == 0) {
                        $_data = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } elseif ($aColumns[$i] == $select[2] || $aColumns[$i] == $select[3]) {
                        if ($_data == null) {
                            $_data = 0;
                        }
                        $_data = format_money($_data, $currency_symbol);
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
                $x++;
            }
            echo json_encode($output);
            die();
        }
    }

    public function payments_received()
    {
        if (has_permission('Report Payment Received', '', 'view') || has_permission('Report Payment Received', '', 'view_own') || is_admin() )
        {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('payment_modes_model');
            $online_modes = $this->payment_modes_model->get_online_payment_modes(true);
            $select       = [
                'tblinvoicepaymentrecords.id',
                'tblinvoicepaymentrecords.date',
                'invoiceid',
				
				
                get_sql_select_client_company(),
				 'amount',
				 'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
				//  'tblitems_in.description',
				//  'tblitems_in.rate',
				//  '(tblinvoices.total_tax/2)as cgst',
				//  '(tblinvoices.total_tax/2)as sgst',
				 'tblcontacts.phonenumber',
				 'tblcontacts.email',
				 
                'paymentmode',
                'transactionid',
                'note',
                'approved_status',
				'approved_by',
            ];
             $staffuserid=get_staff_user_id();
            if (!has_permission('Report Payment Received', '', 'view_own') || is_admin()) 
            {
               $where = [
                   'AND status != 5',
               ];  
      
   }else{
       
       $where = [
        'AND status != 5','AND tblinvoices.addedfrom = "'.$staffuserid.'"',
       ];

   }

            $custom_date_select = $this->get_where_report_period('tblinvoicepaymentrecords.date');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblinvoicepaymentrecords';
            $join         = [
                'JOIN tblinvoices ON tblinvoices.id = tblinvoicepaymentrecords.invoiceid',
                'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
                'LEFT JOIN tblinvoicepaymentsmodes ON tblinvoicepaymentsmodes.id = tblinvoicepaymentrecords.paymentmode',
				'left join tblcontacts on tblclients.userid=tblcontacts.userid',
				'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',
				//'left join tblitems_in on tblitems_in.rel_id=tblinvoicepaymentrecords.invoiceid and rel_type="invoice"',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'number',
                'clientid',
                'tblinvoicepaymentsmodes.name',
                'tblinvoicepaymentsmodes.id as paymentmodeid',
                'paymentmethod',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data['total_amount'] = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($aColumns[$i] == 'paymentmode') {
                        $_data = $aRow['name'];
                        if (is_null($aRow['paymentmodeid'])) {
                            foreach ($online_modes as $online_mode) {
                                if ($aRow['paymentmode'] == $online_mode['id']) {
                                    $_data = $online_mode['name'];
                                }
                            }
                        }
                        if (!empty($aRow['paymentmethod'])) {
                            $_data .= ' - ' . $aRow['paymentmethod'];
                        }
                    } elseif ($aColumns[$i] == 'tblinvoicepaymentrecords.id') {
                        $_data = '<a href="' . admin_url('payments/payment/' . $_data) . '" target="_blank">' . $_data . '</a>';
                    } elseif ($aColumns[$i] == 'tblinvoicepaymentrecords.date') {
                        $_data = _d($_data);
                    } elseif ($aColumns[$i] == 'invoiceid') {
                        $_data = '<a href="' . admin_url('invoices/list_invoices/' . $aRow[$aColumns[$i]]) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                    } elseif ($i == 3) {
                    if(empty($aRow['deleted_customer_name'])) {
                       $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } else {
                        $row[] = $aRow['deleted_customer_name'];
                    }

                    } elseif ($aColumns[$i] == 'amount') {
                        $footer_data['total_amount'] += $_data;
                        $_data = format_money($_data, $currency_symbol);
                    }

                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = format_money($footer_data['total_amount'], $currency_symbol);
            $output['sums']              = $footer_data;
            echo json_encode($output);
            die();
        }
    }
}

// public function overdue_payments()
//     {
		
//         if ($this->input->is_ajax_request()) {
//             $invoice_taxes     = $this->distinct_taxes('invoice');
//             $totalTaxesColumns = count($invoice_taxes);

//             $this->load->model('currencies_model');
//             $this->load->model('invoices_model');

//             $select = [
//                 'number',
//                 get_sql_select_client_company(),
//                 'YEAR(date) as year',
//                 'date',
//                 'tblinvoices.duedate as duedate',
//                 //'subtotal',
//               // 'total',
//                 'total_tax',
// 				'tblinvoices.project_id as projects_id',
// 				//'tblstafftasks.id as stafftask_id',
//                 //'discount_total',
//                 //'adjustment',
//                 '(SELECT SUM(rate*qty) FROM tblitems_in WHERE tblitems_in.rel_id=tblinvoices.id AND tblitems_in.rel_type="invoice") as rate',
//                 'tblinvoices.status as status',
// 				// 'tblitems_in.description as description',
// 				// 'tblitems_in.rate as rate',
// 				'tblprojects.deadline as deadline',
//                 'tblclients.phonenumber as phonenumber',
//                  'tblclients.email_id as email_id',
// 				 'tblprojects.date_finished as date_finished',
// 				'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
// 				// '(total_tax/2)as cgst',
// 				//  '(total_tax/2)as sgst',
// 				//  '(tblitems_in.rate + total_tax)as total_amount ',
                  
// 				 ];

//             $where = [
//                 'AND tblinvoices.status IN (3,1) ','AND DATE_ADD(tblprojects.deadline, INTERVAL 7 DAY) < CURDATE()',
//             ];

				

            



//           /* $invoiceTaxesSelect = array_reverse($invoice_taxes);

//             foreach ($invoiceTaxesSelect as $key => $tax) {
//                 array_splice($select, 8, 0, '(
//                     SELECT CASE
//                     WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
//                     WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
//                     ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
//                     END
//                     FROM tblitems_in
//                     INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
//                     WHERE tblitems_in.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_' . $key);
//             }*/

//             $custom_date_select = $this->get_where_report_period();
//             if ($custom_date_select != '') {
//                 array_push($where, $custom_date_select);
//             }

//             if ($this->input->post('sale_agent_invoices')) {
//                 $agents  = $this->input->post('sale_agent_invoices');
//                 $_agents = [];
//                 if (is_array($agents)) {
//                     foreach ($agents as $agent) {
//                         if ($agent != '') {
//                             array_push($_agents, $agent);
//                         }
//                     }
//                 }
//                 if (count($_agents) > 0) {
//                     array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
//                 }
//             }

//             $by_currency              = $this->input->post('report_currency');
//             $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

//             /*if ($by_currency) {
//                 $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
//                 $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
//                 $select[$totalPaymentsColumnIndex] = $_temp;

//                 $currency = $this->currencies_model->get($by_currency);
//                 array_push($where, 'AND currency=' . $by_currency);
//             } else {
//                 $currency                          = $this->currencies_model->get_base_currency();
//                 $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
//             }*/

//             $currency_symbol = $currency->symbol;

//             if ($this->input->post('invoice_status')) {
//                 $statuses  = $this->input->post('invoice_status');
//                 $_statuses = [];
//                 if (is_array($statuses)) {
//                     foreach ($statuses as $status) {
//                         if ($status != '') {
//                             array_push($_statuses, $status);
//                         }
//                     }
//                 }
//                 if (count($_statuses) > 0) {
//                     array_push($where, 'AND tblinvoices.status IN (' . implode(', ', $_statuses) . ')');
//                 }
//             }

//             $aColumns     = $select;
//             $sIndexColumn = 'id';
//             $sTable       = 'tblinvoices';
//             $join         = [
//                 'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
// 				// 'LEFT JOIN tblitems_in ON tblitems_in.rel_id = tblinvoices.id AND tblitems_in.rel_type = "invoice"',
//                 'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',
// 				'LEFT JOIN tblprojects ON tblprojects.id = tblinvoices.project_id ',
// 				 //'left join tblstafftasks on tblstafftasks.rel_id=tblinvoices.project_id AND tblstafftasks.rel_type = "project"  ',
				 
//             ];

//             $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
//                 'userid',
//                 'tblinvoices.clientid',
//                 'tblinvoices.id',
//                 'discount_percent',
//                 'deleted_customer_name',
//             ]);

//             $output  = $result['output'];
//             $rResult = $result['rResult'];

//             $footer_data = [
//                 'rate'           => 0,
//                 //'subtotal'        => 0,
// 				'tblitems_in.rate'        => 0,
//                 'total_tax'       => 0,
//                 //'discount_total'  => 0,
//                 //'adjustment'      => 0,
//                 //'applied_credits' => 0,
//                 //'amount_open'     => 0,
//             ];

//             foreach ($invoice_taxes as $key => $tax) {
//                 $footer_data['total_tax_single_' . $key] = 0;
            
// 			}

//             foreach ($rResult as $aRow) {
//                 $row = [];
				
			


//                 $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

//                 if(empty($aRow['deleted_customer_name'])) {
//                     $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
//                 } else {
//                     $row[] = $aRow['deleted_customer_name'];
//                 }

//                 $row[] = $aRow['year'];
// 				$row[] = _d($aRow['date']);
//                 $row[]=invoice_description($aRow['id']);
//                 $row[]=invoice_long_description($aRow['id']);
//                 $row[] = $aRow['staff_name'];
// 				$row[] = format_money($aRow['rate'], $currency_symbol);
// 				$igst=igst_check($aRow['clientid']);
// 				$gst=get_gst($aRow['id']);
// 				 if($igst==33)
// 				 {
// 				$cgst=number_format($gst/2,2);
//               $sgst=number_format($gst/2,2);
// 				$icgst="--";
				
// 				 }else{
// 					 $icgst = number_format($gst,2); 
// 					 $cgst="--";
// 					 $sgst="--";
// 				 }
// 				 $row[] = $cgst;
//                 $row[] =  $sgst;
//                 $row[]=$icgst;

//                 $total_amount=invoice_total($aRow['id']);
// 				$row[] = $total_amount;
// 				$footer_data['total_amount'] += $total_amount;
// 				//$row[] = $aRow['total_amount'];
// 				$pay_received = invoice_payment($aRow['id']);
				
// 				$row[] = number_format($pay_received,2);
// 				$row[] = number_format(($total_amount - $pay_received),2);
// 				 $this->db->select('*');
//                 $this->db->where('rel_id',$aRow['projects_id']);
//                 $task = $this->db->get('tblstafftasks')->result_array();
// 				foreach($task as $key => $row3){
//                 $this->db->select('tblstaff.firstname,tblstaff.lastname,tblstafftaskassignees.*');
//                 $this->db->where('tblstafftaskassignees.taskid',$row3['id']);
//                 $this->db->join('tblstaff','tblstaff.staffid = tblstafftaskassignees.staffid');
//                 $task1=$this->db->get('tblstafftaskassignees')->result_array();                


// 				foreach($task1 as $key =>  $row2){
// 				$row1[$key]=$row2['firstname'];
// 				}

// 					}

// 				$row[]=$row1;
// 	            $row[]=task_id_status($aRow['id']);
//                 $row[] = _d($aRow['deadline']);
//                 $row[]=get_project_overdue_status($aRow['projects_id']);       
        
// 				$row[] = $aRow['date_finished'];
				
// 				$row[] =invocie_notes($aRow['id']);
//                 $row[] = overdue_date($aRow['deadline']);
				 

//                 //$row[] = format_money($aRow['subtotal'], $currency_symbol);
//                 //$footer_data['subtotal'] += $aRow['subtotal'];
				
				
// 				//$row[] = $aRow['cgst'];
// 				//$row[] = $aRow['sgst'];
			
				
				
			

//                 //$row[] = format_money($aRow['total'], $currency_symbol);
//                 //$footer_data['total'] += $aRow['total'];

                
//                 //$footer_data['total_tax'] += $aRow['total_tax'];
				
				
//               // $t = $totalTaxesColumns - 1;
//                 //$i = 0;
//                 //foreach ($invoice_taxes as $tax) {
//                   // $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency_symbol);
//                     //$footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
//                   // $t--;
//                   // $i++;
//               //  }

//                 //$row[] = format_money($aRow['discount_total'], $currency_symbol);
//                 //$footer_data['discount_total'] += $aRow['discount_total'];

//               // $row[] = format_money($aRow['adjustment'], $currency_symbol);
//                 //$footer_data['adjustment'] += $aRow['adjustment'];

//               // $row[] = format_money($aRow['credits_applied'], $currency_symbol);
//               // $footer_data['applied_credits'] += $aRow['credits_applied'];

//               // $amountOpen = $aRow['amount_open'];
//               // $row[]      = format_money($amountOpen, $currency_symbol);
//                 //$footer_data['amount_open'] += $amountOpen;
				
// 				//$row[] = project_assigned_to($aRow['projects_id'],$aRow['description']);
				
// 				//$row[] = $aRow['stafftask_id'];
// 				//print_r($stop);
// 				//exit;
				
				
				

				
				
				
//                 $row[] = format_invoice_status($aRow['status']);
                

//                 $output['aaData'][] = $row;
			
				
				
//             }

//             foreach ($footer_data as $key => $rate) {
//                 $footer_data[$key] = format_money($rate, $currency_symbol);
//             }
         
//             $output['sums'] = $footer_data;
//             echo json_encode($output);
//             die();
//         }
//     }




public function overdue_payments()
    {
		if (has_permission('Report OverDue Payment', '', 'view') || has_permission('Report OverDue Payment', '', 'view_own') || is_admin() )
        {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes     = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = [
                'number',
                get_sql_select_client_company(),
                'YEAR(date) as year',
                'date',
                'tblinvoices.duedate as duedate',
                //'subtotal',
               // 'total',
                'total_tax',
				'tblinvoices.project_id as projects_id',
				//'tblstafftasks.id as stafftask_id',
                //'discount_total',
                //'adjustment',
                '(SELECT SUM(rate*qty) FROM tblitems_in WHERE tblitems_in.rel_id=tblinvoices.id AND tblitems_in.rel_type="invoice") as rate',
				 
                'tblinvoices.status as status',
				// 'tblitems_in.description as description',
				// 'tblitems_in.rate as rate',
				//'tblprojects.deadline as deadline',
				'(SELECT deadline FROM tblprojects WHERE tblprojects.id = tblinvoices.project_id ) as deadline',
                'tblclients.phonenumber as phonenumber',
                'tblclients.email_id as email_id',
				 //'tblprojects.date_finished as date_finished',
				 '(SELECT date_finished FROM tblprojects WHERE tblprojects.id = tblinvoices.project_id ) as date_finished',
		         'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
				// '(total_tax/2)as cgst',
				//  '(total_tax/2)as sgst',
				//  '(tblitems_in.rate + total_tax)as total_amount ',
                  
				 ];

            // $where = [
            //     'AND DATE_ADD(deadline, INTERVAL 7 DAY) < CURDATE()',
            // ];

				
            // $where = [
            //     'AND tblinvoices.status IN (3,1) ','AND DATE_ADD(tblprojects.deadline, INTERVAL 7 DAY) < CURDATE()',
            // ];

            $staffuserid=get_staff_user_id();
            if (!has_permission('Report OverDue Payment', '', 'view_own') || is_admin()) 
            {
               $where = [
                'AND tblinvoices.status IN (3,1) ','AND DATE_ADD(tblprojects.deadline, INTERVAL 7 DAY) < CURDATE()',
               ];  
      
   }else{
       
       $where = [
        'AND tblinvoices.status IN (3,1) ','AND DATE_ADD(tblprojects.deadline, INTERVAL 7 DAY) < CURDATE()','AND tblinvoices.addedfrom = "'.$staffuserid.'"',
       ];

   }	



           /* $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach ($invoiceTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_' . $key);
            }*/

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            // if ($this->input->post('sale_agent_invoices')) {
            //     $agents  = $this->input->post('sale_agent_invoices');
            //     $_agents = [];
            //     if (is_array($agents)) {
            //         foreach ($agents as $agent) {
            //             if ($agent != '') {
            //                 array_push($_agents, $agent);
            //             }
            //         }
            //     }
            //     if (count($_agents) > 0) {
            //         array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
            //     }
            // }

            // $by_currency              = $this->input->post('report_currency');
            // $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            /*if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency                          = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }*/

            // $currency_symbol = $currency->symbol;

            // if ($this->input->post('invoice_status')) {
            //     $statuses  = $this->input->post('invoice_status');
            //     $_statuses = [];
            //     if (is_array($statuses)) {
            //         foreach ($statuses as $status) {
            //             if ($status != '') {
            //                 array_push($_statuses, $status);
            //             }
            //         }
            //     }
            //     if (count($_statuses) > 0) {
            //         array_push($where, 'AND tblinvoices.status IN (' . implode(', ', $_statuses) . ')');
            //     }
            // }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblinvoices';
            $join         = [
                'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
				// 'LEFT JOIN tblitems_in ON tblitems_in.rel_id = tblinvoices.id AND tblitems_in.rel_type = "invoice"',
                'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',
				'LEFT JOIN tblprojects ON tblprojects.id = tblinvoices.project_id ',
				 //'left join tblstafftasks on tblstafftasks.rel_id=tblinvoices.project_id AND tblstafftasks.rel_type = "project"  ',
				 
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'tblinvoices.clientid',
                'tblinvoices.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'rate'           => 0,
                //'subtotal'        => 0,
				'tblitems_in.rate'        => 0,
                'total_tax'       => 0,
                //'discount_total'  => 0,
                //'adjustment'      => 0,
                //'applied_credits' => 0,
                //'amount_open'     => 0,
            ];

            foreach ($invoice_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            
			}

            foreach ($rResult as $aRow) {
                $row = [];
				
			


                $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

                if(empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['year'];
				$row[] = _d($aRow['date']);
                $row[]=invoice_description($aRow['id']);
				$row[]=invoice_long_description($aRow['id']);
                $row[] = $aRow['staff_name'];
				$row[] = format_money($aRow['rate'], $currency_symbol);
				$igst=igst_check($aRow['clientid']);
				$gst=get_gst($aRow['id']);
				 if($igst==33)
				 {
				$cgst=number_format($gst/2,2);
               $sgst=number_format($gst/2,2);
				$icgst="--";
				
				 }else{
					 $icgst = number_format($gst,2); 
					 $cgst="--";
					 $sgst="--";
				 }
				 $row[] = $cgst;
                $row[] =  $sgst;
                $row[]=$icgst;

                $total_amount=invoice_total($aRow['id']);
                $row[] = $total_amount['total'];

                $pay_received =$total_amount['total_paid']; 
                $row[] = number_format($pay_received,2);
                $footer_data['adjustment'] += $total_amount['total_paid'];
				$row[] = $total_amount['outstanding'];
				$footer_data['total'] += $total_amount['total'];
		
        
                $footer_data['subtotal'] += $total_amount['outstanding'];
				 $this->db->select('*');
                $this->db->where('rel_id',$aRow['projects_id']);
                $task = $this->db->get('tblstafftasks')->result_array();
				foreach($task as $key => $row3){
                $this->db->select('tblstaff.firstname,tblstaff.lastname,tblstafftaskassignees.*');
                $this->db->where('tblstafftaskassignees.taskid',$row3['id']);
                $this->db->join('tblstaff','tblstaff.staffid = tblstafftaskassignees.staffid');
                $task1=$this->db->get('tblstafftaskassignees')->result_array();                


				foreach($task1 as $key =>  $row2){
				$row1[$key]=$row2['firstname'];
				}

					}

				$row[]=$row1;
	            $row[]=task_id_status($aRow['id']);
                $row[] = _d($aRow['deadline']);
                $row[]=get_project_overdue_status($aRow['projects_id']);       
        
				$row[] = $aRow['date_finished'];
				
				$row[] =invocie_notes($aRow['id']);
                $row[] = overdue_date($aRow['deadline']);
				 

                //$row[] = format_money($aRow['subtotal'], $currency_symbol);
                $footer_data['subtotal'] += $aRow['subtotal'];
				
				
				//$row[] = $aRow['cgst'];
				//$row[] = $aRow['sgst'];
			
				
				
			

                //$row[] = format_money($aRow['total'], $currency_symbol);
                //$footer_data['total'] += $aRow['total'];

                
                //$footer_data['total_tax'] += $aRow['total_tax'];
				
				
               // $t = $totalTaxesColumns - 1;
                //$i = 0;
                //foreach ($invoice_taxes as $tax) {
                   // $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency_symbol);
                    //$footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                   // $t--;
                   // $i++;
              //  }

                //$row[] = format_money($aRow['discount_total'], $currency_symbol);
                //$footer_data['discount_total'] += $aRow['discount_total'];

               // $row[] = format_money($aRow['adjustment'], $currency_symbol);
                //$footer_data['adjustment'] += $aRow['adjustment'];

               // $row[] = format_money($aRow['credits_applied'], $currency_symbol);
               // $footer_data['applied_credits'] += $aRow['credits_applied'];

               // $amountOpen = $aRow['amount_open'];
               // $row[]      = format_money($amountOpen, $currency_symbol);
                //$footer_data['amount_open'] += $amountOpen;
				
				//$row[] = project_assigned_to($aRow['projects_id'],$aRow['description']);
				
				//$row[] = $aRow['stafftask_id'];
				//print_r($stop);
				//exit;
				
				
				

				
				
				
                $row[] = format_invoice_status($aRow['status']);
                

                $output['aaData'][] = $row;
			
				
				
            }

            foreach ($footer_data as $key => $rate) {
                $footer_data[$key] = format_money($rate, $currency_symbol);
            }
         
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }
}






    public function proposals_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('proposals_model');

            $proposalsTaxes    = $this->distinct_taxes('proposal');
            $totalTaxesColumns = count($proposalsTaxes);

            $select = [
                'id',
                'subject',
                'proposal_to',
                'date',
                'open_till',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'status',
            ];

            $proposalsTaxesSelect = array_reverse($proposalsTaxes);

            foreach ($proposalsTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="proposal" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblproposals.id) as total_tax_single_' . $key);
            }

            $where              = [];
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('proposal_status')) {
                $statuses  = $this->input->post('proposal_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('proposals_sale_agents')) {
                $agents  = $this->input->post('proposals_sale_agents');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND assigned IN (' . implode(', ', $_agents) . ')');
                }
            }


            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblproposals';
            $join         = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'rel_id',
                'rel_type',
                'discount_percent',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($proposalsTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . format_proposal_number($aRow['id']) . '</a>';

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . $aRow['subject'] . '</a>';

                if ($aRow['rel_type'] == 'lead') {
                    $row[] = '<a href="#" onclick="init_lead(' . $aRow['rel_id'] . ');return false;" target="_blank" data-toggle="tooltip" data-title="' . _l('lead') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('lead') . '</span>';
                } elseif ($aRow['rel_type'] == 'customer') {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('client') . '</span>';
                } else {
                    $row[] = '';
                }

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['open_till']);

                $row[] = format_money($aRow['subtotal'], $currency_symbol);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = format_money($aRow['total'], $currency_symbol);
                $footer_data['total'] += $aRow['total'];

                $row[] = format_money($aRow['total_tax'], $currency_symbol);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($proposalsTaxes as $tax) {
                    $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency_symbol);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = format_money($aRow['discount_total'], $currency_symbol);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = format_money($aRow['adjustment'], $currency_symbol);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[]              = format_proposal_status($aRow['status']);
                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = format_money($total, $currency_symbol);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function estimates_report()
    {
        if (has_permission('Report Quotation', '', 'view') || has_permission('Report Quotation', '', 'view_own') || is_admin() )
        {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('estimates_model');

            $estimateTaxes     = $this->distinct_taxes('estimate');
            $totalTaxesColumns = count($estimateTaxes);

            $select = [
                'number',
                get_sql_select_client_company(),
                'invoiceid',
                'YEAR(date) as year',
                'date',
                'expirydate',
                 'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
                 'tblitems_in.description as description',
                 'tblitems_in.rate as rate',
                'subtotal',
                '(tblestimates.total_tax/2)as cgst',
                 '(tblestimates.total_tax/2)as sgst',
                'total',
                'total_tax',
                // 'discount_total',
                // 'adjustment',
                // 'reference_no',
                'status',
                'tblclients.phonenumber as phonenumber',
                 'tblclients.email_id as email_id',
            ];

            $estimatesTaxesSelect = array_reverse($estimateTaxes);

            foreach ($estimatesTaxesSelect as $key => $tax) {
                array_splice($select, 9, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="estimate" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblestimates.id) as total_tax_single_' . $key);
            }

           $staffuserid=get_staff_user_id();
            if (!has_permission('Report Quotation', '', 'view_own') || is_admin()) 
            {
               $where = [
                  
          ];  
      
   }else{
       
    $where              = ['AND tblestimates.addedfrom  = "'.$staffuserid.'"',];

   }
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('estimate_status')) {
                $statuses  = $this->input->post('estimate_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('sale_agent_estimates')) {
                $agents  = $this->input->post('sale_agent_estimates');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }
            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblestimates';
            $join         = [
                'LEFT JOIN tblclients ON tblclients.userid = tblestimates.clientid',
                'left join tblstaff on tblestimates.sale_agent=tblstaff.staffid',
                'left join tblitems_in on tblitems_in.rel_id=tblestimates.id and rel_type="estimate"',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                'tblestimates.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($estimateTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('estimates/list_estimates/' . $aRow['id']) . '" target="_blank">' . format_estimate_number($aRow['id']) . '</a>';

                if(empty($aRow['deleted_customer_name'])) {
                   $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                if ($aRow['invoiceid'] === null) {
                    $row[] = '';
                } else {
                    $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoiceid']) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['expirydate']);
                $row[] = $aRow['staff_name'];

                $row[] = $aRow['description'];

                $row[] = $aRow['rate'];


                // $row[] = format_money($aRow['subtotal'], $currency_symbol);
                // $footer_data['subtotal'] += $aRow['subtotal'];
                //$row[] = $aRow['cgst'];
                //$row[] = $aRow['sgst'];

                $igst=igst_check($aRow['clientid']);
				
				 if($igst==33)
				 {
				$cgst=$aRow['cgst'];
               $sgst=$aRow['sgst'];
				$icgst="--";
				
				 }else{
					 $icgst = $aRow['total_tax']; 
					 $cgst="--";
					 $sgst="--";
				 }
				 $row[] = $cgst;
                $row[] =  $sgst;
                $row[]=$icgst;

                $row[] = format_money($aRow['total_tax'], $currency_symbol);
                $footer_data['total_tax'] += $aRow['total_tax'];
                $row[] = format_money($aRow['total'], $currency_symbol);
                $footer_data['total'] += $aRow['total'];

                // $t = $totalTaxesColumns - 1;
                // $i = 0;
                // foreach ($estimateTaxes as $tax) {
                //     $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency_symbol);
                //     $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                //     $t--;
                //     $i++;
                // }

                // $row[] = format_money($aRow['discount_total'], $currency_symbol);
                // $footer_data['discount_total'] += $aRow['discount_total'];

                // $row[] = format_money($aRow['adjustment'], $currency_symbol);
                // $footer_data['adjustment'] += $aRow['adjustment'];


                // $row[] = $aRow['reference_no'];

                $row[] = format_estimate_status($aRow['status']);
                $row[] = $aRow['phonenumber'];
                $row[] = $aRow['email_id'];

                $output['aaData'][] = $row;
            }
            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = format_money($total, $currency_symbol);
            }
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }
}

    private function get_where_report_period($field = 'date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
                if ($months_report == '1') {
                    $beginMonth = date('Y-m-01', strtotime('first day of last month'));
                    $endMonth   = date('Y-m-t', strtotime('last day of last month'));
                } else {
                    $months_report = (int) $months_report;
                    $months_report--;
                    $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                    $endMonth   = date('Y-m-t');
                }

                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif ($months_report == 'this_month') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif ($months_report == 'this_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date('Y-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date('Y-12-31'))) . '")';
            } elseif ($months_report == 'last_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $from_date . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $from_date . '" AND "' . $to_date . '")';
                }
            }
        }

        return $custom_date_select;
    }
    
    
    private function get_where_report_period_invoice($field = 'i.date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
                if ($months_report == '1') {
                    $beginMonth = date('Y-m-01', strtotime('first day of last month'));
                    $endMonth   = date('Y-m-t', strtotime('last day of last month'));
                } else {
                    $months_report = (int) $months_report;
                    $months_report--;
                    $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                    $endMonth   = date('Y-m-t');
                }

                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif ($months_report == 'this_month') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif ($months_report == 'this_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                    date('Y-m-d', strtotime(date('Y-01-01'))) .
                    '" AND "' .
                    date('Y-m-d', strtotime(date('Y-12-31'))) . '")';
            } elseif ($months_report == 'last_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                    date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-01-01'))) .
                    '" AND "' .
                    date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $from_date . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $from_date . '" AND "' . $to_date . '")';
                }
            }
        }

        return $custom_date_select;
    }


    public function items()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $v = $this->db->query('SELECT VERSION() as version')->row();
            // 5.6 mysql version don't have the ANY_VALUE function implemented.

            if ($v && strpos($v->version, '5.7') !== false) {
                $aColumns = [
                        'ANY_VALUE(description) as description',
                        'ANY_VALUE((SUM(tblitems_in.qty))) as quantity_sold',
                        'ANY_VALUE(SUM(rate*qty)) as rate',
                        'ANY_VALUE(AVG(rate*qty)) as avg_price',
                    ];
            } else {
                $aColumns = [
                        'description as description',
                        '(SUM(tblitems_in.qty)) as quantity_sold',
                        'SUM(rate*qty) as rate',
                        'AVG(rate*qty) as avg_price',
                    ];
            }

            $sIndexColumn = 'id';
            $sTable       = 'tblitems_in';
            $join         = ['JOIN tblinvoices ON tblinvoices.id = tblitems_in.rel_id'];

            $where = ['AND rel_type="invoice"', 'AND status != 5', 'AND status=2'];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }
            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('sale_agent_items')) {
                $agents  = $this->input->post('sale_agent_items');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);
            $result          = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [], 'GROUP by description');

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total_amount' => 0,
                'total_qty'    => 0,
            ];

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = $aRow['description'];
                $row[] = $aRow['quantity_sold'];
                $row[] = format_money($aRow['rate'], $currency_symbol);
                $row[] = format_money($aRow['avg_price'], $currency_symbol);
                $footer_data['total_amount'] += $aRow['rate'];
                $footer_data['total_qty'] += $aRow['quantity_sold'];
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = format_money($footer_data['total_amount'], $currency_symbol);

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function credit_notes()
    {
        if ($this->input->is_ajax_request()) {
            $credit_note_taxes = $this->distinct_taxes('credit_note');
            $totalTaxesColumns = count($credit_note_taxes);

            $this->load->model('currencies_model');

            $select = [
                'number',
                'date',
                get_sql_select_client_company(),
                'reference_no',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT tblcreditnotes.total - (SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.credit_id=tblcreditnotes.id)) as remaining_amount',
                'status',
            ];

            $where = [];

            $credit_note_taxes_select = array_reverse($credit_note_taxes);

            foreach ($credit_note_taxes_select as $key => $tax) {
                array_splice($select, 5, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="credit_note" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblcreditnotes.id) as total_tax_single_' . $key);
            }

            $custom_date_select = $this->get_where_report_period();

            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');

            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('credit_note_status')) {
                $statuses  = $this->input->post('credit_note_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblcreditnotes';
            $join         = [
                'LEFT JOIN tblclients ON tblclients.userid = tblcreditnotes.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                'tblcreditnotes.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'            => 0,
                'subtotal'         => 0,
                'total_tax'        => 0,
                'discount_total'   => 0,
                'adjustment'       => 0,
                'remaining_amount' => 0,
            ];

            foreach ($credit_note_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('credit_notes/list_credit_notes/' . $aRow['id']) . '" target="_blank">' . format_credit_note_number($aRow['id']) . '</a>';

                $row[] = _d($aRow['date']);

                if(empty($aRow['deleted_customer_name'])){
                   $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';

                $row[] = $aRow['reference_no'];

                $row[] = format_money($aRow['subtotal'], $currency->symbol);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = format_money($aRow['total'], $currency->symbol);
                $footer_data['total'] += $aRow['total'];

                $row[] = format_money($aRow['total_tax'], $currency->symbol);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($credit_note_taxes as $tax) {
                    $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->symbol);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = format_money($aRow['discount_total'], $currency->symbol);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = format_money($aRow['adjustment'], $currency->symbol);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = format_money($aRow['remaining_amount'], $currency->symbol);
                $footer_data['remaining_amount'] += $aRow['remaining_amount'];

                $row[] = format_credit_note_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = format_money($total, $currency->symbol);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }


// public function task_report()
//     {
//         if ($this->input->is_ajax_request()) {
//             $invoice_taxes     = $this->distinct_taxes('invoice');
//             $totalTaxesColumns = count($invoice_taxes);

//             $this->load->model('currencies_model');
//             $this->load->model('invoices_model');

//             $select = [
//                 'number',
//                 get_sql_select_client_company(),
//                 'YEAR(date) as year',
//                 'date',
//                 'duedate',
//                 //'subtotal',
//               // 'total',
//                 'total_tax',
//                 //'discount_total',
//                 //'adjustment',
//                 //'(SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id) as credits_applied',
//               // '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM tblinvoicepaymentrecords WHERE invoiceid = tblinvoices.id) - (SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id))',
//                 'status',
// 				'project_id',
// 				'tblitems_in.description as description',
// 				'tblitems_in.rate as rate',
//                 'tblclients.phonenumber as phonenumber',
//                  'tblclients.email_id as email_id',
// 				'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
// 				'(total_tax/2)as cgst',
// 				 '(total_tax/2)as sgst',
// 				 '(tblitems_in.rate + total_tax)as total_amount ',
                 
// 				 ];

//             $where = [
//                 'AND status != 5',
//             ];

//           /* $invoiceTaxesSelect = array_reverse($invoice_taxes);

//             foreach ($invoiceTaxesSelect as $key => $tax) {
//                 array_splice($select, 8, 0, '(
//                     SELECT CASE
//                     WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
//                     WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
//                     ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
//                     END
//                     FROM tblitems_in
//                     INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
//                     WHERE tblitems_in.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_' . $key);
//             }*/

//             $custom_date_select = $this->get_where_report_period();
//             if ($custom_date_select != '') {
//                 array_push($where, $custom_date_select);
//             }

//             if ($this->input->post('sale_agent_invoices')) {
//                 $agents  = $this->input->post('sale_agent_invoices');
//                 $_agents = [];
//                 if (is_array($agents)) {
//                     foreach ($agents as $agent) {
//                         if ($agent != '') {
//                             array_push($_agents, $agent);
//                         }
//                     }
//                 }
//                 if (count($_agents) > 0) {
//                     array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
//                 }
//             }

//             $by_currency              = $this->input->post('report_currency');
//             $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

//             /*if ($by_currency) {
//                 $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
//                 $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
//                 $select[$totalPaymentsColumnIndex] = $_temp;

//                 $currency = $this->currencies_model->get($by_currency);
//                 array_push($where, 'AND currency=' . $by_currency);
//             } else {
//                 $currency                          = $this->currencies_model->get_base_currency();
//                 $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
//             }*/

//             $currency_symbol = $currency->symbol;

//             if ($this->input->post('invoice_status')) {
//                 $statuses  = $this->input->post('invoice_status');
//                 $_statuses = [];
//                 if (is_array($statuses)) {
//                     foreach ($statuses as $status) {
//                         if ($status != '') {
//                             array_push($_statuses, $status);
//                         }
//                     }
//                 }
//                 if (count($_statuses) > 0) {
//                     array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
//                 }
//             }

//             $aColumns     = $select;
//             $sIndexColumn = 'id';
//             $sTable       = 'tblinvoices';
//             $join         = [
//                 'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
// 				'LEFT JOIN tblitems_in ON tblitems_in.rel_id = tblinvoices.id AND tblitems_in.rel_type = "invoice"',
//                 'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',
//             ];

//             $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
//                 'userid',
//                 'clientid',
//                 'tblinvoices.id',
//                 'discount_percent',
//                 'deleted_customer_name',
//             ]);

//             $output  = $result['output'];
//             $rResult = $result['rResult'];

//             $footer_data = [
//                 'rate'           => 0,
//                 //'subtotal'        => 0,
// 				'tblitems_in.rate'        => 0,
//                 'total_tax'       => 0,
//                 //'discount_total'  => 0,
//                 //'adjustment'      => 0,
//                 //'applied_credits' => 0,
//                 //'amount_open'     => 0,
//             ];

//             foreach ($invoice_taxes as $key => $tax) {
//                 $footer_data['total_tax_single_' . $key] = 0;
            
// 			}

//             foreach ($rResult as $aRow) {
//                 $row = [];

//                 $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';
					
//                 if(empty($aRow['deleted_customer_name'])) {
//                     $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
//                 } else {
//                     $row[] = $aRow['deleted_customer_name'];
//                 }
 
//                 $row[] = $aRow['year'];
// 				$taskno=task_number($aRow['project_id'],$aRow['description']);
// 				 $row[]=$taskno;
//                 $row[] = _d($aRow['date']);

//               //$row[] = _d($aRow['duedate']);
// 			   $duedate = task_due_date($aRow['project_id'],$aRow['description']);
// 			   $row[] =_d($duedate);
//                 $row[] = $aRow['staff_name'];
// 				 $row[] = $aRow['description'];
				 
// 				 $row[]=task_assign_to($aRow['id'],$aRow['description']);
				
// 				$row[]=task_status($aRow['id'],$aRow['description']);
				
// 				$dateofcompletion = task_complete_date($aRow['project_id'],$aRow['description']);
				
//                 $row[] =_d($dateofcompletion);
                
// 			$row[] = overdue_task_date($duedate);
			
// 			$lastdate = last_modified_date($taskno);
// 			if(	$lastdate!='')
// 			{
//             $row[] =@date("d-M-Y h:i A", strtotime($lastdate));
// 			}else{
// 			   $row[]='-'; 
// 			}
//                 $output['aaData'][] = $row;
//             }

//             foreach ($footer_data as $key => $rate) {
//                 $footer_data[$key] = format_money($rate, $currency_symbol);
//             }
         
//             $output['sums'] = $footer_data;
//             echo json_encode($output);
//             die();
//         }
//     }

public function task_report()
    {
        if (has_permission('Report Task', '', 'view') || has_permission('Report Task', '', 'view_own') || is_admin() )
        {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes     = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = [
                'number',
                get_sql_select_client_company(),
                'YEAR(date) as year',
                'date',
                'duedate',
                //'subtotal',
                // 'total',
                'total_tax',
                //'discount_total',
                //'adjustment',
                //'(SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id) as credits_applied',
                // '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM tblinvoicepaymentrecords WHERE invoiceid = tblinvoices.id) - (SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id))',
                'status',
                'project_id',
                'tblitems_in.description as description',
                'tblitems_in.rate as rate',
                'tblitems_in.id as iid',
                'tblclients.phonenumber as phonenumber',
                'tblclients.email_id as email_id',
                'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
                '(total_tax/2)as cgst',
                '(total_tax/2)as sgst',
                '(tblitems_in.rate + total_tax)as total_amount ',

				 ];


            $where = [
                'AND status != 5','AND project_id != 0',
               
            ];


           /* $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach ($invoiceTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_' . $key);
            }*/

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('sale_agent_invoices')) {
                $agents  = $this->input->post('sale_agent_invoices');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }

                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency              = $this->input->post('report_currency');
            $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            /*if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency                          = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }*/

            $currency_symbol = $currency->symbol;

            if ($this->input->post('invoice_status')) {
                $statuses  = $this->input->post('invoice_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;

            $sIndexColumn = 'id';
            $sTable       = 'tblinvoices';
            $join         = [
                'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
				'LEFT JOIN tblitems_in ON tblitems_in.rel_id = tblinvoices.id AND tblitems_in.rel_type = "invoice"',
                'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',


//                'LEFT JOIN tblestimates  on tblinvoices.id=tblestimates.invoiceid',
//                'LEFT JOIN tblclients  on tblinvoices.clientid=tblclients.userid',
//                'LEFT JOIN tblitems_in  on tblestimates.id=tblitems_in.rel_id',
//                'LEFT JOIN tblprojects  on tblinvoices.project_id=tblprojects.id',
//                'LEFT JOIN tblstafftasks  on tblprojects.id=tblstafftasks.rel_id',
//                'LEFT JOIN tblstafftaskassignees  on tblstafftaskassignees.taskid=tblstafftasks.id',

            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                'tblinvoices.id',
                'discount_percent',
                'deleted_customer_name',
            ]);
            if (is_admin() || !has_permission('Report Task', '', 'view_own')) {
                $output = $result['output'];
            }
            $rResult = $result['rResult'];

            $footer_data = [
                'rate'           => 0,
                //'subtotal'        => 0,
				'tblitems_in.rate'        => 0,
                'total_tax'       => 0,
                //'discount_total'  => 0,
                //'adjustment'      => 0,
                //'applied_credits' => 0,
                //'amount_open'     => 0,
            ];

            foreach ($invoice_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            
			}

                $staffuserid = get_staff_user_id();
//            $where="";
//            if(is_admin()) {
//                $where = "st.name=it.description and it.rel_type='estimate' and st.rel_type='project'";
//            }
//            elseif (has_permission('customers', '', 'view_own')){
//                $where = "'";
//            }else{
//                $where = "st.name=it.description and it.rel_type='estimate' and st.rel_type='project'";
//
//            }
            if (has_permission('Report Task', '', 'view_own') && !is_admin()) {


                //$where=['and st.name=it.description', 'and sta.staffid=$staffuserid', 'and it.rel_type="estimate"','and st.rel_type="project"', 'and i.status!=5'];
                //$where=["st.name=it.description and sta.staffid=$staffuserid and it.rel_type='estimate' and st.rel_type='project' and i.status!=5"];
                $custom_date_select = $this->get_where_report_period_invoice();





//                print_r($custom_date_select);
//                exit;
                //$where = "st.name=it.description and sta.staffid=$staffuserid and it.rel_type='estimate' and st.rel_type='project' and i.status!=5 $custom_date_select";
                $where ="st.name=it.description and sta.staffid=$staffuserid and i.status!=5 and i.project_id !=0  and it.rel_type='estimate' and st.rel_type='project' and i.status!=5 $custom_date_select  ";


                $sql = "select  i.id,i.date,i.duedate,i.deleted_customer_name,year(i.date) as year,i.clientid,i.sale_agent,i.project_id,e.id as esid,c.company,it.description,it.rate,it.id as iid',it.description,p.id as projectid,st.id as stafftaskid,st.name as name,sta.staffid as stafftaskassid,sta.taskid,concat(ts.firstname,ts.lastname )as staff_name from tblinvoices i
                join tblestimates e on i.id=e.invoiceid 
                join tblclients c on i.clientid=c.userid
                 join tblitems_in it on e.id=it.rel_id
                 join tblprojects p on i.project_id=p.id
                 join tblstafftasks st on p.id=st.rel_id
                 
                 join tblstafftaskassignees sta on sta.taskid=st.id
                 join tblstaff ts on i.sale_agent=ts.staffid
               where $where ";
                $query = $this->db->query($sql);
                $rResult = $query->result_array();


            }
            foreach ($rResult as $aRow) {
                $get_id = $this->get_expenses($aRow['iid']);  
               
               if(!empty($get_id)){
                   continue;
               }
                $row = [];

               $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';
                //$row[]=format_invoice_number($aRow['id']);
                if(empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }
 
                $row[] = $aRow['year'];
				$taskno=task_number($aRow['project_id'],$aRow['description']);
				 $row[]=$taskno;
                $row[] = _d($aRow['date']);

               //$row[] = _d($aRow['duedate']);
			   $duedate = task_due_date($aRow['project_id'],$aRow['description']);
			   $row[] =_d($duedate);
                $row[] = $aRow['staff_name'];
				 $row[] = $aRow['description'];
				 
				 $row[]=task_assign_to($aRow['id'],$aRow['description']);
				
				$row[]=task_status($aRow['id'],$aRow['description']);
				
				$dateofcompletion = task_complete_date($aRow['project_id'],$aRow['description']);
				
                $row[] =_d($dateofcompletion);
                
            $row[] = overdue_task_date($duedate);
            
            $lastdate = last_modified_date($taskno);
            if(	$lastdate!='')
			{
            $row[] =@date("d-M-Y h:i A", strtotime($lastdate));
			}else{
			   $row[]='-'; 
			}

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $rate) {
                $footer_data[$key] = format_money($rate, $currency_symbol);
            }
         
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }
    }





    public function invoices_report()
    {
        if (has_permission('Report Invoice', '', 'view') || has_permission('Report Invoice', '', 'view_own') || is_admin() )
        {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes     = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = [
                'number',
                get_sql_select_client_company(),
                'YEAR(date) as year',
                'date',
                'duedate',
                //'subtotal',
               'total',
                'total_tax',
                //'discount_total',
                //'adjustment',
                //'(SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id) as credits_applied',
               // '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM tblinvoicepaymentrecords WHERE invoiceid = tblinvoices.id) - (SELECT COALESCE(SUM(amount),0) FROM tblcredits WHERE tblcredits.invoice_id=tblinvoices.id))',
                 'status',
                	'project_id',
				'tblitems_in.long_description as longdescription',
				'tblitems_in.description as description',
				'tblitems_in.rate as rate',
				'tblitems_in.qty as qty',
				'tblitems_in.id as iid',
				//'(SELECT SUM(rate*qty) FROM tblitems_in)as rate',
				 //'(SELECT SUM(rate*qty) FROM tblitems_in WHERE tblitems_in.rel_id=tblinvoices.id AND tblitems_in.rel_type="invoice") as rate',
                'tblclients.phonenumber as phonenumber',
                 'tblclients.email_id as email_id',
				'concat(tblstaff.firstname,tblstaff.lastname )as staff_name',
				'(total_tax)as cgst',
				 '(total_tax)as sgst',
				 '(tblitems_in.rate + total_tax)as total_amount ',
                 
				 ];

            // $where = [
            //     'AND status != 5','AND YEAR(date) = YEAR(CURDATE())',
            // ];
            $staffuserid=get_staff_user_id();
                 if (!has_permission('Report Invoice', '', 'view_own') || is_admin()) 
                 {
                    $where = [
                        'AND status != 5','AND YEAR(date) = YEAR(CURDATE())',
                    ];  
           
        }else{
            
            $where = [
                'AND status != 5','AND YEAR(date) = YEAR(CURDATE())','AND tblinvoices.addedfrom = "'.$staffuserid.'"',
            ];

        }
        

           /* $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach ($invoiceTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*tblitemstax.taxrate) - (qty*rate/100*tblitemstax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*tblitemstax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM tblitems_in
                    INNER JOIN tblitemstax ON tblitemstax.itemid=tblitems_in.id
                    WHERE tblitems_in.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND tblitems_in.rel_id=tblinvoices.id) as total_tax_single_' . $key);
            }*/

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('sale_agent_invoices')) {
                $agents  = $this->input->post('sale_agent_invoices');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $agent);
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency              = $this->input->post('report_currency');
            $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            /*if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $by_currency);
            } else {
                $currency                          = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }*/

            $currency_symbol = $currency->symbol;

            if ($this->input->post('invoice_status')) {
                $statuses  = $this->input->post('invoice_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $status);
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = 'tblinvoices';
            $join         = [
                'LEFT JOIN tblclients ON tblclients.userid = tblinvoices.clientid',
				'LEFT JOIN tblitems_in ON tblitems_in.rel_id = tblinvoices.id AND tblitems_in.rel_type = "invoice"',
                'left join tblstaff on tblinvoices.sale_agent=tblstaff.staffid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                'tblinvoices.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'rate'           => 0,
                //'subtotal'        => 0,
				'tblitems_in.rate'        => 0,
                'total_tax'       => 0,
                //'discount_total'  => 0,
                //'adjustment'      => 0,
                //'applied_credits' => 0,
                //'amount_open'     => 0,
            ];

            foreach ($invoice_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            
			}

            foreach ($rResult as $aRow) {
                
                $get_id = $this->get_expenses($aRow['iid']);  
               
               if(!empty($get_id)){
                   continue;
               }
                
                $row = [];

                $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

                if(empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['duedate']);
                	$taskno=task_number($aRow['project_id'],$aRow['description']);
				 $row[]=$taskno;
                $row[] = $aRow['staff_name'];
				 $row[] = $aRow['description'];

                //$row[] = format_money($aRow['subtotal'], $currency_symbol);
                //$footer_data['subtotal'] += $aRow['subtotal'];
				$rateqty=$aRow['rate'] * $aRow['qty'];
				$row[] = $rateqty;
				$footer_data['rate'] += $aRow['rate'];
				
				//$row[] = $aRow['cgst'];
				//$row[] = $aRow['sgst'];
			
				$igst=igst_check($aRow['clientid']);
				
				 if($igst==33)
				 {
					 
				$cgstsgst=tax_calculation($rateqty,$aRow['total_tax']);
				$cgst=$cgstsgst/2;
               $sgst=$cgstsgst/2;
				$icgst="--";
				
				 }else{
					 $icgst = $aRow['total_tax']; 
					 $cgst="--";
					 $sgst="--";
				 }
				 $row[] = $cgst;
                $row[] =  $sgst;
                $row[]=$icgst;
				
				
                //$row[] = format_money($aRow['total'], $currency_symbol);
                //$footer_data['total'] += $aRow['total'];

               if($aRow['total_tax'] > 0 && $icgst=="--")
				{
				$totaltax=$cgst + $sgst ;
                $row[] = $totaltax ;
				$totalamount=$rateqty + $totaltax ;
                $row[] = $totalamount;
                   $footer_data['subtotal'] += $totalamount;
				}else{
					$totaltax=$icgst ;
					$row[] = $icgst ;
					$totalamount=$rateqty + $totaltax ;
                $row[] = $totalamount;
                   $footer_data['subtotal'] += $totalamount;
				}
                //$row[] = $aRow['total_amount'];
                   //$footer_data['subtotal'] += $aRow['total_amount'];
               // $t = $totalTaxesColumns - 1;
                //$i = 0;
                //foreach ($invoice_taxes as $tax) {
                   // $row[] = format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency_symbol);
                    //$footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                   // $t--;
                   // $i++;
              //  }

                //$row[] = format_money($aRow['discount_total'], $currency_symbol);
                //$footer_data['discount_total'] += $aRow['discount_total'];

               // $row[] = format_money($aRow['adjustment'], $currency_symbol);
                //$footer_data['adjustment'] += $aRow['adjustment'];

               // $row[] = format_money($aRow['credits_applied'], $currency_symbol);
               // $footer_data['applied_credits'] += $aRow['credits_applied'];

               // $amountOpen = $aRow['amount_open'];
               // $row[]      = format_money($amountOpen, $currency_symbol);
                //$footer_data['amount_open'] += $amountOpen;

                $row[] = format_invoice_status($aRow['status']);
                $row[] = $aRow['phonenumber'];
                $row[] = $aRow['email_id'];
				$row[] = remarks($aRow['longdescription'],$aRow['description']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $rate) {
                $footer_data[$key] = format_money($rate, $currency_symbol);
            }
         
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }
}

    public function expenses($type = 'simple_report')
    {
        if (has_permission('reports', '', 'view') || is_admin())  { 
        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();

        $data['title'] = _l('expenses_report');
        if ($type != 'simple_report') {
            $this->load->model('expenses_model');
            $data['categories'] = $this->expenses_model->get_category();
            $data['years']      = $this->expenses_model->get_expenses_years();

            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    'category',
                    'amount',
                    'expense_name',
                    'tax',
                    'tax2',
                    '(SELECT taxrate FROM tbltaxes WHERE id=tblexpenses.tax)',
                    'amount as amount_with_tax',
                    'billable',
                    'date',
                    get_sql_select_client_company(),
                    'invoiceid',
                    'reference_no',
                    'paymentmode',
                ];
                $join = [
                    'LEFT JOIN tblclients ON tblclients.userid = tblexpenses.clientid',
                    'LEFT JOIN tblexpensescategories ON tblexpensescategories.id = tblexpenses.category',
                ];
                $where  = [];
                $filter = [];
                include_once(APPPATH . 'views/admin/tables/includes/expenses_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $by_currency = $this->input->post('currency');
                if ($by_currency) {
                    $currency = $this->currencies_model->get($by_currency);
                    array_push($where, 'AND currency=' . $by_currency);
                } else {
                    $currency = $this->currencies_model->get_base_currency();
                }
                $currency_symbol = $this->currencies_model->get_currency_symbol($currency->id);

                $sIndexColumn = 'id';
                $sTable       = 'tblexpenses';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                    'tblexpensescategories.name as category_name',
                    'tblexpenses.id',
                    'tblexpenses.clientid',
                    'currency',
                ]);
                $output  = $result['output'];
                $rResult = $result['rResult'];
                $this->load->model('currencies_model');
                $this->load->model('payment_modes_model');

                $footer_data = [
                    'amount'          => 0,
                    'total_tax'       => 0,
                    'amount_with_tax' => 0,
                ];

                foreach ($rResult as $aRow) {
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                            $_data = $aRow[strafter($aColumns[$i], 'as ')];
                        } else {
                            $_data = $aRow[$aColumns[$i]];
                        }
                        if ($aRow['tax'] != 0) {
                            $_tax = get_tax_by_id($aRow['tax']);
                        }
                        if ($aRow['tax2'] != 0) {
                            $_tax2 = get_tax_by_id($aRow['tax2']);
                        }
                        if ($aColumns[$i] == 'category') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['category_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'expense_name') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['expense_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'amount' || $i == 6) {
                            $total = $_data;
                            if ($i != 6) {
                                $footer_data['amount'] += $total;
                            } else {
                                if ($aRow['tax'] != 0 && $i == 6) {
                                    $total += ($total / 100 * $_tax->taxrate);
                                }
                                if ($aRow['tax2'] != 0 && $i == 6) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                }
                                $footer_data['amount_with_tax'] += $total;
                            }

                            $_data = format_money($total, $currency_symbol);
                        } elseif ($i == 9) {
                            $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                        } elseif ($aColumns[$i] == 'paymentmode') {
                            $_data = '';
                            if ($aRow['paymentmode'] != '0' && !empty($aRow['paymentmode'])) {
                                $payment_mode = $this->payment_modes_model->get($aRow['paymentmode'], [], false, true);
                                if ($payment_mode) {
                                    $_data = $payment_mode->name;
                                }
                            }
                        } elseif ($aColumns[$i] == 'date') {
                            $_data = _d($_data);
                        } elseif ($aColumns[$i] == 'tax') {
                            if ($aRow['tax'] != 0) {
                                $_data = $_tax->name . ' - ' . _format_number($_tax->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($aColumns[$i] == 'tax2') {
                            if ($aRow['tax2'] != 0) {
                                $_data = $_tax2->name . ' - ' . _format_number($_tax2->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($i == 5) {
                            if ($aRow['tax'] != 0 || $aRow['tax2'] != 0) {
                                if ($aRow['tax'] != 0) {
                                    $total = ($total / 100 * $_tax->taxrate);
                                }
                                if ($aRow['tax2'] != 0) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                }
                                $_data = format_money($total, $currency_symbol);
                                $footer_data['total_tax'] += $total;
                            } else {
                                $_data = _format_number(0);
                            }
                        } elseif ($aColumns[$i] == 'billable') {
                            if ($aRow['billable'] == 1) {
                                $_data = _l('expenses_list_billable');
                            } else {
                                $_data = _l('expense_not_billable');
                            }
                        } elseif ($aColumns[$i] == 'invoiceid') {
                            if ($_data) {
                                $_data = '<a href="' . admin_url('invoices/list_invoices/' . $_data) . '">' . format_invoice_number($_data) . '</a>';
                            } else {
                                $_data = '';
                            }
                        }
                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                foreach ($footer_data as $key => $total) {
                    $footer_data[$key] = format_money($total, $currency_symbol);
                }

                $output['sums'] = $footer_data;
                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/expenses_detailed', $data);
        } else {
            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), [
                'billable' => 0,
            ], [
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor'     => '#fc2d42',
            ], $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), [
                'billable' => 1,
            ], [
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor'     => '#84c529',
            ], $data['current_year']));

            $data['expense_years'] = $this->expenses_model->get_expenses_years();

            if (count($data['expense_years']) > 0) {
                // Perhaps no expenses in new year?
                if (!in_array_multidimensional($data['expense_years'], 'year', date('Y'))) {
                    array_unshift($data['expense_years'], ['year' => date('Y')]);
                }
            }

            $data['categories'] = $this->expenses_model->get_category();

            $this->load->view('admin/reports/expenses', $data);
        }
        }else{
		$this->sales();
	}
    }

    public function expenses_vs_income($year = '')
    {
        if (has_permission('reports', '', 'view') || is_admin())  { 
        $_expenses_years = [];
        $_years          = [];
        $this->load->model('expenses_model');
        $expenses_years = $this->expenses_model->get_expenses_years();
        $payments_years = $this->reports_model->get_distinct_payments_years();

        foreach ($expenses_years as $y) {
            array_push($_years, $y['year']);
        }
        foreach ($payments_years as $y) {
            array_push($_years, $y['year']);
        }

        $_years = array_map('unserialize', array_unique(array_map('serialize', $_years)));

        if (!in_array(date('Y'), $_years)) {
            $_years[] = date('Y');
        }

        rsort($_years, SORT_NUMERIC);
        $data['report_year'] = $year == '' ? date('Y') : $year;

        $data['years']                           = $_years;
        $data['chart_expenses_vs_income_values'] = json_encode($this->reports_model->get_expenses_vs_income_report($year));
        $data['title']                           = _l('als_expenses_vs_income');
        $this->load->view('admin/reports/expenses_vs_income', $data);
        }else{
		$this->sales();
	}
    }

    /* Total income report / ajax chart*/
    public function total_income_report()
    {
        echo json_encode($this->reports_model->total_income_report());
    }

    public function report_by_payment_modes()
    {
        echo json_encode($this->reports_model->report_by_payment_modes());
    }

    public function report_by_customer_groups()
    {
        echo json_encode($this->reports_model->report_by_customer_groups());
    }

    /* Leads conversion monthly report / ajax chart*/
    public function leads_monthly_report($month)
    {
        echo json_encode($this->reports_model->leads_monthly_report($month));
    }

    private function distinct_taxes($rel_type)
    {
        return $this->db->query("SELECT DISTINCT taxname,taxrate FROM tblitemstax WHERE rel_type='" . $rel_type . "' ORDER BY taxname ASC")->result_array();
    }
    private function get_expenses($id){
        $this->db->where('item_id',$id);
        return $this->db->get('tblitemsrelated')->row_array();
    }
}
