<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_modes_model');
        $this->load->model('settings_model');
    }

    /* View all settings */
    public function index()
    {
        if (!has_permission('settings', '', 'view')) {
            access_denied('settings');
        }
        if ($this->input->post()) {
            if (!has_permission('settings', '', 'edit')) {
                access_denied('settings');
            }


       
            $logo_uploaded     = (handle_company_logo_upload() ? true : false);
            $favicon_uploaded  = (handle_favicon_upload() ? true : false);
            $signatureUploaded = (handle_company_signature_upload() ? true : false);


            
            $post_data = $this->input->post();
            $tmpData   = $this->input->post(null, false);
           
            
            if(isset($post_data['knowlarity_agent_number'])){
                unset($post_data['knowlarity_agent_number']);
            }
            if(isset($post_data['testing_contact'])){
                unset($post_data['testing_contact']);
            }
            if(isset($post_data['indiacode'])){
                unset($post_data['indiacode']);
            }

            if (isset($post_data['settings']['email_header'])) {
                $post_data['settings']['email_header'] = $tmpData['settings']['email_header'];
            }

            if (isset($post_data['settings']['email_footer'])) {
                $post_data['settings']['email_footer'] = $tmpData['settings']['email_footer'];
            }

            if (isset($post_data['settings']['email_signature'])) {
                $post_data['settings']['email_signature'] = $tmpData['settings']['email_signature'];
            }

            if (isset($post_data['settings']['smtp_password'])) {
                $post_data['settings']['smtp_password'] = $tmpData['settings']['smtp_password'];
            }

            $success = $this->settings_model->update($post_data);
            if ($success > 0) {
                set_alert('success', _l('settings_updated'));
            }

            if ($logo_uploaded || $favicon_uploaded) {
                set_debug_alert(_l('logo_favicon_changed_notice'));
            }

            // Do hard refresh on general for the logo
            if ($this->input->get('group') == 'general') {
                redirect(admin_url('settings?group=' . $this->input->get('group')), 'refresh');
            } elseif ($signatureUploaded) {
                redirect(admin_url('settings?group=pdf&tab=signature'));
            } else {
                redirect(admin_url('settings?group=' . $this->input->get('group')));
            }
        }

        $this->load->model('taxes_model');
        $this->load->model('tickets_model');
        $this->load->model('leads_model');
        $this->load->model('currencies_model');
        $data['taxes']                                   = $this->taxes_model->get();
        $data['ticket_priorities']                       = $this->tickets_model->get_priority();
        $data['ticket_priorities']['callback_translate'] = 'ticket_priority_translate';
        $data['roles']                                   = $this->roles_model->get();
        $data['leads_sources']                           = $this->leads_model->get_source();
        $data['leads_statuses']                          = $this->leads_model->get_status();
        $data['title']                                   = _l('options');
        if (!$this->input->get('group') || ($this->input->get('group') == 'update' && !is_admin())) {
            $view = 'general';
        } else {
            $view = $this->input->get('group');
        }

        $view = do_action('settings_group_view_name', $view);

        if ($view == 'update') {
            if (!extension_loaded('curl')) {
                $data['update_errors'][] = 'CURL Extension not enabled';
                $data['latest_version']  = 0;
                $data['update_info']     = json_decode('');
            } else {
                $data['update_info'] = $this->app->get_update_info();
                if (strpos($data['update_info'], 'Curl Error -') !== false) {
                    $data['update_errors'][] = $data['update_info'];
                    $data['latest_version']  = 0;
                    $data['update_info']     = json_decode('');
                } else {
                    $data['update_info']    = json_decode($data['update_info']);
                    $data['latest_version'] = $data['update_info']->latest_version;
                    $data['update_errors']  = [];
                }
            }

            if (!extension_loaded('zip')) {
                $data['update_errors'][] = 'ZIP Extension not enabled';
            }

            $data['current_version'] = $this->app->get_current_db_version();
        }

        $data['contacts_permissions'] = get_contact_permissions();
        $this->load->library('pdf');
        $data['payment_gateways'] = $this->payment_modes_model->get_online_payment_modes(true);
        $data['view_name']        = $view;
        $groups_path              = do_action('settings_groups_path', 'admin/settings/includes');
        $data['group_view']       = $this->load->view($groups_path . '/' . $view, $data, true);
        $this->load->view('admin/settings/all', $data);
    }

    public function delete_tag($id)
    {
        if (!$id) {
            redirect(admin_url('settings?group=tags'));
        }

        if (!has_permission('settings', '', 'delete')) {
            access_denied('settings');
        }

        $this->db->where('id', $id);
        $this->db->delete('tbltags');
        $this->db->where('tag_id', $id);
        $this->db->delete('tbltags_in');

        redirect(admin_url('settings?group=tags'));
    }

    public function remove_signature_image()
    {
        if (!has_permission('settings', '', 'delete')) {
            access_denied('settings');
        }

        $sImage = get_option('signature_image');
        if (file_exists(get_upload_path_by_type('company') . '/' . $sImage)) {
            unlink(get_upload_path_by_type('company') . '/' . $sImage);
        }

        update_option('signature_image', '');

        redirect(admin_url('settings?group=pdf&tab=signature'));
    }

    /* Remove company logo from settings / ajax */
    public function remove_company_logo()
    {
        do_action('before_remove_company_logo');
        if (!has_permission('settings', '', 'delete')) {
            access_denied('settings');
        }
        if (file_exists(get_upload_path_by_type('company') . '/' . get_option('company_logo'))) {
            unlink(get_upload_path_by_type('company') . '/' . get_option('company_logo'));
        }
        update_option('company_logo', '');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function remove_favicon()
    {
        do_action('before_remove_favicon');
        if (!has_permission('settings', '', 'delete')) {
            access_denied('settings');
        }
        if (file_exists(get_upload_path_by_type('company') . '/' . get_option('favicon'))) {
            unlink(get_upload_path_by_type('company') . '/' . get_option('favicon'));
        }
        update_option('favicon', '');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete_option($id)
    {
        if (!has_permission('settings', '', 'delete')) {
            access_denied('settings');
        }
        echo json_encode([
            'success' => delete_option($id),
        ]);
    }
    
      public function get_total_msg()
    {
        $sql="select count(*) from tblchatmessages where reciever_id=".get_staff_user_id()." and viewed=0";
        $query=$this->db->query($sql);
        $count = $query->result_array();
        echo $count[0]['count(*)'];
    }
    
    public function get_logs()
    {
        $dir = "logs_xl/";
         if (is_dir($dir)){
         if ($dh = opendir($dir)){
             $data1=[];
             $i=0;
         while (($file = readdir($dh)) !== false){
             
             if(strlen($file)>4)
             {
             
              $data1[$i]=$file;
              $i++;
             }
    }
        $data['file']=$data1;
    
       $this->load->view('admin/utilities/xl_logs',$data);
       
     }
     }
     
    }
    public function verify_knowlarity_account(){
        $knowlarity_api_key = $this->input->post('knowlarity_api_key');
        $india_code = $this->input->post('india_code');
        $knowlarity_api_url = $this->input->post('knowlarity_api_url');
        $knowlarity_authorization_key = $this->input->post('knowlarity_authorization_key');
        $knowlarity_sr_number = $this->input->post('knowlarity_sr_number');
        $knowlarity_agent_number = $this->input->post('knowlarity_agent_number');
        $testing_contact = $this->input->post('testing_contact');
        $url="$knowlarity_api_url/account/call/makecall";

        
        $post_call=array("k_number"=>"$india_code$knowlarity_sr_number",
        "agent_number"=>"$india_code$knowlarity_agent_number",
        "customer_number"=>"$india_code$testing_contact",
        "caller_id"=>"$india_code$knowlarity_sr_number"
    );
    
    $curl = curl_init();
     curl_setopt_array($curl, array(
     CURLOPT_URL => $url,
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 30,
     CURLOPT_SSL_VERIFYPEER=>false,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "POST",
     CURLOPT_POSTFIELDS => json_encode($post_call),
     CURLOPT_HTTPHEADER => array(
         "authorization: $knowlarity_authorization_key",
         "cache-control: no-cache",
         "x-api-key:$knowlarity_api_key",
         'content-Type: application/json'
      ),
      ));

 $response = curl_exec($curl);
 $err = curl_error($curl);
 curl_close($curl);
 if ($err) {
      echo "cURL Error #:" . $err;
 } else {
    echo $response;
 }
  
    }

    public function add_ratingmenu(){
        // echo 'sastha';
        $this->db->where('name','aside_menu_active');
        $aside_menu=$this->db->get('tbloptions')->row_array();
        $rating='{"name":"rating","url":"rating","permission":"rating","icon":"fa fa-user-o","id":"rating"}';
        $decodedText = html_entity_decode($aside_menu['value']);
        $myArray = json_decode($decodedText, true);
        $objects_data = $myArray['aside_menu_active'];
      
       //Rating
         $decodedText1 = html_entity_decode($rating);
         $myArray1 = json_decode($decodedText1, true);
         $push=array_merge($objects_data,$myArray1);
         $push_data =   json_encode([
            'aside_menu_active' => $push ]);
  
            print_r($push_data);


    }
    
}
