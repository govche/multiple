<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Rating extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('projects_model');

    }

    public function index()
    {
        close_setup_menu();
        $data['title']="Staff Rating";
  
         if(is_admin() || has_permission('rating','','view')){
        $data['ratings']=$this->projects_model->get_details();
        }
        else{
        $data['ratings']=$this->projects_model->get_own_details(get_staff_user_id());
        }
        $this->load->view('admin/rating/manage',$data);
    }

}

?>