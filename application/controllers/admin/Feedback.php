<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Feedback extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('projects_model');

    }

    public function index()
    {
        close_setup_menu();
  
         if(is_admin() || has_permission('feedback','','view')){
        $data['ratings']=$this->projects_model->get_details();
        }
        else{
        $data['ratings']=$this->projects_model->get_own_details(get_staff_user_id());
        }
        $this->load->view('admin/rating/manage',$data);
    }

}

?>