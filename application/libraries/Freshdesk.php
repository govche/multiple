<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Freshdesk Library
 *
 * Provides access to various Freshdesk APIs within the CodeIgniter PHP Framework.
 */
class Freshdesk
{
    private $CI;
    private $params;
    public function __construct($params = array())
    {
        // Get CI instance
        $this->CI =& get_instance($params);

        // Attempt to load config values from file
        if ($config = $this->CI->config->load('freshdesk', TRUE, TRUE))
        {
            $api_key  = $this->CI->config->item('api_key',  'freshdesk');
            $base_url = $this->CI->config->item('base_url', 'freshdesk');
        }
        // Attempt to load config values from params
        $api_key  = @$params['api_key']  ?: @$params['api-key'];
        $username = @$params['username'];
        $password = @$params['password'];
        $base_url = @$params['base_url'] ?: @$params['base-url'];

        // API Key takes precendence
        if ($api_key)
        {
            $username = $api_key;
            $password = 'X';
        }

        // Build list of default params
        $this->params = array(
            'base_url' => $base_url,
            'username' => $username,
            'password' => $password
        );
        // $class = "FreshdeskAgent";
        $this->api = new FreshdeskAPI($this->params);
        
    }
}

/**
 * Freshdesk API Transport
 *
 * Performs HTTP calls to the Freshdesk web service.
 */
class FreshdeskTransport
{
    protected $params;
    private $base_url;
    private $username;
    private $password;

    public function __construct($params)
    {
        $this->params   = $params;
        $this->base_url = $params['base_url'];
        $this->username = $params['username'];
        $this->password = $params['password'];
    }

    /**
     * Perform an API request.
     *
     * @param  string $resource Freshdesk API resource
     * @param  string $method   HTTP request method
     * @param  array  $data     HTTP PUT/POST data
     * @return mixed            JSON object or HTTP response code
     */
    protected function _request($resource, $method = 'GET', $data = NULL)
    {
        // Build request
        $method = strtoupper($method);
        $ch = curl_init ("{$this->base_url}/{$resource}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->username}:{$this->password}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        // Set POST data if passed to method
        if ($data) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        // Execute request
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        log_message('debug', var_export(array($info, htmlspecialchars($data)), TRUE));

        // Curl error handling
        if (curl_errno($ch) and $error = curl_error($ch))
        {
            log_message('error', var_export($error, TRUE));
            curl_close($ch);
            return FALSE;
        }
        if (in_array($info['http_code'], array(400, 404, 406, 302)))
        {
            log_message('error', var_export($data, TRUE));
            curl_close($ch);
            return FALSE;
        }

        // Close rqeuest
        curl_close($ch);

        // Load JSON object if data was returned and properly parsed
        if ($data = @json_decode($data))
        {
            // Return FALSE if data contains an error response
            if ($error = @$data->error)
            {
                log_message('error', var_export($error, TRUE));
                return FALSE;
            }
            // Return data
            return $data;
        }

        // Return HTTP response code by default
        return $info['http_code'];
    }
}

/**
 * Freshdesk Base API
 *
 * Provides common create, get, update, and delete methods.
 */
class FreshdeskAPI extends FreshdeskTransport
{
    protected $NODE;

    /**
     * Create a resource
     *
     * @param  string $endpoint API Endpoint
     * @param  array  $data     Array of resource data
     * @return mixed            JSON object or FALSE
     */
    public function create($endpoint, $data)
    {
        // Return FALSE if we did not receive an array of data
        // if ( ! is_array($data)) return FALSE;
        // Return object else FALSE if we've failed to get a request response
        return @$this->_request($endpoint, 'POST', $data) ?: FALSE;
    }

    /**
     * Retrieve a resource
     *
     * @param  string $endpoint API Endpoint
     * @return mixed            JSON object or FALSE
     */
    public function get($endpoint)
    {
        // Return object(s) else FALSE if we've failed to get a request response
        return @$this->_request($endpoint) ?: FALSE;
    }

    /**
     * Retrieve all resources
     *
     * @param  string $endpoint API Endpoint
     * @return mixed            JSON object or FALSE
     */
    public function get_all($endpoint)
    {
        // Return FALSE if we've failed to get a request response
        if ( ! $response = $this->_request($endpoint)) return FALSE;
        // Default object array
        $objects = array();
        // Return empty array of objects if HTTP 200 received
        if ($response == 200) return $objects;
        // Extract object data from its container node
        foreach ($response as $object) $objects[] = $object->{$this->NODE};
        // Return restructured array of objects
        return $objects;
    }

    /**
     * Update a resource
     *
     * @param  string $endpoint API Endpoint
     * @param  array  $data     Array of resource data
     * @return boolean          TRUE if HTTP 200 else FALSE
     */
    public function update($endpoint, $data)
    {
        // Return FALSE if we did not receive an array of data
        if ( ! is_array($data)) return FALSE;
        // Encapsulate data in container node
        if (array_shift(array_keys($data)) != $this->NODE) $data = array($this->NODE => $data);
        // Return TRUE if HTTP 200 else FALSE
        return $this->_request($endpoint, 'PUT', $data) == 200 ? TRUE : FALSE;
    }

    /**
     * Delete a resource
     *
     * @param  string $endpoint API Endpoint
     * @return boolean          TRUE if HTTP 200 else FALSE
     */
    public function delete($endpoint)
    {
        // Return TRUE if HTTP 200 else FALSE
        return ($response = $this->_request($endpoint, 'DELETE') or $response == 200) ? TRUE : FALSE;
    }
}