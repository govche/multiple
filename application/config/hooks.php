<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'] = array(
        'class'    => 'BadBotBlock',
        'function' => 'init',
        'filename' => 'BadBotBlock.php',
        'filepath' => 'hooks',
        'params'   => array()
);

$hook['pre_system'] = array(
        'class'    => 'BadUserAgentBlock',
        'function' => 'init',
        'filename' => 'BadUserAgentBlock.php',
        'filepath' => 'hooks',
        'params'   => array()
);



$hook['post_controller'] = array(     // 'post_controller' indicated execution of hooks after controller is finished
    'class' => 'Db_log',             // Name of Class
    'function' => 'logQueries',     // Name of function to be executed in from Class
    'filename' => 'db_log.php',    // Name of the Hook file
    'filepath' => 'hooks'         // Name of folder where Hook file is stored
);



if (file_exists(APPPATH.'config/my_hooks.php')) {
    include_once(APPPATH.'config/my_hooks.php');
}
