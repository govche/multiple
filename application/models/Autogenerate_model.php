
<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Autogenerate_model extends CI_Model
{

public function get_complianceDetails($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        return  $this->db->get('tbladmincompliance')->row_array();

    }

    public function service_details($invoice_id,$id){
        $this->db->where('rel_id',$invoice_id);
        $this->db->where('description',$id);
        return $this->db->get('tblitems_in')->row_array();

    }

    public function get_maxnumber(){
        $this->db->select('value');
        $this->db->where('id',51);
        return $this->db->get('tbloptions')->row()->value;
    }

    public function get_staff(){
        $this->db->where('admin',0);
        $this->db->where('role !=',5);
        return $this->db->get('tblstaff')->result_array();
    }

    public function check_invoices($clientid,$itemid,$date,$compliance_id){
        $this->db->where('clientid',$clientid);
        $this->db->where('date',$date);
        $estimates = $this->db->get('tblestimates')->result_array();
        if($estimates !=''){
        foreach($estimates as $estimate){
            $this->db->where('rel_id',$estimate['id']);
            $this->db->where('description',$itemid);
            $this->db->where('compliance_id',$compliance_id);
            $items = $this->db->get('tblitems_in')->row_array();
            if($items !=''){
              return $items;
            }
        }
    }
    else{
        return false;
    }

    }

    public function check_quotes($clientid,$itemid,$compliance_id){

        $this->db->where('clientid',$clientid);
        $this->db->where('type','auto');
        $estimates = $this->db->get('tblestimates')->result_array();
        if($estimates !=''){
        foreach($estimates as $estimate){
            $this->db->where('rel_id',$estimate['id']);
            $this->db->where('description',$itemid);
            $this->db->where('compliance_id',$compliance_id);
            $items = $this->db->get('tblitems_in')->row_array();
            if($items !=''){
              return $items;
            }
        }
    }
    else{
        return false;
    }

    }
    
    public function get_email($id){
        $this->db->select('email_id');
        $this->db->where('userid',$id);
        return $this->db->get('tblclients')->row_array();
    }

   public function recent_invocie(){

    $current_date = date('Y-m-d');
    $first_dt=date_create($current_date.'first day of last month');
    $last_dt=date_create($current_date.'last day of last month');
    $first=$first_dt->format('Y-m-d');
    $last=$last_dt->format('Y-m-d');
    $this->db->select('*');
    $this->db->where('datecreated >=',DATE($first));
    $this->db->where('datecreated <=',DATE($current_date));
    return $this->db->get('tblestimates')->result_array();
    //  return $this->db->last_query();

    }
    public function get_items($id){

        $this->db->select('tblitems_in.*,tblitems_compliance.item_name,tblitems_compliance.compliance_id,tbladmincompliance.frequency');
        $this->db->join('tblitems_compliance','tblitems_compliance.item_name =tblitems_in.description');
        $this->db->join('tbladmincompliance','tbladmincompliance.id=tblitems_compliance.compliance_id');
        $this->db->where('tblitems_in.rel_id',$id);
        $this->db->where('tblitems_in.rel_type','estimate');
        return $this->db->get('tblitems_in')->result_array();
    }

    public function get_estimates($start_date){
        $this->db->select('tblestimates.*,tblclients.phonenumber,tblclients.email_id');
        $this->db->join('tblclients','tblclients.userid=tblestimates.clientid');
        $this->db->where('tblestimates.type','auto');
        $this->db->where('tblestimates.invoiceid',null);
        $this->db->where('DATE(tblestimates.datecreated)',$start_date);
        return $this->db->get('tblestimates')->result_array();
    //    return $this->db->last_query();
    }

    public function checkInvoiceconverted($client_id){
      
        $this->db->select('*');
        $this->db->where('clientid',$client_id);
        $this->db->where('invoiceid',null);
        $this->db->where('type','auto');
        $client_data =$this->db->get('tblestimates')->result_array();

        foreach($client_data as $cdata){
        $datecreated=$cdata['datecreated'];
        $current_date = strtotime($datecreated);
        $present_date = date('Y-m-d',$current_date);
        $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime($present_date)));
        $due_date = $effectiveDate;

    
         // print_r($due_date);
         if($due_date < date('Y-m-d')){
         //clients_details
            $this->db->distinct();
            $this->db->select('tblclients.*,tblcontacts.firstname,tblcontacts.lastname');
            $this->db->join('tblcontacts','tblcontacts.userid=tblclients.userid');
            $this->db->where('tblclients.userid',$cdata['clientid']);
            $clients = $this->db->get('tblclients')->row_array();
         //get services            
            $this->db->where('rel_id',$cdata['id']);
            $this->db->where('rel_type','estimate');
            $service =$this->db->get('tblitems_in')->row_array();
       
            $this->db->where('id',$cdata['id']);
            $this->db->delete('tblestimates');
            if($this->db->affected_rows() >0){       
             $client_name =$clients['firstname'].' '.$clients['lastname'];  
             $data=array(
             'hash'=>app_generate_hash(),
             'name'=>$client_name,
             'company'=>$clients['company'],
             'country'=>$clients['country'],
             'city'=>$clients['city'],
             'zip'=>$clients['zip'],
             'state'=>$clients['state'],
             'address'=>$clients['address'],
             'dateadded'=>date('Y-m-d H:i:s'),
             'email'=>$clients['email'],
             'website'=>$clients['website'],
             'phonenumber'=>$clients['phonenumber'],
             'items'=>$service['description']
            );
             $this->db->insert('tblleads',$data);

             $this->db->where('rel_id',$cdata['id']);
             $this->db->where('rel_type','estimate');
             $this->db->delete('tblitems_in');
             if($this->db->affected_rows() > 0){
                 echo 'success';
                 echo '<br>';
             }
         }

        }
        else{
            echo 'not match';
            echo '<br>';

        }
        
           
        }
    }

    public function recent_invocie_distinct(){
     $this->db->distinct();
     $this->db->select('clientid');
     $this->db->where('type','auto');
   
     return $this->db->get('tblestimates')->result_array();

    }

    public function check_convertNot($description,$client){
        $this->db->where('clientid',$client);
        $this->db->where('type','auto');
       $clients = $this->db->get('tblestimates')->result_array();
    //    return $description  ;
       foreach($clients as $details){
           $estimate_id =$details['id'];
           $this->db->where('rel_id',$estimate_id);
           $this->db->where('description',$description);
           $this->db->where('compliance_id !=',0);
           $items= $this->db->get('tblitems_in')->row_array();
        if($items){
            $this->db->where('id',$items['rel_id']);
            $estimate =$this->db->get('tblestimates')->row_array();

            $datecreated=$estimate['datecreated'];
            $current_date = strtotime($datecreated);
            $present_date = date('Y-m-d',$current_date);
            $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime($present_date)));
            $due_date = $effectiveDate;
       
            if($due_date < date('Y-m-d')){
                return true;
            }

        }
       }



    }

}

?>