<?php

/*
Module Name: Perfex CRM Employee Chat
Description: Chat for Perfex CRM
Author: Aleksandar Stojanov
Author URI: https://aleksandarstojanov.com
*/

defined('BASEPATH') or exit('No direct script access allowed');

class Prchat_model extends CRM_Model
{
    /**
     * Get chat staff members
     * @return mixed
     */
    public function getUsers()
    {
        $data = [];
        $this->db->select('staffid, firstname, lastname, profile_image, last_login, facebook, linkedin, skype, admin');
        $this->db->where('active', 1);
        $users = $this->db->get('tblstaff')->result_array();

        foreach ($users as $key => $user) {
            $sql = "SELECT message,sender_id,time_sent FROM tblchatmessages WHERE (sender_id = ".get_staff_user_id()." AND reciever_id = {$user['staffid']}) OR (sender_id = {$user['staffid']} AND reciever_id = ".get_staff_user_id().") ORDER BY id DESC LIMIT 0, 1";

            $query = $this->db->query($sql)->result();

            foreach ($query as &$chat) {
                $users[$key]['time_sent_formatted'] =  $chat->time_sent_formatted = time_ago($chat->time_sent);
                if ($user['staffid'] !== $chat->sender_id) {
                    $users[$key]['message']             =  _l('chat_message_you') . ' ' . $chat->message;
                } else {
                    $users[$key]['message']             =  $chat->message;
                }
            }
        }

        if ($users) {
            return $users;
        }
        return false;
    }

    /**
     * Get logged in staff profile image
     * @param  mixed
     * @return mixed
     */
    public function getUserImage($id)
    {
        $CI = & get_instance();
        $CI->db->from('tblstaff');
        $CI->db->where('staffid', $id);
        $data = $CI->db->get()->row('profile_image');

        if ($data) {
            return $data;
        }

        return false;
    }

    /**
     * Create message
     * @param  data
     * @return boolean
     */
    public function createMessage($data)
    {
        if ($this->db->insert('tblchatmessages', $data)) {
            return $this->db->insert_id();
        }

        return false;
    }

    /**
     * Get staff firstname and lastname
     * @param  mixed
     * @return mixed
     */
    public function getStaffInfo($id)
    {
        $this->db->select('firstname,lastname');
        $this->db->where('staffid', $id);
        $result = $this->db->get('tblstaff')->row();
        if ($result) {
            return $result;
        }

        return false;
    }

    /**
     * @param  $from sender
     * @param  $to receiver
     * @param  $limit limit messages
     * @param  $offet offet
     * @return mixed
     */
    public function getMessages($from, $to, $limit, $offset)
    {
        $sql = "SELECT * FROM tblchatmessages WHERE (sender_id = {$to} AND reciever_id = {$from}) OR (sender_id = {$from} AND reciever_id = {$to}) ORDER BY id DESC LIMIT {$offset}, {$limit}";

        $query = $this->db->query($sql)->result();

        foreach ($query as &$chat) {
            $chat->message             =  pr_chat_convertLinkImageToString($chat->message, $chat->sender_id, $chat->reciever_id);
            $chat->message             =  check_for_links_lity($chat->message);
            $chat->user_image          = $this->getUserImage($chat->sender_id);
            $chat->sender_fullname     = get_staff_full_name($chat->sender_id);
            $chat->time_sent_formatted = _dt($chat->time_sent);
        }
        
        if ($query) {
            return $query;
        }

        return false;
    }

    /**
     * Get unread messages for the logged in user
     */
    public function getUnread()
    {
        $unreadMessages            = array();

        $staff_id = get_staff_user_id();
        
        $sql                       = "SELECT sender_id FROM tblchatmessages WHERE(reciever_id = $staff_id AND viewed = 0)";

        $query = $this->db->query($sql);

        $result = $query->result_array();

        foreach ($result as $sender) {
            $sender_id = 'sender_id_' . $sender['sender_id'];
            if (array_key_exists($sender_id, $unreadMessages)) {
                $unreadMessages['' . $sender_id . '']['count_messages'] = $unreadMessages['' . $sender_id . '']['count_messages'] + 1;
            } else {
                $unreadMessages['' . $sender_id . ''] = array('sender_id' => $sender['sender_id'], 'count_messages' => 1);
            }
        }
        if ($result) {
            return $unreadMessages;
        }

        return false;
    }

    /**
     * Update unread for sender
     * @param  mixed $id sender id
     * @return mixed
     */
    public function updateUnread($id)
    {
        $staff_id = get_staff_user_id();
        $sql   = "UPDATE tblchatmessages SET viewed = 1 WHERE (reciever_id = $staff_id AND sender_id = {$id})";
        $query = $this->db->query($sql);
        if ($query) {
            return $query;
        }
        return false;
    }

    /**
    * Set theme
    * @param mixed $id the staff id
    * @param string $theme_name 1 or 0 light or dark
    */
    public function updateChatTheme($id, $theme_name)
    {
        $name = 'current_theme';
        $this->db->where('user_id', $id);
        $this->db->where('name', $name);
        
        $exsists = $this->db->get('tblchatsettings')->row();

        if (!$exsists == null) {
            $this->db->where('user_id', $id);
            $this->db->where('name', $name);
            $this->db->update('tblchatsettings', array('name' => $name, 'value' => $theme_name));
        } else {
            $this->db->insert('tblchatsettings', array('name' => $name, 'value' => $theme_name,  'user_id' => $id));
        }
        if ($this->db->affected_rows() != 0) {
            return $theme_name;
        }
        return $theme_name;
    }

    /**
     * Set the chat color
     * @param mixed $id the staff id
     * @param string $color the color to set
     */
    public function setChatColor($color)
    {
        $id = get_staff_user_id();
        $name = 'chat_color';
        if ($this->db->field_exists('value', 'tblchatsettings')) {
            $this->db->where('user_id', $id);
            $this->db->where('name', $name);
            $exsists = $this->db->get('tblchatsettings')->row();
            if (!$exsists == null) {
                $this->db->where('user_id', $id);
                $this->db->where('name', $name);
                $this->db->update('tblchatsettings', array('name' => $name, 'value' => $color));
            } else {
                $this->db->insert('tblchatsettings', array('name' => $name, 'value' => $color, 'user_id' => $id));
            }
            if ($this->db->affected_rows() != 0) {
                $message['success'] = $color;
                return $message;
            }
            $message['success'] = false;
            return $message;
        } else {
            $this->db->where('user_id', $id);
            $this->db->where('name', $name);
            $exsists = $this->db->get('tblchatsettings')->row();
            if (!$exsists == null) {
                $this->db->where('user_id', $id);
                $this->db->where('name', $name);
                $this->db->update('tblchatsettings', array('chat_color' => $color));
            } else {
                $this->db->insert('tblchatsettings', array('chat_color' => $color, 'user_id' => $id));
            }
            if ($this->db->affected_rows() != 0) {
                $message['success'] = $color;
                return $message;
            }
            $message['success'] = false;
            return $message;
        }
    }

    /**
     * Delete chat messages including pictures and files
     * @param mixed $id the staff id
     * @param mixed $contact_id the contact_id id
     * @return boolean
     */
    public function deleteMessage($id, $contact_id)
    {
        $staff_id = get_staff_user_id();
        $possible_file = $this->db->select()->where('id', $id)->get('tblchatmessages')->row()->message;
        
        if (prchat_checkMessageIfFileExists($possible_file)) {
            $file_name = getImageFullName($possible_file);
            if (is_dir(PR_CHAT_MODULE_UPLOAD_FOLDER)) {
                unlink(PR_CHAT_MODULE_UPLOAD_FOLDER . '/' . $file_name);
            }
            $this->db->delete('tblchatsharedfiles', array('sender_id' => get_staff_user_id(),'reciever_id' => $contact_id, 'file_name' => $file_name));
            $this->db->delete('tblchatsharedfiles', array('sender_id' => $contact_id, 'reciever_id' => get_staff_user_id(), 'file_name' => $file_name));
        }

        $this->db->where('id', $id);
        $files_deleted = $this->db->update('tblchatmessages', [
            'is_deleted'=>1,
            'message'=>''
        ]);
        if ($files_deleted) {
            return true;
        }
        return false;
    }

    /**
     * Handles shared files between two users
     * @param mixed $own_id session id
     * @param mixed $id the contact shared files id
     * @return boolean
     */
    public function get_shared_files_and_create_template($own_id, $contact_id)
    {
        $files = [];
        $allFiles = "unknown|rar|zip|mp3|mp4|mov|flv|wmv|avi|doc|docx|pdf|xls|xlsx|zip|rar|txt|php|html|css|jpeg|jpg|png|swf|PNG|JPG|JPEG";
        $photoExtensions = "unknown|jpeg|jpg|png|swf|PNG|JPG|JPEG|";
        $docFiles = "unknown|rar|zip|mp3|mp4|mov|flv|wmv|avi|doc|docx|pdf|xls|xlsx|zip|rar|txt|php|html|css";

        $dir = list_files(PR_CHAT_MODULE_UPLOAD_FOLDER);

        $from_messages_table  = $this->db->query("SELECT file_name FROM ".'tblchatsharedfiles'." WHERE file_name REGEXP '^.*\.(".$allFiles.")$' AND sender_id  = '".$own_id."' AND reciever_id = '".$contact_id."' OR sender_id = '".$contact_id."' AND reciever_id = '".$own_id."'");
        if ($from_messages_table) {
            $from_messages_table = $from_messages_table->result_array();
        } else {
            return false;
        }
        foreach ($dir as $file_name) {
            foreach ($from_messages_table as $value) {
                if (strpos($file_name, $value['file_name']) !== false) {
                    if (!in_array($file_name, $files)) {
                        array_push($files, $file_name);
                    }
                }
            }
        }

        $html = '';
        $html .= '<ul class="nav nav-tabs" role="tablist">';
        $html .= '<li class="active"><a href="#photos" role="tab" data-toggle="tab"><i class="fa fa-file-image-o icon_shared_files" aria-hidden="true"></i>'._l('chat_photos_text').'</a></li>';
        $html .= '<li><a href="#files" role="tab" data-toggle="tab"><i class="fa fa-file-o icon_shared_files" aria-hidden="true"></i>'._l('chat_files_text').'</a></li>';
        $html .= '</ul>';

        $html .= '<div class="tab-content">';
        $html .= '<div class="tab-pane active" id="photos">';
        $html .= '<span class="text-center shared_items_span">'._l('chat_shared_photos_text').'</span>';

        foreach ($files as $file) {
            if (preg_match("/^[^\?]+\.('".$photoExtensions."')$/", $file)) {
                $html .= "<a data-lity href='".base_url('uploads/' . $file)."'>
                <div class='col-xs-3 shared_files_ahref' style='background-image:url(".base_url('uploads/' . $file).");'></div></a>";
            }
        }
        $html .= '</div>';
        $html .= '<div class="tab-pane" id="files">';
        $html .= '<span class="text-center shared_items_span">'._l('chat_shared_files_text').'</span>';

        foreach ($files as $file) {
            if (preg_match("/^[^\?]+\.('".$docFiles."')$/", $file)) {
                $html .= "<div class='col-md-12'><a target='_blank' href ='".base_url('uploads/' . $file)."'><i class='fa fa-file-o icon_shared_files' aria-hidden='true'></i>
                ".$file."</a></div>";
            }
        }
        $html .= '</div></div>';
        return $html;
    }
}

/* End of file PRChat_model.php */
/* Location: ./modules/prchat/models/perfex_chat/PRChat_model.php */
